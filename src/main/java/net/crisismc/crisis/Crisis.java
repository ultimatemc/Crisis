package net.crisismc.crisis;

import co.aikar.commands.ACF;
import co.aikar.commands.CommandManager;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.google.gson.Gson;
import fr.zcraft.zlib.core.ZPlugin;
import net.crisismc.crisis.bases.BaseManager;
import net.crisismc.crisis.bases.MultiBlockManager;
import net.crisismc.crisis.blocks.BlockHandler;
import net.crisismc.crisis.building.BlockDurability;
import net.crisismc.crisis.building.Town;
import net.crisismc.crisis.crafting.CraftingListener;
import net.crisismc.crisis.credits.CreditAdminCommand;
import net.crisismc.crisis.credits.CrisisCredits;
import net.crisismc.crisis.database.CrisisPlayerLoader;
import net.crisismc.crisis.database.SqlDatabase;
import net.crisismc.crisis.database.StatisticsLoader;
import net.crisismc.crisis.economy.BalanceCommand;
import net.crisismc.crisis.economy.CrisisEconomy;
import net.crisismc.crisis.economy.PayCommand;
import net.crisismc.crisis.events.EventScheduler;
import net.crisismc.crisis.ffa.CrisisFFA;
import net.crisismc.crisis.guns.CrisisGuns;
import net.crisismc.crisis.inventory.Cellphone;
import net.crisismc.crisis.lib.Callback;
import net.crisismc.crisis.lib.ChatUtil;
import net.crisismc.crisis.listeners.CrisisEntityListener;
import net.crisismc.crisis.listeners.CrisisListener;
import net.crisismc.crisis.listeners.CrisisPlayerListener;
import net.crisismc.crisis.loot.LootCommand;
import net.crisismc.crisis.loot.LootManager;
import net.crisismc.crisis.mobs.CrisisEntityTypes;
import net.crisismc.crisis.mobs.MobSpawning;
import net.crisismc.crisis.player.CrisisPlayer;
import net.crisismc.crisis.player.CrisisPlayerPlaceholders;
import net.crisismc.crisis.resources.ResourceManager;
import net.crisismc.crisis.statistics.StatisticsManager;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_12_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;


public class Crisis extends ZPlugin implements Callback {

    private static Crisis plugin;
    private ProtocolManager protocolManager;
    private BlockHandler blockHandler;
    private BaseManager baseManager;
    private MultiBlockManager multiBlockManager;
    private LootManager lootManager;
    private RespawnManager respawnManager;
    private CrisisEconomy economy;
    private StatisticsManager statisticManager;
    public CrisisFFA ffa;
    private CrisisGuns guns;

    private static Gson gson;

    public static final String WORLD_NAME = "world";
    public static Location SPAWN_LOC;
    public static List<Town> TOWNS = new ArrayList<>();

    public SqlDatabase db;
    public StatisticsLoader statsLoader;


    private ResourceManager resourceManager;

    public static Crisis getInstance() {
        return plugin;
    }

    public ProtocolManager getProtocolManager() {
        return protocolManager;
    }

    public BlockHandler getBlockHandler() {
        return blockHandler;
    }

    public BaseManager getBaseManager() {
        return baseManager;
    }

    public MultiBlockManager getMultiBlockManager() {
        return multiBlockManager;
    }

    public LootManager getLootManager() {
        return lootManager;
    }

    public Gson getGson() {
        return gson;
    }

    public CrisisEconomy getEconomy() {
        return economy;
    }

    @Override
    public void onEnable() {
        plugin = this;
        SPAWN_LOC = new Location(Bukkit.getWorld("spawn"), 31.5, 11.1, 5.5, 90, 0);

        gson = new Gson();
        protocolManager = ProtocolLibrary.getProtocolManager();

        db = new SqlDatabase(this, "jdbc:mysql://localhost:3306/crisis", "root", "e262Y%39f0d7e956642#0((47d19fc9e94c");
        setGameRules();
        setupFurnaceRecipes();
        setupTowns();
        CrisisPlayer.cpLoader = new CrisisPlayerLoader(this, db);
        blockHandler = new BlockHandler(this, db);

        new CrisisPlayerPlaceholders();
        Bukkit.getOnlinePlayers().forEach(CrisisPlayer::new);
        CrisisPlayer.task(this);

        plugin.getServer().getPluginManager().registerEvents(new Cellphone(), plugin);

        guns = new CrisisGuns();
        ffa = new CrisisFFA(this);

        new EventScheduler(this);
        new Sustenance(this).registerEvents();
        respawnManager = new RespawnManager(this, ffa);
        lootManager = new LootManager();
        resourceManager = new ResourceManager(this);
        economy = new CrisisEconomy();
        new CrisisCredits(db);

        statsLoader = new StatisticsLoader(this, db);
        statisticManager = new StatisticsManager(statsLoader);

        //new NPCManager();

		/*
         * Building
		 */
        new BlockDurability(this).registerEvents();

        CrisisEntityTypes.registerEntities();
        new MobSpawning(this);
		
		/*
		 * Bases
		 */
        baseManager = new BaseManager(this, db, protocolManager);
        multiBlockManager = new MultiBlockManager(db);

        new CrisisPlayerListener(this, baseManager);
        new CrisisEntityListener(this);
        new CrisisListener(this);
        new CraftingListener(this);

        setupCommands();
    }

    @Override
    public void onDisable() {
        CrisisPlayer.saveCrisisPlayers();
        blockHandler.onDisable();
        resourceManager.onDisable();
        //EntityTypes.unregisterEntities();
        baseManager.onDisable();
        lootManager.onDisable();
        statisticManager.onShutDown();
    }

    @SuppressWarnings("deprecation")
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("experience")
                || cmd.getName().equalsIgnoreCase("exp")
                || cmd.getName().equalsIgnoreCase("xp")) {
            if (sender instanceof Player) {
                Player p = (Player) sender;
                p.sendMessage(ChatUtil.getPrefix() + ChatColor.GOLD + "Experience:"); //TODO proper setup
                p.sendMessage("Total: " + CrisisPlayer.getCrisisPlayer(p).getXp());
                p.sendMessage("Life: " + CrisisPlayer.getCrisisPlayer(p).getLifeXp());
                p.sendMessage("Level " + CrisisPlayer.getCrisisPlayer(p).getLevel());
            }

        } else if (cmd.getName().equalsIgnoreCase("spawn")) {
            if (sender instanceof Player && ((Player) sender).getWorld().getName().equalsIgnoreCase("spawn")) {
                Player p = (Player) sender;
                respawnManager.openSpawnInv(p);
            }
        } else if (cmd.getName().equalsIgnoreCase("test") && sender.isOp()) {
            if (args.length > 0 && sender instanceof Player) {
                Player p = ((Player) sender);
                if (args[0].equalsIgnoreCase("armorstand")) {
                    WorldServer s = ((CraftWorld) p.getLocation().getWorld()).getHandle();

                    EntityArmorStand stand = new EntityArmorStand(s);
                    stand.setLocation(p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ(), 0, 0);
                    stand.setRightArmPose(new Vector3f(Float.parseFloat(args[1]), Float.parseFloat(args[2]), Float.parseFloat(args[3])));

                    PacketPlayOutEntityEquipment equipment = new PacketPlayOutEntityEquipment(stand.getId(), EnumItemSlot.MAINHAND, CraftItemStack.asNMSCopy(new ItemStack(Material.COAL, 1)));
                    PacketPlayOutSpawnEntityLiving create = new PacketPlayOutSpawnEntityLiving(stand);

                    ((CraftPlayer) p).getHandle().playerConnection.sendPacket(create);
                    ((CraftPlayer) p).getHandle().playerConnection.sendPacket(equipment);
                }
            }
        } else if (cmd.getName().equalsIgnoreCase("crItem")
                || cmd.getName().equalsIgnoreCase("crisisitem")) {
            if (args.length == 1 && sender.isOp() && sender instanceof Player) {
                Player player = ((Player) sender);

                CrisisItem ci = CrisisItem.valueOf(args[0]);

                player.getInventory().addItem(ci.getItemStack(1));
                player.sendMessage(ChatUtil.getPrefix() + ChatColor.GREEN + "Added " + ci.toString() + " to your inventory.");
            }
        }

        return false;
    }

    /**
     * Sets up command executors
     */
    private void setupCommands() {
        getCommand("loot").setExecutor(new LootCommand());
        getCommand("ccadmin").setExecutor(new CreditAdminCommand());

        CommandManager manager = ACF.createManager(this);

        //manager.registerCommand(new EconomyCommands());
        manager.registerCommand(new BalanceCommand(economy));
        manager.registerCommand(new PayCommand(economy));
    }

    /**
     * Stores the location of towns for later determining if a Location is within a town
     */
    private static void setupTowns() {
        TOWNS.add(new Town(-1809, 1839, -1930, 1638)); //Small airfield - Hughes
        TOWNS.add(new Town(1595, 487, 1365, 259)); //Large airfield - Maxwell
        TOWNS.add(new Town(-591, 897, -720, 1029)); //Pinefield
        TOWNS.add(new Town(-157, 2550, -385, 2354)); //Sutton
        TOWNS.add(new Town(747, 2552, 797, 2501)); //Gas station - GREEN
        TOWNS.add(new Town(2396, 1742, 2103, 2152)); //Maine
        TOWNS.add(new Town(1254, -2290, 1000, -2024)); //Ryerson
        TOWNS.add(new Town(152, -2245, -158, -2470)); //Military fort - NOT BUILT
        TOWNS.add(new Town(-672, -1203, -959, -982)); //Horton
        TOWNS.add(new Town(-1929, -2301, -2241, -1985)); //Thirsk
        TOWNS.add(new Town(-1253, 720, -1201, 776)); //Gas station - RED
    }

    /**
     * Ensures Minecraft game rules are in the correct configuration
     */
    private void setGameRules() {
        World world = Bukkit.getWorld("world");
        world.setGameRuleValue("naturalRegeneration", "false");
        world.setGameRuleValue("doTileDrops", "false");
        world.setGameRuleValue("keepInventory", "false");
        world.setGameRuleValue("doDaylightCycle", "true");
    }

    private void setupFurnaceRecipes() {
        FurnaceRecipe recipe = new FurnaceRecipe(CrisisItem.GLASS_ROUGH.getItemStack(1), Material.SUGAR);
        recipe.setInput(Material.SUGAR);
        getServer().addRecipe(recipe);
        Bukkit.addRecipe(recipe);

        recipe = new FurnaceRecipe(CrisisItem.METAL_FRAG.getItemStack(1), Material.PRISMARINE_SHARD);
        recipe.setInput(Material.PRISMARINE_SHARD);
        getServer().addRecipe(recipe);
        Bukkit.addRecipe(recipe);

        recipe = new FurnaceRecipe(CrisisItem.GUNPOWDER.getItemStack(1), Material.PRISMARINE_CRYSTALS);
        recipe.setInput(Material.PRISMARINE_CRYSTALS);
        getServer().addRecipe(recipe);
        Bukkit.addRecipe(recipe);
    }

    @Override
    public void callback(String result) {
        System.out.println(result);
    }
}
