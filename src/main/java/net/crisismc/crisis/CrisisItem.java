package net.crisismc.crisis;

import net.crisismc.crisis.economy.CrisisEconomy;
import net.crisismc.crisis.guns.CrisisGunAPI;
import net.crisismc.crisis.lib.NbtTags;
import net.crisismc.crisis.loot.CrisisItemLootInfo;
import net.crisismc.crisis.loot.LootCategory;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Enum that stores info about every custom Item within crisis.
 * Provides a standard method for obtaining a ItemStack object from Enum value {@link CrisisItem#getItemStack(int)}
 * As well as more specific methods for certain items
 */
public enum CrisisItem {

    INVENTORY_ITEM(Material.PAINTING, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.PAINTING, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    RESEARCH_PAPER(Material.GLOWSTONE_DUST, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.GLOWSTONE_DUST, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Research Paper");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    BLUEPRINT(Material.EMPTY_MAP, (short) 0) {
        @Override
        public ItemStack getItemStack(ItemStack bp) {
            ItemStack item = new ItemStack(Material.EMPTY_MAP, 1);
            ItemMeta itemMeta = item.getItemMeta();
            String name = bp.hasItemMeta() ? ChatColor.stripColor(bp.getItemMeta().getDisplayName().split("<")[0]) : bp.getType().toString();
            itemMeta.setDisplayName(ChatColor.GOLD + name + " Blueprint");
            itemMeta.addEnchant(Enchantment.LURE, new Random().nextInt(100) + 1, true);
            itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            item.setItemMeta(itemMeta);
            return item;
        }

        @Override
        public ItemStack getItemStack(int i) {
            return null;
        }
    },
    RESEARCH_TABLE(Material.WORKBENCH, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.WORKBENCH, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Research Table");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    CAMERA(Material.TRIPWIRE_HOOK, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.TRIPWIRE_HOOK, i);
            ItemMeta im = item.getItemMeta();
            im.setDisplayName(ChatColor.WHITE + "Security Camera");
            item.setItemMeta(im);
            return item;
        }
    },
    ANVIL_NAME_ITEM(Material.WATER_LILY, (short) 0) {
        @Override
        public ItemStack getItemStack(String prompt, int i) {
            ItemStack nameItem = new ItemStack(Material.WATER_LILY, i);
            ItemMeta im = nameItem.getItemMeta();
            im.setDisplayName(prompt);
            nameItem.setItemMeta(im);
            return nameItem;
        }

        @Override
        public ItemStack getItemStack(int i) {
            return null;
        }
    },
    BACKPACK(Material.GOLD_CHESTPLATE, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.GOLD_CHESTPLATE, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Backpack");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    CANNED_BEANS(Material.BAKED_POTATO, (short) 0,
            new CrisisItemLootInfo(LootCategory.RESIDENTIAL, 1F, 1),
            new CrisisItemLootInfo(LootCategory.FOOD, 1F, 1)) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.BAKED_POTATO, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Canned Beans");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    CANNED_SARDINES(Material.PUMPKIN_PIE, (short) 0,
            new CrisisItemLootInfo(LootCategory.RESIDENTIAL, 1F, 1),
            new CrisisItemLootInfo(LootCategory.FOOD, 1F, 1)) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.PUMPKIN_PIE, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Canned Sardines");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    HAMMER(Material.ARROW, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.ARROW, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Hammer");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    CANNED_PEACHES(Material.MELON, (short) 0,
            new CrisisItemLootInfo(LootCategory.RESIDENTIAL, 1F, 1),
            new CrisisItemLootInfo(LootCategory.FOOD, 1F, 1)) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.MELON, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Canned Peaches");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    INVENTORY_BLOCKER(Material.POWERED_RAIL, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.POWERED_RAIL, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "X");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    CAMPFIRE(Material.PUMPKIN_SEEDS, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.PUMPKIN_SEEDS, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.GOLD + "Campfire");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    GRENADE_FRAG(Material.COAL, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.COAL, i, (short) 0);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "M67 Fragmentation Grenade");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    GRENADE_MOLOTOV(Material.BLAZE_POWDER, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.BLAZE_POWDER, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Molotov Cocktail");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    GRENADE_GAS(Material.IRON_NUGGET, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.IRON_NUGGET, i, (short) 0);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Chlorine Gas Grenade");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    ROCK(Material.INK_SACK, (short) 8) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.INK_SACK, i, (short) 8);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Rock");
            List<String> lore = Arrays.asList(ChatColor.GOLD + "Hitting things with a rock is fun!",
                    ChatColor.GOLD + "Shift + right click to hit yourself!");
            itemMeta.setLore(lore);
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    SAND(Material.SUGAR, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.SUGAR, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Pile of sand");
            List<String> lore = Collections.singletonList(ChatColor.GOLD + "Can be smelted for a chance to produce Rough Glass.");
            itemMeta.setLore(lore);
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    GLASS_ROUGH(Material.INK_SACK, (short) 6) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.INK_SACK, i, (short) 6);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Rough Glass");
            List<String> lore = Collections.singletonList(ChatColor.GOLD + "Produced by smelting a pile of sand.");
            itemMeta.setLore(lore);
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    MEAT_RAW(Material.RAW_BEEF, (short) 0, new CrisisItemLootInfo(LootCategory.RESIDENTIAL, 2F, 1)) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.RAW_BEEF, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Raw Meat");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    MEAT_COOKED(Material.COOKED_BEEF, (short) 0,
            new CrisisItemLootInfo(LootCategory.RESIDENTIAL, 0.5F, 1),
            new CrisisItemLootInfo(LootCategory.RESIDENTIAL, 1F, 1)) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.COOKED_BEEF, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Cooked Meat");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    SQUIRREL_RAW(Material.ROTTEN_FLESH, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.ROTTEN_FLESH, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Raw Squirrel");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    SQUIRREL_COOKED(Material.PORK, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.PORK, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Cooked Squirrel");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    CLOTH(Material.LEATHER, (short) 0, new CrisisItemLootInfo(LootCategory.RESIDENTIAL, 1F, 3)) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.LEATHER, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Cloth");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    ANIMAL_FAT(Material.QUARTZ, (short) 0, new CrisisItemLootInfo(LootCategory.RESIDENTIAL, 1F, 3)) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.QUARTZ, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Animal Fat");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    LOG(Material.SAPLING, (short) 2,
            new CrisisItemLootInfo(LootCategory.RESIDENTIAL, 1F, 2),
            new CrisisItemLootInfo(LootCategory.INDUSTRIAL, 1, 3)) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.SAPLING, i, (short) 2);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Log");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    STICK(Material.SAPLING, (short) 1, new CrisisItemLootInfo(LootCategory.RESIDENTIAL, 2F, 8)) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.SAPLING, i, (short) 1);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Stick");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    STONE(Material.INK_SACK, (short) 7) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.INK_SACK, i, (short) 7);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Stone");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    METAL_FRAG(Material.IRON_INGOT, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.IRON_INGOT, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Metal Fragment");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    GUNPOWDER(Material.GOLD_INGOT, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.GOLD_INGOT, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Gun Powder");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    METAL_ORE(Material.PRISMARINE_SHARD, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.PRISMARINE_SHARD, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Metal Ore");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    SULFUR_ORE(Material.PRISMARINE_CRYSTALS, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.PRISMARINE_CRYSTALS, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Sulfur Ore");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    EXPLOSIVES(Material.BOOK, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.BOOK, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Explosives");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    C4_CONTROLLER(Material.SUGAR_CANE, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.SUGAR_CANE, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "C4 Controller");
            List<String> lore = Arrays.asList(ChatColor.GOLD + "A vital part in the creation of a C4 explosive.");
            itemMeta.setLore(lore);
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    C4_EXPLOSIVE(Material.IRON_BARDING, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.IRON_BARDING, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "C4 Explosive");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    HATCHET_STONE(Material.STONE_AXE, (short) 0, true, new CrisisItemLootInfo(LootCategory.RESIDENTIAL, 1F, 1)) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.STONE_AXE, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Stone Hatchet");
            item.setItemMeta(itemMeta);
            return NbtTags.setUnbreakable(item);
        }
    },
    SHOES_BURLAP(Material.LEATHER_BOOTS, (short) 0, new CrisisItemLootInfo(LootCategory.RESIDENTIAL, 1F, 1)) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.LEATHER_BOOTS, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Burlap Shoes");
            item.setItemMeta(itemMeta);
            return NbtTags.setUnbreakable(item);
        }
    },
    TROUSERS_BURLAP(Material.LEATHER_LEGGINGS, (short) 0, new CrisisItemLootInfo(LootCategory.RESIDENTIAL, 1F, 1)) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.LEATHER_LEGGINGS, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Burlap Trousers");
            item.setItemMeta(itemMeta);
            return NbtTags.setUnbreakable(item);
        }
    },
    SHIRT_BURLAP(Material.LEATHER_CHESTPLATE, (short) 0, new CrisisItemLootInfo(LootCategory.RESIDENTIAL, 1F, 1)) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.LEATHER_CHESTPLATE, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Burlap Shirt");
            item.setItemMeta(itemMeta);
            return NbtTags.setUnbreakable(item);
        }
    },
    HELMET_COMBAT(Material.IRON_HELMET, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.IRON_HELMET, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Combat Helmet");
            item.setItemMeta(itemMeta);
            return NbtTags.setUnbreakable(item);
        }
    },
    VEST_COMBAT(Material.IRON_CHESTPLATE, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.IRON_CHESTPLATE, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Tactical Vest");
            item.setItemMeta(itemMeta);
            return NbtTags.setUnbreakable(item);
        }
    },
    PANTS_COMBAT(Material.IRON_LEGGINGS, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.IRON_LEGGINGS, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Combat Pants");
            item.setItemMeta(itemMeta);
            return NbtTags.setUnbreakable(item);
        }
    },
    BOOTS_COMBAT(Material.IRON_BOOTS, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.IRON_BOOTS, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Combat Boots");
            item.setItemMeta(itemMeta);
            return NbtTags.setUnbreakable(item);
        }
    },
    CHESTPLATE_KEVLAR(Material.DIAMOND_CHESTPLATE, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.DIAMOND_CHESTPLATE, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Kevlar Vest");
            item.setItemMeta(itemMeta);
            return NbtTags.setUnbreakable(item);
        }
    },
    PANTS_KEVLAR(Material.DIAMOND_LEGGINGS, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.DIAMOND_LEGGINGS, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Kevlar Pants");
            item.setItemMeta(itemMeta);
            return NbtTags.setUnbreakable(item);
        }
    },
    BOOTS_KEVLAR(Material.DIAMOND_BOOTS, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.DIAMOND_BOOTS, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Kevlar Boots");
            item.setItemMeta(itemMeta);
            return NbtTags.setUnbreakable(item);
        }
    },
    HATCHET(Material.IRON_AXE, (short) 0, true, new CrisisItemLootInfo(LootCategory.RESIDENTIAL, 0.25F, 1)) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.IRON_AXE, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Hatchet");
            item.setItemMeta(itemMeta);
            return NbtTags.setUnbreakable(item);
        }
    },
    CROWBAR(Material.STONE_PICKAXE, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.STONE_PICKAXE, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Crow bar");
            item.setItemMeta(itemMeta);
            return NbtTags.setUnbreakable(item);
        }
    },
    PICKAXE(Material.IRON_PICKAXE, (short) 0, true) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.IRON_PICKAXE, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Pickaxe");
            item.setItemMeta(itemMeta);
            return NbtTags.setUnbreakable(item);
        }
    },
    AXE_REINFORCED(Material.DIAMOND_AXE, (short) 0, true) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.DIAMOND_AXE, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Reinforced");
            item.setItemMeta(itemMeta);
            return NbtTags.setUnbreakable(item);
        }
    },
    PICK_ICE(Material.DIAMOND_PICKAXE, (short) 0,  true) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.DIAMOND_PICKAXE, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Icepick");
            item.setItemMeta(itemMeta);
            return NbtTags.setUnbreakable(item);
        }
    },
    FUEL_LOW_GRADE(Material.SAPLING, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.SAPLING, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Low Grade Fuel");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    FURNACE(Material.INK_SACK, (short) 11) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.INK_SACK, i, (short) 11);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Furnace");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    /*
     * GUNS
     */
    SHOTGUN_WATERPIPE(Material.DIAMOND_HOE, (short) 0, true,
            new CrisisItemLootInfo(LootCategory.RESIDENTIAL, 0.25F, 1),
            new CrisisItemLootInfo(LootCategory.MILITARY, 1F, 1),
            new CrisisItemLootInfo(LootCategory.MILITARY_GUNS, 2F, 1)) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.DIAMOND_HOE, i);
            item = CrisisGunAPI.getGun(item.getType(), (short) 352);
            return NbtTags.setUnbreakable(item);
        }
    },
    AMMO_SHOTGUN_WATERPIPE(Material.SHEARS, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = CrisisGunAPI.getGunAmmo(Material.SHEARS, (short) 44);
            item.setAmount(i);
            return NbtTags.setUnstackable(item);
        }
    },
    RIFLE_BOLT(Material.DIAMOND_HOE, (short) 0, true,
            new CrisisItemLootInfo(LootCategory.RESIDENTIAL, 0.05F, 1),
            new CrisisItemLootInfo(LootCategory.MILITARY, 0.8F, 1),
            new CrisisItemLootInfo(LootCategory.MILITARY_GUNS, 2F, 1)) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.DIAMOND_HOE, i);
            item = CrisisGunAPI.getGun(item.getType(), (short) 96);
            return NbtTags.setUnbreakable(item);
        }
    },
    AMMO_RIFLE_BOLT(Material.SHEARS, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = CrisisGunAPI.getGunAmmo(Material.SHEARS, (short) 12);
            item.setAmount(i);
            return item;
        }
    },
    AKS74U(Material.DIAMOND_HOE, (short) 0, true) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.DIAMOND_HOE, i);
            item = CrisisGunAPI.getGun(item.getType(), (short) 0);
            return item;
        }
    },
    AMMO_AKS74U(Material.SHEARS, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = CrisisGunAPI.getGunAmmo(Material.SHEARS, (short) 0);
            item.setAmount(i);
            return NbtTags.setUnstackable(item);
        }
    },
    THOMPSON(Material.DIAMOND_HOE, (short) 0, true) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.DIAMOND_HOE, i);
            item = CrisisGunAPI.getGun(item.getType(), (short) 320);
            return item;
        }
    },
    AMMO_THOMPSON(Material.SHEARS, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = CrisisGunAPI.getGunAmmo(Material.SHEARS, (short) 40);
            item.setAmount(i);
            return NbtTags.setUnstackable(item);
        }
    },
    M40A1(Material.DIAMOND_HOE, (short) 0, true) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.DIAMOND_HOE, i);
            item = CrisisGunAPI.getGun(item.getType(), (short) 192);
            return NbtTags.setUnbreakable(item);
        }
    },
    AMMO_M40A1(Material.SHEARS, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = CrisisGunAPI.getGunAmmo(Material.SHEARS, (short) 24);
            item.setAmount(i);
            return NbtTags.setUnstackable(item);
        }
    },
    MP5(Material.DIAMOND_HOE, (short) 0, true) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.DIAMOND_HOE, i);
            item = CrisisGunAPI.getGun(item.getType(), (short) 256);
            return NbtTags.setUnbreakable(item);
        }
    },
    AMMO_MP5(Material.SHEARS, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = CrisisGunAPI.getGunAmmo(Material.SHEARS, (short) 32);
            item.setAmount(i);
            return NbtTags.setUnstackable(item);
        }
    },
    M4A4(Material.DIAMOND_HOE, (short) 0, true) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.DIAMOND_HOE, i);
            item = CrisisGunAPI.getGun(item.getType(), (short) 128);
            return NbtTags.setUnbreakable(item);
        }
    },
    AMMO_M4A4(Material.SHEARS, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = CrisisGunAPI.getGunAmmo(Material.SHEARS, (short) 16);
            item.setAmount(i);
            return NbtTags.setUnstackable(item);
        }
    },
    SCARH_DMR(Material.DIAMOND_HOE, (short) 0, true) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.DIAMOND_HOE, i);
            item = CrisisGunAPI.getGun(item.getType(), (short) 288);
            return NbtTags.setUnbreakable(item);
        }
    },
    AMMO_SCARH_DMR(Material.SHEARS, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = CrisisGunAPI.getGunAmmo(Material.SHEARS, (short) 36);
            item.setAmount(i);
            return NbtTags.setUnstackable(item);
        }
    },
    M9(Material.DIAMOND_HOE, (short) 0, true) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.DIAMOND_HOE, i);
            item = CrisisGunAPI.getGun(item.getType(), (short) 160);
            return item;
        }
    },
    AMMO_M9(Material.SHEARS, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = CrisisGunAPI.getGunAmmo(Material.SHEARS, (short) 20);
            item.setAmount(i);
            return NbtTags.setUnstackable(item);
        }
    },
    DESERT_EAGLE(Material.DIAMOND_HOE, (short) 0, true) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.DIAMOND_HOE, i);
            item = CrisisGunAPI.getGun(item.getType(), (short) 32);
            return NbtTags.setUnbreakable(item);
        }
    },
    AMMO_DESERT_EAGLE(Material.SHEARS, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = CrisisGunAPI.getGunAmmo(Material.SHEARS, (short) 4);
            item.setAmount(i);
            return NbtTags.setUnstackable(item);
        }
    },
    BANDAGE(Material.PAPER, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.PAPER, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Bandage");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    SLEEPING_BAG(Material.INK_SACK, (short) 2) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.INK_SACK, i, (short) 2);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Sleeping Bag");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    KNIFE_BONE(Material.WOOD_SWORD, (short) 0, true) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.WOOD_SWORD, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Bone Knife");
            item.setItemMeta(itemMeta);
            return NbtTags.setUnbreakable(item);
        }
    },
    STORAGE_BOX(Material.CHEST, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.CHEST, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Wooden Storage Box");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    SPLINT(Material.WHEAT, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.WHEAT, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Splint");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    CANTEEN(Material.GLASS_BOTTLE, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.GLASS_BOTTLE, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Water Bottle");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    RAFT(Material.BOAT, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.BOAT, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Wooden Raft");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    ENERGY_BAR(Material.BREAD, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.BREAD, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Energy Bar");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    BONE_FRAGMENTS(Material.INK_SACK, (short) 15) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.INK_SACK, i, (short) 15);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Bone Fragments");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    BARBED_WIRE(Material.WEB, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.WEB, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Barbed Wire");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    BARRICADE(Material.ACACIA_FENCE, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.ACACIA_FENCE, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Barricade");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    DOOR_MAKESHIFT(Material.JUNGLE_DOOR_ITEM, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.JUNGLE_DOOR_ITEM, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Makeshift Door");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    LANTERN(Material.TORCH, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.TORCH, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Lantern");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    SANDBAG(Material.SAND, (short) 1) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.SAND, i);
            item.getData().setData((byte) 1);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.WHITE + "Sandbag");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    SPAWN(Material.COMPASS, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.COMPASS, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.RED.toString() + ChatColor.BOLD + "Coming Soon");
            List<String> lore = Arrays.asList(ChatColor.RED + "Coming soon -" + ChatColor.GRAY + " Spawn into the Crisis world");
            itemMeta.setLore(lore);
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    MENU_ITEM(Material.NETHER_STAR, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.NETHER_STAR, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.GOLD.toString() + ChatColor.BOLD + "Crisis Menu");
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    FFA_ITEM(Material.STICK, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.STICK, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.GOLD.toString() + ChatColor.BOLD + "FFA");
            List<String> lore = Arrays.asList(ChatColor.GRAY + "Practice the crisis weapons", ChatColor.GRAY + "in a free-for-all arena");
            itemMeta.setLore(lore);
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    TRAIL_ITEM(Material.CONCRETE, (short) 5) {

        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.CONCRETE, i);
            return item;
        }

        @Override
        public ItemStack getItemStack(int i, boolean enabled) {
            ItemStack item = new ItemStack(Material.CONCRETE, i, (short) (enabled ? 5 : 14));
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.YELLOW + "Bullet Trails: " + (enabled ? ChatColor.GREEN + "Enabled" : ChatColor.RED + "Disabled"));
            List<String> lore = Arrays.asList(
                    ChatColor.GRAY + "Bullet trails provide visual indicators of the path bullets take.",
                    ChatColor.GRAY + "They are very useful for learning a weapon's range and accuracy.",
                    ChatColor.GRAY + "",
                    ChatColor.GRAY + "Enabling bullet trails only enables them for you; no one else can see them.");
            itemMeta.setLore(lore);
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    PLAYER_SKULL(Material.SKULL_ITEM, (short) 3) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.SKULL_ITEM, i, (byte) 3);
            return item;
        }

        @Override
        public ItemStack getItemStack(int i, Player player) {
            ItemStack item = new ItemStack(Material.SKULL_ITEM, i, (byte) 3);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.GRAY.toString() + ChatColor.BOLD + player.getName());
            item.setItemMeta(itemMeta);
            SkullMeta meta = (SkullMeta) item.getItemMeta();
            meta.setOwner(player.getName());
            item.setItemMeta(meta);
            return item;
        }

        @Override
        public ItemStack getItemStack(int i, OfflinePlayer player) {
            ItemStack item = new ItemStack(Material.SKULL_ITEM, i, (byte) 3);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.GRAY.toString() + ChatColor.BOLD + player.getName());
            item.setItemMeta(itemMeta);
            SkullMeta meta = (SkullMeta) item.getItemMeta();
            meta.setOwner(player.getName());
            item.setItemMeta(meta);
            return item;
        }
    },
    MONEY(Material.DIAMOND, (short) 0,
            new CrisisItemLootInfo(LootCategory.RESIDENTIAL, 1F, 75),
            new CrisisItemLootInfo(LootCategory.INDUSTRIAL, 1F, 100)) {
        @Override
        public ItemStack getItemStack(int i) {
            ItemStack item = new ItemStack(Material.DIAMOND, i);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.GREEN + CrisisEconomy.formatEconomyColor(((double) i )/ 100D));
            item.setItemMeta(itemMeta);
            return item;
        }
        @Override
        public ItemStack getItemStack(double amount) {
            ItemStack item = new ItemStack(Material.DIAMOND, 1);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(ChatColor.GREEN + CrisisEconomy.formatEconomyColor(amount));
            if (amount >= 1000) {
                itemMeta.addEnchant(Enchantment.LURE, 1, true);
                itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            }
            item.setItemMeta(itemMeta);
            return item;
        }
    },
    DEFAULT(Material.AIR, (short) 0) {
        @Override
        public ItemStack getItemStack(int i) {
            return new ItemStack(Material.AIR, i);
        }
    };


    private Material mat;
    private short data;
    private boolean inflictsBleed;
    private CrisisItemLootInfo[] lootInfo;

    CrisisItem(Material mat, short data) {
        this.mat = mat;
        this.data = data;
        this.inflictsBleed = false;
    }

    CrisisItem(Material mat, short data, boolean inflictsBleed) {
        this.mat = mat;
        this.data = data;
        this.inflictsBleed = inflictsBleed;
    }


    CrisisItem(Material mat, short data, boolean inflictsBleed, CrisisItemLootInfo... lootInfo) {
        this.mat = mat;
        this.data = data;
        this.inflictsBleed = inflictsBleed;
        this.lootInfo = lootInfo;
    }

    CrisisItem(Material mat, short data, CrisisItemLootInfo... lootInfo) {
        this.mat = mat;
        this.data = data;
        this.inflictsBleed = false;
        this.lootInfo = lootInfo;
    }


    /**
     * @param i The amount of items in the ItemStack
     * @return
     */
    public abstract ItemStack getItemStack(int i);

    public ItemStack getItemStack(double amount){ return null;}

    public ItemStack getItemStack(String s, int i) {
        return null;
    }

    public ItemStack getItemStack(int i, Player player) {
        return null;
    }

    public ItemStack getItemStack(int i, OfflinePlayer player) {
        return null;
    }

    public ItemStack getItemStack(ItemStack item) {
        return null;
    }

    public ItemStack getItemStack(int i, boolean bool) {
        return null;
    }

    public Material getMaterial() {
        return mat;
    }

    public short getData() {
        return data;
    }

    public boolean inflictsBleeding() {
        return inflictsBleed;
    }

    public CrisisItemLootInfo[] getLootInfo() {
        return lootInfo;
    }


    /*
            Static methods
     */

    /**
     * Should only be used to find exact matches (ignores quantity)
     *
     * @param item
     * @return The CrisisItem if found; otherwise, null.
     */
    public static CrisisItem getCrisisItem(ItemStack item) {
        if (item == null) return null;

        ItemStack i = item.clone();
        i.setAmount(1);
        for (CrisisItem ci : CrisisItem.values()) {
            if (ci.getItemStack(1) != null && ci.getItemStack(1).equals(i)) return ci;
        }
        return DEFAULT;
    }

    /**
     * @param mat  Material of item
     * @param data Damage value of item
     * @return The CrisisItem if found; otherwise, null.
     */
    public static CrisisItem getCrisisItem(Material mat, short data) {
        for (CrisisItem ci : CrisisItem.values()) {
            if (ci.getMaterial() == mat && ci.getData() == data) return ci;
        }
        return DEFAULT;
    }
}


