package net.crisismc.crisis;

import net.crisismc.crisis.ffa.CrisisFFA;
import net.crisismc.crisis.lib.ChatUtil;
import net.crisismc.crisis.player.CrisisPlayer;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;


public class RespawnManager implements Listener {

    private Crisis plugin;
    private CrisisFFA ffa;

    private Inventory mainInventory;
    private Inventory randomSpawnInv;
    private Inventory bedSpawnInv;

    private SpawnRegion sr;


    public RespawnManager(Crisis plugin, CrisisFFA ffa) {
        this.plugin = plugin;
        this.ffa = ffa;
        setupInventories();

        sr = new SpawnRegion(-1000, 2000, 2000, 3000);


        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    public void openSpawnInv(Player p) {
        CrisisPlayer cp = CrisisPlayer.getCrisisPlayer(p);
        if (cp.hasBed()) {
            p.openInventory(bedSpawnInv);
        } else {
            p.openInventory(randomSpawnInv);
        }
    }

    private void setupInventories() {
        Map<String, ItemStack> items = new HashMap<>();

        ItemStack item = new ItemStack(Material.COMPASS, 1);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(ChatColor.BLUE + "Random Spawn");
        item.setItemMeta(itemMeta);
        items.put("spawn_random", item);

        item = new ItemStack(Material.INK_SACK, 1, (short) 2);
        itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(ChatColor.BLUE + "Respawn at sleeping bag");
        item.setItemMeta(itemMeta);
        items.put("spawn_bed", item);

        items.put("spawn", CrisisItem.SPAWN.getItemStack(1));

        items.put("ffa", CrisisItem.FFA_ITEM.getItemStack(1));

        Inventory mainInv = Bukkit.createInventory(null, 9, ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Crisis");
        mainInv.setItem(3, items.get("spawn"));
        mainInv.setItem(5, items.get("ffa"));
        mainInventory = mainInv;

        Inventory bedInv = Bukkit.createInventory(null, 9, ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Spawn");
        bedInv.setItem(3, items.get("spawn_random"));
        bedInv.setItem(5, items.get("spawn_bed"));
        bedSpawnInv = bedInv;

        Inventory randomInv = Bukkit.createInventory(null, 9, ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Spawn");
        randomInv.setItem(4, items.get("spawn_random"));
        randomSpawnInv = randomInv;
    }

    private void respawn(Player p, boolean random) {
        CrisisPlayer cp = CrisisPlayer.getCrisisPlayer(p);

        if (!random && cp.getSpawnDelay() > 0) {
            p.sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "Cannot respawn at sleeping bag for another " + ChatColor.GOLD + cp.getSpawnDelay() + "s");
            return;
        }

        p.getInventory().clear();
        p.getInventory().setHeldItemSlot(4);

        //set rock
        p.getInventory().setItem(4, CrisisItem.ROCK.getItemStack(1));

        if (random) {
            World world = Bukkit.getWorld(Crisis.WORLD_NAME);
            p.sendMessage(ChatUtil.getPrefix() + ChatColor.GRAY + "Spawning...");


            while (true) {
                Location loc = sr.getRandomColumn(world);
                loc.setY(world.getHighestBlockYAt(loc) - 1);
                if (loc.getBlock().getType() == Material.SAND) {
                    loc.add(0.5, 1.5, 0.5);
                    p.teleport(loc);
                    return;
                }
            }


        } else {
            p.sendMessage(ChatUtil.getPrefix() + ChatColor.GRAY + "Spawning...");
            p.teleport(cp.getBed().loc1.add(0.5, 0.5, 0.5));
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player p = event.getPlayer();

        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (p.getInventory().getItemInMainHand().getType() == CrisisItem.SPAWN.getItemStack(1).getType()) {
                openSpawnInv(p);
            } else if (p.getInventory().getItemInMainHand().getType() == CrisisItem.MENU_ITEM.getMaterial()) {
                p.openInventory(mainInventory);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.isCancelled()) return;
        if (!event.getWhoClicked().getWorld().getName().equalsIgnoreCase("spawn")) return;

        if (ChatColor.stripColor(event.getInventory().getName()).equalsIgnoreCase("spawn")) {
            event.setCancelled(true);

            if (event.getCurrentItem() == null) return;
            if (event.getCurrentItem().getType() == Material.COMPASS) {
                respawn((Player) event.getWhoClicked(), true);
            } else if (event.getCurrentItem().getType() == Material.INK_SACK) {
                respawn((Player) event.getWhoClicked(), false);
            }
        } else if (ChatColor.stripColor(event.getInventory().getName()).equalsIgnoreCase("crisis")) {
            event.setCancelled(true);
            if (event.getCurrentItem() == null) return;
            if (event.getCurrentItem().getType() == Material.COMPASS) {
                // openSpawnInv((Player) event.getWhoClicked()); TODO disabled until FFA is updated
            } else if (event.getCurrentItem().getType() == Material.STICK) {
                ffa.openInventory((Player) event.getWhoClicked());
            }
        }
    }


    @EventHandler
    public void onRespawn(final PlayerRespawnEvent event) {
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> {
            event.getPlayer().getInventory().setItem(4, CrisisItem.MENU_ITEM.getItemStack(1));
        }, 1);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player p = event.getPlayer();
        if (!p.hasPlayedBefore()) {
            event.getPlayer().getInventory().setItem(4, CrisisItem.MENU_ITEM.getItemStack(1));
        }
    }

	/*
     *
	 * Crisis Spawn Protection
	 * 
	 */

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        if (event.getBlock().getWorld().getName().equals("spawn")) {
            if (event.getPlayer().hasPermission("crisis.op")) return;
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        if (event.getBlock().getWorld().getName().equals("spawn")) {
            if (event.getPlayer().hasPermission("crisis.op")) return;
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onAttack(EntityDamageByEntityEvent event) {
        if (event.getEntity().getWorld().getName().equals("spawn")) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent event) {
        if (event.getPlayer().getWorld().getName().equals("spawn")) {
            if (event.getPlayer().hasPermission("crisis.op")) return;
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onMove(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (player.getWorld().getName().equals("spawn")) {
            if (player.hasPermission("crisis.op")) return;
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        if (event.getEntity().getWorld().getName().equals("spawn")) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        if (event.getPlayer().getWorld().getName().equals("spawn")) {
            event.getPlayer().setSaturation(100);
        }
    }


}

class SpawnRegion {

    private Random r = new Random();
    private final int x1;
    private final int y1;
    private final int x2;
    private final int y2;

    public SpawnRegion(int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public Location getRandomColumn(World world) {
        return new Location(world, r.nextInt(x2 - x1) + x1, 0, r.nextInt(y2 - y1) + y1);
    }
}
