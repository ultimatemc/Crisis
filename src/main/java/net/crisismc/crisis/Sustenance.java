package net.crisismc.crisis;

import net.crisismc.crisis.player.CrisisPlayer;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.BlockIterator;

import java.util.HashMap;

public class Sustenance {

    private Crisis plugin;

    private HashMap<Material, Integer> foodItems = new HashMap<>();

    public Sustenance(Crisis plugin) {
        this.plugin = plugin;

        foodItems.put(Material.COOKED_BEEF, 1200); //Cooked meat
        foodItems.put(Material.RAW_BEEF, 300); //Raw meat
        foodItems.put(Material.BREAD, 750); //Energy bars
        foodItems.put(Material.PORK, 800); //Cooked squirrel
        foodItems.put(Material.MELON, 1200); //Canned fruit
        foodItems.put(Material.BAKED_POTATO, 1600); //Canned beans
        foodItems.put(Material.PUMPKIN_PIE, 1200); //Canned sardines

        lowTask();
    }

    public void registerEvents() {
        new SustenanceListener(this, plugin);
    }

    public void handlePlayerMove(PlayerMoveEvent event) {
        Player p = event.getPlayer();
        if (p.getGameMode() == GameMode.CREATIVE || p.isFlying() || p.isInsideVehicle() || !p.getWorld().getName().equals(Crisis.WORLD_NAME)) {
            return;
        }

        if (!event.getFrom().getBlock().equals(event.getTo().getBlock())) {
            CrisisPlayer cp = CrisisPlayer.getCrisisPlayer(p);
            p.setSaturation(Float.MAX_VALUE);
            if (p.isSprinting()) {
                cp.setFood(cp.getFood() - 1);
                cp.setWater(cp.getWater() - 2);
            } else {
                cp.setFood(cp.getFood() - 0.25);
                cp.setWater(cp.getWater() - 0.5);
            }
        }
    }

    public void handleBlockBreak(BlockBreakEvent event) {
        Player p = event.getPlayer();
        if (p.getGameMode() == GameMode.CREATIVE || !p.getWorld().getName().equals(Crisis.WORLD_NAME)) return;

        Material mat = event.getBlock().getType();
        if (mat == Material.STONE || mat == Material.WOOD) {
            CrisisPlayer cp = CrisisPlayer.getCrisisPlayer(p);
            cp.setFood(cp.getFood() - 5);
            cp.setWater(cp.getWater() - 1);
        }
    }

    public void handleItemConsume(PlayerItemConsumeEvent event) {
        Player p = event.getPlayer();
        CrisisPlayer cp = CrisisPlayer.getCrisisPlayer(p);
        if (event.getItem().getType() == Material.POTION) {
            event.setCancelled(true);
            p.getInventory().setItem(p.getInventory().getHeldItemSlot(), CrisisItem.CANTEEN.getItemStack(1));
            double water = cp.getWater() + 1500;
            if (water > 3000) water = 3000;
            cp.setWater(water);
        } else {
            event.setCancelled(true);
            ItemStack item = p.getItemInHand();
            if (item.getAmount() > 1) {
                item.setAmount(item.getAmount() - 1);
                p.getInventory().setItem(p.getInventory().getHeldItemSlot(), item);
            } else {
                p.getInventory().setItem(p.getInventory().getHeldItemSlot(), null);
            }
            if (!foodItems.containsKey(item.getType())) return;
            double food = cp.getFood() + foodItems.get(item.getType());
            cp.setFood(food);
            if (p.getHealth() < 18) {
                p.setHealth(p.getHealth() + 2);
            } else {
                p.setHealth(20);
            }

        }
    }

    public void handleRightClick(PlayerInteractEvent event) {
        Player p = event.getPlayer();
        if (!p.getWorld().getName().equals(Crisis.WORLD_NAME)) return;

        Block block = getTargetBlock(p, 5);

        if (block != null && block.getType() == Material.STATIONARY_WATER) {
            CrisisPlayer cp = CrisisPlayer.crisisPlayers.get(p.getUniqueId());
            block.breakNaturally();
            p.playSound(block.getLocation(), Sound.ENTITY_GENERIC_DRINK, 1, 1);
            cp.setWater(cp.getWater() + 500);
            if (cp.getWater() > 3100) cp.setWater(3100);
            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> block.setType(Material.STATIONARY_WATER), 2);
        }
    }

    private Block getTargetBlock(Player p, int range) {
        BlockIterator iterator = new BlockIterator(p, range);
        while (iterator.hasNext()) {
            Block b = iterator.next();
            if (b.getType() != Material.AIR) {
                return b;
            }
        }
        return null;
    }

    private void lowTask() {
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            for (CrisisPlayer cp : CrisisPlayer.crisisPlayers.values()) {
                if (cp.getFood() <= 300) {
                    cp.p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 100, 2, true, false));
                    if (cp.getFood() <= 0) {
                        cp.p.damage(1);
                    }
                }
                if (cp.getWater() <= 300) {
                    cp.p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, (int) (300 - cp.getWater()), 5, true, false));
                    if (cp.getWater() <= 0) {
                        cp.p.damage(1);
                    }
                }
            }
        }, 20, 100);
    }
}

class SustenanceListener implements Listener {

    private Crisis plugin;
    private Sustenance sus;

    public SustenanceListener(Sustenance sus, Crisis plugin) {
        this.plugin = plugin;
        this.sus = sus;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        sus.handleBlockBreak(event);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            sus.handleRightClick(event);
        }

    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        sus.handlePlayerMove(event);
    }

    @EventHandler
    public void onItemConsume(PlayerItemConsumeEvent event) {
        sus.handleItemConsume(event);
    }
}
