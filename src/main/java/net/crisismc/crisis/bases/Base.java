package net.crisismc.crisis.bases;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Base {

    private final String uuid;
    private List<UUID> addedPlayers;
    private List<MultiBlock> multiBlocks;
    private Location emeraldLoc;

    final private Chunk chunk;

    //Cannot be more than 5
    private byte range;

    /**
     * To be used on first Base setup
     */
    public Base(Chunk chunk, Player p) {
        addedPlayers = new ArrayList<>();
        addedPlayers.add(p.getUniqueId());
        this.chunk = chunk;
        range = 1;
        uuid = UUID.randomUUID().toString();
    }

    public Base(String uuid, List<UUID> addedPlayers, List<MultiBlock> multiBlocks, Chunk chunk, byte range) {
        this.uuid = uuid;
        this.addedPlayers = addedPlayers;
        this.multiBlocks = multiBlocks;
        this.chunk = chunk;
        if (range > 5) range = 5;
        this.range = range;

    }

    public Base(String uuid, List<UUID> addedPlayers, int chunkx, int chunky, World world, byte range) {
        this.uuid = uuid;
        this.addedPlayers = addedPlayers;
        this.chunk = new Location(world, chunkx * 16, 0, chunky * 16).getChunk();
        if (range > 5) range = 5;
        this.range = range;

    }

    public boolean isInBase(Chunk c) {
        return Math.abs(c.getX() - chunk.getX()) <= range && Math.abs(c.getZ() - chunk.getZ()) <= range;

    }

    public boolean containsPlayer(Player p) {
        return addedPlayers.contains(p.getUniqueId());
    }

    public boolean containsPlayer(UUID uuid) {
        return addedPlayers.contains(uuid);
    }

    public void addPlayer(Player p) {
        addedPlayers.add(p.getUniqueId());
    }

    public void addPlayer(UUID uuid) {
        addedPlayers.add(uuid);
    }

    public void removePlayer(Player p) {
        addedPlayers.remove(p.getUniqueId());
    }

    public void removePlayer(UUID uuid) {
        addedPlayers.remove(uuid);
    }

    public List<UUID> getPlayers() {
        return addedPlayers;
    }

    public byte getRange() {
        return range;
    }

    public void setRange(byte range) {
        if (range > 5) range = 5;
        this.range = range;
    }

    public String getUuid() {
        return uuid;
    }

    public Chunk getChunk() {
        return chunk;
    }

    public Location getEmeraldLoc() {
        return emeraldLoc;
    }

    public void setEmeraldLoc(Location emeraldLoc) {
        this.emeraldLoc = emeraldLoc;
    }
}
