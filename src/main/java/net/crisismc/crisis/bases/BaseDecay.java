package net.crisismc.crisis.bases;

import net.crisismc.crisis.Crisis;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.SplittableRandom;

public class BaseDecay {

    private World world;
    private BaseManager bm;
    private SplittableRandom r;

    public BaseDecay(World world, BaseManager bm) {
        this.world = world;
        this.bm = bm;
        r = new SplittableRandom();
        this.task();
    }

    private void task() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Crisis.getInstance(), () -> preformDecay(), 10, 10);
    }

    private void preformDecay() {
        Chunk[] chunks = world.getLoadedChunks();

        Chunk chunk = chunks[r.nextInt(chunks.length)];
        int x = r.nextInt(16) + (chunk.getX() * 16);
        int z = r.nextInt(16) + (chunk.getZ() * 16);

        Location loc = new Location(world, x, 255, z);

        //Only perform decay on abandoned bases
        if (bm.getBase(loc) == null) {
            while (!bm.getBaseBlocks().contains(loc.getBlock().getType()) && loc.getY() > 0) {
                loc.add(0, -1, 0);
            }
            if (loc.getY() > 0) {
                loc.getBlock().breakNaturally();
            }
        }
    }
}
