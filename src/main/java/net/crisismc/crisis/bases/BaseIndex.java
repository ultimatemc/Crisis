package net.crisismc.crisis.bases;

public class BaseIndex {

    private int chunkx;
    private int chunkz;
    private byte range;

    public BaseIndex(int chunkx, int chunkz, byte range) {
        this.chunkx = chunkx;
        this.chunkz = chunkz;
        this.range = range;
    }

    @Override
    public int hashCode() {
        return 31 * chunkx * chunkz;
    }

    public int getChunkx() {
        return chunkx;
    }

    public void setChunkx(int chunkx) {
        this.chunkx = chunkx;
    }

    public int getChunkz() {
        return chunkz;
    }

    public void setChunkz(int chunkz) {
        this.chunkz = chunkz;
    }

    public byte getRange() {
        return range;
    }

    public void setRange(byte range) {
        this.range = range;
    }
}
