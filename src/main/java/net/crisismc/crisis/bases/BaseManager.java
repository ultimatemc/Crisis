package net.crisismc.crisis.bases;

import com.comphenix.protocol.ProtocolManager;
import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.data.DataException;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.bases.baseparts.*;
import net.crisismc.crisis.bases.multiblocks.Well;
import net.crisismc.crisis.database.BaseLoader;
import net.crisismc.crisis.database.SqlDatabase;
import net.crisismc.crisis.lib.ChatUtil;
import net.crisismc.crisis.lib.NbtTags;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

public class BaseManager implements Listener {

    private final String BASE_INV_NAME = ChatColor.DARK_GRAY + "Base Management";
    private final String ADD_INV_NAME = ChatColor.DARK_GRAY + "Add Players";
    private final String REMOVE_INV_NAME = ChatColor.DARK_GRAY + "Remove Players";
    private final String UPGRADE_INV_NAME = ChatColor.DARK_GRAY + "Upgrade Base";
    private final String ABANDON_INV_NAME = ChatColor.DARK_GRAY + "Abandon Base";

    public Map<BaseIndex, Base> bases = new ConcurrentHashMap<>();
    private static List<Town> towns = new ArrayList<>();
    private BaseLoader bl;
    private List<BasePart> baseParts = new ArrayList<>();
    private Inventory baseInv;
    private List<Material> baseBlocks = new ArrayList<>();

    public BaseManager(Crisis plugin, SqlDatabase sql, ProtocolManager pm) {
        bl = new BaseLoader(plugin, sql, this);
        try {
            baseParts.add(new BaseCore("Base Core", Material.EMPTY_MAP, this, bl));
            baseParts.add(new Foundation("Foundation", Material.EMPTY_MAP, this));
            baseParts.add(new Wall("Wall", Material.EMPTY_MAP, this));
            baseParts.add(new Pillar("Pillar", Material.EMPTY_MAP, this));
            baseParts.add(new Floor("Floor", Material.EMPTY_MAP, this));
            baseParts.add(new Door("Door", Material.EMPTY_MAP, this));
            baseParts.add(new ExternalWall("External Wall", Material.EMPTY_MAP, this));
            baseParts.add(new Well("Well", Material.EMPTY_MAP));
        } catch (DataException | IOException e) {
            System.out.println("Error: Could not load BaseParts, plugin may not preform correctly");
            e.printStackTrace();
        }
        setupInv();
        setupTowns();

        new Hammer(plugin, baseParts, pm, this);
        new BaseDecay(Bukkit.getWorld(Crisis.WORLD_NAME), this);

        for (Chunk c : Bukkit.getWorld(Crisis.WORLD_NAME).getLoadedChunks()) {
            bl.loadBaseInChunk(c);
        }

        baseBlocks.addAll(Arrays.asList(
                Material.OBSIDIAN,
                Material.BEDROCK,
                Material.PISTON_BASE,
                Material.PISTON_STICKY_BASE,
                Material.ACACIA_DOOR
        ));

        plugin.getServer().getPluginManager().registerEvents(this, plugin);

    }

    private void setupInv() {
        baseInv = Bukkit.createInventory(null, 18, BASE_INV_NAME);
        for (int i = 0; i < 18; i++) {
            baseInv.setItem(i, CrisisItem.INVENTORY_ITEM.getItemStack(1));
        }

        ItemStack addPlayer = new ItemStack(Material.WOOL, 1, (byte) 5);
        ItemMeta im = addPlayer.getItemMeta();
        im.setDisplayName(ChatColor.GREEN + "Add Players");
        addPlayer.setItemMeta(im);

        ItemStack removePlayer = new ItemStack(Material.WOOL, 1, (byte) 14);
        im = removePlayer.getItemMeta();
        im.setDisplayName(ChatColor.RED + "Remove Players");
        removePlayer.setItemMeta(im);

        ItemStack upgrade = new ItemStack(Material.GOLD_BLOCK, 1);
        im = upgrade.getItemMeta();
        im.setDisplayName(ChatColor.GOLD + "Upgrade Base");
        upgrade.setItemMeta(im);

        ItemStack unclaim = new ItemStack(Material.BARRIER, 1);
        im = unclaim.getItemMeta();
        im.setDisplayName(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Abandon base");
        unclaim.setItemMeta(im);

        baseInv.setItem(3, addPlayer);
        baseInv.setItem(4, upgrade);
        baseInv.setItem(5, removePlayer);
        baseInv.setItem(13, unclaim);

    }

    private void openAddInv(Player p, Base base) {
        Inventory addInv = Bukkit.createInventory(null, 54, ADD_INV_NAME);
        int i = 0;
        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            if (base.containsPlayer(onlinePlayer)) continue;
            addInv.setItem(i, NbtTags.setNBTData(NbtTags.setNBTData(CrisisItem.PLAYER_SKULL.getItemStack(1, onlinePlayer), "crisis_base", base.getUuid()), "crisis_playeruuid", onlinePlayer.getUniqueId().toString()));
            i++;
        }
        p.openInventory(addInv);
    }

    private void openRemoveInv(Player p, Base base) {
        Inventory removeInv = Bukkit.createInventory(null, 54, REMOVE_INV_NAME);
        int i = 0;
        for (UUID uuid : base.getPlayers()) {
            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(uuid);
            removeInv.setItem(i, NbtTags.setNBTData(NbtTags.setNBTData(CrisisItem.PLAYER_SKULL.getItemStack(1, offlinePlayer), "crisis_base", base.getUuid()), "crisis_playeruuid", offlinePlayer.getUniqueId().toString()));
            i++;
        }
        p.openInventory(removeInv);
    }

    private void openUpgradeInv(Player p, Base base) {
        Inventory upgradeInv = Bukkit.createInventory(null, 18, UPGRADE_INV_NAME);
        for (int i = 0; i < 18; i++) {
            if (i == 4) continue;
            upgradeInv.setItem(i, CrisisItem.INVENTORY_ITEM.getItemStack(1));
        }
        ItemStack upgrade = new ItemStack(Material.GOLD_BLOCK);
        ItemMeta im = upgrade.getItemMeta();
        im.setDisplayName(ChatColor.GOLD + "Upgrade Base");
        im.setLore(Arrays.asList(
                ChatColor.AQUA + "Current level: " + ChatColor.GOLD + base.getRange(),
                ChatColor.GRAY + "Add a" + ChatColor.GOLD + " Base Upgrade Piece " + ChatColor.GRAY + "above",
                ChatColor.GRAY + "and click this item to upgrade",
                ChatColor.GRAY + "the range of this base by 1 chunk"
        ));
        upgrade.setItemMeta(im);
        upgrade = NbtTags.setNBTData(upgrade, "crisis_base", base.getUuid());
        upgradeInv.setItem(13, upgrade);

        p.openInventory(upgradeInv);
    }

    private void openAbondonInv(Player p, Base base) {
        Inventory abandonInv = Bukkit.createInventory(null, 9, ABANDON_INV_NAME);
        for (int i = 0; i < 9; i++) {
            abandonInv.setItem(i, CrisisItem.INVENTORY_ITEM.getItemStack(1));
        }

        ItemStack confirm = new ItemStack(Material.EMERALD_BLOCK);
        ItemMeta im = confirm.getItemMeta();
        im.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Confirm");
        im.setLore(Arrays.asList(
                ChatColor.GRAY + "Abandoning a base removes your ownership of the base",
                ChatColor.GRAY + "and allows others to claim this area as their own."
        ));
        confirm.setItemMeta(im);
        confirm = NbtTags.setNBTData(confirm, "crisis_base", base.getUuid());

        abandonInv.setItem(4, confirm);

        p.openInventory(abandonInv);
    }

    /**
     * To be called whenever a block that belongs to a base needs to be broken
     * Contains logic for special cases
     * @param block The block to break
     */
    public void breakBaseBlock(Block block) {
        BasePart externalWall = getBasePart("External Wall");
        if (externalWall.getMaterials().contains(block.getType())) {
            do {
                block.breakNaturally();
                block = block.getRelative(BlockFace.UP);
            } while (externalWall.getMaterials().contains(block.getType()));
        }

        block.breakNaturally();
    }

    public void addBase(Base base) {
        bases.put(new BaseIndex(base.getChunk().getX(), base.getChunk().getZ(), base.getRange()), base);
    }

    public void removeBase(Base base) {
        if (base.getEmeraldLoc() != null) {
            base.getEmeraldLoc().getBlock().setType(Material.AIR);
        }
        bases.values().remove(base);
    }


    public Collection<Base> getBases() {
        return bases.values();
    }

    public boolean isInTown(Location loc) {
        for (Town town : towns) {
            if (town.isInside(loc)) return true;
        }

        return false;
    }

    public boolean isInFriendlyBase(Location loc, Player p) {
        Chunk c = loc.getChunk();
        for (Entry<BaseIndex, Base> e : bases.entrySet()) {
            //If is in base
            if (Math.abs(c.getX() - e.getKey().getChunkx()) <= e.getKey().getRange() && Math.abs(c.getZ() - e.getKey().getChunkz()) <= e.getKey().getRange()) {
                return e.getValue().containsPlayer(p);
            }
        }
        return false;
    }

    public boolean isInForeignBase(Location loc, Player p) {
        Chunk c = loc.getChunk();
        for (Entry<BaseIndex, Base> e : bases.entrySet()) {
            //If is in base
            if (Math.abs(c.getX() - e.getKey().getChunkx()) <= e.getKey().getRange() && Math.abs(c.getZ() - e.getKey().getChunkz()) <= e.getKey().getRange()) {
                return !e.getValue().containsPlayer(p);
            }
        }
        return false;
    }

    private void setupTowns() {
        towns.add(new Town(-1809, 1839, -1930, 1638)); //Small airfield - Hughes
        towns.add(new Town(1595, 487, 1365, 259)); //Large airfield - Maxwell
        towns.add(new Town(-591, 897, -720, 1029)); //Pinefield
        towns.add(new Town(-157, 2550, -385, 2354)); //Sutton
        towns.add(new Town(747, 2552, 797, 2501)); //Gas station - GREEN
        towns.add(new Town(2396, 1742, 2103, 2152)); //Maine
        towns.add(new Town(1254, -2290, 1000, -2024)); //Ryerson
        towns.add(new Town(152, -2245, -158, -2470)); //Military fort - NOT BUILT
        towns.add(new Town(-672, -1203, -959, -982)); //Horton
        towns.add(new Town(-1929, -2301, -2241, -1985)); //Thirsk
        towns.add(new Town(-1253, 720, -1201, 776)); //Gas station - RED
        towns.add(new Town(-1910, -1208, -2000, -1295)); //Military camp
    }

    public Base getBase(Location loc) {
        Chunk c = loc.getChunk();
        for (Entry<BaseIndex, Base> e : bases.entrySet()) {
            //If is in base
            if (Math.abs(c.getX() - e.getKey().getChunkx()) <= e.getKey().getRange() && Math.abs(c.getZ() - e.getKey().getChunkz()) <= e.getKey().getRange()) {
                return e.getValue();
            }
        }
        return null;
    }

    private Base getBase(ItemStack item) {
        String uuid = NbtTags.getNBTData(item, "crisis_base");
        for (Base b : bases.values()) {
            if (b.getUuid().equals(uuid)) return b;
        }
        return null;
    }

    public Base getBase(String baseid) {
        for (Base b : bases.values()) {
            if (b.getUuid().equals(baseid)) return b;
        }
        return null;
    }

    public BasePart getBasePart(String name) {
        for (BasePart bp : baseParts) {
            if (bp.getName().equals(name)) return bp;
        }
        return null;
    }

    public CuboidClipboard getSchematic(BasePart basePart) {
        for (BasePart bp : baseParts) {
            if (bp.getName().equals(basePart.getName())) return bp.getSchematic();
        }
        return null;
    }

    public BaseLoader getBaseLoader() {
        return bl;
    }

    public void onDisable() {
        bl.saveAll();
    }

    /*
     *  Listeners
     */
    @EventHandler
    public void onChunkLoad(ChunkLoadEvent event) {
        bl.loadBaseInChunk(event.getChunk());
    }

    @EventHandler
    public void onChunkUnload(ChunkUnloadEvent event) {
        bl.saveBaseInChunk(event.getChunk());
    }

    @EventHandler
    public void onEmeraldClick(PlayerInteractEvent event) {
        if (event.getHand() != EquipmentSlot.HAND) return;

        //if right click emerald
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getClickedBlock().getType() == Material.EMERALD_BLOCK) {
            Base base = getBase(event.getClickedBlock().getLocation());
            if (base == null) return;
            baseInv.setItem(4, NbtTags.setNBTData(baseInv.getItem(4), "crisis_base", base.getUuid()));
            event.getPlayer().openInventory(baseInv);
            System.out.println(NbtTags.getNBTData(baseInv.getItem(4), "crisis_base"));

            base.setEmeraldLoc(event.getClickedBlock().getLocation());
        }
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent event) {
        if (event.getClickedInventory() == null || event.getClickedInventory().getName() == null) return;
        if (event.getCurrentItem() == null || !event.getCurrentItem().hasItemMeta()) return;
        Player p = (Player) event.getWhoClicked();
        String name = event.getClickedInventory().getName();
        Material mat = event.getCurrentItem().getType();

        if (name.equals(BASE_INV_NAME)) {
            p.playSound(p.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
            event.setCancelled(true);
            byte data = event.getCurrentItem().getData().getData();
            Base base = getBase(event.getClickedInventory().getItem(4));
            if (base == null) {
                p.closeInventory();
                return;
            }

            if (mat == Material.GOLD_BLOCK) {
                openUpgradeInv(p, base);
            } else if (data == 5) {
                openAddInv(p, base);
            } else if (data == 14) {
                openRemoveInv(p, base);
            } else if (mat == Material.BARRIER) {
                openAbondonInv(p, base);
            }

        } else if (name.equals(ADD_INV_NAME)) {
            p.playSound(p.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
            event.setCancelled(true);
            Base base = getBase(event.getCurrentItem());
            if (base == null) {
                p.closeInventory();
                return;
            }

            if (base.getPlayers().size() < 50) {
                if (!base.getPlayers().contains(UUID.fromString(NbtTags.getNBTData(event.getCurrentItem(), "crisis_playeruuid")))) {
                    base.addPlayer(UUID.fromString(NbtTags.getNBTData(event.getCurrentItem(), "crisis_playeruuid")));
                    p.sendMessage(ChatUtil.getPrefix() + ChatColor.GRAY + "Added player " + ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName()) + " to base.");
                } else {
                    p.sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "That player is already added to this placeChest.");
                }
            } else {
                p.sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "You can only add a maximum of 50 players to a base.");
            }
            openAddInv(p, base);

        } else if (name.equals(REMOVE_INV_NAME)) {
            p.playSound(p.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
            event.setCancelled(true);
            Base base = getBase(event.getCurrentItem());
            if (base == null) {
                p.closeInventory();
                return;
            }

            String uuid = NbtTags.getNBTData(event.getCurrentItem(), "crisis_playeruuid");
            if (base.containsPlayer(UUID.fromString(uuid))) {
                p.sendMessage(ChatUtil.getPrefix() + ChatColor.GRAY + "Removed player " + ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName()) + " from base.");
                base.removePlayer(UUID.fromString(uuid));
            }
            openRemoveInv(p, base);
        } else if (name.equals(UPGRADE_INV_NAME)) {
            p.playSound(p.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
            if (mat == Material.GOLD_BLOCK) {
                Base base = getBase(event.getCurrentItem());
                if (base == null) {
                    p.closeInventory();
                    return;
                }
                if (event.getClickedInventory().getItem(4) != null && event.getClickedInventory().getItem(4).getType() == Material.EYE_OF_ENDER) {
                    if (base.getRange() < 5) {
                        base.setRange((byte) (base.getRange() + 1));
                        p.sendMessage(ChatUtil.getPrefix() + ChatColor.GREEN + "Base level upgraded.");
                        p.sendMessage(ChatUtil.getPrefix() + ChatColor.AQUA + "Base level: " + ChatColor.GOLD + base.getRange());

                        if (event.getClickedInventory().getItem(4).getAmount() > 1) {
                            ItemStack item = event.getClickedInventory().getItem(4);
                            item.setAmount(item.getAmount() - 1);
                            event.getClickedInventory().setItem(4, item);
                        } else {
                            event.getClickedInventory().setItem(4, null);
                        }

                        p.closeInventory();
                    } else {
                        p.sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "This base is already at the maximum level.");
                    }
                } else {
                    p.sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "You must add a " + ChatColor.GOLD + "Base Upgrade Piece" + ChatColor.RED + " before you can upgrade this base.");
                }
            }
            event.setCancelled(true);
        } else if (name.equals(ABANDON_INV_NAME)) {
            p.playSound(p.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
            event.setCancelled(true);
            if (mat == Material.EMERALD_BLOCK) {
                Base base = getBase(event.getClickedInventory().getItem(4));
                if (base == null) {
                    p.closeInventory();
                    return;
                }
                p.sendMessage(ChatUtil.getPrefix() + ChatColor.GRAY + "You have abandoned this base");
                removeBase(base);
                bl.deleteBase(base);
            }
        }
    }

    @EventHandler
    public void onInvClose(InventoryCloseEvent event) {
        if (event.getInventory().getName().equals(UPGRADE_INV_NAME)) {
            if (event.getInventory().getItem(4) != null) {
                event.getPlayer().getWorld().dropItemNaturally(event.getPlayer().getLocation(), event.getInventory().getItem(4));
            }
        }
    }

    public List<Material> getBaseBlocks() {
        return baseBlocks;
    }

}

class Town {

    private int x1;
    private int x2;
    private int z1;
    private int z2;

    public Town(int x1, int z1, int x2, int z2) {
        this.x1 = x1;
        this.z1 = z1;
        this.x2 = x2;
        this.z2 = z2;
    }

    public boolean isInside(Location loc) {
        return loc.getX() < Math.max(x1, x2) && loc.getX() > Math.min(x1, x2)
                && loc.getZ() < Math.max(z1, z2) && loc.getZ() > Math.min(z1, z2);

    }


}