package net.crisismc.crisis.bases;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.schematic.MCEditSchematicFormat;
import net.crisismc.crisis.lib.ChatUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("deprecation")
public abstract class BasePart {

    private CuboidClipboard schematic;
    private List<ItemStack> cost;
    private int rotation;
    private String name;
    private Material material;
    private boolean oneTime;

    public BasePart() {

    }

    public BasePart(File schematic, Vector offset, List<ItemStack> cost, String name, Material material) throws DataException, IOException {
        if (schematic != null) this.setSchematic(MCEditSchematicFormat.getFormat(schematic).load(schematic));
        if (schematic != null && offset != null)
            this.schematic.setOffset(new com.sk89q.worldedit.Vector(offset.getX(), offset.getY(), offset.getZ()));
        if (schematic != null) rotation = 0;
        this.cost = cost;
        this.name = name;
        this.material = material;
        this.oneTime = false;
    }

    public BasePart(File schematic, Vector offset, List<ItemStack> cost, String name, Material material, boolean oneTime) throws DataException, IOException {
        if (schematic != null) this.setSchematic(MCEditSchematicFormat.getFormat(schematic).load(schematic));
        if (schematic != null && offset != null)
            this.schematic.setOffset(new com.sk89q.worldedit.Vector(offset.getX(), offset.getY(), offset.getZ()));
        if (schematic != null) rotation = 0;
        this.cost = cost;
        this.name = name;
        this.material = material;
        this.oneTime = oneTime;
    }

    public abstract boolean canPlace(Location loc, Player p);

    /**
     * @return The materials that are part of this base part
     */
    public List<Material> getMaterials() {
        return Arrays.asList();
    }

    public List<Vector> getDisplayLocations(Location loc, Player p) {
        List<Vector> locs = new ArrayList<>();
        schematic.rotate2D(getRotation(p.getLocation().getYaw()) * 90);
        for (int x = loc.getBlockX(); x < loc.getBlockX() + schematic.getWidth(); ++x) {
            for (int y = loc.getBlockY(); y < loc.getBlockY() + schematic.getHeight(); ++y) {
                for (int z = loc.getBlockZ(); z < loc.getBlockZ() + schematic.getLength(); ++z) {
                    locs.add(new Vector(x, y, z).add(new Vector(0.5, -1.2, 0.5).add(new Vector(schematic.getOffset().getX(), schematic.getOffset().getY(), schematic.getOffset().getZ()))));
                }
            }
        }
        schematic.rotate2D(-getRotation(p.getLocation().getYaw()) * 90);

        return locs;
    }

    public List<Block> getPlaceBlocks(Location loc, Player p) {
        List<Block> locs = new ArrayList<>();
        schematic.rotate2D(getRotation(p.getLocation().getYaw()) * 90);
        for (int x = loc.getBlockX(); x < loc.getBlockX() + schematic.getWidth(); ++x) {
            for (int y = loc.getBlockY(); y < loc.getBlockY() + schematic.getHeight(); ++y) {
                for (int z = loc.getBlockZ(); z < loc.getBlockZ() + schematic.getLength(); ++z) {
                    Vector v = new Vector(x, y, z).add(new Vector(schematic.getOffset().getX(), schematic.getOffset().getY(), schematic.getOffset().getZ())).add(new Vector(0.5, 0.5, 0.5));
                    locs.add(new Location(p.getWorld(), v.getX(), v.getY(), v.getZ()).getBlock());
                }
            }
        }
        schematic.rotate2D(-getRotation(p.getLocation().getYaw()) * 90);

        return locs;
    }

    public List<Block> getBottomBlocks(Location loc, Player p) {
        List<Block> locs = new ArrayList<>();
        schematic.rotate2D(getRotation(p.getLocation().getYaw()) * 90);
        for (int x = loc.getBlockX(); x < loc.getBlockX() + schematic.getWidth(); ++x) {
            for (int z = loc.getBlockZ(); z < loc.getBlockZ() + schematic.getLength(); ++z) {
                Vector v = new Vector(x, loc.getBlockY(), z).add(new Vector(schematic.getOffset().getX(), schematic.getOffset().getY(), schematic.getOffset().getZ())).add(new Vector(0.5, 0.5, 0.5));
                locs.add(new Location(p.getWorld(), v.getX(), v.getY(), v.getZ()).getBlock());
            }
        }
        schematic.rotate2D(-getRotation(p.getLocation().getYaw()) * 90);
        return locs;
    }

    /**
     * Can be overridden, but super must be called
     * @param loc
     * @param p
     * @return
     */
    public boolean place(Location loc, Player p) {
        if (!canAfford(p)) {
            p.sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "Insufficient materials.");
            return false;
        }
        schematic.rotate2D(getRotation(p.getLocation().getYaw()) * 90);
        WorldEditPlugin we = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
        EditSession session = we.getWorldEdit().getEditSessionFactory().getEditSession(new BukkitWorld(loc.getWorld()), 1000000);

        try {
            schematic.paste(session, new com.sk89q.worldedit.Vector(loc.getX(), loc.getY(), loc.getZ()), true);
            schematic.rotate2D(-getRotation(p.getLocation().getYaw()) * 90);
            p.getInventory().removeItem(cost.toArray(new ItemStack[cost.size()]));
            return true;
        } catch (MaxChangedBlocksException e) {
            e.printStackTrace();
            schematic.rotate2D(-getRotation(p.getLocation().getYaw()) * 90);
            return false;
        }
    }

    private boolean canAfford(Player p) {
        for (ItemStack item : cost) {
            if (!p.getInventory().containsAtLeast(item, item.getAmount())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Assumes that the default facing direction is south
     *
     * @param yaw of the player placing the BasePart
     */
    protected int getRotation(float yaw) {
        return (Math.round(yaw / 90.0F) & 0x3);
    }

    public CuboidClipboard getSchematic() {
        return schematic;
    }

    public void setSchematic(CuboidClipboard schematic) {
        this.schematic = schematic;
    }

    public List<ItemStack> getCost() {
        return cost;
    }

    public void setCost(List<ItemStack> cost) {
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }
}
