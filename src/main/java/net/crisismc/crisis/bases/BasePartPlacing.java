package net.crisismc.crisis.bases;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.*;
import com.comphenix.protocol.events.PacketListener;
import com.comphenix.protocol.wrappers.EnumWrappers.EntityUseAction;
import net.minecraft.server.v1_12_R1.*;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.lib.ChatUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class BasePartPlacing implements Listener {

    private Player p;
    private Crisis plugin;
    private List<BasePart> baseParts;
    private BasePart basePart;
    private ProtocolManager pm;

    private Location lastLoc = new Location(Bukkit.getWorld(Crisis.WORLD_NAME), 0, 0, 0);
    private List<EntityArmorStand> stands = new ArrayList<>();
    private int taskId;
    private PacketListener pl;

    boolean done = false;

    public BasePartPlacing(Crisis plugin, Player p, List<BasePart> baseParts, BasePart basePart, ProtocolManager pm) {
        this.p = p;
        this.plugin = plugin;
        this.baseParts = baseParts;
        this.basePart = basePart;
        this.pm = pm;
        setupStands();
        task();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        pl = setupPacketListener();
    }

    private void setupStands() {
        WorldServer s = ((CraftWorld) p.getLocation().getWorld()).getHandle();
        Location loc = calcLoc();

        for (Vector v : basePart.getDisplayLocations(loc, p)) {
            EntityArmorStand stand = new EntityArmorStand(s);
            stands.add(stand);

            stand.setLocation(v.getX(), v.getY(), v.getZ(), 0, 0);
            stand.setInvisible(true);

            PacketPlayOutSpawnEntityLiving create = new PacketPlayOutSpawnEntityLiving(stand);
            byte data = (byte) (basePart.canPlace(loc, p) ? 5 : 14); // Green if it can be placed, red if it is blocked
            PacketPlayOutEntityEquipment equipment = new PacketPlayOutEntityEquipment(stand.getId(), EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(new ItemStack(Material.WOOL, 1, data)));

            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(create);
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(equipment);
        }
    }

    private void task() {

        taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            Location loc = calcLoc();

            if (p.getInventory().getItemInMainHand().getType() != Material.ARROW) {
                end(true);
                return;
            }

            if (!loc.getBlock().equals(lastLoc)) {
                int i = 0;
                for (Vector v : basePart.getDisplayLocations(loc, p)) {
                    byte data = (byte) (basePart.canPlace(loc, p) ? 5 : 14); // Green if it can be placed, red if it is blocked
                    PacketPlayOutEntityEquipment equipment = new PacketPlayOutEntityEquipment(stands.get(i).getId(), EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(new ItemStack(Material.WOOL, 1, data)));
                    stands.get(i).setLocation(v.getX(), v.getY(), v.getZ(), 0.0F, 0.0F);
                    PacketPlayOutEntityTeleport teleport = new PacketPlayOutEntityTeleport(stands.get(i));

                    ((CraftPlayer) p).getHandle().playerConnection.sendPacket(teleport);
                    ((CraftPlayer) p).getHandle().playerConnection.sendPacket(equipment);
                    i++;
                }
            }
            lastLoc = loc;
        }, 0, 1);
    }

    private Location calcLoc() {
        int scalar = p.isSneaking() ? 2 : 5;
        return p.getEyeLocation().add(p.getLocation().getDirection().normalize().multiply(scalar));
    }

    private void end(boolean cancelled) {
        done = true;
        Bukkit.getScheduler().cancelTask(taskId);
        if (cancelled) {
            p.sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "Cancelled base part placement.");
        }
        for (EntityArmorStand stand : stands) {
            PacketPlayOutEntityDestroy kill = new PacketPlayOutEntityDestroy(stand.getId());
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(kill);
        }
        HandlerList.unregisterAll(this);
        pm.removePacketListener(pl);
    }

	/*
     * Listeners
	 */

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (!event.getPlayer().equals(p)) return;
        if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
            Location loc = calcLoc();
            if (basePart.canPlace(loc, p)) {
                boolean placed = basePart.place(loc, p);
                if (placed) {
                    p.sendMessage(ChatUtil.getPrefix() + ChatColor.GREEN + "Placed base part.");
                }
                event.setCancelled(true);
            } else {
                end(true);
            }
        } else if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)
            end(true);
    }


    private PacketListener setupPacketListener() {
        PacketListener pl = new PacketAdapter(plugin, ListenerPriority.NORMAL, PacketType.Play.Client.USE_ENTITY) {
            @Override
            public void onPacketReceiving(PacketEvent event) {
                if (done) return;
                if (!event.getPlayer().equals(p)) return;
                event.setCancelled(true);
                Bukkit.getScheduler().runTask(plugin, () -> {
                    if (event.getPacketType() == PacketType.Play.Client.USE_ENTITY) {
                        Player p = event.getPlayer();
                        PacketContainer packet = event.getPacket();
                        EntityUseAction action = packet.getEntityUseActions().readSafely(0);
                        if (action == EntityUseAction.ATTACK) { //Left click
                            Location loc = calcLoc();
                            if (basePart.canPlace(loc, p)) {
                                boolean placed = basePart.place(loc, p);
                                if (placed) {
                                    p.sendMessage(ChatUtil.getPrefix() + ChatColor.GREEN + "Placed base part.");
                                }
                            } else {
                                end(true);
                            }
                        } else { //Right click
                            end(true);
                        }
                    }

                });
            }
        };
        pm.addPacketListener(pl);
        return pl;
    }
}
