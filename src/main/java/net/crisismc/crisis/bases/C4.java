package net.crisismc.crisis.bases;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.lib.ChatUtil;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class C4 {

    private Player p;
    private Block clickedBlock;
    private BlockFace face;
    private BaseManager bm;

    int taskID;


    public C4(Player p, final Block clickedBlock, final BlockFace face, BaseManager bm) {
        if (bm.getBaseBlocks().contains(clickedBlock.getType())) {
            this.p = p;
            p.playSound(clickedBlock.getLocation(), Sound.ENTITY_TNT_PRIMED, 1, 0.5F);
            this.clickedBlock = clickedBlock;
            this.face = face;
            this.bm = bm;
            placeC4(p, clickedBlock, face);
        } else {
            p.sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "C4 must be placed on a base.");
        }
    }

    private void placeC4(Player p, final Block clickedBlock, final BlockFace face) {
        p.setItemInHand(new ItemStack(Material.AIR, 1));
        final Block c4 = clickedBlock.getRelative(face);
        c4.setType(Material.WOOD_BUTTON);
        switch (face) {
            case UP:
                c4.setData((byte) 5);
                break;
            case DOWN:
                c4.setData((byte) 0);
                break;
            case NORTH:
                c4.setData((byte) 4);
                break;
            case SOUTH:
                c4.setData((byte) 3);
                break;
            case WEST:
                c4.setData((byte) 2);
                break;
            case EAST:
                c4.setData((byte) 1);
                break;
            default:
        }


        taskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Crisis.getInstance(), new Runnable() {
            int count = 0;
            World world = clickedBlock.getWorld();

            public void run() {
                world.spawnParticle(Particle.SMOKE_NORMAL, clickedBlock.getRelative(face).getLocation().add(0.5, 0.5, 0.5), 2, 0F, 0F, 0F, 0F);
                count++;
                if (count % 20 == 0) {
                    world.spawnParticle(Particle.SMOKE_LARGE, clickedBlock.getRelative(face).getLocation().add(0.5, 0.5, 0.5), 10, 0F, 0F, 0F, 0F);
                }
                if (count >= 100) {
                    c4.setType(Material.AIR);
                    bm.breakBaseBlock(clickedBlock);
                    Bukkit.getScheduler().cancelTask(taskID);
                    world.spawnParticle(Particle.SMOKE_LARGE, clickedBlock.getRelative(face).getLocation().add(0.5, 0.5, 0.5), 200, 0F, 0F, 0F, 0.5F);
                    world.spawnParticle(Particle.FLAME, clickedBlock.getRelative(face).getLocation().add(0.5, 0.5, 0.5), 50, 0F, 0F, 0F, 0.2F);
                    world.playSound(clickedBlock.getLocation(), Sound.ENTITY_FIREWORK_BLAST, 4, 1);
                    for (Player p : Bukkit.getOnlinePlayers()) {
                        if (p.getLocation().distance(clickedBlock.getLocation()) <= 10) {
                            p.damage(20 - (p.getLocation().distance(clickedBlock.getLocation()) * 2));
                        }
                    }
                }
            }
        }, 0, 1);
    }
}