package net.crisismc.crisis.bases;

import com.comphenix.protocol.ProtocolManager;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.lib.PacketUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Hammer implements Listener {

    private Inventory inv;
    private List<BasePart> baseParts;
    private Crisis plugin;
    private ProtocolManager pm;
    private BaseManager bm;

    public Hammer(Crisis plugin, List<BasePart> baseParts, ProtocolManager pm, BaseManager bm) {
        this.plugin = plugin;
        this.pm = pm;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.baseParts = baseParts;
        this.bm = bm;

        inv = Bukkit.createInventory(null, 54, ChatColor.DARK_GRAY + "Building Parts");

        ItemStack item = new ItemStack(Material.EMPTY_MAP, 1);
        ItemMeta itemMeta = item.getItemMeta();

        for (BasePart basePart : baseParts) {
            itemMeta.setDisplayName(ChatColor.GOLD + basePart.getName());
            List<String> lore = new ArrayList<>();
            for (ItemStack costItem : basePart.getCost()) {
                lore.add(ChatColor.WHITE + costItem.getItemMeta().getDisplayName() + " - " + costItem.getAmount());
            }
            itemMeta.setLore(lore);
            item.setItemMeta(itemMeta);
            inv.addItem(item);
        }
        ItemStack d = new ItemStack(Material.TNT, 1);
        ItemMeta dMeta = d.getItemMeta();
        dMeta.setDisplayName(ChatColor.RED + "Destroy");
        d.setItemMeta(dMeta);
        inv.setItem(53, d);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (event.getPlayer().getInventory().getItemInMainHand().getType() == Material.ARROW)
                event.getPlayer().openInventory(inv);
        }
        if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
            if (event.getPlayer().getInventory().getItemInMainHand().getType() == Material.ARROW) {
                if (!event.getPlayer().getInventory().getItemInMainHand().hasItemMeta()) return;
                String name = ChatColor.stripColor(event.getPlayer().getInventory().getItemInMainHand().getItemMeta().getDisplayName());
                if (!name.contains("Destroy")) return;

                if (bm.isInFriendlyBase(event.getClickedBlock().getLocation(), event.getPlayer())) {
                    Material m = event.getClickedBlock().getType();
                    if (bm.getBaseBlocks().contains(m)) new HammerBlockBreak(bm, event.getPlayer(), event.getClickedBlock()).runTaskTimer(Crisis.getInstance(), 0, 1);
                }
            }
        }
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent event) {
        if (event.getClickedInventory() == null) return;
        if (event.getClickedInventory().equals(inv)) {
            event.setCancelled(true);
            if (event.getCurrentItem() == null || !event.getCurrentItem().hasItemMeta()) return;
            String name = ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName());

            if (name.equals("Destroy")) {
                if (event.getWhoClicked().getInventory().getItemInMainHand().getType() == Material.ARROW) {
                    ItemStack hammer = new ItemStack(Material.ARROW, 1);
                    ItemMeta meta = hammer.getItemMeta();
                    meta.setDisplayName(ChatColor.WHITE + "Hammer" + ChatColor.BOLD + " > " + ChatColor.RESET.toString() + ChatColor.RED + "Destroy");
                    hammer.setItemMeta(meta);

                    event.getWhoClicked().getInventory().setItemInMainHand(hammer);
                    event.getWhoClicked().closeInventory();
                }
            } else {
                for (BasePart bp : baseParts) {
                    if (bp.getName().equals(name)) {
                        if (event.getWhoClicked().getInventory().getItemInMainHand().getType() == Material.ARROW) {
                            ItemStack hammer = new ItemStack(Material.ARROW, 1);
                            ItemMeta meta = hammer.getItemMeta();
                            meta.setDisplayName(ChatColor.WHITE + "Hammer" + ChatColor.BOLD + " > " + ChatColor.RESET.toString() + ChatColor.GOLD + "Build");
                            hammer.setItemMeta(meta);

                            event.getWhoClicked().getInventory().setItemInMainHand(hammer);
                            event.getWhoClicked().closeInventory();

                            new BasePartPlacing(plugin, (Player) event.getWhoClicked(), baseParts, bp, pm);
                            break;
                        }
                    }
                }
            }
        }
    }
}

class HammerBlockBreak extends BukkitRunnable {

    private BaseManager bm;
    private Player p;
    private Block block;

    private int counter = 100;

    public HammerBlockBreak(BaseManager bm, Player p, Block block) {
        this.bm = bm;
        this.p = p;
        this.block = block;
    }

    @Override
    public void run() {
        if (counter > 0) {
            if (p.getTargetBlock((Set<Material>) null, 5).equals(block)) {
                if (counter % 10 == 0) p.playSound(p.getLocation(), Sound.BLOCK_STONE_PLACE, 1, 1);

                String breakingStr = "Breaking";
                int process = (int) Math.ceil((((double) (100 - counter)) / (double) (100)) * breakingStr.length());
                String msg = "";
                for (int i = 0; i < process; i++) {
                    msg = msg + ChatColor.RED + breakingStr.charAt(i);
                }
                for (int i = process; i < breakingStr.length(); i++) {
                    msg = msg + ChatColor.GRAY + breakingStr.charAt(i);
                }
                PacketUtil.sendActionBarPacket(p, msg);

                counter--;
            } else {
                this.cancel();
            }
        } else {
            p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_LAND, 1, 0.5F);
            bm.breakBaseBlock(block);
            this.cancel();
        }
    }

}



