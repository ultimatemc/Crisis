package net.crisismc.crisis.bases;

import com.google.gson.annotations.SerializedName;
import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.event.platform.BlockInteractEvent;
import net.crisismc.crisis.Crisis;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.block.BlockEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Abstract class to be inherited by child MultiBlocks
 * Methods that begin with "On" will be called in child classes when
 * appropriate by BaseManager class.
 */
public abstract class MultiBlock extends BasePart {

    @SerializedName("l")
    private Location loc;

    private transient String uuid;


    public MultiBlock(String uuid, Location loc, File schematic, Vector offset, List<ItemStack> cost, String name, Material material) throws DataException, IOException {
        super(schematic, offset, cost, name, material);
        this.uuid = uuid;
        this.loc = loc;
    }

    public CuboidClipboard getSchematic() {
        return Crisis.getInstance().getBaseManager().getSchematic(this);
    }

    public abstract boolean isPart(Block block);

    /**
     * Is part must have already returned true for this method to be called
     *
     * @param event
     */
    public void onDestroy(BlockEvent event) {
        Crisis.getInstance().getMultiBlockManager().getMultiBlockLoader().deleteMultiBlock(uuid);
    }

    public void onInteract(BlockInteractEvent event) {
        //Do nothing
    }

    public String getUuid() {
        return uuid;
    }

    public Location getLoc() {
        return loc;
    }

    public void setLoc(Location loc) {
        this.loc = loc;
    }
}
