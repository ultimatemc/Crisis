package net.crisismc.crisis.bases;

import net.crisismc.crisis.database.SqlDatabase;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.database.MultiBlockLoader;
import org.bukkit.event.Listener;


public class MultiBlockManager implements Listener {

    private MultiBlockLoader bl;

    public MultiBlockManager(SqlDatabase sql) {
        bl = new MultiBlockLoader(sql);
        Crisis.getInstance().getServer().getPluginManager().registerEvents(this, Crisis.getInstance());
    }

    public MultiBlockLoader getMultiBlockLoader() {
        return bl;
    }
}
