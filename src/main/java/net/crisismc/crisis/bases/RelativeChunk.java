package net.crisismc.crisis.bases;

public class RelativeChunk {

    private byte x;
    private byte z;

    public RelativeChunk() {
        x = z = 0;
    }

    public RelativeChunk(byte x, byte z) {
        this.x = x;
        this.z = z;
    }

    public String sterilize() {
        String data = "(" + x + "," + z + ")";
        return data;
    }

    public byte getX() {
        return x;
    }

    public void setX(byte x) {
        this.x = x;
    }

    public byte getZ() {
        return z;
    }

    public void setZ(byte z) {
        this.z = z;
    }

    public static RelativeChunk desterilize(String data) {
        data.replaceAll("\\(", "");
        data.replaceAll("\\)", "");
        return new RelativeChunk(Byte.parseByte(data.split(",")[0]), Byte.parseByte(data.split(",")[1]));
    }
}
