package net.crisismc.crisis.bases.baseparts;

import com.sk89q.worldedit.data.DataException;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.bases.BaseManager;
import net.crisismc.crisis.bases.BasePart;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class Door extends BasePart {

    private BaseManager bm;

    public Door(String name, Material material, BaseManager bm) throws DataException, IOException {
        super(new File("plugins"+File.separator+"Crisis"+File.separator+"schematics"+File.separator+"door.schematic"), new Vector(0, -1, 0), Arrays.asList(CrisisItem.LOG.getItemStack(20)), name, material);
        this.bm = bm;
    }

    @Override
    public boolean canPlace(Location loc, Player p) {
        if (!bm.isInFriendlyBase(loc, p)) return false;

        for (Block b : getPlaceBlocks(loc, p)) {
            if (b.getType() != Material.AIR) return false;
        }
        for (Block b : getBottomBlocks(loc, p)) {
            if (b.getRelative(BlockFace.DOWN).getType() != Material.BEDROCK && b.getRelative(BlockFace.DOWN).getType() != Material.OBSIDIAN)
                return false;
        }
        return true;
    }
}