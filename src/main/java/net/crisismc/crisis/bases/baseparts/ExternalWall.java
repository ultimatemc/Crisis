package net.crisismc.crisis.bases.baseparts;

import com.sk89q.worldedit.data.DataException;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.bases.BaseManager;
import net.crisismc.crisis.bases.BasePart;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ExternalWall extends BasePart {
    private BaseManager bm;

    public ExternalWall(String name, Material material, BaseManager bm) throws DataException, IOException {
        super(new File("plugins"+File.separator+"Crisis"+File.separator+"schematics"+File.separator+"externalwall.schematic"), new Vector(0, -1, 0), Arrays.asList(CrisisItem.LOG.getItemStack(20)), name, material);
        this.bm = bm;
    }

    @Override
    public boolean canPlace(Location loc, Player p) {
        if (!bm.isInFriendlyBase(loc, p)) return false;

        for (Block b : getPlaceBlocks(loc, p)) {
            if (b.getType() != Material.AIR && b.getType() != Material.LONG_GRASS) return false;
        }
        for (Block b : getBottomBlocks(loc, p)) {
            if (b.getRelative(BlockFace.DOWN).getType() != Material.GRASS && b.getRelative(BlockFace.DOWN).getType() != Material.DIRT)
                return false;
        }
        return true;
    }

    @Override
    public List<Material> getMaterials() {
        return Arrays.asList(Material.PISTON_BASE, Material.PISTON_STICKY_BASE);
    }
}

