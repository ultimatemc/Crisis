package net.crisismc.crisis.bases.baseparts;

import com.sk89q.worldedit.data.DataException;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.bases.BaseManager;
import net.crisismc.crisis.bases.BasePart;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class Floor extends BasePart {
    private BaseManager bm;

    public Floor(String name, Material material, BaseManager bm) throws DataException, IOException {
        super(new File("plugins"+File.separator+"Crisis"+File.separator+"schematics"+File.separator+"floor.schematic"), new Vector(-2, 0, -2), Arrays.asList(CrisisItem.LOG.getItemStack(20)), name, material);
        this.bm = bm;
    }

    @Override
    public boolean canPlace(Location loc, Player p) {
        if (!bm.isInFriendlyBase(loc, p)) return false;

        int supports = 0;
        for (Block b : getPlaceBlocks(loc, p)) {
            if (b.getType() == Material.BEDROCK) {
                supports++;
            } else if (b.getType() != Material.AIR) return false;
        }
        return supports >= 6;
    }
}
