package net.crisismc.crisis.bases.baseparts;

import com.sk89q.worldedit.data.DataException;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.bases.BaseManager;
import net.crisismc.crisis.bases.BasePart;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class Foundation extends BasePart {
    private BaseManager bm;

    public Foundation(String name, Material material, BaseManager bm) throws DataException, IOException {
        super(new File("plugins"+File.separator+"Crisis"+File.separator+"schematics"+File.separator+"foundation.schematic"), new Vector(-2, 0, -2), Arrays.asList(CrisisItem.LOG.getItemStack(20)), name, material);
        this.bm = bm;
    }

    @Override
    public boolean canPlace(Location loc, Player p) {
        if (!bm.isInFriendlyBase(loc, p)) return false;

        for (Block b : getPlaceBlocks(loc, p)) {
            if (b.getType() != Material.AIR
                    && b.getType() != Material.OBSIDIAN
                    && b.getType() != Material.LONG_GRASS) return false;
        }
        for (Block b : getBottomBlocks(loc, p)) {
            Block below = b.getRelative(BlockFace.DOWN);
            if (below.getType() == Material.AIR || below.getType() == Material.LONG_GRASS) {
                Block bot = below.getRelative(BlockFace.DOWN);
                if (bot.getType() != Material.GRASS && bot.getType() != Material.DIRT) {
                    return false;
                }
            } else if (below.getType() != Material.GRASS && below.getType() != Material.DIRT) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean place(Location loc, Player p) {
        boolean result = super.place(loc, p);
        if (!result) return false;

        for (Block b : getBottomBlocks(loc, p)) {
            Block below = b.getRelative(BlockFace.DOWN);
            if (below.getType() == Material.AIR || below.getType() == Material.LONG_GRASS) {
                below.setType(Material.OBSIDIAN);
            }
        }

        return true;
    }
}
