package net.crisismc.crisis.bases.baseparts;

import com.sk89q.worldedit.data.DataException;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.bases.BaseManager;
import net.crisismc.crisis.bases.BasePart;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class Wall extends BasePart {
    private BaseManager bm;

    public Wall(String name, Material material, BaseManager bm) throws DataException, IOException {
        super(new File("plugins"+File.separator+"Crisis"+File.separator+"schematics"+File.separator+"wall.schematic"), new Vector(-2, -1, 0), Arrays.asList(CrisisItem.LOG.getItemStack(20)), name, material);
        this.bm = bm;
    }

    @Override
    public boolean canPlace(Location loc, Player p) {
        if (!bm.isInFriendlyBase(loc, p)) return false;

        for (Block b : getPlaceBlocks(loc, p)) {
            if (b.getType() != Material.AIR && b.getType() != Material.BEDROCK) return false;
        }
        for (Block b : getBottomBlocks(loc, p)) {
            if (b.getRelative(BlockFace.DOWN).getType() != Material.OBSIDIAN && b.getRelative(BlockFace.DOWN).getType() != Material.BEDROCK)
                return false;
        }
        return true;
    }
}
