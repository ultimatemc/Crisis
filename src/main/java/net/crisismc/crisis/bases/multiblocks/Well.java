package net.crisismc.crisis.bases.multiblocks;

import com.google.gson.annotations.SerializedName;
import com.sk89q.worldedit.data.DataException;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.bases.Base;
import net.crisismc.crisis.bases.MultiBlock;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockEvent;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class Well extends MultiBlock {

    @SerializedName("t")
    private int minsSinceFilled;

    @SerializedName("b")
    private String baseid;

    private transient int taskId;
    private transient Base base;

    /**
     * Called onEnable to store info for BaseManager
     *
     * @param name
     * @param material
     * @throws DataException
     * @throws IOException
     */
    public Well(String name, Material material) throws DataException, IOException {
        super(null, null, new File("plugins"+File.separator+"Crisis"+File.separator+"schematics"+File.separator+"industrialfurnace.schematic"), new Vector(-2, 0, -2), Arrays.asList(CrisisItem.LOG.getItemStack(20)), name, material);
    }

    /**
     * Called for the actual objects
     *
     * @param name
     * @throws DataException
     * @throws IOException
     */
    public Well(String uuid, String name, Location loc, String baseid, int blockid) throws DataException, IOException {
        super(uuid, null, null, null, null, name, null); //Redundant info is omitted (info that is the same for all Wells)
        minsSinceFilled = 0;
        this.baseid = baseid;
        base = Crisis.getInstance().getBaseManager().getBase(baseid);
        if (base != null) {
            task();
        }
    }

    private void task() {
        taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(Crisis.getInstance(), () -> {
            minsSinceFilled++;
            if (minsSinceFilled == 5) {
                fillWell();
                minsSinceFilled = 0;
            }
        }, 1200, 1200);
    }

    private void fillWell() {
        getLoc().getBlock().getRelative(BlockFace.DOWN).setType(Material.STATIONARY_WATER);
    }

    @Override
    public boolean isPart(Block block) {
        return false;
    }

    @Override
    public void onDestroy(BlockEvent event) {
        super.onDestroy(event);
        Bukkit.getScheduler().cancelTask(taskId);
    }

    @Override
    public boolean canPlace(Location loc, Player p) {
        return false;
    }
}
