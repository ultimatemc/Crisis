package net.crisismc.crisis.blocks;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.database.BlockLoader;
import net.crisismc.crisis.database.SqlDatabase;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BlockHandler implements Listener {

    private Crisis plugin;
    private BlockLoader bl;

    public Map<Vector, ResearchTable> researchTables = new ConcurrentHashMap<>();
    public Map<Vector, Camera> cameras = new ConcurrentHashMap<>();

    public BlockHandler(Crisis plugin, SqlDatabase sql) {
        this.plugin = plugin;
        bl = new BlockLoader(plugin, sql);
        loadBlocks();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.researchTableTask();
        this.blockEffectTask();
    }

    private void blockEffectTask() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            for (LivingEntity ent : Bukkit.getWorld(Crisis.WORLD_NAME).getLivingEntities()) {
                Material type1 = ent.getLocation().getBlock().getType();
                Material type2 = ent.getEyeLocation().getBlock().getType();
                Material typeBelow = ent.getLocation().add(0, -0.1, 0).getBlock().getType();

                if (type1 == Material.WEB || type2 == Material.WEB) {
                    ent.damage(2);
                }
                if (typeBelow == Material.PISTON_BASE) {
                    ent.damage(4);
                }

            }
        }, 1, 5);
    }

    private void loadBlocks() {
        for (Chunk chunk : Bukkit.getWorld(Crisis.WORLD_NAME).getLoadedChunks()) {
            bl.loadResearchTablesInChunk(chunk, this);
            bl.loadCamerasInChunk(chunk, this);
        }
    }

    public void onDisable() {
        bl.saveResearchTables(researchTables.values());
        bl.saveCameras(cameras.values());
    }

    public void researchTableTask() {
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin,
                () -> researchTables.values()
                        .stream().filter(rt -> rt.getPlayers().size() > 0)
                        .forEach(ResearchTable::tick), 1, 1);
    }

    public void addReseachTable(ResearchTable rt) {
        researchTables.put(rt.getLoc().toVector(), rt);
    }

    public void removeReseachTable(ResearchTable rt) {
        researchTables.remove(rt.getLoc().toVector());
    }

    public void addCamera(Camera cam) {
        cameras.put(cam.getLoc().toVector(), cam);
    }

    public void removeCamera(Camera cam) {
        cameras.put(cam.getLoc().toVector(), cam);
    }

    /*
     *
     * Event Handlers
     *
     */
    @EventHandler
    public void onChuckLoad(ChunkLoadEvent event) {
        bl.loadResearchTablesInChunk(event.getChunk(), this);
        bl.loadCamerasInChunk(event.getChunk(), this);
    }

    @EventHandler
    public void onChunkUnload(ChunkUnloadEvent event) {
        List<ResearchTable> rts = new ArrayList<>();
        researchTables.entrySet().stream().filter(entry -> entry.getValue().getChunk().equals(event.getChunk())).forEach(entry -> {
            researchTables.remove(entry.getKey());
            rts.add(entry.getValue());
        });
        bl.unloadResearchTables(rts, this);

        List<Camera> cams = new ArrayList<>();
        cameras.entrySet().stream().filter(entry -> entry.getValue().getChunk().equals(event.getChunk())).forEach(entry -> {
            cameras.remove(entry.getKey());
            cams.add(entry.getValue());
        });
        bl.unloadCameras(cams, this);

    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (event.getBlock().getType() == Material.WORKBENCH) {
            ResearchTable rt = new ResearchTable(event.getBlock().getLocation());
            researchTables.put(event.getBlock().getLocation().toVector(), rt);
            bl.createResearchTable(rt);
        } else if (event.getBlock().getType() == Material.TRIPWIRE_HOOK) {
            Camera cam = new Camera(event.getBlock().getLocation());
            cameras.put(event.getBlock().getLocation().toVector(), cam);
            bl.createCamera(cam);
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.getBlock().getType() == Material.WORKBENCH) {
            event.setCancelled(false);

            ResearchTable rt = researchTables.get(event.getBlock().getLocation().toVector());
            rt.getPlayers().forEach(HumanEntity::closeInventory);
            bl.deleteReseachTable(rt);
            researchTables.remove(event.getBlock().getLocation().toVector());

            Location dropLoc = event.getBlock().getLocation().add(0.5, 0.5, 0.5);

            if (rt.getItem() != null) dropLoc.getWorld().dropItemNaturally(dropLoc, rt.getItem());
            if (rt.getPaperAmount() > 0)
                dropLoc.getWorld().dropItemNaturally(dropLoc, CrisisItem.RESEARCH_PAPER.getItemStack(rt.getPaperAmount()));
            dropLoc.getWorld().dropItemNaturally(dropLoc, CrisisItem.RESEARCH_TABLE.getItemStack(1));
        }
        if (event.getBlock().getType() == Material.TRIPWIRE_HOOK) {
            event.setCancelled(false);

            Camera cam = cameras.get(event.getBlock().getLocation().toVector());
            cam.getPlayers().forEach(HumanEntity::closeInventory);
            bl.deleteCamera(cam);
            cameras.remove(event.getBlock().getLocation().toVector());

            Location dropLoc = event.getBlock().getLocation().add(0.5, 0.5, 0.5);

            dropLoc.getWorld().dropItemNaturally(dropLoc, CrisisItem.CAMERA.getItemStack(1));
        }
    }

    @EventHandler
    public void onBlockClick(PlayerInteractEvent event) {
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
        if (event.getClickedBlock().getType() == Material.WORKBENCH) {
            Location loc = event.getClickedBlock().getLocation();
            if (researchTables.containsKey(loc.toVector())) {
                ResearchTable rt = researchTables.get(loc.toVector());
                rt.openInv(event.getPlayer());
            } else {
                event.getPlayer().sendMessage(ChatColor.RED + "Error loading ResearchTable data");
            }
            event.setCancelled(true);
        } else if (event.getClickedBlock().getType() == Material.TRIPWIRE_HOOK) {
            Location loc = event.getClickedBlock().getLocation();
            if (cameras.containsKey(loc.toVector())) {
                Camera cam = cameras.get(loc.toVector());
                cam.openInv(event.getPlayer());
            } else {
                event.getPlayer().sendMessage(ChatColor.RED + "Error loading Camera data");
            }
        }
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent event) {
        if (event.getInventory().getName().equals(ResearchTable.INV_NAME)) {
            if (event.isShiftClick()) {
                event.setCancelled(true);
                return;
            }
            for (ResearchTable rt : researchTables.values()) {
                if (rt.getPlayers().contains(event.getWhoClicked())) {
                    rt.onInvClick(event);
                }
            }
        } else if (event.getInventory().getName().equals(Camera.INV_NAME)) {
            if (event.isShiftClick()) {
                event.setCancelled(true);
                return;
            }
            for (Camera cam : cameras.values()) {
                if (cam.getPlayers().contains(event.getWhoClicked())) {
                    cam.onInvClick(event);
                }
            }
        }
    }

    @EventHandler
    public void onInvDrag(InventoryDragEvent event) {
        if (event.getInventory() != null && event.getInventory().getName().equals(ResearchTable.INV_NAME)) {
            for (ResearchTable rt : researchTables.values()) {
                if (rt.getPlayers().contains(event.getWhoClicked())) {
                    rt.onInvDrag(event);
                }
            }
        } else if (event.getInventory().getName().equals(Camera.INV_NAME)) {
            for (Camera cam : cameras.values()) {
                if (cam.getPlayers().contains(event.getWhoClicked())) {
                    cam.onInvDrag(event);
                }
            }
        }
    }

    @EventHandler
    public void onInvClose(InventoryCloseEvent event) {
        if (event.getInventory().getName().equals(ResearchTable.INV_NAME)) {
            for (ResearchTable rt : researchTables.values()) {
                if (rt.getPlayers().contains(event.getPlayer())) {
                    rt.onInvClose((Player) event.getPlayer());
                }
            }
        } else if (event.getInventory().getName().equals(Camera.INV_NAME)) {
            for (Camera cam : cameras.values()) {
                if (cam.getPlayers().contains(event.getPlayer())) {
                    cam.onInvClose((Player) event.getPlayer());
                }
            }
        }
    }
}
