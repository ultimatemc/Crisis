package net.crisismc.crisis.blocks;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.lib.Callback;
import net.crisismc.crisis.inventory.AnvilNameSession;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class Camera implements InventoryBlock, Callback {

    public static final String INV_NAME = ChatColor.DARK_GRAY + "Security Camera";
    private String uuid;
    private String name;
    private String password;

    private Location loc;
    private Chunk chunk;

    private List<Player> players = new ArrayList<>();
    private Inventory inv = null;

    /**
     * To be used on first Camera setup
     */
    public Camera(Location loc) {
        this.loc = loc;
        this.chunk = loc.getChunk();
        this.uuid = UUID.randomUUID().toString();
        this.name = "Camera_" + (new Random().nextInt(8999) + 1000);
        this.password = "password";

        setupInv();
    }

    public Camera(Location loc, String uuid, String name, String password) {
        this.loc = loc;
        this.chunk = loc.getChunk();
        this.uuid = uuid;
        this.name = name;
        this.password = password;

        setupInv();
    }

    @Override
    public void setupInv() {
        inv = Bukkit.createInventory(null, 18, INV_NAME);

        for (int i = 0; i < 18; i++) {
            inv.setItem(i, CrisisItem.INVENTORY_ITEM.getItemStack(1));
        }

        ItemStack nameItem = new ItemStack(Material.WOOL, 1, (byte) 14);
        ItemMeta im = nameItem.getItemMeta();
        im.setDisplayName(ChatColor.GRAY + "Set camera name");
        im.setLore(Arrays.asList(ChatColor.GRAY + "Current name: " + name));
        nameItem.setItemMeta(im);

        ItemStack passItem = new ItemStack(Material.WOOL, 1, (byte) 15);
        ItemMeta im2 = passItem.getItemMeta();
        im2.setDisplayName(ChatColor.GRAY + "Set camera password");
        passItem.setItemMeta(im2);

        inv.setItem(4, nameItem);
        inv.setItem(13, passItem);
    }

    @Override
    public void openInv(Player player) {
        players.add(player);
        player.openInventory(inv);
    }

    @Override
    public void onInvClick(InventoryClickEvent event) {
        if (event.getCurrentItem() != null) {
            event.setCancelled(true);

            byte data = event.getCurrentItem().getData().getData();
            if (data == 14) { //Name
                event.getWhoClicked().closeInventory();
                new AnvilNameSession(Crisis.getInstance(), this, "name", CrisisItem.ANVIL_NAME_ITEM.getItemStack(name, 1), (Player) event.getWhoClicked(), Crisis.getInstance().getProtocolManager()).openInv();
            } else if (data == 15) { //Password
                event.getWhoClicked().closeInventory();
                new AnvilNameSession(Crisis.getInstance(), this, "password", CrisisItem.ANVIL_NAME_ITEM.getItemStack("password", 1), (Player) event.getWhoClicked(), Crisis.getInstance().getProtocolManager()).openInv();
            }
        }
    }

    @Override
    public void onInvDrag(InventoryDragEvent event) {

    }

    @Override
    public void onInvClose(Player p) {
        players.remove(p);
    }

    @Override
    public void callback(String result) {
        String[] split = result.split(":", 2);
        if (split.length != 2) return;

        if (split[0].equals("name")) {
            name = split[1];
        } else if (split[0].equals("password")) {
            password = split[1];
        }

        ItemStack nameItem = new ItemStack(Material.WOOL, 1, (byte) 14);
        ItemMeta im = nameItem.getItemMeta();
        im.setDisplayName(ChatColor.GRAY + "Set camera name");
        im.setLore(Arrays.asList(ChatColor.GRAY + "Current name: " + name));
        nameItem.setItemMeta(im);

        inv.setItem(4, nameItem);

    }

    public Location getLoc() {
        return loc;
    }

    public void setLoc(Location loc) {
        this.loc = loc;
    }

    public Chunk getChunk() {
        return chunk;
    }

    public void setChunk(Chunk chunk) {
        this.chunk = chunk;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
