package net.crisismc.crisis.blocks;

import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.lib.Callback;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_12_R1.EnumItemSlot;
import net.minecraft.server.v1_12_R1.PacketPlayOutCamera;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityEquipment;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.inventory.AnvilNameSession;
import net.crisismc.crisis.lib.ChatUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.ItemStack;

public class CameraSession implements Listener, Callback {

    private Player p;
    private Camera cam;
    private ArmorStand stand;
    private int taskId;
    private int deltaYaw = 0;
    private boolean dir = true;

    public CameraSession(Player p, Camera cam) {
        this.p = p;
        this.cam = cam;
    }

    public void start() {
        //Default password
        if (cam.getPassword().equals("password")) {
            this.callback("password:password");
        } else {
            //Authenticate
            new AnvilNameSession(Crisis.getInstance(), this, "password", CrisisItem.ANVIL_NAME_ITEM.getItemStack("password", 1), p, Crisis.getInstance().getProtocolManager()).openInv();
            p.sendMessage(ChatUtil.getPrefix() + ChatColor.GRAY + "Enter camera password.");
        }
    }

    @Override
    public void callback(String result) {
        if (!p.getLocation().clone().add(0, -0.1, 0).getBlock().getType().isSolid()) {
            p.sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "You must be on the ground to connect to a camera");
            return;
        }

        String[] split = result.split(":", 2);

        if (split.length == 2 && split[1].equals(cam.getPassword())) {
            p.sendMessage(ChatUtil.getPrefix() + ChatColor.GREEN + "Connected to " + cam.getName() + " successfully.");

            Location standLoc = cam.getLoc().clone().add(0.5, -1.5, 0.5);
            standLoc.setYaw(getRotation(cam.getLoc().getBlock().getData()));

            stand = (ArmorStand) cam.getLoc().getWorld().spawnEntity(standLoc, EntityType.ARMOR_STAND);
            stand.setVisible(false);
            stand.setBasePlate(false);
            stand.setGravity(false);

            PacketPlayOutCamera camera = new PacketPlayOutCamera(((CraftEntity) stand).getHandle());
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(camera);

            PacketPlayOutEntityEquipment packet = new PacketPlayOutEntityEquipment(p.getEntityId(), EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(new ItemStack(Material.PUMPKIN, 1)));
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);

            p.sendBlockChange(cam.getLoc(), Material.AIR, (byte) 0);

            Crisis.getInstance().getServer().getPluginManager().registerEvents(this, Crisis.getInstance());
            task();
        } else {
            p.sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "Incorrect password. Connection aborted.");
        }
    }

    private void task() {
        final Location standLoc = stand.getLocation();
        taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(Crisis.getInstance(), () -> {
            deltaYaw = dir ? deltaYaw + 1 : deltaYaw - 1;
            if (Math.abs(deltaYaw) > 45) dir = !dir;

            Location newLoc = stand.getLocation();
            newLoc.setYaw(standLoc.getYaw() + deltaYaw);
            stand.teleport(newLoc);

            if (!p.getLocation().clone().add(0, -0.1, 0).getBlock().getType().isSolid()) stop();
        }, 0, 1);
    }

    private void stop() {
        PacketPlayOutCamera camera = new PacketPlayOutCamera(((CraftEntity) p).getHandle());
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(camera);

        PacketPlayOutEntityEquipment packet = new PacketPlayOutEntityEquipment(p.getEntityId(), EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(p.getInventory().getHelmet()));
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);

        HandlerList.unregisterAll(this);
        Bukkit.getScheduler().cancelTask(taskId);
        stand.remove();
        p.sendBlockChange(cam.getLoc(), cam.getLoc().getBlock().getType(), cam.getLoc().getBlock().getData());
    }

    private float getRotation(int direction) {
        return Math.round((Math.round(direction * 90F) | 0x3) / 90) * 90;
    }


    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getPlayer().equals(p)) {
            stop();
        }
    }

    @EventHandler
    public void onPlayerToggleShift(PlayerToggleSneakEvent event) {
        if (event.getPlayer().equals(p)) {
            stop();
        }
    }

    @EventHandler
    public void onInventoryOpen(InventoryOpenEvent event) {
        if (event.getPlayer().equals(p)) {
            stop();
        }
    }


}
