package net.crisismc.crisis.blocks;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;

public interface InventoryBlock {

    void setupInv();

    void openInv(Player player);

    void onInvClick(InventoryClickEvent event);

    void onInvDrag(InventoryDragEvent event);

    void onInvClose(Player p);
}
