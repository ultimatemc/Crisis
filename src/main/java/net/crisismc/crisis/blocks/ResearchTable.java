package net.crisismc.crisis.blocks;

import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.lib.DateUtil;
import net.crisismc.crisis.crafting.CrisisRecipe;
import net.crisismc.crisis.lib.ChatUtil;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ResearchTable implements InventoryBlock {

    public static final String INV_NAME = ChatColor.DARK_GRAY + "Research Table";
    private String uuid;
    private ItemStack item;
    private int paperAmount;
    private long endTime;
    private boolean researching;

    private Location loc;
    private Chunk chunk;

    private List<Player> players = new ArrayList<>();
    private Inventory inv = null;

    /**
     * To be used on first ResearchTable setup
     */
    public ResearchTable(Location loc) {
        this.setUuid(UUID.randomUUID().toString());
        this.setItem(null);
        this.setPaperAmount(0);
        this.setEndTime(0);
        this.setResearching(false);

        this.setLoc(loc);
        this.setChunk(loc.getChunk());

        setupInv();
    }

    public ResearchTable(String uuid, ItemStack item, int paperAmount, boolean researching, long endTime, Location loc, Chunk chunk) {
        this.setUuid(uuid);
        this.setItem(item);
        this.setPaperAmount(paperAmount);
        this.setEndTime(endTime);
        this.setResearching(researching);

        this.setLoc(loc);
        this.setChunk(chunk);

        setupInv();
    }

    private void sendMsg(String msg) {
        for (Player p : players) p.sendMessage(msg);
    }

    @Override
    public void setupInv() {
        inv = Bukkit.createInventory(null, 27, INV_NAME);

        for (int i = 0; i < 27; i++) {
            if (i == 3 || i == 5 || i == 13 || i == 22) continue;
            inv.setItem(i, CrisisItem.INVENTORY_ITEM.getItemStack(1));
        }
        if (item != null) {
            inv.setItem(3, item);
        }

        if (researching && endTime > System.currentTimeMillis()) {

            ItemStack arrow = new ItemStack(Material.ITEM_FRAME, 1);
            ItemMeta im = arrow.getItemMeta();
            im.setDisplayName(ChatColor.GRAY + "Time Remaining: " + ChatColor.GOLD + DateUtil.getTimeFromMillis(endTime - System.currentTimeMillis()));
            arrow.setItemMeta(im);

            inv.setItem(13, arrow);
        } else {
            ItemStack arrow = new ItemStack(Material.ITEM_FRAME, 1);
            ItemMeta im = arrow.getItemMeta();
            im.setDisplayName(ChatColor.GRAY + "Time Remaining: " + ChatColor.GOLD + DateUtil.getTimeFromMillis(endTime - System.currentTimeMillis()));
            arrow.setItemMeta(im);

            inv.setItem(13, arrow);
        }

        if (paperAmount > 0) {
            inv.setItem(5, CrisisItem.RESEARCH_PAPER.getItemStack(paperAmount));
        }

    }

    @Override
    public void openInv(Player p) {
        players.add(p);

        if (item != null) {
            inv.setItem(3, item);
        }

        if (researching && endTime > System.currentTimeMillis()) {
            ItemStack arrow = new ItemStack(Material.ITEM_FRAME, 1);
            ItemMeta im = arrow.getItemMeta();
            im.setDisplayName(ChatColor.GRAY + "Time Remaining: " + ChatColor.GOLD + DateUtil.getTimeFromMillis(endTime - System.currentTimeMillis()));
            arrow.setItemMeta(im);

            inv.setItem(13, arrow);
        } else {
            ItemStack arrow = new ItemStack(Material.ITEM_FRAME, 1);
            ItemMeta im = arrow.getItemMeta();
            im.setDisplayName(ChatColor.GRAY + "");
            arrow.setItemMeta(im);
            inv.setItem(13, arrow);
        }

        if (paperAmount > 0) {
            inv.setItem(5, CrisisItem.RESEARCH_PAPER.getItemStack(paperAmount));
        }


        p.openInventory(inv);
    }

    public void tick() {
        paperAmount = inv.getItem(5) == null ? 0 : inv.getItem(5).getAmount();
        item = inv.getItem(3);
        if (!researching && inv.getItem(3) != null && CrisisRecipe.isResearchable(inv.getItem(3)) && paperAmount > 0) { //Begin research
            CrisisRecipe cr = CrisisRecipe.getRecipe(item);
            researching = true;
            endTime = System.currentTimeMillis() + cr.getResearchTime();
            if (inv.getItem(3).hasItemMeta())
                sendMsg(ChatUtil.getPrefix() + ChatColor.GRAY + "Began researching " + ChatColor.stripColor(inv.getItem(3).getItemMeta().getDisplayName().split("<")[0]));
        }

        if (researching) {
            ItemStack arrow = new ItemStack(Material.ITEM_FRAME, 1);
            ItemMeta im = arrow.getItemMeta();
            im.setDisplayName(ChatColor.GRAY + "Time Remaining: " + ChatColor.GOLD + DateUtil.getTimeFromMillis(endTime - System.currentTimeMillis()));
            arrow.setItemMeta(im);

            inv.setItem(13, arrow);

            if (paperAmount < 1 || inv.getItem(3) == null) {
                researching = false;
                sendMsg(ChatUtil.getPrefix() + ChatColor.RED + "Researching cancelled");
                im.setDisplayName(ChatColor.GRAY + "");
                arrow.setItemMeta(im);
                inv.setItem(13, arrow);
            }
        }

        if (researching && endTime <= System.currentTimeMillis()) { //On Finish researching
            finishResearch();
        }
    }


    private void finishResearch() {
        System.out.println("Research finished");
        paperAmount--;
        if (paperAmount == 0) {
            inv.setItem(5, null);
        } else {
            inv.setItem(5, CrisisItem.RESEARCH_PAPER.getItemStack(paperAmount));
        }
        researching = false;

        inv.setItem(22, CrisisItem.BLUEPRINT.getItemStack(inv.getItem(3)));

        item = null;
        inv.setItem(3, null);

        ItemStack arrow = new ItemStack(Material.ITEM_FRAME, 1);
        ItemMeta im = arrow.getItemMeta();
        im.setDisplayName(ChatColor.GRAY + "");
        arrow.setItemMeta(im);
        inv.setItem(13, arrow);
    }

    @Override
    public void onInvClick(InventoryClickEvent event) {
        //if(event.getSlot() == 22 && event.getCursor() != null) event.setCancelled(true);
        //if(event.getSlot() == 3 && CrisisRecipe.isResearchable(event.getCursor())) event.setCancelled(true);
        //if(event.getSlot() == 5 && event.getCursor() != null && event.getCursor().getType() == Items.getResearchPaper(1).getType()) event.setCancelled(true);
    }


    @Override
    public void onInvDrag(InventoryDragEvent event) {
        if (event.getRawSlots().contains(22)) {
            event.setCancelled(true);
            ((Player) event.getWhoClicked()).updateInventory();
        }
        if (event.getNewItems().containsKey(3) && !CrisisRecipe.isResearchable(event.getNewItems().get(3))) {
            event.setCancelled(true);
            ((Player) event.getWhoClicked()).updateInventory();
        }
        if (event.getNewItems().containsKey(5) && event.getNewItems().get(5).getType() != CrisisItem.RESEARCH_PAPER.getMaterial()) {
            event.setCancelled(true);
            ((Player) event.getWhoClicked()).updateInventory();
        }
    }

    @Override
    public void onInvClose(Player p) {
        players.remove(p);

    }

    public ItemStack getItem() {
        return item;
    }

    public void setItem(ItemStack item) {
        this.item = item;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public boolean isResearching() {
        return researching;
    }

    public void setResearching(boolean researching) {
        this.researching = researching;
    }

    public Location getLoc() {
        return loc;
    }

    public void setLoc(Location loc) {
        this.loc = loc;
    }

    public Chunk getChunk() {
        return chunk;
    }

    public void setChunk(Chunk chunk) {
        this.chunk = chunk;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getPaperAmount() {
        return paperAmount;
    }

    public void setPaperAmount(int paperAmount) {
        this.paperAmount = paperAmount;
    }

    public List<Player> getPlayers() {
        return players;
    }


}
