package net.crisismc.crisis.building;

import net.crisismc.crisis.Crisis;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.util.Vector;

import java.util.HashMap;

public class BlockDurability {

    private HashMap<Material, BlockData> blockDurability = new HashMap<Material, BlockData>();
    private HashMap<Vector, Integer> damagedBlocks = new HashMap<Vector, Integer>();

    private Crisis plugin;

    public BlockDurability(Crisis plugin) {
        this.plugin = plugin;
        setupBlocks();
    }

    public void registerEvents() {
        new BDListener(this, plugin);
    }

    private void setupBlocks() {
        blockDurability.put(Material.JUNGLE_DOOR, new BlockData(1000, (byte) -1)); // 5m with diamond
        blockDurability.put(Material.WEB, new BlockData(10, (byte) 0));
        //blockDurability.put(Material.GRAVEL, new BlockData(666, (byte) 0)); // 10m
        blockDurability.put(Material.ACACIA_FENCE, new BlockData(600, (byte) 0)); // 2.5m with diamond
        blockDurability.put(Material.SAND, new BlockData(600, (byte) 1));
    }

    public boolean isDurabilityBlock(Block block) {
        return blockDurability.containsKey(block.getType()) && blockDurability.get(block.getType()).getData() == block.getData();
    }

    public int getDurability(Block block) {
        if (damagedBlocks.containsKey(block.getLocation().toVector())) {
            return damagedBlocks.get(block.getLocation().toVector());
        } else {
            return blockDurability.get(block.getType()).getDurability();
        }
    }

    public int getMaxDurability(Block block) {
        return blockDurability.get(block.getType()).getDurability();
    }

    public void setDurability(Block block, int durability) {
        if (damagedBlocks.containsKey(block.getLocation().toVector())) {
            damagedBlocks.replace(block.getLocation().toVector(), durability);
        } else {
            damagedBlocks.put(block.getLocation().toVector(), durability);
        }
    }

    public void removeBlock(Block block) {
        if (damagedBlocks.containsKey(block.getLocation().toVector())) {
            damagedBlocks.remove(block.getLocation().toVector());
        }
    }
}

class BlockData {

    private int durability;
    private byte data;

    public BlockData(int durability, byte data) {
        this.durability = durability;
        this.data = data;
    }

    public int getDurability() {
        return durability;
    }

    public void setDurability(int durability) {
        this.durability = durability;
    }

    public byte getData() {
        return data;
    }

    public void setData(byte data) {
        this.data = data;
    }
}

class BDListener implements Listener {

    private BlockDurability bd;

    public BDListener(BlockDurability bd, Crisis plugin) {
        this.bd = bd;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockPlace(BlockPlaceEvent event) {
        if (bd.isDurabilityBlock(event.getBlock())) event.setCancelled(false); //TODO advanced placement logic
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.getBlock().getWorld().getName().equals(Crisis.WORLD_NAME) && event.getPlayer().getGameMode() != GameMode.CREATIVE) {
            Block b = event.getBlock();
            if (bd.isDurabilityBlock(b)) {
                bd.setDurability(b, bd.getDurability(b) - 1);
                //TTA_Methods.sendActionBar(event.getPlayer(), bd.getDurability(b) + "/" + bd.getMaxDurability(b));
                event.setCancelled(bd.getDurability(b) > 0);
            }
        }
    }
}
