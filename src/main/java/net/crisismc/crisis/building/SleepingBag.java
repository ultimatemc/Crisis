package net.crisismc.crisis.building;

import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.player.CrisisPlayer;
import net.crisismc.crisis.lib.ChatUtil;
import net.crisismc.crisis.lib.DirectionUtil;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class SleepingBag {

    public Location loc1;
    public Location loc2;

    public SleepingBag(Location loc1, Location loc2) {
        this.loc1 = loc1;
        this.loc2 = loc2;
    }

    public boolean isSleepingBag(Block b) {
        return b.getLocation().equals(loc1) || b.getLocation().equals(loc2);
    }

    public static void placeBag(Player p) {
        p.getInventory().setItemInMainHand(null);

        CrisisPlayer cp = CrisisPlayer.crisisPlayers.get(p.getUniqueId());

        if (cp.hasBed()) {
            cp.getBed().loc1.getBlock().setType(Material.AIR);
            cp.getBed().loc2.getBlock().setType(Material.AIR);
        }


        cp.setHasBed(true);

        Location loc1 = p.getLocation().getBlock().getLocation();
        Location loc2 = loc1.getBlock().getRelative(DirectionUtil.yawToFace(p.getLocation().getYaw()).getOppositeFace()).getLocation();

        cp.setBed(new SleepingBag(loc1, loc2));

        Block b1 = loc1.getBlock();
        Block b2 = loc2.getBlock();

        b1.setType(Material.CARPET);
        b2.setType(Material.CARPET);

        //TODO vip ability to customize carpet color
        b1.setData((byte) 13);
    }

    public static boolean canPlace(Player p) {
        Location loc1 = p.getLocation().getBlock().getLocation();
        Location ground1 = p.getLocation().add(0, -1, 0);
        Location loc2 = loc1.getBlock().getRelative(DirectionUtil.yawToFace(p.getLocation().getYaw()).getOppositeFace()).getLocation();
        Location ground2 = ground1.getBlock().getRelative(DirectionUtil.yawToFace(p.getLocation().getYaw()).getOppositeFace()).getLocation();

        if (loc1.getBlock().getType() != Material.AIR
                || loc2.getBlock().getType() != Material.AIR) return false;
        return ground1.getBlock().getType().isSolid() && ground1.getBlock().getType().isOccluding()
                && ground2.getBlock().getType().isSolid() && ground2.getBlock().getType().isOccluding();
    }

    public static void onCarpetBreak(Player p, Block b) {
        for (CrisisPlayer cp : CrisisPlayer.crisisPlayers.values()) {
            if (cp.hasBed()) {
                SleepingBag bag = cp.getBed();
                if (bag.isSleepingBag(b)) {
                    bag.loc1.getBlock().setType(Material.AIR);
                    bag.loc2.getBlock().setType(Material.AIR);
                    cp.setHasBed(false);

                    p.getWorld().dropItemNaturally(bag.loc1.add(0.5, 0.5, 0.5), CrisisItem.SLEEPING_BAG.getItemStack(1));
                    p.sendMessage(ChatUtil.getPrefix() + ChatColor.GRAY + "You broke " + ChatColor.YELLOW + cp.p.getName() + "'s " + ChatColor.GRAY + "sleeping bag.");
                    return;
                }
            }
        }
        CrisisPlayer.cpLoader.handleCarpetClick(p, b);

    }
}
