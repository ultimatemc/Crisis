package net.crisismc.crisis.building;

import org.bukkit.Location;

public class Town {

    private int x1;
    private int x2;
    private int z1;
    private int z2;

    public Town(int x1, int z1, int x2, int z2) {
        this.x1 = x1;
        this.z1 = z1;
        this.x2 = x2;
        this.z2 = z2;
    }

    public boolean isInside(Location loc) {
        return loc.getX() < Math.max(x1, x2) && loc.getX() > Math.min(x1, x2)
                && loc.getZ() < Math.max(z1, z2) && loc.getZ() > Math.min(z1, z2);

    }

}

