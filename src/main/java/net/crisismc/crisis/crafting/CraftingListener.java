package net.crisismc.crisis.crafting;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.player.CrisisPlayer;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class CraftingListener implements Listener {

    private Crisis plugin;

    public CraftingListener(Crisis plugin) {
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (p.getWorld().equals(plugin.ffa.getWorld())) return;

        ItemStack testItem = e.getItem();
        if (testItem != null)
            if (!testItem.isSimilar(p.getInventory().getItemInMainHand())) return; //Fix your shit Spigot!

        if (e.getAction().name().equals("RIGHT_CLICK_BLOCK")) {
            if (p.getInventory().getItemInMainHand().getType() == Material.AIR && p.isSneaking()) {
                List<ItemStack> nearbyItemStacks = new ArrayList<>();
                for (Item item : getNearbyItems(e.getClickedBlock().getLocation())) {
                    nearbyItemStacks.add(item.getItemStack());
                }
                p.openInventory(Recipes.getCraftingInv(nearbyItemStacks));
            }
        }
    }//End of listener

    public List<ItemStack> getNearbyItemStacks(Location loc) {
        List<ItemStack> nearbyItemStacks = new ArrayList<ItemStack>();
        for (Entity ent : getNearbyItems(loc)) {
            if (ent instanceof Item) {
                Item item = (Item) ent;
                nearbyItemStacks.add(item.getItemStack());
            }
        }
        return nearbyItemStacks;
    }


    public List<Item> getNearbyItems(Location loc) {
        List<Item> items = new ArrayList<Item>();
        for (Entity ent : loc.getWorld().getEntities()) {
            if (ent instanceof Item) {
                Item item = (Item) ent;
                if (item.getLocation().distanceSquared(loc) < 2.5) {
                    items.add(item);
                }
            }
        }
        return items;
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (ChatColor.stripColor(event.getInventory().getName()).equalsIgnoreCase("crafting") && event.getCurrentItem() != null && event.getInventory().getType() == InventoryType.CHEST) {
            event.setCancelled(true);
            Player player = (Player) event.getWhoClicked();
            ItemStack item = event.getCurrentItem();

            if (item == null) return;
            if (item.getType() == Material.AIR) return;


            List<Item> nearbyItems = getNearbyItems(player.getTargetBlock((HashSet<Byte>) null, 7).getLocation());

            String craftStr = Recipes.canCraft(CrisisPlayer.getCrisisPlayer(player), Recipes.getRecipe(item), getNearbyItemStacks(player.getTargetBlock((HashSet<Byte>) null, 7).getLocation()), nearbyItems.get(0).getLocation());
            if (craftStr == null) {

                Recipes.removeItems(Recipes.getRecipe(item), nearbyItems);
                Recipes.craftItem(item, nearbyItems.get(0).getLocation(), player);

            } else {
                player.sendMessage(ChatColor.RED + craftStr);
                player.closeInventory();
            }
        } else if (ChatColor.stripColor(event.getInventory().getName()).equalsIgnoreCase("crafting recipes") && event.getCurrentItem() != null && event.getInventory().getType() == InventoryType.CHEST)
            event.setCancelled(true);
    }
}
