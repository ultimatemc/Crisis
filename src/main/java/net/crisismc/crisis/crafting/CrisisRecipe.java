package net.crisismc.crisis.crafting;

import net.crisismc.crisis.CrisisItem;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;


public enum CrisisRecipe {

    //Materials
    LOG(CrisisItem.LOG.getItemStack(1), Arrays.asList(
            CrisisItem.STICK.getItemStack(5)), CraftingCatagory.MATERIALS, true, 0),
    STICK(CrisisItem.STICK.getItemStack(5), Arrays.asList(
            CrisisItem.LOG.getItemStack(1)), CraftingCatagory.MATERIALS, true, 0),

    //Armor
    BURLAP_SHIRT(CrisisItem.SHIRT_BURLAP.getItemStack(1), Arrays.asList(
            CrisisItem.CLOTH.getItemStack(5)), CraftingCatagory.ARMOR, true, 0),
    BURLAP_TROUSERS(CrisisItem.TROUSERS_BURLAP.getItemStack(1), Arrays.asList(
            CrisisItem.CLOTH.getItemStack(5)), CraftingCatagory.ARMOR, true, 0),
    BURLAP_SHOES(CrisisItem.SHOES_BURLAP.getItemStack(1), Arrays.asList(
            CrisisItem.CLOTH.getItemStack(4)), CraftingCatagory.ARMOR, true, 0),

    //Weapons
    MOLOTOV(CrisisItem.GRENADE_MOLOTOV.getItemStack(1), Arrays.asList(
            CrisisItem.GLASS_ROUGH.getItemStack(5),
            CrisisItem.FUEL_LOW_GRADE.getItemStack(10),
            CrisisItem.CLOTH.getItemStack(5)), CraftingCatagory.WEAPONS, true, 0),
    BONE_KNIFE(CrisisItem.KNIFE_BONE.getItemStack(1), Arrays.asList(
            CrisisItem.BONE_FRAGMENTS.getItemStack(5),
            CrisisItem.CLOTH.getItemStack(2)), CraftingCatagory.WEAPONS, true, 0),

    HATCHET_STONE(CrisisItem.HATCHET_STONE.getItemStack(1), Arrays.asList(
            CrisisItem.BONE_FRAGMENTS.getItemStack(5),
            CrisisItem.CLOTH.getItemStack(2)), CraftingCatagory.WEAPONS, true, 0),
    HATCHET(CrisisItem.HATCHET.getItemStack(1), Arrays.asList(
            CrisisItem.STICK.getItemStack(50),
            CrisisItem.METAL_FRAG.getItemStack(20)), CraftingCatagory.WEAPONS, true, 0),
    SALVAGED_AXE(CrisisItem.AXE_REINFORCED.getItemStack(1), Arrays.asList(
            CrisisItem.STICK.getItemStack(50),
            CrisisItem.STONE.getItemStack(100),
            CrisisItem.METAL_FRAG.getItemStack(50)), CraftingCatagory.WEAPONS, false, 10800000),
    PICKAXE(CrisisItem.PICKAXE.getItemStack(1), Arrays.asList(
            CrisisItem.STICK.getItemStack(50),
            CrisisItem.STONE.getItemStack(20)), CraftingCatagory.WEAPONS, true, 0),
    ICEPICK(CrisisItem.PICK_ICE.getItemStack(1), Arrays.asList(CrisisItem.STICK.getItemStack(50), CrisisItem.STONE.getItemStack(100), CrisisItem.METAL_FRAG.getItemStack(50)), CraftingCatagory.WEAPONS, false, 10800000),

    //Fuel
    FUEL_LOW_GRADE(CrisisItem.FUEL_LOW_GRADE.getItemStack(1), Arrays.asList(
            CrisisItem.CLOTH.getItemStack(5),
            CrisisItem.ANIMAL_FAT.getItemStack(5)), CraftingCatagory.FUEL, true, 0),

    //Medical
    BANDAGE(CrisisItem.BANDAGE.getItemStack(1), Arrays.asList(
            CrisisItem.CLOTH.getItemStack(3)), CraftingCatagory.MEDICAL, true, 0),
    SPLINT(CrisisItem.SPLINT.getItemStack(1), Arrays.asList(
            CrisisItem.CLOTH.getItemStack(5),
            CrisisItem.STICK.getItemStack(10)), CraftingCatagory.MEDICAL, true, 0),

    //Guns
    SHOTGUN_WATERPIPE(CrisisItem.SHOTGUN_WATERPIPE.getItemStack(1), Arrays.asList(
            CrisisItem.METAL_FRAG.getItemStack(50),
            CrisisItem.STICK.getItemStack(20),
            CrisisItem.CLOTH.getItemStack(15)), CraftingCatagory.GUNS, false, 10000), //10800000
    RIFLE_HUNTING(CrisisItem.RIFLE_BOLT.getItemStack(1), Arrays.asList(
            CrisisItem.METAL_FRAG.getItemStack(150),
            CrisisItem.STICK.getItemStack(200),
            CrisisItem.CLOTH.getItemStack(50)), CraftingCatagory.GUNS, false, 10000), //10800000

    //Ammo
    AMMO_SHOTGUN(CrisisItem.AMMO_SHOTGUN_WATERPIPE.getItemStack(1), Arrays.asList(
            CrisisItem.METAL_FRAG.getItemStack(2),
            CrisisItem.GUNPOWDER.getItemStack(1)), CraftingCatagory.AMMO, false, 3600000),
    AMMO_RIFLE(CrisisItem.AMMO_RIFLE_BOLT.getItemStack(1), Arrays.asList(
            CrisisItem.METAL_FRAG.getItemStack(1),
            CrisisItem.GUNPOWDER.getItemStack(2)), CraftingCatagory.AMMO, false, 3600000),

    //Building
    HAMMER(CrisisItem.HAMMER.getItemStack(1), Arrays.asList(
            CrisisItem.METAL_FRAG.getItemStack(5),
            CrisisItem.STICK.getItemStack(10)), CraftingCatagory.BUILDING, true, 0),
    SANDBAG(CrisisItem.SANDBAG.getItemStack(1), Arrays.asList(
            CrisisItem.SAND.getItemStack(25)), CraftingCatagory.BUILDING, true, 0),
    SLEEPING_BAG(CrisisItem.SLEEPING_BAG.getItemStack(1), Arrays.asList(
            CrisisItem.CLOTH.getItemStack(10)), CraftingCatagory.BUILDING, true, 0),
    STORAGE_BOX(CrisisItem.STORAGE_BOX.getItemStack(1), Arrays.asList(
            CrisisItem.LOG.getItemStack(10)), CraftingCatagory.BUILDING, true, 0),
    CAMPFIRE(CrisisItem.CAMPFIRE.getItemStack(1), Arrays.asList(
            CrisisItem.LOG.getItemStack(5),
            CrisisItem.CLOTH.getItemStack(1)), CraftingCatagory.BUILDING, true, 0),
    BARRICADE(CrisisItem.BARRICADE.getItemStack(1), Arrays.asList(
            CrisisItem.STICK.getItemStack(25)), CraftingCatagory.BUILDING, true, 0),
    DOOR_MAKESHIFT(CrisisItem.DOOR_MAKESHIFT.getItemStack(1), Arrays.asList(
            CrisisItem.STICK.getItemStack(50),
            CrisisItem.METAL_FRAG.getItemStack(5)), CraftingCatagory.BUILDING, true, 0),
    LANTERN(CrisisItem.LANTERN.getItemStack(1), Arrays.asList(
            CrisisItem.METAL_FRAG.getItemStack(5),
            CrisisItem.GLASS_ROUGH.getItemStack(5),
            CrisisItem.STICK.getItemStack(1),
            CrisisItem.FUEL_LOW_GRADE.getItemStack(3)), CraftingCatagory.BUILDING, true, 0),
    FURNACE(CrisisItem.FURNACE.getItemStack(1), Arrays.asList(
            CrisisItem.STONE.getItemStack(25),
            CrisisItem.FUEL_LOW_GRADE.getItemStack(5)), CraftingCatagory.BUILDING, true, 0),

    //Equipment
    CANTEEN(CrisisItem.CANTEEN.getItemStack(1), Arrays.asList(
            CrisisItem.METAL_FRAG.getItemStack(5)), CraftingCatagory.EQUIPMENT, true, 0),

    //Explosives
    EXPLOSIVES(CrisisItem.EXPLOSIVES.getItemStack(1), Arrays.asList(
            CrisisItem.GUNPOWDER.getItemStack(50),
            CrisisItem.SAND.getItemStack(50)), CraftingCatagory.EXPLOSIVES, false, 43200000),
    C4(CrisisItem.C4_EXPLOSIVE.getItemStack(1), Arrays.asList(
            CrisisItem.C4_CONTROLLER.getItemStack(1),
            CrisisItem.EXPLOSIVES.getItemStack(6),
            CrisisItem.CLOTH.getItemStack(50)), CraftingCatagory.EXPLOSIVES, true, 0),

    //Misc
    RAFT(CrisisItem.RAFT.getItemStack(1), Arrays.asList(
            CrisisItem.LOG.getItemStack(25)), CraftingCatagory.MISC, true, 0),

    //TEST
    TEST(new ItemStack(Material.STONE, 1), Arrays.asList(new ItemStack(Material.STONE, 1)), CraftingCatagory.MISC, false, 30000);

    private final ItemStack result;
    private final List<ItemStack> materials;
    private final CraftingCatagory catagory;
    private final boolean knownByDefault;
    private final long researchTime;


    CrisisRecipe(ItemStack result, List<ItemStack> materials, CraftingCatagory catagory, boolean knownByDefault, long researchTime) {
        this.result = result;
        this.materials = materials;
        this.catagory = catagory;
        this.knownByDefault = knownByDefault;
        this.researchTime = researchTime;
    }

    public ItemStack getResult() {
        return result;
    }

    public List<ItemStack> getMaterials() {
        return materials;
    }

    public CraftingCatagory getCatagory() {
        return catagory;
    }

    public boolean isKnownByDefault() {
        return knownByDefault;
    }

    public long getResearchTime() {
        return researchTime;
    }

    public static CrisisRecipe getRecipe(ItemStack item) {
        for (CrisisRecipe cr : CrisisRecipe.values()) {
            if (cr.getResult().getType() == item.getType() && cr.getResult().getDurability() == item.getDurability()) {
                return cr;
            }
        }
        return null;
    }

    public static CrisisRecipe getRecipe(String name) {
        for (CrisisRecipe cr : CrisisRecipe.values()) {
            if (!cr.getResult().hasItemMeta()) continue;
            if (ChatColor.stripColor(cr.getResult().getItemMeta().getDisplayName().replaceAll(" ", "")).startsWith(ChatColor.stripColor(name.replaceAll(" ", ""))))
                return cr;
        }
        return null;
    }

    public static boolean isResearchable(ItemStack item) {
        if (item == null) return false;
        CrisisRecipe cr = getRecipe(item);
        if (cr == null) return false;
        return !cr.isKnownByDefault();
    }

}

enum CraftingCatagory {
    MATERIALS, ARMOR, WEAPONS, FUEL, MEDICAL, GUNS, AMMO, BUILDING, EQUIPMENT, EXPLOSIVES, MISC
}

		