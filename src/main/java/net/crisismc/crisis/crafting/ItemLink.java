package net.crisismc.crisis.crafting;

import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

public class ItemLink {

    private ItemStack itemstack;
    private Item item;

    public ItemLink(ItemStack itemstack, Item item) {
        this.itemstack = itemstack;
        this.item = item;
    }

    public ItemStack getItemStack() {
        return itemstack;
    }

    public void setItemStack(ItemStack itemstack) {
        this.itemstack = itemstack;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
