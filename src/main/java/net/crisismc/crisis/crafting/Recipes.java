package net.crisismc.crisis.crafting;

import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.player.CrisisPlayer;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Recipes {

    public static String canCraft(CrisisPlayer cp, CrisisRecipe recipe, List<ItemStack> materials, Location loc) {
        if (!cp.getKnownRecipes().contains(recipe) && !recipe.isKnownByDefault())
            return "You do not know how to craft that recipe";
        //CAMPFIRE
        if (recipe.getResult().getType() == Material.PUMPKIN_SEEDS) {
            Block base = loc.add(0, -1, 0).getBlock();
            if (base.getType() != Material.GRASS && base.getType() != Material.DIRT) {
                return "You must build a campfire on grass or dirt.";
            }
        }

        HashMap<ItemStack, Integer> items = new HashMap<ItemStack, Integer>();
        for (ItemStack item : materials) {//Loop though items on ground
            for (ItemStack craftingItem : recipe.getMaterials()) { //Loop thought crafting items on item
                if (item.isSimilar(craftingItem)) {//Item on ground is part of the recipe
                    if (items.containsKey(createSimilar(item))) {//If items contains add to the count
                        items.put(createSimilar(item), items.get(createSimilar(item)) + item.getAmount());
                    } else {//If not create new entry
                        items.put(createSimilar(item), item.getAmount());
                    }
                }
            }
        }//End of creating hashmap

        for (ItemStack craftingItem : recipe.getMaterials()) {
            if (!items.containsKey(createSimilar(craftingItem)))
                return "You do not have the required materials to craft this!";
            int amount = items.get(createSimilar(craftingItem));
            int neededAmount = craftingItem.getAmount();
            if (amount < neededAmount) return "You do not have the required materials to craft this!";
        }// This loop returns false if any recipe item does not exist or is less than needed.

        return null;

    }

    public static boolean canCraft(CrisisRecipe recipe, List<ItemStack> materials) {

        HashMap<ItemStack, Integer> items = new HashMap<ItemStack, Integer>();
        for (ItemStack item : materials) {//Loop though items on ground
            for (ItemStack craftingItem : recipe.getMaterials()) { //Loop thought crafting items on item
                if (item.isSimilar(craftingItem)) {//Item on ground is part of the recipe
                    if (items.containsKey(createSimilar(item))) {//If items contains add to the count
                        items.put(createSimilar(item), items.get(createSimilar(item)) + item.getAmount());
                    } else {//If not create new entry
                        items.put(createSimilar(item), item.getAmount());
                    }
                }
            }
        }//End of creating hashmap

        for (ItemStack craftingItem : recipe.getMaterials()) {
            if (!items.containsKey(createSimilar(craftingItem))) return false;
            int amount = items.get(createSimilar(craftingItem));
            int neededAmount = craftingItem.getAmount();
            if (amount < neededAmount) return false;
        }// This loop returns false if any recipe item does not exist or is less than needed.

        return true;

    }

    private static ItemStack createSimilar(ItemStack item) {
        ItemStack similar = new ItemStack(item.getType(), 1, item.getDurability());
        similar.setData(item.getData());

        return similar;
    }

    public static Inventory getCraftingInv(List<ItemStack> items) {
        Inventory inv = Bukkit.createInventory(null, 54, ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Crafting");
        int slot = 0;
        for (CrisisRecipe recipe : CrisisRecipe.values()) {
            if (canCraft(recipe, items)) {
                inv.setItem(slot, addMaterialText(recipe.getResult(), recipe));
                slot++;
            }
        }

        return inv;
    }

    private static ItemStack addMaterialText(ItemStack item, CrisisRecipe recipe) {
        ItemMeta itemMeta = item.getItemMeta();
        List<String> lore = new ArrayList<>();
        for (ItemStack mat : recipe.getMaterials()) {
            lore.add(ChatColor.WHITE + ChatColor.stripColor(mat.getItemMeta().getDisplayName()) + " - " + mat.getAmount());
        }
        itemMeta.setLore(lore);
        item.setItemMeta(itemMeta);
        return item;

    }

    public static void craftItem(ItemStack item, Location loc, Player crafter) {
        if (item.getType() == Material.PUMPKIN_SEEDS) {
            Location base = loc.add(0, -1, 0);
            base.getBlock().setType(Material.NETHERRACK);
            base.add(0, 1, 0).getBlock().setType(Material.FIRE);
            return;
        }
        if (item.getType() == Material.JUNGLE_DOOR_ITEM) {
            loc.getWorld().dropItem(loc, item);
            for (Player p : Bukkit.getOnlinePlayers()) {
                if (loc.distance(p.getLocation()) <= 32) {
                    p.playSound(loc, Sound.BLOCK_ANVIL_LAND, 2, 0.5F);
                }
            }
            return;
        }
        item = CrisisItem.getCrisisItem(item.getType(), (short) item.getData().getData()).getItemStack(1);
        loc.getWorld().dropItem(loc, item);
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (loc.distance(p.getLocation()) <= 32) {
                p.playSound(loc, Sound.BLOCK_ANVIL_LAND, 2, 0.5F);
            }
        }
    }

    public static void removeItems(CrisisRecipe recipe, List<Item> items) {
        List<ItemLink> itemLinks = new ArrayList<ItemLink>();
        for (Item item : items) {
            itemLinks.add(new ItemLink(item.getItemStack(), item));
        }//End of setting up item links

        List<ItemStack> craftingItems = new ArrayList<ItemStack>();
        for (ItemStack item : recipe.getMaterials()) { //Clone list
            craftingItems.add(item.clone());
        }//End of cloning loop

        for (ItemLink itemLink : itemLinks) {
            int amount = itemLink.getItemStack().getAmount();
            if (containsSimilar(craftingItems, itemLink.getItemStack())) {
                ItemStack newItemStack = itemLink.getItemStack();
                newItemStack.setAmount(getAmountofSimilar(craftingItems, itemLink.getItemStack().clone()));
                int index = craftingItems.indexOf(newItemStack);
                int neededAmount = craftingItems.get(index).getAmount();

                if (amount <= neededAmount) { //Remove entire item
                    itemLink.getItem().remove();

                    ItemStack newItem = craftingItems.get(index);
                    newItem.setAmount(newItem.getAmount() - amount);
                    craftingItems.set(index, newItem);

                } else {
                    itemLink.getItem().remove();
                    ItemStack newItem = itemLink.getItemStack();
                    newItem.setAmount(amount - neededAmount);
                    Item droppedItem = itemLink.getItem().getLocation().getWorld().dropItem(itemLink.getItem().getLocation(), newItem);
                    droppedItem.setVelocity(new Vector(0, 0, 0)); //Prevent items from flying everywhere
                    craftingItems.remove(index);
                }
            }
        }
    }

    private static boolean containsSimilar(List<ItemStack> list, ItemStack itemstack) {
        ItemStack item = new ItemStack(itemstack.getType(), itemstack.getAmount(), itemstack.getDurability());
        item.setItemMeta(itemstack.getItemMeta());
        for (int i = 1; i <= 65; i++) {
            item.setAmount(i);
            if (list.contains(item)) return true;
        }
        return false;
    }

    private static int getAmountofSimilar(List<ItemStack> list, ItemStack itemstack) {
        ItemStack item = new ItemStack(itemstack.getType(), itemstack.getAmount(), itemstack.getDurability());
        item.setItemMeta(itemstack.getItemMeta());
        for (int i = 1; i <= 65; i++) {
            item.setAmount(i);
            if (list.contains(item)) return i;
        }
        return 0;
    }

    public static CrisisRecipe getRecipe(ItemStack result) {
        for (CrisisRecipe recipe : CrisisRecipe.values()) {
            if (recipe.getResult().equals(result)) return recipe;
        }
        return null;
    }

}