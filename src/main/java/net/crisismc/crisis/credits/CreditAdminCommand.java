package net.crisismc.crisis.credits;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CreditAdminCommand implements CommandExecutor {

    public CreditAdminCommand() {

    }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 3) {
            if (args[0].equalsIgnoreCase("add")) {
                CrisisCreditAPI.addCrisisCredits(Bukkit.getOfflinePlayer(args[1]), Integer.parseInt(args[2]));
                CrisisCreditAPI.getCrisisCredits(Bukkit.getOfflinePlayer(args[1])).thenAccept(integer -> sender.sendMessage("New Credit balance: " + integer));
                return true;
            }
        }
        return false;
    }
}
