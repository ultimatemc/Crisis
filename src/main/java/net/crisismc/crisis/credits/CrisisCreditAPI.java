package net.crisismc.crisis.credits;

import org.bukkit.OfflinePlayer;

import java.util.concurrent.CompletableFuture;

public class CrisisCreditAPI {

    private static CrisisCredits cc;

    public CrisisCreditAPI(CrisisCredits cc) {
        CrisisCreditAPI.cc = cc;
    }

    public static CompletableFuture<Integer> getCrisisCredits(OfflinePlayer player) {
        return cc.getCredits(player.getUniqueId());
    }

    public static void setCrisisCredits(OfflinePlayer player, int credits) {
        cc.setCredits(player.getUniqueId(), credits);
    }

    /**
     * Add a given amount of Crisis credits (can be negative)
     * @param player
     * @param change
     */
    public static void addCrisisCredits(OfflinePlayer player, int change) {
        cc.getCredits(player.getUniqueId()).thenAccept(integer -> {
           cc.setCredits(player.getUniqueId(), integer + change);
        });
    }

}

