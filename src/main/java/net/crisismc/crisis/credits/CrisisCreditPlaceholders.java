package net.crisismc.crisis.credits;

import be.maximvdw.placeholderapi.PlaceholderAPI;
import net.crisismc.crisis.Crisis;
import org.bukkit.Bukkit;

import java.util.concurrent.ExecutionException;

public class CrisisCreditPlaceholders {

    public CrisisCreditPlaceholders() {
        if (Bukkit.getPluginManager().isPluginEnabled("MVdWPlaceholderAPI")) {
            registerPlaceholders();
        }
    }

    private void registerPlaceholders() {
        PlaceholderAPI.registerPlaceholder(Crisis.getInstance(), "crisis_credits", event -> {
            if (event.isOnline()) {
                try {
                    return CrisisCreditAPI.getCrisisCredits(event.getOfflinePlayer()).get() + "";
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
            return "";
        });

    }
}
