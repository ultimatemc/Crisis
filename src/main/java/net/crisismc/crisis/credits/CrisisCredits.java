package net.crisismc.crisis.credits;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.database.CreditLoader;
import net.crisismc.crisis.database.SqlDatabase;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class CrisisCredits implements Listener {

    private CreditLoader cl;

    private Map<UUID, Integer> cachedCredits = new HashMap<>();

    public CrisisCredits(SqlDatabase db) {
        cl = new CreditLoader(Crisis.getInstance(), db);

        Crisis.getInstance().getServer().getPluginManager().registerEvents(this, Crisis.getInstance());

        for (Player p : Bukkit.getOnlinePlayers()) {
            cl.createCreditRecord(p).thenAccept(result -> {
                cl.getPlayerCredits(p.getUniqueId().toString()). thenAccept(integer -> cachedCredits.put(p.getUniqueId(), integer));
            });
        }
        new CrisisCreditAPI(this);
        new CrisisCreditPlaceholders();
    }

    public CompletableFuture<Integer> getCredits(UUID uuid) {
        CompletableFuture<Integer> promise = new CompletableFuture<>();

        if (cachedCredits.containsKey(uuid)) {
            promise.complete(cachedCredits.get(uuid));
        } else {
            promise = cl.getPlayerCredits(uuid.toString());
            promise.thenAccept(integer -> cachedCredits.put(uuid, integer));
        }

        return promise;
    }

    public void setCredits(UUID uuid, int credits) {
        cachedCredits.put(uuid, credits);
        cl.savePlayerCredits(uuid.toString(), credits);
    }

    @EventHandler
    public void onLogin(PlayerLoginEvent event) {
        Player p = event.getPlayer();

        cl.createCreditRecord(p).thenAccept(result -> {
            cl.getPlayerCredits(p.getUniqueId().toString()). thenAccept(integer -> cachedCredits.put(p.getUniqueId(), integer));
        });
    }

    @EventHandler
    public void onLogout(PlayerQuitEvent event) {
        cl.savePlayerCredits(event.getPlayer().getUniqueId().toString(), cachedCredits.get(event.getPlayer().getUniqueId()));
    }
}
