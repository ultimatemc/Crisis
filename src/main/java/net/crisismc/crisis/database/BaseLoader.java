package net.crisismc.crisis.database;

import com.google.gson.Gson;
import com.zaxxer.hikari.HikariDataSource;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.bases.Base;
import net.crisismc.crisis.bases.BaseIndex;
import net.crisismc.crisis.bases.MultiBlock;
import net.crisismc.crisis.bases.BaseManager;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class BaseLoader {
    private Crisis plugin;
    private SqlDatabase db;
    private HikariDataSource ds;
    private BaseManager bm;

    private final String INSERT = "INSERT INTO Bases VALUES(?,?,?,?,?)";
    private final String SELECT = "SELECT * FROM Bases WHERE chunkx>=? AND chunkx<=? AND chunkz>=? AND chunkz<=?"; //(chunkx - diameter) (chunkx + diameter) (chunkz - diameter) (chunkz + diameter)
    private final String SAVE = "UPDATE Bases SET radius=?,addedplayers=? WHERE uuid=?";
    private final String DELETE = "DELETE FROM Bases WHERE uuid=?";

    private final String INSERT_MB = "INSERT INTO Multiblocks VALUES(?,?)";
    private final String SELECT_MB = "SELECT * FROM Multiblocks WHERE base=?";
    private final String SAVE_MB = "UPDATE Multiblocks SET data=? WHERE base=?";
    private final String DELETE_MB = "DELETE FROM Multiblocks WHERE id=?";

    public BaseLoader(Crisis plugin, SqlDatabase db, BaseManager bm) {
        this.plugin = plugin;
        this.db = db;
        this.ds = db.getDataSource();
        this.bm = bm;
    }

    public void createBase(Base base) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                 PreparedStatement insert = connection.prepareStatement(INSERT)) {
                insert.setString(1, base.getUuid());
                insert.setInt(2, base.getChunk().getX());
                insert.setInt(3, base.getChunk().getZ());
                insert.setByte(4, base.getRange());
                insert.setString(5, base.getPlayers().get(0).toString());

                insert.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void deleteBase(Base base) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                 PreparedStatement delete = connection.prepareStatement(DELETE)) {
                delete.setString(1, base.getUuid());
                delete.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Runs on main thread. Should only be called on shutdown
     */
    public void saveAll() {
        Collection<Base> bases = bm.getBases();
        for (Base b : bases) {
            try (Connection connection = ds.getConnection();
                 PreparedStatement save = connection.prepareStatement(SAVE)) {

                save.setByte(1, b.getRange());

                if (!b.getPlayers().isEmpty()) {
                    String addedplayers = "";
                    for (UUID uuid : b.getPlayers()) {
                        addedplayers = addedplayers + uuid.toString() + ",";
                    }
                    addedplayers.substring(0, addedplayers.length() - 2); //Truncate end comma

                    save.setString(2, addedplayers);
                } else {
                    save.setString(2, "");
                }
                save.setString(3, b.getUuid());

                save.execute();

                return;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadBaseInChunk(Chunk chunk) {
        int chunkx = chunk.getX();
        int chunkz = chunk.getZ();
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                 PreparedStatement select = connection.prepareStatement(SELECT)) {

                select.setInt(1, chunkx - 10);
                select.setInt(2, chunkx + 10);
                select.setInt(3, chunkz - 10);
                select.setInt(4, chunkz + 10);

                ResultSet result = select.executeQuery();
                while (result.next()) {
                    int chunkx1 = result.getInt("chunkx");
                    int chunkz1 = result.getInt("chunkz");
                    byte range = result.getByte("radius");

                    if (Math.abs(chunk.getX() - chunkx1) <= range && Math.abs(chunk.getZ() - chunkz1) <= range) {
                        try (PreparedStatement selectMultiblocks = connection.prepareStatement(SELECT_MB)) {
                            List<UUID> addedplayers = new ArrayList<>();

                            if (result.getString("addedplayers").length() > 0) {
                                for (String s : result.getString("addedplayers").split(",")) {
                                    addedplayers.add(UUID.fromString(s));
                                }
                            }

                            String uuid = result.getString("uuid");

                            //Add multiblocks
                            List<MultiBlock> multiBlocks = new ArrayList<>();
                            selectMultiblocks.setString(1, uuid);

                            ResultSet mbResult = selectMultiblocks.executeQuery();
                            while (mbResult.next()) {
                                String json = StorageUtil.stringFromBase64(mbResult.getString("data"));

                                //Extract Multiblock name from json String
                                JSONObject jsonObj = (JSONObject) JSONValue.parse(json);
                                String name = (String) jsonObj.get("name");

                                Class mbClass = Crisis.getInstance().getBaseManager().getBasePart(name).getClass();
                                if (mbClass == null) continue;

                                //Use Gson to turn json string into Multiblock object
                                Gson gson = Crisis.getInstance().getGson();
                                multiBlocks.add((MultiBlock) gson.fromJson(json, mbClass));


                            }

                            //Switch to main thread
                            Bukkit.getScheduler().runTask(plugin, () -> {
                                Chunk baseChunk = new Location(chunk.getWorld(), chunkx1 * 16, 0, chunkz1 * 16).getChunk();

                                bm.addBase(new Base(uuid, addedplayers, multiBlocks, baseChunk, range));
                            });
                            return;
                        }
                    }

                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void saveBaseInChunk(Chunk chunk) {
        Collection<Base> bases = bm.getBases();
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            for (Base b : bases) {
                if (b.getChunk().equals(chunk)) {
                    System.out.println(b.getRange());
                    try (Connection connection = ds.getConnection();
                         PreparedStatement save = connection.prepareStatement(SAVE)) {

                        save.setByte(1, b.getRange());

                        String addedplayers = "";
                        for (UUID uuid : b.getPlayers()) {
                            addedplayers = addedplayers + uuid.toString() + ",";
                        }
                        if (b.getPlayers().size() > 0)
                            addedplayers.substring(0, addedplayers.length() - 2); //Truncate end comma

                        save.setString(2, addedplayers);
                        save.setString(3, b.getUuid());

                        save.execute();

                        return;
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    return;
                }
                for (Map.Entry<BaseIndex, Base> entry : bm.bases.entrySet()) {
                    if (entry.getValue().getChunk().equals(chunk)) bases.remove(entry.getKey());
                }
            }
        });
    }
}
