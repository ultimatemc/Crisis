package net.crisismc.crisis.database;

import com.zaxxer.hikari.HikariDataSource;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.blocks.Camera;
import net.crisismc.crisis.blocks.ResearchTable;
import net.crisismc.crisis.blocks.BlockHandler;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

/**
 * Manages the blocks in Crisis that have special functionality or data that needs to persist though restarts
 */
public class BlockLoader {

    private Crisis plugin;
    private SqlDatabase db;
    private HikariDataSource ds;


    //Prepared SQL queries for interacting with ResearchTables
    private final String INSERT_RT = "INSERT INTO ResearchTables VALUES(?,?,?,?,?,?,?,?)";
    private final String SELECT_RT = "SELECT * FROM ResearchTables WHERE chunkx=? AND chunkz=?";
    private final String SAVE_RT = "UPDATE ResearchTables SET item=?,researching=?,paperamount=?,endtime=?,location=?,chunkx=?,chunkz=? WHERE uuid=?";
    private final String DELETE_RT = "DELETE FROM ResearchTables WHERE uuid=?";

    //Prepared SQL queries for interacting with Cameras
    private final String INSERT_CAM = "INSERT INTO Cameras VALUES(?,?,?,?,?,?)";
    private final String SELECT_CAM = "SELECT * FROM Cameras WHERE chunkx=? AND chunkz=?";
    private final String SAVE_CAM = "UPDATE Cameras SET name=?,password=? WHERE uuid=?";
    private final String DELETE_CAM = "DELETE FROM Cameras WHERE uuid=?";

    public BlockLoader(Crisis plugin, SqlDatabase db) {
        this.plugin = plugin;
        this.db = db;
        this.ds = db.getDataSource();

    }

	/*

		Research Tables

	 */

    public void createResearchTable(ResearchTable rt) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                 PreparedStatement insert = connection.prepareStatement(INSERT_RT)) {

                insert.setString(1, rt.getUuid());
                insert.setString(2, null);
                insert.setInt(3, rt.getPaperAmount());
                insert.setBoolean(4, rt.isResearching());
                insert.setLong(5, rt.getEndTime());
                insert.setString(6, StorageUtil.sterilizeLoc(rt.getLoc()));
                insert.setInt(7, rt.getChunk().getX());
                insert.setInt(8, rt.getChunk().getZ());

                insert.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void deleteReseachTable(ResearchTable rt) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                 PreparedStatement delete = connection.prepareStatement(DELETE_RT)) {
                delete.setString(1, rt.getUuid());
                delete.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void loadResearchTablesInChunk(Chunk chunk, BlockHandler bh) {
        int chunkx = chunk.getX();
        int chunkz = chunk.getZ();
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                 PreparedStatement select = connection.prepareStatement(SELECT_RT)) {

                select.setInt(1, chunkx);
                select.setInt(2, chunkz);

                ResultSet result = select.executeQuery();
                while (result.next()) {
                    String uuid = result.getString("uuid");
                    ItemStack item = StorageUtil.itemFromBase64(result.getString("item"));
                    int paperAmount = result.getInt("paperamount");
                    boolean researching = result.getBoolean("researching");
                    long endTime = result.getLong("endtime");
                    Location location = StorageUtil.desterilizeLoc(result.getString("location"));

                    Bukkit.getScheduler().runTask(plugin, () -> {
                        ResearchTable rt = new ResearchTable(uuid, item, paperAmount, researching, endTime, location, chunk);
                        bh.addReseachTable(rt);
                    });
                }

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void unloadResearchTables(List<ResearchTable> rts, BlockHandler bh) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            for (ResearchTable rt : rts) {
                try (Connection connection = ds.getConnection();
                     PreparedStatement save = connection.prepareStatement(SAVE_RT)) {

                    save.setString(1, StorageUtil.itemToBase64(rt.getItem()));
                    save.setBoolean(2, rt.isResearching());
                    save.setInt(3, rt.getPaperAmount());
                    save.setLong(4, rt.getEndTime());
                    save.setString(5, StorageUtil.sterilizeLoc(rt.getLoc()));
                    save.setInt(6, rt.getChunk().getX());
                    save.setInt(7, rt.getChunk().getZ());

                    save.setString(8, rt.getUuid());

                    save.execute();
                } catch (SQLException | IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * WARNING: runs in main thread. Only to be called onDisable
     */
    public void saveResearchTables(Collection<ResearchTable> collection) {
        for (ResearchTable rt : collection) {
            try (Connection connection = ds.getConnection();
                 PreparedStatement save = connection.prepareStatement(SAVE_RT)) {

                save.setString(1, StorageUtil.itemToBase64(rt.getItem()));
                save.setBoolean(2, rt.isResearching());
                save.setInt(3, rt.getPaperAmount());
                save.setLong(4, rt.getEndTime());
                save.setString(5, StorageUtil.sterilizeLoc(rt.getLoc()));
                save.setInt(6, rt.getChunk().getX());
                save.setInt(7, rt.getChunk().getZ());

                save.setString(8, rt.getUuid());

                save.execute();
            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    /*

    	Cameras

     */

    public void createCamera(Camera cam) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                 PreparedStatement insert = connection.prepareStatement(INSERT_CAM)) {

                insert.setString(1, cam.getUuid());
                insert.setString(2, cam.getName());
                insert.setString(3, cam.getPassword());
                insert.setString(4, StorageUtil.sterilizeLoc(cam.getLoc()));
                insert.setInt(5, cam.getChunk().getX());
                insert.setInt(6, cam.getChunk().getZ());

                insert.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void deleteCamera(Camera cam) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                 PreparedStatement delete = connection.prepareStatement(DELETE_CAM)) {
                delete.setString(1, cam.getUuid());
                delete.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void loadCamerasInChunk(Chunk chunk, BlockHandler bh) {
        int chunkx = chunk.getX();
        int chunkz = chunk.getZ();
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                 PreparedStatement select = connection.prepareStatement(SELECT_CAM)) {

                select.setInt(1, chunkx);
                select.setInt(2, chunkz);

                ResultSet result = select.executeQuery();
                while (result.next()) {
                    String uuid = result.getString("uuid");
                    String name = result.getString("name");
                    String password = result.getString("password");
                    Location location = StorageUtil.desterilizeLoc(result.getString("location"));

                    Bukkit.getScheduler().runTask(plugin, () -> {
                        Camera cam = new Camera(location, uuid, name, password);
                        bh.addCamera(cam);
                    });
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void unloadCameras(List<Camera> cams, BlockHandler bh) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            for (Camera cam : cams) {
                try (Connection connection = ds.getConnection();
                     PreparedStatement save = connection.prepareStatement(SAVE_CAM)) {

                    save.setString(1, cam.getName());
                    save.setString(2, cam.getPassword());

                    save.setString(3, cam.getUuid());

                    save.execute();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * WARNING: runs in main thread. Only to be called onDisable
     */
    public void saveCameras(Collection<Camera> collection) {
        for (Camera cam : collection) {
            try (Connection connection = ds.getConnection();
                 PreparedStatement save = connection.prepareStatement(SAVE_CAM)) {

                save.setString(1, cam.getName());
                save.setString(2, cam.getPassword());

                save.setString(3, cam.getUuid());

                save.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
