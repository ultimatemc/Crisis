package net.crisismc.crisis.database;

import com.zaxxer.hikari.HikariDataSource;
import net.crisismc.crisis.Crisis;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.CompletableFuture;

public class CreditLoader {

    private Crisis plugin;
    private HikariDataSource ds;

    private final String INSERT = "INSERT INTO Crisis_Credits VALUES(?,?) ON DUPLICATE KEY UPDATE uuid=uuid";
    private final String SELECT = "SELECT * FROM Crisis_Credits WHERE uuid=?";
    private final String SAVE = "UPDATE Crisis_Credits SET credits=? WHERE uuid=?";

    public CreditLoader(Crisis plugin, SqlDatabase db) {
        this.plugin = plugin;
        this.ds = db.getDataSource();
    }

    public CompletableFuture<Boolean> createCreditRecord(Player player) {
        CompletableFuture<Boolean> promise = new CompletableFuture<>();
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                 PreparedStatement insert = connection.prepareStatement(INSERT)) {

                insert.setString(1, player.getUniqueId().toString());
                insert.setInt(2, 0);

                insert.execute();
                promise.complete(true);
            } catch (SQLException e) {
                e.printStackTrace();
                promise.complete(false);
            }
        });
        return promise;
    }

    public CompletableFuture<Integer> getPlayerCredits(String uuid) {
        CompletableFuture<Integer> promise = new CompletableFuture<>();
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                PreparedStatement select = connection.prepareStatement(SELECT)) {

                select.setString(1, uuid);


                ResultSet result = select.executeQuery();
                while (result.next()) {
                    promise.complete(result.getInt("credits"));
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
        return promise;
    }

    public void savePlayerCredits(String uuid, int credits) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                PreparedStatement save = connection.prepareStatement(SAVE)) {

                save.setInt(1, credits);
                save.setString(2, uuid);

                save.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }
}
