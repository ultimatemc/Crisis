package net.crisismc.crisis.database;

import com.zaxxer.hikari.HikariDataSource;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.building.SleepingBag;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.player.CrisisPlayer;
import net.crisismc.crisis.crafting.CrisisRecipe;
import net.crisismc.crisis.lib.ChatUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class CrisisPlayerLoader {

    private Crisis plugin;
    private HikariDataSource ds;

    private final String INSERT = "INSERT INTO Players VALUES(?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE name=?";
    private final String SELECT = "SELECT * FROM Players WHERE uuid=?";
    private final String SELECT_BED = "SELECT * FROM Players WHERE hasbed=true AND (bedloc1=? OR bedloc2=?)";
    private final String SAVE = "UPDATE Players SET food=?,water=?,xp=?,lifexp=?,hasbed=?,legbroken=?,bedloc1=?,bedloc2=?,knownrecipes=? WHERE uuid=?";
    private final String SAVE_BED = "UPDATE Players SET hasbed=? WHERE uuid=?";

    public CrisisPlayerLoader(Crisis plugin, SqlDatabase db) {
        this.plugin = plugin;
        this.ds = db.getDataSource();
    }

    public void loadCrisisPlayer(Player p, CrisisPlayer cp) {
        String name = p.getName();
        UUID uuid = p.getUniqueId();
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                 PreparedStatement insert = connection.prepareStatement(INSERT);
                 PreparedStatement select = connection.prepareStatement(SELECT)) {

                insert.setString(1, uuid.toString());
                insert.setString(2, name);
                insert.setDouble(3, 3000.0);
                insert.setDouble(4, 3000.0);
                insert.setInt(5, 0);
                insert.setInt(6, 0);
                insert.setBoolean(7, false);
                insert.setBoolean(8, false);
                insert.setString(9, null);
                insert.setString(10, null);
                insert.setString(11, null);

                insert.setString(12, name);
                insert.execute();

                select.setString(1, uuid.toString());
                ResultSet result = select.executeQuery();
                if (result.next()) {
                    double food = result.getDouble("food");
                    double water = result.getDouble("water");
                    int xp = result.getInt("xp");
                    int lifeXp = result.getInt("lifexp");
                    boolean legBroken = result.getBoolean("legbroken");
                    boolean hasBed = result.getBoolean("hasbed");
                    SleepingBag bed = hasBed ? new SleepingBag(StorageUtil.desterilizeLoc(result.getString("bedloc1")), StorageUtil.desterilizeLoc(result.getString("bedloc2"))) : null;
                    List<CrisisRecipe> knownRecipes = StorageUtil.desterilizeRecipeList(result.getString("knownrecipes"));

                    Bukkit.getScheduler().runTask(plugin, () -> {
                        cp.updateDbValues(food, water, xp, lifeXp, legBroken, hasBed, bed, knownRecipes);
                        CrisisPlayer.crisisPlayers.put(uuid, cp);
                        System.out.println("Loaded player " + name + " from SQL Database");
                    });
                }
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void saveCrisisPlayer(CrisisPlayer cp) {
        String uuid = cp.p.getUniqueId().toString();
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> saveCrisisPlayerSync(cp, uuid));
    }

    public void saveCrisisPlayerSync(CrisisPlayer cp, String uuid) {
        try (Connection connection = ds.getConnection();
             PreparedStatement statement = connection.prepareStatement(SAVE)) {

            statement.setDouble(1, cp.getFood());
            statement.setDouble(2, cp.getWater());
            statement.setInt(3, cp.getXp());
            statement.setInt(4, cp.getLifeXp());
            statement.setBoolean(5, cp.hasBed());
            statement.setBoolean(6, cp.isLegBroken());
            if (cp.hasBed()) {
                statement.setString(7, StorageUtil.sterilizeLoc(cp.getBed().loc1));
                statement.setString(8, StorageUtil.sterilizeLoc(cp.getBed().loc2));
            } else {
                statement.setString(7, null);
                statement.setString(8, null);
            }
            statement.setString(9, StorageUtil.sterilizeRecipeList(cp.getKnownRecipes()));


            statement.setString(10, uuid);
            statement.execute();
            CrisisPlayer.crisisPlayers.remove(uuid);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void handleCarpetClick(Player p, Block b) {
        String locStr = StorageUtil.sterilizeLoc(b.getLocation());
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                 PreparedStatement select = connection.prepareStatement(SELECT_BED);
                 PreparedStatement save = connection.prepareStatement(SAVE_BED)) {

                select.setString(1, locStr);
                select.setString(2, locStr);
                ResultSet result = select.executeQuery();

                if (result.next()) {
                    String uuid = result.getString("uuid");
                    String name = result.getString("name");
                    String bedloc1 = result.getString("bedloc1");
                    String bedloc2 = result.getString("bedloc2");
                    Bukkit.getScheduler().runTask(plugin, () -> {
                        StorageUtil.desterilizeLoc(bedloc1).getBlock().setType(Material.AIR);
                        StorageUtil.desterilizeLoc(bedloc2).getBlock().setType(Material.AIR);

                        p.getWorld().dropItemNaturally(StorageUtil.desterilizeLoc(bedloc1).add(0.5, 0.5, 0.5), CrisisItem.SLEEPING_BAG.getItemStack(1));
                        p.sendMessage(ChatUtil.getPrefix() + ChatColor.GRAY + "You broke " + ChatColor.YELLOW + name + "'s " + ChatColor.GRAY + "sleeping bag.");
                        return;
                    });

                    save.setBoolean(1, false);
                    save.setString(2, uuid);
                    save.execute();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

    }
}
