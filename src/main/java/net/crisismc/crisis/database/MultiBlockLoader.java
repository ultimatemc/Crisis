package net.crisismc.crisis.database;

import com.zaxxer.hikari.HikariDataSource;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.bases.MultiBlock;
import net.crisismc.crisis.bases.BaseManager;
import org.bukkit.Bukkit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MultiBlockLoader {

    private Crisis plugin;
    private BaseManager bm;
    private SqlDatabase db;
    private HikariDataSource ds;

    private final String INSERT = "INSERT INTO Multiblocks VALUES(?,?,?,?)";
    private final String SELECT = "SELECT * FROM Multiblocks WHERE base=?";
    private final String SAVE = "UPDATE Multiblocks SET data=? WHERE base=?";
    private final String DELETE = "DELETE FROM Multiblocks WHERE uuid=?";

    public MultiBlockLoader(SqlDatabase db) {
        this.plugin = Crisis.getInstance();
        bm = Crisis.getInstance().getBaseManager();
        this.db = db;
        this.ds = db.getDataSource();
    }

    public void createMultiBlock(MultiBlock mb) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                 PreparedStatement insert = connection.prepareStatement(INSERT)) {
                insert.setString(1, mb.getUuid());
                insert.setInt(2, mb.getLoc().getChunk().getX());
                insert.setInt(3, mb.getLoc().getChunk().getZ());
                insert.setString(4, StorageUtil.stringToBase64(Crisis.getInstance().getGson().toJson(mb)));

                insert.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }


    public void deleteMultiBlock(String uuid) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                 PreparedStatement delete = connection.prepareStatement(DELETE)) {
                delete.setString(1, uuid);
                delete.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Runs on main thread. Should only be called on shutdown
     */
    public void saveAll() {

    }
}
