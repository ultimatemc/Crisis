package net.crisismc.crisis.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import net.crisismc.crisis.Crisis;
import org.bukkit.Bukkit;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlDatabase {

    private Crisis plugin;
    private HikariDataSource ds;

    public SqlDatabase(Crisis plugin, String url, String username, String password) {
        this.plugin = plugin;

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(url);
        config.setUsername(username);
        config.setPassword(password);
        config.setLeakDetectionThreshold(10000);
        ds = new HikariDataSource(config);

        setupTables();

    }

    private void setupTables() {
        System.out.println("Setting up MySQL tables");
        try (Connection con = ds.getConnection()) {
            Statement players = con.createStatement();
            players.executeUpdate("CREATE TABLE IF NOT EXISTS Players("
                    + "uuid VARCHAR(36), "
                    + "name VARCHAR(16), "
                    + "food DECIMAL(8,4), "
                    + "water DECIMAL(8,4), "
                    + "xp INT, "
                    + "lifexp INT, "
                    + "hasbed BOOLEAN,"
                    + "legbroken BOOLEAN,"
                    + "bedloc1 VARCHAR(48),"
                    + "bedloc2 VARCHAR(48), "
                    + "knownrecipes BLOB, "
                    + "PRIMARY KEY (uuid))");

            Statement rts = con.createStatement();
            rts.executeUpdate("CREATE TABLE IF NOT EXISTS ResearchTables("
                    + "uuid VARCHAR(36), "
                    + "item BLOB, "
                    + "paperamount INT, "
                    + "researching BOOLEAN, "
                    + "endtime BIGINT, "
                    + "location VARCHAR(48), "
                    + "chunkx INT, "
                    + "chunkz INT, "
                    + "PRIMARY KEY (uuid))");

            Statement cams = con.createStatement();
            cams.executeUpdate("CREATE TABLE IF NOT EXISTS Cameras("
                    + "uuid VARCHAR(36), "
                    + "name VARCHAR(30), "
                    + "password VARCHAR(30), "
                    + "location VARCHAR(48), "
                    + "chunkx INT, "
                    + "chunkz INT, "
                    + "PRIMARY KEY (uuid))");


            Statement bases = con.createStatement();
            bases.executeUpdate("CREATE TABLE IF NOT EXISTS Bases("
                    + "uuid VARCHAR(36), "
                    + "chunkx INT, "
                    + "chunkz INT, "
                    + "radius TINYINT, "
                    + "addedplayers BLOB, "
                    + "PRIMARY KEY (uuid))");

            Statement multiblocks = con.createStatement();
            multiblocks.executeUpdate("CREATE TABLE IF NOT EXISTS Multiblocks("
                    + "uuid VARCHAR(36),"
                    + "chunkx INT, "
                    + "chunkz INT, "
                    + "data BLOB,"
                    + "PRIMARY KEY (uuid))");

            Statement marketListings = con.createStatement();
            marketListings.executeUpdate("CREATE TABLE IF NOT EXISTS Market_Listings("
                    + "id INT NOT NULL AUTO_INCREMENT,"
                    + "player VARCHAR(36),"
                    + "creation_time TIMESTAMP,"
                    + "item BLOB,"
                    + "amount INT,"
                    + "price INT," //Cent value
                    + "PRIMARY KEY (id))");

            Statement marketTransactions = con.createStatement();
            marketTransactions.executeUpdate("CREATE TABLE IF NOT EXISTS Market_Transactions("
                    + "id INT NOT NULL AUTO_INCREMENT,"
                    + "player VARCHAR(36),"
                    + "listing INT NOT NULL,"
                    + "timestamp TIMESTAMP,"
                    + "quantity INT,"
                    + "amount INT," //Cent Value
                    + "PRIMARY KEY (id),"
                    + "FOREIGN KEY (listing) REFERENCES Market_Listings(id) ON DELETE CASCADE)");

            Statement playerStatistics = con.createStatement();
            playerStatistics.executeUpdate("CREATE TABLE IF NOT EXISTS Player_Statistics("
                    + "uuid VARCHAR(36), "
                    + "play_time BIGINT,"
                    + "kills_ffa INT,"
                    + "kills INT,"
                    + "deaths_ffa INT,"
                    + "deaths INT,"
                    + "PRIMARY KEY (uuid))");

            Statement crisisCredits = con.createStatement();
            crisisCredits.executeUpdate("CREATE TABLE IF NOT EXISTS Crisis_Credits("
                    + "uuid VARCHAR(36), "
                    + "credits INT,"
                    + "PRIMARY KEY (uuid))");

        } catch (SQLException e) {
            Bukkit.getPluginManager().disablePlugin(plugin);
            e.printStackTrace();
        }
    }

    public HikariDataSource getDataSource() {
        return ds;

    }


}
