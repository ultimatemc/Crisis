package net.crisismc.crisis.database;

import com.zaxxer.hikari.HikariDataSource;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.statistics.PlayerStatistics;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class StatisticsLoader {

    private Crisis plugin;
    private HikariDataSource ds;

    private final String SELECT = "SELECT * FROM Player_Statistics WHERE uuid=?";
    private final String SELECT_TOP = "SELECT * FROM Player_Statistics ORDER BY kills_ffa DESC LIMIT 5";
    private final String INSERT = "INSERT INTO Player_Statistics VALUES(?,?,?,?,?,?)";
    private final String SAVE = "UPDATE Player_Statistics SET play_time=?,kills_ffa=?,kills=?,deaths_ffa=?,deaths=? WHERE uuid=?";


    public StatisticsLoader(Crisis plugin, SqlDatabase db) {
        this.plugin = plugin;
        this.ds = db.getDataSource();
    }

    public CompletableFuture<PlayerStatistics> loadPlayerStatistics(Player p) {
        CompletableFuture<PlayerStatistics> promise = new CompletableFuture<>();
        final UUID uuid = p.getUniqueId();

        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                PreparedStatement select = connection.prepareStatement(SELECT)) {
                select.setString(1, uuid.toString());

                ResultSet result = select.executeQuery();
                if (result.next()) {
                    promise.complete(new PlayerStatistics(
                            uuid,
                            result.getLong("play_time"),
                            result.getInt("kills_ffa"),
                            result.getInt("kills"),
                            result.getInt("deaths_ffa"),
                            result.getInt("deaths")
                    ));
                } else {
                    PlayerStatistics ps = new PlayerStatistics(uuid);
                    promise.complete(ps);
                    insertPlayerStatistics(ps);
                }
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        return promise;
    }

    public CompletableFuture<List<PlayerStatistics>> topFfaKills() {
        CompletableFuture<List<PlayerStatistics>> promise = new CompletableFuture<>();

        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                 PreparedStatement select = connection.prepareStatement(SELECT_TOP)) {

                ResultSet result = select.executeQuery();
                List<PlayerStatistics> topStats = new ArrayList<>();
                while (result.next()) {
                    topStats.add(new PlayerStatistics(
                            UUID.fromString(result.getString("uuid")),
                            result.getLong("play_time"),
                            result.getInt("kills_ffa"),
                            result.getInt("kills"),
                            result.getInt("deaths_ffa"),
                            result.getInt("deaths")));
                }

                promise.complete(topStats);

                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        return promise;
    }

    private void insertPlayerStatistics(PlayerStatistics ps) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = ds.getConnection();
                PreparedStatement insert = connection.prepareStatement(INSERT)) {

                insert.setString(1, ps.getUuid().toString());
                insert.setLong(2, ps.getPlayTime());
                insert.setInt(3, ps.getFfaKills());
                insert.setInt(4, ps.getKills());
                insert.setInt(5, ps.getFfaDeaths());
                insert.setInt(6, ps.getDeaths());

                insert.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * @param ps
     * @param sync Sync must be used on server shutdown
     */
    public void savePlayerStatistics(PlayerStatistics ps, boolean sync) {
        if (sync) {
            savePlayerStatistics(ps);
        } else {
            Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
                savePlayerStatistics(ps);
            });
        }
    }

    private void savePlayerStatistics(PlayerStatistics ps) {
        try (Connection connection = ds.getConnection();
            PreparedStatement save = connection.prepareStatement(SAVE)) {

            save.setLong(1, ps.getPlayTime());
            save.setInt(2, ps.getFfaKills());
            save.setInt(3, ps.getKills());
            save.setInt(4, ps.getFfaDeaths());
            save.setInt(5, ps.getDeaths());

            save.setString(6, ps.getUuid().toString());

            save.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
