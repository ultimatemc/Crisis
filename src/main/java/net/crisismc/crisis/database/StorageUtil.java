package net.crisismc.crisis.database;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.crafting.CrisisRecipe;
import net.minecraft.server.v1_12_R1.NBTCompressedStreamTools;
import net.minecraft.server.v1_12_R1.NBTTagCompound;
import net.minecraft.server.v1_12_R1.NBTTagList;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import java.io.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * Provides useful methods for converting Objects into a form that can be easily stored in a database.
 */
public class StorageUtil {

    public static String sterilizeLoc(Location loc) {
        String s = loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ();
        return s;
    }

    public static Location desterilizeLoc(String s) {
        Location loc = new Location(Bukkit.getWorld(Crisis.WORLD_NAME),
                Double.parseDouble(s.split(",")[0]),
                Double.parseDouble(s.split(",")[1]),
                Double.parseDouble(s.split(",")[2]));

        return loc;
    }

    public static Chunk desterilizeChunk(int x, int z) {
        return new Location(Bukkit.getWorld(Crisis.WORLD_NAME), x * 16, 0, z * 16).getChunk();
    }

    public static String sterilizeRecipeList(List<CrisisRecipe> list) {
        String s = "";
        for (CrisisRecipe e : list) {
            if (e == null) continue;
            s = s + e.toString() + ",";
        }
        return s;
    }

    public static List<CrisisRecipe> desterilizeRecipeList(String list) {
        List<CrisisRecipe> enumList = new ArrayList<CrisisRecipe>();
        if (list == null) return enumList;

        for (String recipe : list.split(",")) {
            if (recipe.length() == 0) continue;
            enumList.add(CrisisRecipe.valueOf(recipe));
        }

        return enumList;
    }

    /**
     * Converts an item in its entirety into a {@link net.minecraft.server.v1_12_R1.NBTTagCompound NBTTTagCompound}
     * which is then encoded into base64 for compact storage
     *
     * @param item The ItemStack to sterilize
     * @return The resulting base64 String
     * @throws IOException
     */
    public static String itemToBase64(ItemStack item) throws IOException {
        if (item != null) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutput = new DataOutputStream(outputStream);

            NBTTagList nbtTagListItems = new NBTTagList();
            NBTTagCompound nbtTagCompoundItem = new NBTTagCompound();

            net.minecraft.server.v1_12_R1.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);

            nmsItem.save(nbtTagCompoundItem);

            nbtTagListItems.add(nbtTagCompoundItem);

            NBTCompressedStreamTools.a(nbtTagCompoundItem, (DataOutput) dataOutput);

            return new BigInteger(1, outputStream.toByteArray()).toString(32);
        }
        return null;
    }

    /**
     * Undoes the effect of {@link #itemToBase64(ItemStack) itemToBase64}
     *
     * @param data The base64 string representation of an ItemStack
     * @return The restored ItemStack
     * @throws IOException
     */
    public static ItemStack itemFromBase64(String data) throws IOException {
        if (data != null) {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(new BigInteger(data, 32).toByteArray());

            NBTTagCompound nbtTagCompoundRoot = NBTCompressedStreamTools.a(new DataInputStream(inputStream));

            net.minecraft.server.v1_12_R1.ItemStack nmsItem = new net.minecraft.server.v1_12_R1.ItemStack(nbtTagCompoundRoot);
            ItemStack item = CraftItemStack.asBukkitCopy(nmsItem);

            return item;
        }
        return null;
    }

    public static String stringToBase64(String str) {
        String base64 = Base64.getEncoder().withoutPadding().encodeToString(str.getBytes());
        return base64;
    }

    public static String stringFromBase64(String base64) {
        return new String(Base64.getDecoder().decode(base64));
    }
}
