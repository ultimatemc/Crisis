package net.crisismc.crisis.economy;


import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Optional;
import net.crisismc.crisis.lib.ChatUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BalanceCommand extends BaseCommand {

    private CrisisEconomy ce;

    public BalanceCommand(CrisisEconomy ce) {
        this.ce = ce;
    }

    @CommandAlias("balance|bal|money")
    public void onCommand(CommandSender sender, @Optional String player) {
        if (player != null) {
            if (sender.hasPermission("crisis.economy.balanceothers")) {
                sender.sendMessage(ChatUtil.getPrefix() + ChatColor.GRAY + player + "'s balance: " +
                        ChatColor.GREEN +  "$" + CrisisEconomy.formatEconomy(ce.getEcon().getBalance(Bukkit.getOfflinePlayer(player))));
            }
        } else {
            if (sender instanceof Player) {
                System.out.println(ce.getEcon());
                sender.sendMessage(ChatUtil.getPrefix() + ChatColor.GRAY + "Balance: "
                        + ChatColor.GREEN + "$" + CrisisEconomy.formatEconomy(ce.getEcon().getBalance((Player) sender)));
            }
        }
    }
}
