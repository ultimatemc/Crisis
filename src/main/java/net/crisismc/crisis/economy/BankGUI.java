package net.crisismc.crisis.economy;

import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.lib.ChatUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class BankGUI implements Listener {

    private static final String TITLE = ChatColor.GREEN + "" + ChatColor.BOLD + "Bank";
    private static CrisisEconomy ce;

    public BankGUI(CrisisEconomy ce) {
        BankGUI.ce = ce;
        Crisis.getInstance().getServer().getPluginManager().registerEvents(this, Crisis.getInstance());
        invTask();
    }

    private void invTask() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Crisis.getInstance(), () -> Bukkit.getOnlinePlayers().forEach(player -> invTick(player)), 1, 1);
    }

    public static void openInventory(Player player) {
        Inventory inv = Bukkit.createInventory(null, 27, TITLE);
        for(int i = 0; i < 27; i++) {
            if (i == 10 || (i >= 14 && i <= 17)) continue;
            inv.setItem(i, CrisisItem.INVENTORY_ITEM.getItemStack(1));
        }

        double balance = ce.getEcon().getBalance(player);

        inv.setItem(12, getBankItem(balance));

        if(balance >= 1121) {
            inv.setItem(17, CrisisItem.MONEY.getItemStack(1000D));
        }
        if (balance >= 121) {
            inv.setItem(16, CrisisItem.MONEY.getItemStack(100D));
        }
        if (balance >= 21) {
            inv.setItem(15, CrisisItem.MONEY.getItemStack(20D));
        }
        if (balance >= 1) {
            inv.setItem(14, CrisisItem.MONEY.getItemStack(1D));
        }

        player.openInventory(inv);
    }

    private static ItemStack getBankItem(double balance) {
        ItemStack bankItem = new ItemStack(Material.EMERALD_BLOCK, 1);
        ItemMeta im = bankItem.getItemMeta();
        im.setDisplayName(TITLE);
        im.setLore(Arrays.asList(
                ChatColor.GRAY + "Balance: " + CrisisEconomy.formatEconomyColor(balance),
                ChatColor.GRAY + "<-- Deposit",
                ChatColor.GRAY + "Withdraw -->"
        ));
        bankItem.setItemMeta(im);
        return bankItem;
    }

    @EventHandler
    public void onInvClick(final InventoryClickEvent event) {
        if (event.getClickedInventory() != null && event.getClickedInventory().getName().equals(TITLE)) {
            Player player = (Player) event.getWhoClicked();
            player.playSound(event.getWhoClicked().getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
            event.setCancelled(true);

            //Deposit
            if (event.getSlot() == 10) {
                ItemStack handItem = event.getCursor();
                if (handItem != null && handItem.getType() == CrisisItem.MONEY.getMaterial()) {
                    if (event.getAction() == InventoryAction.PLACE_ALL
                            || event.getAction() == InventoryAction.PLACE_SOME
                            || event.getAction() == InventoryAction.PLACE_ONE
                            || event.getAction() == InventoryAction.DROP_ALL_CURSOR
                            || event.getAction() == InventoryAction.DROP_ONE_CURSOR) {
                        event.setCancelled(false);
                    }
                }
            //Withdraw
            } else if (event.getSlot() >= 14 && event.getSlot() <= 17) {
                ItemStack item = event.getCurrentItem();
                if (item != null) {
                    double money = CrisisEconomy.parseMoney(item);
                    if (money != 0) {
                        if (ce.getEcon().getBalance(player) >= money) {
                            ce.getEcon().withdrawPlayer(player, money);
                            player.getInventory().addItem(CrisisItem.MONEY.getItemStack(money));
                            player.sendMessage(ChatUtil.getPrefix() + ChatColor.GRAY + "Withdrew " + CrisisEconomy.formatEconomyColor(money));
                            openInventory(player);
                        } else {
                            player.sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "Insufficient funds.");
                        }
                    }
                }
            }
        }
    }

    private void invTick(Player player) {
        if (player.getOpenInventory().getTopInventory() != null && player.getOpenInventory().getTopInventory().getName().equals(TITLE)) {
            Inventory bankInv = player.getOpenInventory().getTopInventory();

            ItemStack item = bankInv.getItem(10);
            if (item != null) {
                double money = CrisisEconomy.parseMoney(item) * item.getAmount();
                if (money > 0D) {
                    bankInv.setItem(10, new ItemStack(Material.AIR, 1));
                    ce.getEcon().depositPlayer(player, money);
                    player.sendMessage(ChatUtil.getPrefix() + ChatColor.GRAY + "Deposited " + CrisisEconomy.formatEconomyColor(money));
                    openInventory(player);
                }
            }
        }
    }




}
