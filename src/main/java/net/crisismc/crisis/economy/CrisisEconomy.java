package net.crisismc.crisis.economy;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.CrisisItem;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.logging.Level;

public class CrisisEconomy {

    private Crisis plugin;
    private Economy econ;

    public CrisisEconomy() {
        plugin = Crisis.getInstance();

        if (plugin.getServer().getPluginManager().getPlugin("Vault") == null) {
            plugin.getLogger().log(Level.SEVERE, "Vault not found. Crisis could not load economy");
            return;
        }

        setupEconomy();
        loadRecipes();
        new CrisisEconomyPlaceholders(this);
        new EconomyListener();
        new BankGUI(this);
    }

    private boolean setupEconomy() {
        if (plugin.getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = plugin.getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    private void loadRecipes() {

        for (int items = 2; items <= 4; items++) {
            ShapelessRecipe recipe = new ShapelessRecipe(new ItemStack(CrisisItem.MONEY.getMaterial(), 1));
            for (int i = 0; i < items; i++) {
                recipe.addIngredient(CrisisItem.MONEY.getMaterial());
            }
            if (!plugin.getServer().getRecipesFor(new ItemStack(CrisisItem.MONEY.getMaterial())).contains(recipe)) {
                plugin.getServer().addRecipe(recipe);
            }
        }
    }

    public Economy getEcon() {
        return econ;
    }

    public static String formatEconomy(double money) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        return formatter.format(money);
    }

    public static String formatEconomyColor(double money) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        return ChatColor.GREEN + "$" + formatter.format(money);
    }

    public static double parseMoney(ItemStack item) {
        if (item == null || item.getType() != CrisisItem.MONEY.getMaterial() || item.getItemMeta() == null) return 0;

        String moneyStr = item.getItemMeta().getDisplayName();
        moneyStr = moneyStr.replace(",", "");
        return Double.parseDouble(moneyStr.split("\\$")[1]);
    }
}
