package net.crisismc.crisis.economy;

import be.maximvdw.placeholderapi.PlaceholderAPI;
import net.crisismc.crisis.Crisis;
import org.bukkit.Bukkit;

public class CrisisEconomyPlaceholders {

    private CrisisEconomy ce;

    public CrisisEconomyPlaceholders(CrisisEconomy economy) {
        ce = economy;
        if (Bukkit.getPluginManager().isPluginEnabled("MVdWPlaceholderAPI")) {
            registerPlaceholders();
        }
    }

    private void registerPlaceholders() {
        PlaceholderAPI.registerPlaceholder(Crisis.getInstance(), "crisis_balance", event -> {
            if (event.isOnline()) {
                return CrisisEconomy.formatEconomy(ce.getEcon().getBalance(event.getPlayer()));
            }
            return "";
        });

    }
}
