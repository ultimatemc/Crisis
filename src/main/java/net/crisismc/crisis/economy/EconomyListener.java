package net.crisismc.crisis.economy;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.CrisisItem;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;

public class EconomyListener implements Listener {

    public EconomyListener() {
        Crisis.getInstance().getServer().getPluginManager().registerEvents(this, Crisis.getInstance());
    }
    @EventHandler
    public void onItemCraftPrep(PrepareItemCraftEvent event) {

        if (CrisisItem.MONEY.getMaterial() != event.getRecipe().getResult().getType()) {
            return;
        }

        CraftingInventory craftInv = event.getInventory();

        double money = 0;
        for (ItemStack item : craftInv.getMatrix()) {
            if (item == null || !item.hasItemMeta()) continue;
            String displayName = item.getItemMeta().getDisplayName();
            money +=  Double.parseDouble(displayName.split("\\$")[1]);
        }

        craftInv.setResult(CrisisItem.MONEY.getItemStack(money));
    }

}
