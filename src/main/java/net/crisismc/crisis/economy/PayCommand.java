package net.crisismc.crisis.economy;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import net.crisismc.crisis.lib.ChatUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import static org.bukkit.ChatColor.GRAY;
import static org.bukkit.ChatColor.RED;
import static org.bukkit.ChatColor.RESET;

public class PayCommand extends BaseCommand {

    private CrisisEconomy ce;

    public PayCommand(CrisisEconomy ce) {
        this.ce = ce;
    }

    @CommandAlias("pay")
    public void onCommand(Player player, String target, Double amount) {
        double bal = ce.getEcon().getBalance(player);

        if (bal < amount) {
            player.sendMessage(ChatUtil.getPrefix() + RED + "You have insufficient funds to send "
                    + CrisisEconomy.formatEconomyColor(amount) + RESET + "" + RED + " to " + target + ".");
            return;
        }

        ce.getEcon().withdrawPlayer(player, amount);
        ce.getEcon().depositPlayer(Bukkit.getOfflinePlayer(target), amount);

        player.sendMessage(ChatUtil.getPrefix() + GRAY + "You sent " + CrisisEconomy.formatEconomyColor(amount)
        + RESET + "" + GRAY + " to " + target + ".");

        if (Bukkit.getOfflinePlayer(target).isOnline()) {
             ((Player) Bukkit.getOfflinePlayer(target)).sendMessage(ChatUtil.getPrefix() + GRAY + player.getName() + " has sent you " + CrisisEconomy.formatEconomyColor(amount));
        }
    }
}
