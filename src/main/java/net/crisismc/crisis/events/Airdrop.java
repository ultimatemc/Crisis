package net.crisismc.crisis.events;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.schematic.MCEditSchematicFormat;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.events.vehicles.AirdropChest;
import net.crisismc.crisis.events.vehicles.AirdropHelicopter;
import net.crisismc.crisis.events.vehicles.TransportTarget;
import net.crisismc.crisis.guns.targets.TargetRegistrar;
import net.crisismc.crisis.lib.DateUtil;
import net.crisismc.crisis.lib.RandomCollection;
import net.crisismc.crisis.vehicles.ArmorStandVehicle;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Airdrop extends CrisisEvent implements Listener {


    //General fields
    private Crisis plugin;
    private Random r = new Random();
    private List<Location> airdropLocs = new ArrayList<>();
    private RandomCollection<ItemStack> airdropItems = new RandomCollection<>();

    private CuboidClipboard normalTransport;
    WorldEditPlugin we = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
    EditSession session = we.getWorldEdit().getEditSessionFactory().getEditSession(new BukkitWorld(Bukkit.getWorld("world")), Integer.MAX_VALUE);

    private final int EAST_X = 3071;
    private final int WEST_X = -2943;
    private final int NORTH_Z = -2943;
    private final int SOUTH_Z = 3071;

    //Specific fields
    private Location transportLoc;
    private Location airdropLoc;
    private Location chestLoc;
    private long eta;
    private int endX;
    private int endZ;
    public boolean chestDropped = false;
    private boolean intercepted = false;
    private Location interceptedLoc;

    TransportTarget target;
    ArmorStandVehicle transport;

    int chestTaskId;
    int transportTaskId;


    public Airdrop(Crisis plugin, EventScheduler eventScheduler) {
        super(eventScheduler);

        this.plugin = plugin;

        File schematic = new File("plugins"+File.separator+"Crisis"+File.separator+"schematics"+File.separator+"airdrop_normal.schematic");
        try {
            normalTransport = MCEditSchematicFormat.getFormat(schematic).load(schematic);
        } catch (IOException | com.sk89q.worldedit.world.DataException e) {
            e.printStackTrace();
            return;
        }

        airdropLocs.add(new Location(Bukkit.getWorld("world"), -1844.5, 44, 1807.5)); //Small airfield
        airdropLocs.add(new Location(Bukkit.getWorld("world"), 1478.5, 37, 292.5)); //Large airfield
        airdropLocs.add(new Location(Bukkit.getWorld("world"), 2241.5, 75, 1909.5)); //plugine hospital
        airdropLocs.add(new Location(Bukkit.getWorld("world"), 1190.5, 79, -2173.5)); //Ryerson hospital
        airdropLocs.add(new Location(Bukkit.getWorld("world"), -2196.5, 76, -2095.5)); //Thrisk hospital

        //Setup airdrop items
        airdropItems.add(3, CrisisItem.BANDAGE.getItemStack(3));
        airdropItems.add(3, CrisisItem.GUNPOWDER.getItemStack(30));
        airdropItems.add(3, CrisisItem.AMMO_AKS74U.getItemStack(1));
        airdropItems.add(3, CrisisItem.AMMO_RIFLE_BOLT.getItemStack(1));
        airdropItems.add(3, CrisisItem.AMMO_DESERT_EAGLE.getItemStack(1));
        airdropItems.add(3, CrisisItem.AMMO_M4A4.getItemStack(1));
        airdropItems.add(3, CrisisItem.M9.getItemStack(1));
        airdropItems.add(1, CrisisItem.AMMO_M40A1.getItemStack(1));
        airdropItems.add(3, CrisisItem.AMMO_MP5.getItemStack(1));
        airdropItems.add(3, CrisisItem.AMMO_SCARH_DMR.getItemStack(1));
        airdropItems.add(3, CrisisItem.AMMO_THOMPSON.getItemStack(1));
        airdropItems.add(3, CrisisItem.AMMO_SHOTGUN_WATERPIPE.getItemStack(1));
        airdropItems.add(1, CrisisItem.GRENADE_FRAG.getItemStack(1));

        airdropItems.add(1, CrisisItem.HELMET_COMBAT.getItemStack(1));
        airdropItems.add(1, CrisisItem.VEST_COMBAT.getItemStack(1));
        airdropItems.add(1, CrisisItem.PANTS_COMBAT.getItemStack(1));
        airdropItems.add(1, CrisisItem.BOOTS_COMBAT.getItemStack(1));
        airdropItems.add(0.05, CrisisItem.CHESTPLATE_KEVLAR.getItemStack(1));

        airdropItems.add(3, CrisisItem.BARBED_WIRE.getItemStack(10));
        airdropItems.add(3, CrisisItem.METAL_FRAG.getItemStack(30));
        airdropItems.add(3, CrisisItem.ENERGY_BAR.getItemStack(16));
        airdropItems.add(0.1, CrisisItem.C4_CONTROLLER.getItemStack(1));
        airdropItems.add(0.5, CrisisItem.M9.getItemStack(1));
        airdropItems.add(0.5, CrisisItem.DESERT_EAGLE.getItemStack(1));
        airdropItems.add(0.5, CrisisItem.AKS74U.getItemStack(1));
        airdropItems.add(0.5, CrisisItem.M4A4.getItemStack(1));
        airdropItems.add(0.5, CrisisItem.MP5.getItemStack(1));
        airdropItems.add(0.5, CrisisItem.THOMPSON.getItemStack(1));
        airdropItems.add(0.3, CrisisItem.SCARH_DMR.getItemStack(1));
        airdropItems.add(0.8, CrisisItem.SHOTGUN_WATERPIPE.getItemStack(1));
        airdropItems.add(0.05, CrisisItem.M40A1.getItemStack(1));

        Bukkit.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public Location getLocation() {
        return airdropLoc;
    }

    @Override
    public void beginNewEvent() {
        eta = System.currentTimeMillis() + 900000;
        chestDropped = false;
        intercepted = false;
        selectLocations();

        //The delay is 15 mins minus the travel time of the heli
        long delay = (long) (18000 - (airdropLoc.distance(transportLoc)));

        delay = 100; //TODO remove

        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> dispatchTransport(), delay);
    }

    private void selectLocations() {
        airdropLoc = airdropLocs.get(r.nextInt(airdropLocs.size()));
        transportLoc = new Location(Bukkit.getWorld("world"), 0, 200, 0);

        switch (r.nextInt(4)) {
            case 0:
                transportLoc.setX(EAST_X);
                transportLoc.setZ(r.nextInt(Math.abs(NORTH_Z) + Math.abs(SOUTH_Z)) + NORTH_Z);
                endX = WEST_X;
                endZ = Integer.MAX_VALUE;
                break;
            case 1:
                transportLoc.setX(WEST_X);
                transportLoc.setZ(r.nextInt(Math.abs(NORTH_Z) + Math.abs(SOUTH_Z)) + NORTH_Z);
                endX = EAST_X;
                endZ = Integer.MAX_VALUE;
                break;
            case 2:
                transportLoc.setX(r.nextInt(Math.abs(WEST_X) + Math.abs(EAST_X)) + WEST_X);
                transportLoc.setZ(NORTH_Z);
                endZ = SOUTH_Z;
                endX = Integer.MAX_VALUE;
                break;
            case 3:
                transportLoc.setX(airdropLoc.getX());
                transportLoc.setZ(SOUTH_Z);
                endZ = NORTH_Z;
                endX = Integer.MAX_VALUE;
        }
    }

    private void dispatchTransport() {
        System.out.println("Airdrop Loc: " + airdropLoc);
        System.out.println("Transport loc: " + transportLoc);
        System.out.println("Airdrop transport dispatched");
        clearChests();

        target = new TransportTarget(transportLoc, this);
        Vector path = new Vector(airdropLoc.getBlockX(), 0, airdropLoc.getBlockZ())
                .subtract(new Vector(transportLoc.getBlockX(), 0, transportLoc.getBlockZ()))
                .normalize();
        TargetRegistrar.registerCustomTarget(target);

        try {
            transport = new AirdropHelicopter(this, transportLoc, airdropLoc.clone(), endX, endZ);
        } catch (IOException | DataException e) {
            e.printStackTrace();
        }

        transportTaskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> moveTransport(path), 1, 1);
    }

    private void clearChests() {
        for (Location airdropLoc : airdropLocs) {
            for (int x = -2; x <= 2; x++) {
                for (int y = 0; y <= 5; y++) {
                    for (int z = -2; z <= 2; z++) {
                        Location loc = airdropLoc.clone().add(x, y, z);
                        if (loc.getBlock().getType() == Material.TRAPPED_CHEST) {
                            Chest chest = (Chest) loc.getBlock().getState();
                            chest.getBlockInventory().clear();
                        }
                        loc.getBlock().setType(Material.AIR);
                    }
                }
            }
        }
    }

    private void moveTransport(Vector path) {
        if (transport.isValid()) {
            transport.move(path);
            target.setLocation(target.getLocation().add(path));
            if (Math.abs(transport.getLocation().getBlockX() - airdropLoc.getBlockX()) < 3 && Math.abs(transport.getLocation().getBlockZ() - airdropLoc.getBlockZ()) < 3 && !chestDropped) {
                dropChest();
                chestDropped = true;
            }
        } else {
            Bukkit.getScheduler().cancelTask(transportTaskId);
            transport = null;
            target = null;
        }
    }

    private void dropChest() {
        try {
            new AirdropChest(this, airdropLoc.clone().add(0, 200 - airdropLoc.getY(), 0));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DataException e) {
            e.printStackTrace();
        }
    }

    public void placeChest(Location l) {
        chestLoc = l;
        l.getBlock().setType(Material.TRAPPED_CHEST);
        fill(l);
    }

    private void fill(Location l) {
        Chest c = (Chest) l.getBlock().getState();
        int itemAmount = r.nextInt(15) + 10;
        System.out.println("Item Amount: " + itemAmount); //TODO REMOVE!!!
        List<Integer> slotsToFill = new ArrayList<>();
        for (int i = 0; i < 27; i++) slotsToFill.add(i); //Populate list with empty slots in chest

        //Loop once for every item to add
        for (int i = 0; i < itemAmount; i++) {
            //Place an item in a random slot that is yet to be filled then mark the slot as filled
            int slot = slotsToFill.get(r.nextInt(slotsToFill.size()));
            c.getInventory().setItem(slot, randomAirdropItem());
            slotsToFill.remove((Object) slot);
        }
    }

    private ItemStack randomAirdropItem() {
        ItemStack item = airdropItems.next().clone();
        item.setAmount(r.nextInt(item.getAmount()) + 1);
        return item;
    }

    @EventHandler
    public void onChestClick(PlayerInteractEvent event) {
        if (!airdropLoc.getWorld().equals(event.getPlayer().getWorld())) return;
        if (event.getClickedBlock() != null) {
            if(event.getClickedBlock().getLocation().distanceSquared(airdropLoc) < 100 && event.getClickedBlock().getType() == Material.TRAPPED_CHEST) {
                System.out.println("Event ended");
                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> finish(), 100);
                HandlerList.unregisterAll(this);
            }
        }
    }

    @Override
    public String getEventLine1() {
        if (intercepted) {
            return ChatColor.RED + "Airdrop intercepted";
        } else if (chestDropped) {
            return ChatColor.AQUA + "Airdrop landed";
        } else {
            return ChatColor.AQUA + "Airdrop inbound";
        }
    }

    @Override
    public String getEventLine2() {
        if (intercepted) {
            return ChatColor.RED + "" + interceptedLoc.getBlockX() + ", " + interceptedLoc.getBlockZ();
        } else if (chestDropped) {
            return ChatColor.GRAY + "DZ: " + ChatColor.AQUA + "" + airdropLoc.getBlockX() + ", " + airdropLoc.getBlockZ();
        } else {
            return ChatColor.GRAY + "DZ: " + ChatColor.AQUA + "" + airdropLoc.getBlockX() + ", " + airdropLoc.getBlockZ();
        }
    }

    @Override
    public String getEventLine3() {
        if (!(intercepted || chestDropped)) {
            return ChatColor.GRAY + "ETA: " + ChatColor.AQUA + "" + DateUtil.getTimeFromMillis(eta - System.currentTimeMillis());
        }
        return "";
    }

    /**
     * Airdrop transport has been intercepted
     */
    public void explode() {
        intercepted = true;
        interceptedLoc = target.getLocation().clone();
        Bukkit.getScheduler().cancelTask(transportTaskId);
        ((AirdropHelicopter) transport).explode();
    }
}


