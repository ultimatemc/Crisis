package net.crisismc.crisis.events;

import org.bukkit.Location;

public abstract class CrisisEvent {

    private EventScheduler eventScheduler;

    public CrisisEvent(EventScheduler eventScheduler) {
        this.eventScheduler = eventScheduler;
    }

    public abstract void beginNewEvent();
    /**
     * Informed the EventScheduler that the event has finished
     */
    public void finish() {
        eventScheduler.notifyFinished(this);
    }

    public String getEventLine1() {
        return "";
    }

    public String getEventLine2() {
        return "";
    }

    public String getEventLine3() {
        return "";
    }

    public abstract Location getLocation();
}
