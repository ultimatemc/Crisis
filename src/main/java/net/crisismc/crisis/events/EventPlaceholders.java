package net.crisismc.crisis.events;

import be.maximvdw.placeholderapi.PlaceholderAPI;
import net.crisismc.crisis.Crisis;
import org.bukkit.Bukkit;

public class EventPlaceholders {

    private EventScheduler eventScheduler;

    public EventPlaceholders(EventScheduler eventScheduler) {
        this.eventScheduler = eventScheduler;
        if(Bukkit.getPluginManager().isPluginEnabled("MVdWPlaceholderAPI")) {
            registerPlaceholders();
        }
    }

    private void registerPlaceholders() {
        PlaceholderAPI.registerPlaceholder(Crisis.getInstance(), "crisis_event_line1", event -> "" + eventScheduler.getEventLine1());
        PlaceholderAPI.registerPlaceholder(Crisis.getInstance(), "crisis_event_line2", event -> "" + eventScheduler.getEventLine2());
        PlaceholderAPI.registerPlaceholder(Crisis.getInstance(), "crisis_event_line3", event -> "" + eventScheduler.getEventLine3());
    }
}
