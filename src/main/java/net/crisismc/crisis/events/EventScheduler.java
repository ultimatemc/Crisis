package net.crisismc.crisis.events;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.lib.DateUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class EventScheduler {

    //The time between events in m/s
    private static final long EVENT_INTERVAL = 300000;


    private Crisis plugin;
    private Random r = new Random();
    private List<CrisisEvent> events = new ArrayList<>();

    private long nextEvent = 0;
    private int taskID;

    private CrisisEvent currentEvent;


    public EventScheduler(Crisis plugin) {
        this.plugin = plugin;
        new EventPlaceholders(this);
        registerEvent(new Airdrop(plugin, this));
        nextEvent();
    }

    private void registerEvent(CrisisEvent event) {
        events.add(event);
    }

    private void nextEvent() {
        currentEvent = events.get(r.nextInt(events.size()));
        currentEvent.beginNewEvent();
    }

    public void notifyFinished(CrisisEvent event) {
        if (event.equals(currentEvent)) {
            currentEvent = null;
            nextEvent = System.currentTimeMillis() + EVENT_INTERVAL;
            delay();
        }
    }


    private void delay() {
        currentEvent = null;
        taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            if (System.currentTimeMillis() > nextEvent) {
                nextEvent();
                Bukkit.getScheduler().cancelTask(taskID);
            }
        }, 20, 20);
    }

    public String getEventLine1() {
        if (currentEvent != null) {
            return currentEvent.getEventLine1();
        } else {
            return ChatColor.GRAY + "No active events";
        }
    }

    public String getEventLine2() {
        if (currentEvent != null) {
            return currentEvent.getEventLine2();
        } else {
            return ChatColor.GRAY + "Next event in " + DateUtil.getTimeFromMillis(nextEvent - System.currentTimeMillis());
        }
    }

    public String getEventLine3() {
        if (currentEvent != null) {
            return currentEvent.getEventLine3();
        } else {
            return "";
        }
    }


}
