package net.crisismc.crisis.events;

import net.crisismc.crisis.Crisis;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class Train {

    //The material trains use as tracks
    private static Material m = Material.GRAVEL;
    /*
        Info About the train
     */
    //The head (engine) of the train
    private Vector head;
    //Current direction of the head
    private Vector dir;
    private Vector[] trainCars;
    int speed = 0;


    private World world;

    private int taskId;
    private long tick = 0;

    public Train(Location startLoc, Player p) {
        world = startLoc.getWorld();
        head = startLoc.getBlock().getLocation().add(0.5, 0.5, 0.5).toVector();
        trainCars = new Vector[20];

        for (int i = 0; i < trainCars.length; i++) {
            trainCars[i] = head.clone();
        }

        int dirInt = getRotation(p.getLocation().getYaw());

        if (dirInt == 0) dir = new Vector(0, 0, 1);
        else if (dirInt == 1) dir = new Vector(-1, 0, 0);
        else if (dirInt == 2) dir = new Vector(0, 0, -1);
        else dir = new Vector(1, 0, 0);

        trainMovementTask();
    }

    private void trainMovementTask() {
        System.out.println(dir);
        taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(Crisis.getInstance(), () -> {
            tick++;

            //Modify Speed
            if (speed < 20) speed++;

            moveTrainHead();

            //Move train armorstands
            for (int i = 0; i < 20; i++) {
                Vector car = trainCars[i];
                Vector prevCar = (i == 0) ? head.clone() : trainCars[i - 1].clone();

                if (car.distanceSquared(prevCar) >= 25) {
                    Vector pullDir = prevCar.clone().subtract(car);
                    Vector closestDir = null;
                    //Determine where the stand is being "pulled"
                    for (int x = -1; x <= 1; x++) {
                        for (int z = -1; z <= 1; z++) {
                            for (int y = -1; y <= 1; y++) {
                                if (y != 0 && x != 0 && z != 0) continue;
                                if (x == 0 && z == 0) continue;
                                if (car.toLocation(world).add(x, y, z).getBlock().getType() != m) continue;

                                Vector dir = new Vector(x, y, z);
                                double diff = Math.abs(dir.clone().subtract(pullDir).lengthSquared());
                                if (closestDir == null) {
                                    closestDir = dir;
                                } else {
                                    if (diff < Math.abs(closestDir.clone().subtract(pullDir).lengthSquared())) {
                                        closestDir = dir;
                                    }
                                }
                            }
                        }
                    }
                    //if(i == 0)System.out.println(stand.getLocation() + " : " + closestDir);
                    car.add(closestDir);
                }
            }

        }, 1, 1);
    }

    private void moveTrainHead() {
        //Debug
        for (Player p : Bukkit.getOnlinePlayers()) {
            for (Vector car : trainCars) {
                final Location loc = car.toLocation(world);
                p.sendBlockChange(loc, 35, (byte) 0);
                Bukkit.getScheduler().scheduleSyncDelayedTask(Crisis.getInstance(), () -> {
                    p.sendBlockChange(loc, Material.GRAVEL.getId(), (byte) 0);
                }, 1);
            }
        }

        Location standLoc = head.toLocation(world);
        Location l = head.toLocation(world);


        //Forward
        l.add(dir);
        //Bukkit.getOnlinePlayers().forEach(p -> p.sendBlockChange(l, Material.GOLD_BLOCK, (byte) 0));
        if (l.getBlock().getType() == m) {
            move(l);
            dir = l.getBlock().getLocation().toVector().subtract(standLoc.getBlock().getLocation().toVector());
            return;
        }

        //Side
        if (goingDiagonal(dir)) l.subtract(dir); //If going sideways
        l.setX(l.getX() - dir.getZ());
        //Bukkit.getOnlinePlayers().forEach(p -> p.sendBlockChange(l, Material.GOLD_BLOCK, (byte) 0));
        if (l.getBlock().getType() == m) {
            move(l);
            dir = l.getBlock().getLocation().toVector().subtract(standLoc.getBlock().getLocation().toVector());
            speed = 15;
            return;
        }

        //Side
        l.setX(l.getX() + dir.getZ());
        l.setZ(l.getZ() - dir.getX());
        // Bukkit.getOnlinePlayers().forEach(p -> p.sendBlockChange(l, Material.GOLD_BLOCK, (byte) 0));
        if (l.getBlock().getType() == m) {
            move(l);
            dir = l.getBlock().getLocation().toVector().subtract(standLoc.getBlock().getLocation().toVector());
            speed = 15;
            return;
        }

        l.setZ(l.getZ() + dir.getX());
        l.setX(l.getX() + dir.getZ());
        //Bukkit.getOnlinePlayers().forEach(p -> p.sendBlockChange(l, Material.GOLD_BLOCK, (byte) 0));
        if (l.getBlock().getType() == m) {
            move(l);
            dir = l.getBlock().getLocation().toVector().subtract(standLoc.getBlock().getLocation().toVector());
            speed = 15;
            return;
        }

        //Side
        l.setX(l.getX() - dir.getZ());
        l.setZ(l.getZ() + dir.getX());
        // Bukkit.getOnlinePlayers().forEach(p -> p.sendBlockChange(l, Material.GOLD_BLOCK, (byte) 0));
        if (l.getBlock().getType() == m) {
            move(l);
            dir = l.getBlock().getLocation().toVector().subtract(standLoc.getBlock().getLocation().toVector());
            speed = 15;
            return;
        }


        l = standLoc;
        l.add(dir);

        //Down
        l.setZ(l.getZ() + dir.getZ());
        l.setY(l.getY() - 1);
        //Bukkit.getOnlinePlayers().forEach(p -> p.sendBlockChange(l, Material.GOLD_BLOCK, (byte) 0));
        if (l.getBlock().getType() == m) {
            move(l);
            //dir = l.getBlock().getLocation().toVector().subtract(standLoc.getBlock().getLocation().toVector());
            return;
        }

        //Up
        l.setY(l.getY() + 2);
        //Bukkit.getOnlinePlayers().forEach(p -> p.sendBlockChange(l, Material.GOLD_BLOCK, (byte) 0));
        if (l.getBlock().getType() == m) {
            move(l);
            //dir = l.getBlock().getLocation().toVector().subtract(standLoc.getBlock().getLocation().toVector());
            return;
        }

        //If it gets here train is derailed/has no where to go
        if (speed >= 5) {
            destroy();
        }
    }

    private void destroy() {
        Bukkit.getScheduler().cancelTask(taskId);

        //Effects
        world.playEffect(head.toLocation(world), Effect.EXPLOSION_HUGE, 5);
        for (Vector car : trainCars) world.playEffect(car.toLocation(world), Effect.EXPLOSION_HUGE, 5);
    }

    private boolean goingDiagonal(Vector dir) {
        return Math.abs(dir.getX()) > 0 && Math.abs(dir.getZ()) > 0;
    }

    private void move(Location loc) {
        Location newLoc = loc.getBlock().getLocation().add(0.5, 0.5, 0.5);
        head = newLoc.toVector();
    }

    /**
     * Assumes that the default facing direction is south
     *
     * @param yaw of the player placing the BasePart
     */
    private int getRotation(float yaw) {
        return (Math.round(yaw / 90.0F) & 0x3);
    }
}
