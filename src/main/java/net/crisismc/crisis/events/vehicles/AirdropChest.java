package net.crisismc.crisis.events.vehicles;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.schematic.MCEditSchematicFormat;
import net.crisismc.crisis.events.Airdrop;
import net.crisismc.crisis.vehicles.ArmorStandData;
import net.crisismc.crisis.vehicles.ArmorStandVehicle;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Chicken;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AirdropChest extends ArmorStandVehicle {

    private static final Vector movement = new Vector(0, -0.1, 0);
    private Airdrop airdrop;
    private Location loc;

    private boolean chestLanded = false;
    private List<Chicken> chuteChickens = new ArrayList<>();
    private List<Chicken> chestChickens = new ArrayList<>();

    public AirdropChest(Airdrop airdrop, Location loc) throws IOException, DataException {
        super(loc);
        this.airdrop = airdrop;
        this.loc = loc;

        File chuteFile = new File("plugins"+File.separator+"Crisis"+File.separator+"schematics"+File.separator+"airdrop_chute.schematic");
        CuboidClipboard chute = MCEditSchematicFormat.getFormat(chuteFile).load(chuteFile);
        chute.setOffset(new com.sk89q.worldedit.Vector(0, 7, 0));

        File chestFile = new File("plugins"+File.separator+"Crisis"+File.separator+"schematics"+File.separator+"airdrop_chest.schematic");
        CuboidClipboard chest = MCEditSchematicFormat.getFormat(chestFile).load(chestFile);
        chest.setOffset(new com.sk89q.worldedit.Vector(0, 1, 0));

        registerSection("Chest", chest);
        registerSection("Chute", chute);

        setupRopes();
        start();

    }


    private void setupRopes() {
        for(ArmorStandData standData : getSection("Chute").getStands()) {
            if (standData.equipment.getType() == Material.FENCE) {
                Location loc = standData.loc.clone().add(0, 1, 0);

                Chicken chuteChicken = loc.getWorld().spawn(loc, Chicken.class);
                applyProperties(chuteChicken);

                Chicken chestChicken = getLocation().getWorld().spawn(
                        getSection("Chest").getCenter().loc.clone().add(0.625, 1, -0.125)
                                .add(loc.toVector().subtract(getLocation().toVector()).multiply(0.1)),
                        Chicken.class);
                applyProperties(chestChicken);

                chuteChicken.setLeashHolder(chestChicken);

                chuteChickens.add(chuteChicken);
                chestChickens.add(chestChicken);
            }
        }
        /*
        WorldServer s = ((CraftWorld) loc.getWorld()).getHandle();

        int ropeNum = 1;
        for(ArmorStandData sd : getSection("Chute").getStands()) {
            if (sd.equipment.getType() == Material.FENCE) {
                List<ArmorStandData> stands = new ArrayList<>();

                Vector loc = sd.loc.clone().add(0,1,0).toVector();
                Vector chestLoc = getLocation().toVector();
                while (loc.getY() > chestLoc.getY()) {
                    loc.add(chestLoc.clone().subtract(loc).normalize().multiply(0.5));
                    System.out.println(chestLoc.clone().subtract(loc).normalize().multiply(0.5));

                    EntityArmorStand stand = new EntityArmorStand(s);
                    ArmorStandData standData;
                    Location location = loc.toLocation(getLocation().getWorld());
                    ItemStack equipment = new ItemStack(Material.IRON_FENCE, 1);
                    stand.setLocation(loc.getX(), loc.getY(), loc.getZ(), 0, 0);
                    stand.setSlot(EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(equipment));
                    stand.setSmall(true);
                    stand.setInvisible(true);
                    Vector heading = chestLoc.clone().subtract(loc);
                    standData = new ArmorStandData(stand, equipment, loc.toLocation(getLocation().getWorld()).setDirection(heading));
                    stands.add(standData);
                }

                registerSection("rope"+ropeNum, new ArmorStandVehicleSection("rope"+ropeNum, stands, stands.get(stands.size() - 1), new Vector(0,0,0)));
                ropeNum++;
            }
        }
        */

    }

    private void applyProperties(Chicken chicken) {
        chicken.setAI(false);
        chicken.setCollidable(false);
        chicken.setInvulnerable(true);
        chicken.setSilent(true);
        chicken.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1, true, false));
    }

    @Override
    protected void tick(long tick) {
        if (getLocation().getY() + 0.6875 > airdrop.getLocation().getY()) {
            move(movement);
            for (Chicken chicken : chuteChickens) {
                chicken.teleport(chicken.getLocation().add(movement));
            }
            for (Chicken chicken : chestChickens) {
                chicken.teleport(chicken.getLocation().add(movement));
            }
        } else if (getSection("Chute").getCenter().loc.getY() + 1.5 > airdrop.getLocation().getY()) {
            if (!chestLanded) { //Runs once
                getLocation().getWorld().playEffect(getLocation(), Effect.ZOMBIE_CHEW_WOODEN_DOOR, 1);
            }
            for (Chicken chicken : chuteChickens) {
                chicken.teleport(chicken.getLocation().add(movement));
            }
            moveSection("Chute", movement);
            chestLanded = true;
        } else {
            for (ArmorStandData standData : getSection("Chute").getStands()) {
                Location loc = standData.loc.add(0, 1.5, 0);
                loc.getWorld().spigot().playEffect(loc.clone().add(0, 1.5, 0), Effect.TILE_DUST, 35, 13, 0.5F, 0.5F, 0.5F, 0.1F, 4, 64);
            }
            loc.getWorld().playSound(loc, Sound.BLOCK_CLOTH_BREAK, 4, 1);
            airdrop.placeChest(airdrop.getLocation());
            destroy();
            for (Chicken chicken : chuteChickens) {
                chicken.remove();
            }
            for (Chicken chicken : chestChickens) {
                chicken.remove();
            }
        }
    }

    @Override
    public void changeYaw(double degrees) {

    }

    @Override
    public void changePitch(double degrees) {

    }
}
