package net.crisismc.crisis.events.vehicles;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.schematic.MCEditSchematicFormat;
import net.crisismc.crisis.events.Airdrop;
import net.crisismc.crisis.vehicles.ArmorStandVehicle;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.FallingBlock;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.IOException;
import java.util.Random;

public class AirdropHelicopter extends ArmorStandVehicle {

    Location dir;
    private Airdrop airdrop;
    private int endX;
    private int endZ;
    private boolean exploded = false;
    private Random random = new Random();

    //Explode variables
    private Vector descent;

    public AirdropHelicopter(Airdrop airdrop, Location loc, Location target, int endX, int endZ) throws IOException, DataException {
        super(loc);
        this.airdrop = airdrop;
        this.endX = endX;
        this.endZ = endZ;
        descent = target.toVector().subtract(loc.toVector()).normalize();

        File bodyFile = new File("plugins"+File.separator+"Crisis"+File.separator+"schematics"+File.separator+"airdrop_normal.schematic");
        CuboidClipboard body = MCEditSchematicFormat.getFormat(bodyFile).load(bodyFile);
        body.setOffset(new com.sk89q.worldedit.Vector(0, 0, 0));

        File rotorFile = new File("plugins"+File.separator+"Crisis"+File.separator+"schematics"+File.separator+"helicopter_rotor.schematic");
        CuboidClipboard rotor = MCEditSchematicFormat.getFormat(rotorFile).load(rotorFile);
        rotor.setOffset(new com.sk89q.worldedit.Vector(0, 4, 0));

        registerSection("Body", body);
        registerSection("Rotor", rotor);
        start();
    }

    @Override
    protected void tick(long tick) {
        if (!exploded) {
            rotateSection("Rotor", 18);
        } else {
            rotateSection("Rotor", 3);
            descent.setY(descent.getY() - 0.02);
            move(descent);
            if (!getLocation().getBlock().isEmpty()){
                impact();
                destroy();
            }
        }

        //Removal logic
        if (Math.abs(getSection("Body").getCenter().loc.getX() - endX) < 20 || Math.abs(getSection("Body").getCenter().loc.getZ() - endZ) < 20) {
            destroy();
        }
    }

    @Override
    public void changeYaw(double degrees) {

    }

    @Override
    public void changePitch(double degrees) {

    }

    public void explode() {
        exploded = true;
    }

    private void impact() {
        Location impactLoc = getLocation().clone();
        do {
            impactLoc.add(0, 1, 0);
        } while(!impactLoc.getBlock().isEmpty());
        if (!airdrop.chestDropped) {
            airdrop.placeChest(impactLoc.clone().add(0, -1, 0));
        }

        for (int i = 0; i < 20; i++) {
            FallingBlock debris = getLocation().getWorld().spawnFallingBlock(impactLoc.clone().add((random.nextDouble() * 5) - 2.5, 1.5, (random.nextDouble() * 5) - 2.5), Material.WOOL, (byte) 15);
            Vector vector = new Vector((Math.random()/2) - 0.25, (Math.random()/4) + 0.3, (Math.random()/2) - 0.25);
            debris.setVelocity(vector);

            FallingBlock fire = getLocation().getWorld().spawnFallingBlock(impactLoc.clone().add((random.nextDouble() * 5) - 2.5, 1.5, (random.nextDouble() * 5) - 2.5), Material.FIRE, (byte) 0);
            Vector vector2 = new Vector((Math.random()/2) - 0.25, (Math.random()/4) + 0.3, (Math.random()/2) - 0.25);
            fire.setVelocity(vector2);
        }
    }
}
