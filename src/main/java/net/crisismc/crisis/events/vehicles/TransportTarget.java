package net.crisismc.crisis.events.vehicles;

import net.crisismc.crisis.events.Airdrop;
import net.crisismc.crisis.guns.GunUtil;
import net.crisismc.crisis.guns.projectiles.ProjectileType;
import net.crisismc.crisis.guns.targets.Target;
import net.crisismc.crisis.guns.weapons.Gun;
import net.minecraft.server.v1_12_R1.AxisAlignedBB;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

public class TransportTarget implements Target {

    private Location loc;
    private Airdrop airdrop;

    public TransportTarget(Location loc, Airdrop airdrop) {
        this.airdrop = airdrop;
        this.loc = loc;
    }

    @Override
    public boolean onHit(Gun gun, Entity ent, double damage) {
        if (gun.getSpecifications().projectileType == ProjectileType.ROCKET) {
            airdrop.explode();
            return true;
        }
        return false;
    }

    @Override
    public Location isHit(Vector start, Vector path, Vector pathfrac) {
        Location loc = null;
        double dist = GunUtil.getHitDistance(start, pathfrac, getBoundingBox());
        if (dist >= 0) {
            loc = start.add(path.normalize().multiply(dist)).toLocation(getLocation().getWorld());
        }
        return loc;
    }

    @Override
    public Location getLocation() {
        return loc;
    }

    @Override
    public int getPenetrationCost() {
        return 0;
    }

    public AxisAlignedBB getBoundingBox() {
        return new AxisAlignedBB(loc.getX() - 5, loc.getY() - 3, loc.getZ() - 5, loc.getX() + 5, loc.getY() + 3, loc.getZ() + 5);
    }


    public void setLocation(Location loc) {
        this.loc = loc;
    }
}