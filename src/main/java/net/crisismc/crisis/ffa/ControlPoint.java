package net.crisismc.crisis.ffa;

import org.bukkit.Location;
import org.bukkit.Material;

public class ControlPoint {

    private Location loc;

    private int kills = 0;

    public ControlPoint(Location loc) {
        this.loc = loc;
    }

    public void activate() {
        loc.getBlock().setType(Material.AIR);
        kills = 0;
    }

    public void deactivate() {
        loc.getBlock().setType(Material.SMOOTH_BRICK);
    }

    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }
}
