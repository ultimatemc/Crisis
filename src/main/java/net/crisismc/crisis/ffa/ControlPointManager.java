package net.crisismc.crisis.ffa;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.lib.ChatUtil;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ControlPointManager implements Listener {

    public static final int MAX_KILLS = 20;

    private BossBar bossBar;
    private List<ControlPoint> ctrlPoints = new ArrayList<>();
    private int activePoint = -1;

    public ControlPointManager() {
        Crisis.getInstance().getServer().getPluginManager().registerEvents(this, Crisis.getInstance());

        setupBossBar();
        addControlPoints();

        nextPoint();
    }

    private void setupBossBar() {
        bossBar = Bukkit.createBossBar("---", BarColor.BLUE, BarStyle.SEGMENTED_20);

        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.getWorld().getName().equalsIgnoreCase("ffa")) {
                bossBar.addPlayer(p);
            }
        }

    }

    private void addControlPoints() {
        World world = Bukkit.getWorld("ffa");

        ctrlPoints.add(new ControlPoint(new Location(world, -2107, 37, -2022)));
        ctrlPoints.add(new ControlPoint(new Location(world, -2197, 74, -2096)));
        ctrlPoints.add(new ControlPoint(new Location(world, -2154, 27, -2226)));
        ctrlPoints.add(new ControlPoint(new Location(world, -2015, 45, -2270)));
        ctrlPoints.add(new ControlPoint(new Location(world, -2048, 30, -2139)));
    }

    private void nextPoint() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.getWorld().getName().equalsIgnoreCase("ffa")) {
                p.sendMessage(ChatUtil.getPrefix() + ChatColor.GOLD + "Control point kills reached. The next control point is marked by a beacon");
                p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
            }
        }

        activePoint = new Random().nextInt(ctrlPoints.size());

        for(int i = 0; i < ctrlPoints.size(); i++) {
            if (i == activePoint) {
                ctrlPoints.get(i).activate();
            } else {
                ctrlPoints.get(i).deactivate();
            }
        }

        bossBar.setTitle(getTitle(0));
        bossBar.setProgress(0);
    }

    private String getTitle(int kills) {
        return ChatColor.GRAY + "Control Point - " + ChatColor.AQUA + kills + "/" + MAX_KILLS + ChatColor.GRAY + " kills";
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player p = event.getEntity();
        bossBar.removePlayer(p);

        if (!p.getWorld().getName().equalsIgnoreCase("ffa")) return;
        if (p.getKiller() == null) return;

        for (PotionEffect potionEffect : p.getKiller().getActivePotionEffects()) {
            if (potionEffect.getType().getName().equals("FAST_DIGGING")) {

                int kills = ctrlPoints.get(activePoint).getKills();
                kills++;

                ctrlPoints.get(activePoint).setKills(kills);
                bossBar.setTitle(getTitle(kills));
                bossBar.setProgress(((double) kills)/MAX_KILLS);

                if (kills >= MAX_KILLS) {
                    nextPoint();
                }

                break;
            }
        }
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        if (event.getTo().getWorld().getName().equalsIgnoreCase("ffa")) {
            bossBar.addPlayer(event.getPlayer());
        } else {
            bossBar.removePlayer(event.getPlayer());
        }
    }
}
