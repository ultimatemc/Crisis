package net.crisismc.crisis.ffa;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.lib.ChatUtil;
import net.crisismc.crisis.lib.InventorySelector;
import net.crisismc.crisis.lib.ItemBuilder;
import net.crisismc.crisis.lib.UniqueInventoryHolder;
import net.crisismc.crisis.player.CrisisPlayer;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DecimalFormat;
import java.util.*;

import static org.bukkit.ChatColor.*;

public class CrisisFFA {

    private Crisis plugin;
    private World world;

    public Map<UUID, List<Integer>> kitValues = new HashMap<>();

    private InventorySelector<CrisisItem> primarySelector;
    private InventorySelector<CrisisItem> secondarySelector;
    private InventorySelector<CrisisItem> grenadeSelector1;
    private InventorySelector<CrisisItem> grenadeSelector2;

    private Random r = new Random();

    private ArrayList<Location> spawnLocs;


    public CrisisFFA(Crisis plugin) {
        this.plugin = plugin;
        this.world = Bukkit.getWorld("ffa");

        world.setGameRuleValue("naturalRegeneration", "false");
        world.setGameRuleValue("doTileDrops", "false");
        world.setGameRuleValue("keepInventory", "true");
        world.setGameRuleValue("doDaylightCycle", "true");

        spawnLocs = new ArrayList<Location>(){{
            add(new Location(world, -2047, 26.5, -2285));
            add(new Location(world, -1960, 26.5, -2273));
            add(new Location(world, -1946, 26.5, -2204));
            add(new Location(world, -1951, 26.5, -2149));
            add(new Location(world, -1970, 26.5, -2083));
            add(new Location(world, -2059, 31.5, -2029));
            add(new Location(world, -2178, 37.5, -2040));
            add(new Location(world, -2232, 30.5, -2118));
            add(new Location(world, -2196, 26.5, -2190));
            add(new Location(world, -2103, 26.5, -2256));
        }};

        setupSelectors();
        waterDamageTask();

        new ControlPointManager();
        new CrisisFFAListener(this);
    }

    private void setupSelectors() {
        primarySelector = new InventorySelector<>("Primary Weapon", 18);

        List<CrisisItem> primaryGuns = Arrays.asList(
                CrisisItem.AKS74U,
                CrisisItem.M4A4,
                CrisisItem.THOMPSON,
                CrisisItem.MP5,
                CrisisItem.SCARH_DMR,
                CrisisItem.RIFLE_BOLT,
                CrisisItem.M40A1,
                CrisisItem.SHOTGUN_WATERPIPE);

        int i = 0;
        for (CrisisItem gun : primaryGuns) {
            primarySelector.set(i, gun.getItemStack(1), gun);
            i++;
        }

        secondarySelector = new InventorySelector<>("Secondary Weapon", 9);

        List<CrisisItem> secondaryGuns = Arrays.asList(
                CrisisItem.M9,
                CrisisItem.DESERT_EAGLE);

        i = 0;
        for (CrisisItem gun : secondaryGuns) {
            secondarySelector.set(i, gun.getItemStack(1), gun);
            i++;
        }

        grenadeSelector1 = new InventorySelector<>("Grenade", 9);

        List<CrisisItem> grenades1 = Arrays.asList(
                CrisisItem.GRENADE_FRAG,
                CrisisItem.GRENADE_MOLOTOV,
                CrisisItem.GRENADE_GAS);

        i = 0;
        for (CrisisItem grenade : grenades1) {
            grenadeSelector1.set(i, grenade.getItemStack(1), grenade);
            i++;
        }
    }

    private void waterDamageTask() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (player.getWorld().equals(world)) {
                    if (player.getLocation().getBlock().isLiquid()) {
                        player.damage(2);
                    }
                }
            }
        }, 1, 5);
    }

    public void openInventory(Player player) {
        Inventory inv = Bukkit.createInventory(new UniqueInventoryHolder("ffa_kit_menu"), 9, ChatColor.GRAY + "" + ChatColor.BOLD + "FFA");
        for (int i = 0; i < 9; i++) inv.setItem(i, CrisisItem.INVENTORY_ITEM.getItemStack(1));

        List<Integer> values;
        if  (kitValues.containsKey(player.getUniqueId())) {
            values = kitValues.get(player.getUniqueId());
        } else {
            values = Arrays.asList(0, 0, 0, 0, 0);
            kitValues.put(player.getUniqueId(), values);
        }

        inv.setItem(0, createKitItem(primarySelector.get(values.get(0)), "Primary"));
        inv.setItem(1, createKitItem(secondarySelector.get(values.get(1)), "Secondary"));
        inv.setItem(2, createKitItem(grenadeSelector1.get(values.get(2)), "Grenade"));



        //TODO grenades


        inv.setItem(6, CrisisItem.TRAIL_ITEM.getItemStack(1, values.get(4) == 1));

        inv.setItem(8, new ItemBuilder(Material.COMPASS, 1).setName(ChatColor.GOLD + "" + ChatColor.BOLD + "Spawn").toItemStack());

        player.openInventory(inv);
    }

    public void randomSpawn(Player player) {
        CrisisPlayer cp = CrisisPlayer.getCrisisPlayer(player);
        

        player.getInventory().clear();
        player.getInventory().setHeldItemSlot(1);

        //Set items
        List<Integer> values = kitValues.get(player.getUniqueId());
        player.getInventory().addItem(primarySelector.getValue(values.get(0)).getItemStack(1));
        player.getInventory().addItem(secondarySelector.getValue(values.get(1)).getItemStack(1));
        player.getInventory().addItem(grenadeSelector1.getValue(values.get(2)).getItemStack(1));
        player.getInventory().addItem(CrisisItem.BANDAGE.getItemStack(8));

        player.getInventory().setHelmet(CrisisItem.HELMET_COMBAT.getItemStack(1));
        player.getInventory().setChestplate(CrisisItem.SHIRT_BURLAP.getItemStack(1));
        player.getInventory().setLeggings(CrisisItem.TROUSERS_BURLAP.getItemStack(1));
        player.getInventory().setBoots(CrisisItem.SHOES_BURLAP.getItemStack(1));

        player.teleport(spawnLocs.get(r.nextInt(spawnLocs.size())));
        player.sendMessage(ChatUtil.getPrefix() + ChatColor.GRAY + "Press 'F' to reload your weapon.");
    }

    public InventorySelector<CrisisItem> getSelector(int slot) {
        switch (slot) {
            case 0:
                return primarySelector;
            case 1:
                return secondarySelector;
            case 2:
                return grenadeSelector1;
            case 3:
                return grenadeSelector2;
            default:
                return null;
        }
    }

    public void changeValue(Player player, InventorySelector<CrisisItem> selector, int value) {
        List<Integer> values = kitValues.get(player.getUniqueId());

        int index = -1;
        if (selector == primarySelector) {
            index = 0;
        } else if (selector == secondarySelector) {
            index = 1;
        } else if (selector == grenadeSelector1) {
            index = 2;
        } else if (selector == grenadeSelector2) {
            index = 3;
        }

        values.set(index, value);

        kitValues.put(player.getUniqueId(), values);
    }

    public boolean trailEnabled(Player player) {
        if (kitValues.containsKey(player.getUniqueId())) {
            return kitValues.get(player.getUniqueId()).get(4) == 1;
        }
        return false;
    }

    public World getWorld() {
        return world;
    }

    private ItemStack createKitItem(ItemStack itemStack, String type) {
        ItemStack item = itemStack.clone();

        ItemMeta im = item.getItemMeta();
        im.setDisplayName(YELLOW + type + ": " + im.getDisplayName());
        im.setLore(Arrays.asList(GRAY + "Click to change " + type));

        item.setItemMeta(im);

        return item;
    }

    public static String getKillMessage(Player killer, Player killed, double distance) {
        ItemStack killItem = killer.getInventory().getItemInMainHand();
        String gun = "";
        if (killItem.hasItemMeta()) {
            gun = ChatColor.stripColor(killItem.getItemMeta().getDisplayName().split(" <")[0]);
        }

        return GREEN + "" + BOLD + "[+1 Kill] " + GRAY + "You killed " + AQUA + killed.getName() + "" + GRAY + " using " + AQUA + gun + GRAY + " at distance of " + AQUA + formatDouble(distance) + " m";
    }

    public static String getDeathMessage(Player killer, double distance) {
        ItemStack killItem = killer.getInventory().getItemInMainHand();
        String gun = "";
        if (killItem.hasItemMeta()) {
            gun = ChatColor.stripColor(killItem.getItemMeta().getDisplayName().split(" <")[0]);
        }

        return RED + "" + BOLD + "[+1 Death] " + GRAY + "You were killed by " + AQUA + killer.getName() + "" + GRAY + " using " + AQUA + gun + GRAY + " at distance of " + AQUA + formatDouble(distance) + " m";

    }

    private static String formatDouble(double d) {
        DecimalFormat df2 = new DecimalFormat(".#");
        return df2.format(d);
    }
}
