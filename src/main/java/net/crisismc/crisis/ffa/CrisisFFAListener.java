package net.crisismc.crisis.ffa;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.lib.InventorySelector;
import net.crisismc.crisis.lib.UniqueInventoryHolder;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class CrisisFFAListener implements Listener {

    private CrisisFFA ffa;

    public CrisisFFAListener(CrisisFFA ffa) {
        this.ffa = ffa;
        Crisis.getInstance().getServer().getPluginManager().registerEvents(this, Crisis.getInstance());
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (ffa.kitValues.containsKey(event.getPlayer().getUniqueId())) {
            ffa.kitValues.remove(event.getPlayer().getUniqueId());
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getInventory() == null) return;

        if (UniqueInventoryHolder.matchesKey(event.getInventory().getHolder(), "ffa_kit_menu")) {
            ItemStack item = event.getCurrentItem();
            if (item != null) {
                Player player = (Player) event.getWhoClicked();
                if (item.getType() == Material.COMPASS) {
                    ffa.randomSpawn(player);
                } else if (item.getType() == Material.CONCRETE) { //Trails
                    List<Integer> values = ffa.kitValues.get(player.getUniqueId());
                    values.set(4, 1 - values.get(4)); //Flip value 1 -> 0 or 0 -> 1
                    ffa.kitValues.put(player.getUniqueId(), values);
                    ffa.openInventory(player);
                } else {
                    InventorySelector<CrisisItem> invSelector = ffa.getSelector(event.getSlot());
                    if (invSelector != null) {
                        invSelector.getSelection(player).thenAccept(value -> {
                            ffa.changeValue(player, invSelector, value);
                            ffa.openInventory(player);
                        });
                    }
                }
            }
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onItemDrop(PlayerDropItemEvent event) {
        if (event.getPlayer().getWorld().getName().equalsIgnoreCase("ffa")) {
            if (event.getPlayer().getGameMode() != GameMode.CREATIVE) {
                event.setCancelled(true);
            }
        }
    }
}
