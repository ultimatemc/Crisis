package net.crisismc.crisis.guns;

import net.crisismc.crisis.guns.weapons.GunLoader;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


public class CrisisGunAPI {

    public static CrisisGuns crisisGuns;

    public static ItemStack getGun(Material material, short damage) {
        return GunLoader.createGun(material, damage);
    }

    public static ItemStack getGunAmmo(Material material, short damage, int ammo) {
        return GunLoader.getGunAmmo(material, damage, ammo);
    }

    public static ItemStack getGunAmmo(Material material, short damage) {
        return GunLoader.getGunAmmo(material, damage);
    }
}
