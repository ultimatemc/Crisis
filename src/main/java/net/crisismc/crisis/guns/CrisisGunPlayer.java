package net.crisismc.crisis.guns;

import org.bukkit.entity.Player;

public class CrisisGunPlayer {

    public Player p;

    public double speed;
    public boolean scoped = false;

    public CrisisGunPlayer(Player p) {
        this.p = p;
    }

    public boolean isOnGround() {
        return (p.getLocation().add(0, -0.1, 0).getBlock().getType().isSolid()
                || p.getLocation().add(-0.3, -0.1, -0.3).getBlock().getType().isSolid()
                || p.getLocation().add(-0.3, -0.1, 0.3).getBlock().getType().isSolid()
                || p.getLocation().add(0.3, -0.1, 0.3).getBlock().getType().isSolid()
                || p.getLocation().add(-0.3, -0.1, 0.3).getBlock().getType().isSolid());
    }

}