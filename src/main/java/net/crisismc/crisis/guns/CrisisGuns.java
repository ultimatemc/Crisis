package net.crisismc.crisis.guns;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.guns.listeners.GeneralListener;
import net.crisismc.crisis.guns.listeners.PlayerListener;
import net.crisismc.crisis.guns.weapons.Gun;
import net.crisismc.crisis.guns.weapons.GunKey;
import net.crisismc.crisis.guns.weapons.GunLoader;
import net.crisismc.crisis.guns.weapons.GunSpecifications;
import net.crisismc.crisis.lib.ConfigLoader;
import net.crisismc.crisis.lib.PacketUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CrisisGuns {

    public static short GUN_DAMAGE_BUFFER = 32;
    public static short CLIP_DAMAGE_BUFFER = 4;

    public Map<GunKey, GunSpecifications> gunSpecifications = new HashMap<>();
    public Map<UUID, CrisisGunPlayer> gunPlayers = new HashMap<>();
    public int tick = 0;


    final public static int MAX_DISTANCE = 255;


    public CrisisGuns() {
        loadGunSpecs();

        for (Player player : Bukkit.getOnlinePlayers()) {
            gunPlayers.put(player.getUniqueId(), new CrisisGunPlayer(player));
        }

        GunLoader.crisisGuns = this;
        CrisisGunAPI.crisisGuns = this;
        new GeneralListener();
        new PlayerListener(this);

        tickTask();
    }


    private void loadGunSpecs() {
        File folder = new File(Crisis.getInstance().getDataFolder(), "guns");
        if (!folder.exists()) {
            folder.mkdir();
        } else if (!folder.isDirectory()) {
            folder.delete();
            folder.mkdir();
        }
        for (File gunFile : folder.listFiles()) {
            System.out.println(gunFile.getName());
            try {
                GunSpecifications gunSpec = ConfigLoader.loadConfig(GunSpecifications.class, gunFile);
                gunSpecifications.put(new GunKey(gunSpec.gunType, gunSpec.gunTypeDamage), gunSpec);
                System.out.println(gunSpec.gunName + " loaded");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void tickTask() {
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Crisis.getInstance(), () -> {
            tick++;
            for (final Player p : Bukkit.getOnlinePlayers()) {
                CrisisGunPlayer gp = gunPlayers.get(p.getUniqueId());
                ItemStack item = p.getInventory().getItemInMainHand();
                if (gunSpecifications.containsKey(new GunKey(item.getType(), item.getDurability()))) {
                    if (!item.hasItemMeta()) return;
                    final Gun gun = GunLoader.loadGun(item);

                    //Reload check
                    int reloadTime = gun.getReloadTime();
                    if (reloadTime > 0) {
                        gun.setReloadTime(reloadTime - 1);
                        GunSpecifications gunSpec = gunSpecifications.get(new GunKey(item.getType(), item.getDurability()));
                        String reloadStr = "Reloading";
                        int process = (int) Math.ceil((((double) (gunSpec.reloadTime - gun.getReloadTime())) / (double) (gunSpec.reloadTime)) * reloadStr.length());
                        String msg = "";
                        for (int i = 0; i < process; i++) {
                            msg = msg + ChatColor.GREEN + reloadStr.charAt(i);
                        }
                        for (int i = process; i < reloadStr.length(); i++) {
                            msg = msg + ChatColor.GRAY + reloadStr.charAt(i);
                        }
                        PacketUtil.sendActionBarPacket(p, msg);
                        if (reloadTime == 1) {
                            p.getWorld().playSound(p.getLocation(), gunSpec.gunSound + "_clipin", 1, 1);
                            gun.setRemainingAmmo(gun.getReloadAmount(), p);
                        }
                    }

                }
            }
        }, 1, 1);
    }


}
