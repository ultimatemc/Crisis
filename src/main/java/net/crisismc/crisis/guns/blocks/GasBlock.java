package net.crisismc.crisis.guns.blocks;

import net.crisismc.crisis.guns.grenades.Gas;
import net.crisismc.crisis.guns.grenades.GasManager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class GasBlock {

    private Block block;
    private int density;
    private ThreadLocalRandom random;

    /**
     * @param density Refers to how much the GasBlock can spread (density halves each time the GasBlock duplicates)
     */
    public GasBlock(Block block, int density) {
        this.block = block;
        this.density = density;
        GasManager.getInstance().addGasBlock(this);
        random = ThreadLocalRandom.current();
    }

    public void doPhysics(int tick) {
        //Effect with quantity directly proportional to density
        Location pLoc = block.getLocation().add(0.5, 0.5, 0.5);
        if (GasManager.getInstance().random.nextInt(10) == 0) {
            block.getWorld().spawnParticle(Particle.SPELL_MOB,
                    pLoc.getX() + random.nextDouble(-0.5, 0.5),
                    pLoc.getY() + random.nextDouble(-0.5, 0.5),
                    pLoc.getZ() + random.nextDouble(-0.5, 0.5),
                    0,
                    19.0 / 255.0,
                    219.0 / 255.0,
                    73.0 / 255.0,
                    1);//block.getWorld().spawnParticle(Particle.SPELL, pLoc.getX(), pLoc.getY(), pLoc.getZ(), 0, 19.0/255.0, 219.0/255.0, 73.0/255.0, 1); ;
        }
        //Spreading
        if (density > 1) {
            List<Block> blocks = getSpreadBlocks();
            if (blocks.size() == 0) return;
            Block spreadBlock = getSpreadBlocks().get(0);

            //Put out fire
            if (spreadBlock.getType() == Material.FIRE && spreadBlock.getRelative(BlockFace.DOWN).getType() != Material.NETHERRACK) {
                spreadBlock.setType(Material.AIR);
            }

            setDensity(density / 2);
            GasBlock gb = GasManager.getInstance().getGasBlock(spreadBlock);
            if (gb != null) {
                gb.setDensity(gb.getDensity() + density);
            } else {
                new GasBlock(spreadBlock, density);
            }
        } else {
            if (tick % 20 == 0 && GasManager.getInstance().random.nextInt(10) == 0) {
                GasManager.getInstance().removeGasBlock(this);
            }
        }

        //Gravity
        if (GasManager.getInstance().getGasBlock(block.getRelative(BlockFace.DOWN)) == null && block.getRelative(BlockFace.DOWN).isEmpty()) {
            //block.setType(Material.AIR);
            Block oldBlock = block;
            block = block.getRelative(BlockFace.DOWN);
            //block.setType(Gas.MATERIAL);

            GasManager.getInstance().updateGasBlock(this, oldBlock);
        }

        //Compression
        doCompression();

    }

    private void doCompression() {
        GasBlock[] aboveBlocks = getAboveBlocks();
        for (GasBlock gasBlock : aboveBlocks) {
            if (gasBlock == null) return;
        }
        //If it gets here all 3 blocks above are gas
        GasBlock compressedBlock = aboveBlocks[0];
        setDensity(density + compressedBlock.getDensity());
        compressedBlock.setDensity(0);
        GasManager.getInstance().removeGasBlock(compressedBlock);
    }

    private List<Block> getSpreadBlocks() {
        ComparableBlock[] surroundings = new ComparableBlock[]{
                new ComparableBlock(block.getRelative(BlockFace.EAST)), new ComparableBlock(block.getRelative(BlockFace.WEST)),
                new ComparableBlock(block.getRelative(BlockFace.NORTH)), new ComparableBlock(block.getRelative(BlockFace.SOUTH)),
                new ComparableBlock(block.getRelative(BlockFace.DOWN)), new ComparableBlock(block.getRelative(BlockFace.UP)),
        };

        shuffleArray(surroundings);
        Arrays.sort(surroundings);
        List<Block> spreadBlocks = new ArrayList<>();
        for (ComparableBlock cb : surroundings) {
            if (cb.block.getType() == Material.AIR || cb.block.getType() == Gas.MATERIAL || cb.block.getType() == Material.FIRE) spreadBlocks.add(cb.block);
        }

        return spreadBlocks;
    }

    /**
     * Array index is null if no GasBlock is at that location
     * @return An array of the 3 GasBlocks above
     */
    private GasBlock[] getAboveBlocks() {
        GasBlock[] aboveBlocks = new GasBlock[3];
        Block above = block;
        for (int i = 0; i < 3; i++) {
            above = above.getRelative(BlockFace.UP);
            aboveBlocks[i] = GasManager.getInstance().getGasBlock(above);
        }
        return aboveBlocks;
    }

    private void shuffleArray(Object[] a) {
        int n = a.length;
        Random random = new Random();
        random.nextInt();
        for (int i = 0; i < n; i++) {
            int change = i + random.nextInt(n - i);
            swap(a, i, change);
        }
    }

    private void swap(Object[] a, int i, int change) {
        Object helper = a[i];
        a[i] = a[change];
        a[change] = helper;
    }

    public Block getBlock() {
        return block;
    }

    public int getDensity() {
        return density;
    }

    public void setDensity(int density) {
        this.density = density;
    }

}

class ComparableBlock implements Comparable<ComparableBlock> {

    final Block block;

    public ComparableBlock(Block block) {
        this.block = block;
    }

    @Override
    public int compareTo(ComparableBlock other) {
        Block o = other.block;
        GasBlock thisGb = GasManager.getInstance().getGasBlock(this.block);
        if (thisGb != null) {
            GasBlock otherGb = GasManager.getInstance().getGasBlock(o);
            if (otherGb != null) {
                return thisGb.getDensity() - otherGb.getDensity();
            } else {
                return 1;
            }
        } else {
            GasBlock otherGb = GasManager.getInstance().getGasBlock(o);
            if (otherGb != null) {
                return -1;
            } else {
                return GasManager.getInstance().random.nextInt(3) - 1;
            }
        }
    }
}