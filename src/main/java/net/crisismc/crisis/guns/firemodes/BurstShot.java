package net.crisismc.crisis.guns.firemodes;

import net.crisismc.crisis.guns.weapons.Gun;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class BurstShot extends BukkitRunnable {

    private int burst;
    private Gun gun;
    private Player p;

    public BurstShot( Gun gun, Player p, int burst) {
        this.gun = gun;
        this.p = p;
        this.burst = burst;
    }

    @Override
    public void run() {
        if (burst > 1) {
            gun.shoot(p);
            burst--;
        } else {
            this.cancel();
        }
    }
}