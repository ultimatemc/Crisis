package net.crisismc.crisis.guns.firemodes;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.guns.CrisisGuns;
import net.crisismc.crisis.guns.weapons.Gun;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class DelayShot extends BukkitRunnable {

    private CrisisGuns crisisGuns;
    private int counter;
    private Gun gun;
    private Player p;

    public DelayShot(CrisisGuns crisisGuns, Gun gun, Player p, int counter) {
        this.crisisGuns = crisisGuns;
        this.gun = gun;
        this.p = p;
        this.counter = counter;
    }

    @Override
    public void run() {
        if (counter > 0) {
            gun.shoot(p);
            gun.setLastFired(crisisGuns.tick);
            counter--;
        } else {
            this.cancel();
        }
    }
}
