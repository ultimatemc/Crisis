package net.crisismc.crisis.guns.grenades;

import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.guns.CrisisGuns;
import net.crisismc.crisis.guns.GunUtil;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

import java.util.*;

public class Frag implements Grenade {


    public final static Material FRAG = Material.COAL;

    private Player p;
    private CrisisGuns crisisGuns;
    private Projectile proj;
    Random r = new Random();


    public Frag(Player p, CrisisGuns crisisGuns) {
        this.p = p;
        this.crisisGuns = crisisGuns;
    }

    public void throwFrag() {
        proj = new Projectile(crisisGuns, p, p.getEyeLocation(), this, Material.COAL);
        proj.start(p.getEyeLocation(), 25);


        ItemStack item = p.getInventory().getItemInMainHand();

        if (item.getAmount() == 1) {
            p.getInventory().setItemInMainHand(new ItemStack(Material.AIR, 1));
        } else {
            item.setAmount(item.getAmount() - 1);
            p.getInventory().setItemInMainHand(item);
        }

    }

    public void explode(Location loc) {
        Location center = loc.getBlock().getLocation().add(0.5, 0.5, 0.5);
        Map<Block, ArrayList<LivingEntity>> targets = new HashMap<>();

        Item item = p.getWorld().dropItem(loc, new ItemStack(CrisisItem.INVENTORY_BLOCKER.getMaterial(), 1));
        item.getNearbyEntities(11, 11, 11).stream().filter(ent -> ent instanceof LivingEntity).forEach(ent -> {
            Block b = ent.getLocation().getBlock();
            if (targets.containsKey(b)) {
                ArrayList<LivingEntity> ents = targets.get(b);
                ents.add(((LivingEntity) ent));
                targets.replace(b, ents);
            } else {
                ArrayList<LivingEntity> ents = new ArrayList<>();
                ents.add((LivingEntity) ent);
                targets.put(ent.getLocation().getBlock(), ents);
            }
        });
        item.remove();

        List<LivingEntity> hitTargets = new ArrayList<>();

        Vector start = item.getLocation().getBlock().getLocation().add(0.5, 0.5, 0.5).toVector();
        List<Vector> vectors = getExplosionVectors(item.getLocation().getBlock().getLocation().toVector(), Material.STAINED_GLASS, 10, 10, 10, false);
        for (Vector path : vectors) {
            BlockIterator iterator = new BlockIterator(p.getWorld(), start, path, 0, (int) Math.ceil(path.length()));
            while (iterator.hasNext()) {
                Block b = iterator.next();
                if (targets.containsKey(b)) {
                    hitTargets.addAll(targets.get(b));
                    targets.remove(b);
                }
                if (b.getType().isSolid()) break;
            }
        }

        for (LivingEntity ent : hitTargets) {
            ent.damage(30 - (2.5 * center.distance(ent.getLocation())), p);
            ent.setVelocity(ent.getLocation().toVector().subtract(item.getLocation().toVector()).normalize());
            if (ent instanceof Player) {
                Player player = (Player) ent;
                player.playSound(player.getEyeLocation(), Sound.ENTITY_FIREWORK_TWINKLE, 1, 1);
                player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 10, 10));
            }
        }

    }

    @Override
    public boolean onImpact(Vector path, Location loc) {
        return false;
    }

    @Override
    public void onTick(Location loc, int tick) {
        if (tick >= 80) { //Explode
            explodeEffect(loc.getBlock().getLocation().add(0.5, 0.5, 0.5));
            explode(loc);
            proj.end();
        }
    }

    public List<Vector> getExplosionVectors(Vector pos, Material block, double radiusX, double radiusY, double radiusZ, boolean filled) {
        List<Vector> vectors = new ArrayList<>();

        radiusX += 0.5;
        radiusY += 0.5;
        radiusZ += 0.5;

        final double invRadiusX = 1 / radiusX;
        final double invRadiusY = 1 / radiusY;
        final double invRadiusZ = 1 / radiusZ;

        final int ceilRadiusX = (int) Math.ceil(radiusX);
        final int ceilRadiusY = (int) Math.ceil(radiusY);
        final int ceilRadiusZ = (int) Math.ceil(radiusZ);

        double nextXn = 0;
        forX:
        for (double x = 0; x <= ceilRadiusX; ++x) {
            final double xn = nextXn;
            nextXn = (x + 1) * invRadiusX;
            double nextYn = 0;
            forY:
            for (double y = 0; y <= ceilRadiusY; ++y) {
                final double yn = nextYn;
                nextYn = (y + 1) * invRadiusY;
                double nextZn = 0;
                forZ:
                for (double z = 0; z <= ceilRadiusZ; ++z) {
                    final double zn = nextZn;
                    nextZn = (z + 1) * invRadiusZ;

                    double distanceSq = lengthSq(xn, yn, zn);
                    if (distanceSq > 1) {
                        if (z == 0) {
                            if (y == 0) {
                                break forX;
                            }
                            break forY;
                        }
                        break forZ;
                    }

                    if (!filled) {
                        if (lengthSq(nextXn, yn, zn) <= 1 && lengthSq(xn, nextYn, zn) <= 1 && lengthSq(xn, yn, nextZn) <= 1) {
                            continue;
                        }
                    }

                    vectors.add(pos.clone().add(new Vector(x, y, z).subtract(pos)));
                    vectors.add(pos.clone().add(new Vector(-x, y, z).subtract(pos)));
                    vectors.add(pos.clone().add(new Vector(x, -y, z).subtract(pos)));
                    vectors.add(pos.clone().add(new Vector(x, y, -z).subtract(pos)));
                    vectors.add(pos.clone().add(new Vector(-x, -y, z).subtract(pos)));
                    vectors.add(pos.clone().add(new Vector(x, -y, -z).subtract(pos)));
                    vectors.add(pos.clone().add(new Vector(-x, y, -z).subtract(pos)));
                    vectors.add(pos.clone().add(new Vector(-x, -y, -z).subtract(pos)));

                }
            }
        }

        return vectors;
    }

    private static double lengthSq(double x, double y, double z) {
        return (x * x) + (y * y) + (z * z);
    }

    private boolean setBlock(Vector loc, Material mat) {
        Block b = new Location(p.getWorld(), loc.getX(), loc.getY(), loc.getZ()).getBlock();
        b.setType(mat);
        return true;
    }


    public void playShotEffect(Location start, Vector path) {
        Location loc = start;
        Vector step = path.clone().normalize();
        for (int i = 0; i < path.length(); i++) {
            if (i % 5 == 0) {
                loc.getWorld().spigot().playEffect(loc, Effect.EXPLOSION, 0, 0, 0, 0, 0, 0, 1, 128);
            }
            loc.add(step);
        }
    }

    private void playImpactEffect(Location end, Block endBlock, Vector path) {
        if (endBlock == null) return;
        Vector v = path.clone().normalize().multiply(-1);
        v.divide(new Vector(25, 25, 25));
        for (int i = 0; i < 25; i++) {
            end.add(v);
            end.getWorld().spigot().playEffect(end, Effect.TILE_DUST, endBlock.getTypeId(), 0, i * 0.015F, i * 0.015F, i * 0.015F, 0.05F, 1, 32);
        }
    }

    public void playHitEffect(Location loc, LivingEntity ent, Vector path) {
        if (ent instanceof Player) {
            Player t = (Player) ent;
            t.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 10, 5));
        }
        Vector v = path.clone().normalize();
        v.divide(new Vector(25, 25, 25));
        for (int i = 0; i < 35; i++) {
            loc.add(v);
            loc.getWorld().spigot().playEffect(loc, Effect.COLOURED_DUST, 0, 0, i * 0.015F, i * 0.015F, i * 0.015F, 0, 3, 32);
        }
    }

    public void explodeEffect(Location loc) {
        loc.getWorld().spigot().playEffect(loc, Effect.EXPLOSION_LARGE, 0, 0, 0, 0, 0, 1, 20, 128);
        loc.getWorld().spigot().playEffect(loc, Effect.EXPLOSION, 0, 0, 0, 0, 0, 0.25F, 200, 128);
        loc.getWorld().spigot().playEffect(loc, Effect.EXPLOSION, 0, 0, 0, 0, 0, 1, 500, 128);
        loc.getWorld().spigot().playEffect(loc, Effect.EXPLOSION, 0, 0, 0, 0, 0, 5, 100, 128);

        GunUtil.playRealisticSound(loc, Sound.ENTITY_FIREWORK_BLAST, 10, 1);
    }
}
