package net.crisismc.crisis.guns.grenades;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.guns.CrisisGuns;
import net.crisismc.crisis.guns.blocks.GasBlock;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class Gas implements Grenade {

    public final static Material GAS = Material.IRON_NUGGET;
    public final static Material MATERIAL = Material.SLIME_BLOCK;

    private Player p;
    private CrisisGuns crisisGuns;

    private Projectile proj;
    private int TASK_ID;
    private int tick = 0;
    private boolean exploded = false;

    public Gas(Player p, CrisisGuns crisisGuns) {
        this.p = p;
        this.crisisGuns = crisisGuns;
    }

    public void throwGas() {
        proj = new Projectile(crisisGuns, p, p.getEyeLocation(), this, Material.IRON_NUGGET);
        proj.start(p.getEyeLocation(), 25);

        ItemStack item = p.getInventory().getItemInMainHand();

        if (item.getAmount() == 1) {
            p.getInventory().setItemInMainHand(new ItemStack(Material.AIR, 1));
        } else {
            item.setAmount(item.getAmount() - 1);
            p.getInventory().setItemInMainHand(item);
        }
    }

    private void explode(final Location loc) {
        //Slowly release gas
        TASK_ID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Crisis.getInstance(), () -> {
            tick++;
            if (tick == 200) {
                Bukkit.getScheduler().cancelTask(TASK_ID);
                proj.end();
            }
            new GasBlock(loc.getBlock(), 6);
        }, 0, 1);
    }

    @Override
    public boolean onImpact(Vector path, Location loc) {
        return false;
    }

    @Override
    public void onTick(Location loc, int tick) {
        if (tick >= 80 && !exploded) { //Explode
            loc.getWorld().playSound(loc, Sound.ENTITY_CREEPER_PRIMED, 1, 2);
            loc.getWorld().playSound(loc, Sound.BLOCK_FIRE_EXTINGUISH, 1, 0.5F);
            explode(loc);
            exploded = true;
        }
    }
}