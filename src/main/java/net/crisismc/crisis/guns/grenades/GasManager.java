package net.crisismc.crisis.guns.grenades;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.guns.blocks.GasBlock;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.Map;
import java.util.SplittableRandom;
import java.util.WeakHashMap;

public class GasManager {
    private static GasManager ourInstance = new GasManager();

    public static GasManager getInstance() {
        return ourInstance;
    }

    private Map<ImmutableVector, GasBlock> gasBlocks;
    private Map<Player, Integer> playerEffects = new WeakHashMap<>();
    public SplittableRandom random;
    private int tick;

    private GasManager() {
        gasBlocks = new HashMap<>();
        random = new SplittableRandom();
        task();
    }

    private void task() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Crisis.getInstance(), () -> {
            tick++;
            Map<ImmutableVector, GasBlock> gasBlocksClone = new HashMap<>(gasBlocks);
            for (GasBlock gasBlock : gasBlocksClone.values()) {
                gasBlock.doPhysics(tick);
            }

            if (tick % 5 == 0) {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    GasBlock gp = getGasBlock(player.getEyeLocation().getBlock());
                    //We only care if the player's face is in gas not the rest of their body
                    if (gp != null) {
                        int effectTime = playerEffects.containsKey(player) ? playerEffects.get(player) + 1 : 1;
                        playerEffects.put(player, effectTime);

                        player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 200, 3, true, false), true);
                        player.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 20 * effectTime, 2, true, false), true);
                    } else {
                        playerEffects.remove(player);
                    }
                }
            }
        }, 1, 1);
    }

    public GasBlock getGasBlock(ImmutableVector vector) {
        return gasBlocks.get(vector);
    }

    public GasBlock getGasBlock(Block b) {
        ImmutableVector vector = new ImmutableVector(b);
        return gasBlocks.get(vector);
    }

    public void addGasBlock(GasBlock gb) {
        gasBlocks.put(new ImmutableVector(gb.getBlock()), gb);
        //gb.getBlock().setType(Material.SLIME_BLOCK);
    }

    public void removeGasBlock(GasBlock gb) {
        gasBlocks.remove(new ImmutableVector(gb.getBlock()));
        //gb.getBlock().setType(Material.AIR);
    }

    public void updateGasBlock(GasBlock gb, Block oldLoc) {
        //Remove
        ImmutableVector key = new ImmutableVector(oldLoc);
        gasBlocks.remove(key);

        //Add
        ImmutableVector newKey = new ImmutableVector(gb.getBlock());
        gasBlocks.put(newKey, gb);
    }


    class ImmutableVector {

        private final int x;
        private final int y;
        private final int z;

        public ImmutableVector(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public ImmutableVector(Block b) {
            this.x = b.getX();
            this.y = b.getY();
            this.z = b.getZ();
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public int getZ() {
            return z;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof ImmutableVector)) return false;
            ImmutableVector other = (ImmutableVector) obj;
            if (other.getX() == x && other.getY() == y && other.getZ() == z) return true;
            return false;
        }

        @Override
        public int hashCode() {
            int result = 37 + x;
            result = 37 * result + y;
            result = 37 * result + z;
            return result;
        }
    }
}

