package net.crisismc.crisis.guns.grenades;

import org.bukkit.Location;
import org.bukkit.util.Vector;

public interface Grenade {

    boolean onImpact(Vector path, Location loc);

    void onTick(Location loc, int tick);
}
