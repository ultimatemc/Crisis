package net.crisismc.crisis.guns.grenades;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.guns.CrisisGuns;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class Molotov implements Grenade {

    public final static Material MOLOTOV = Material.BLAZE_POWDER;
    private int fireEffectTaskId;

    private Player p;
    private CrisisGuns crisisGuns;
    private Projectile proj;

    public Molotov(Player p, CrisisGuns crisisGuns) {
        this.p = p;
        this.crisisGuns = crisisGuns;
    }


    public boolean throwMolotov() {
        ItemStack item = p.getInventory().getItemInMainHand();
        if (item.getType() != MOLOTOV) return false;

        proj = new Projectile(crisisGuns, p, p.getEyeLocation(), this, Material.BLAZE_POWDER);
        proj.start(p.getEyeLocation(), 25);

        p.playSound(p.getLocation(), Sound.BLOCK_FIRE_AMBIENT, 2, 1);

        if (item.getAmount() == 1) {
            p.getInventory().setItemInMainHand(new ItemStack(Material.AIR, 1));
        } else {
            item.setAmount(item.getAmount() - 1);
            p.getInventory().setItemInMainHand(item);
        }

        return true;
    }

    @Override
    public boolean onImpact(Vector path, Location loc) {
        if (!loc.clone().subtract(path.normalize().multiply(0.1)).getBlock().isLiquid()) {
            explode(path.normalize().multiply(0.5), loc);
        } else {
            loc.getWorld().spawnParticle(Particle.SMOKE_LARGE, loc.add(0, 0.5, 0), 50, 0.5, 0.5, 0.5, 0.1);
            loc.getWorld().playSound(loc, Sound.BLOCK_FIRE_EXTINGUISH, 4F, 0.8F);
            loc.getWorld().playSound(loc, Sound.BLOCK_GLASS_BREAK, 4, 1);
        }
        return true;
    }

    @Override
    public void onTick(Location loc, int tick) {
    }

    private void explode(Vector path, Location loc) {
        loc.getWorld().playEffect(loc, Effect.POTION_BREAK, 0);

        loc.getWorld().playSound(loc, Sound.BLOCK_GLASS_BREAK, 4, 1);

        List<FallingBlock> blocks = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            FallingBlock fire = loc.getWorld().spawnFallingBlock(loc.clone().subtract(path), Material.FIRE, (byte) 0);
            Vector vector = new Vector(Math.random() - 0.5, Math.random() + 0.3, Math.random() - 0.5);
            fire.setVelocity(vector);
            blocks.add(fire);
        }
        for (int i = 0; i < 25; i++) {
            FallingBlock fire = loc.getWorld().spawnFallingBlock(loc.clone().subtract(path), Material.FIRE, (byte) 0);
            Vector vector = new Vector(Math.random() / 4 - 0.125, 0.1, Math.random() - 0.125);
            fire.setVelocity(vector);
            blocks.add(fire);
        }
        fireBlockEffect(blocks);
    }

    private void fireBlockEffect(final List<FallingBlock> blocks) {
        fireEffectTaskId = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Crisis.getInstance(), () -> {
            List<FallingBlock> fireBlocks = blocks;
            List<FallingBlock> blocksToRemove = new ArrayList<>();
            for (FallingBlock fire : fireBlocks) {
                if (fire.isValid()) {
                    for (Entity e : fire.getNearbyEntities(1.5, 1.5, 1.5)) {
                        e.setFireTicks(40);
                    }
                    if (fire.getLocation().getBlock().getType() == Material.AIR) {
                        fire.getLocation().getBlock().setType(Material.FIRE);
                    }
                } else {
                    blocksToRemove.add(fire);
                }
            }
            fireBlocks.removeAll(blocksToRemove);
            blocksToRemove.clear();
            if (fireBlocks.isEmpty()) Bukkit.getServer().getScheduler().cancelTask(fireEffectTaskId);
        }, 1, 1);
    }
}
