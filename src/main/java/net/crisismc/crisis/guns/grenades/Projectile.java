package net.crisismc.crisis.guns.grenades;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.guns.CrisisGuns;
import net.crisismc.crisis.guns.GunUtil;
import net.crisismc.crisis.guns.targets.BlockTarget;
import net.minecraft.server.v1_12_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.Map;


public class Projectile implements Listener {

    private CrisisGuns crisisGuns;
    private Material material;
    private Entity ent;
    private Location loc;
    private Grenade grenade;

    private int tick = 0;
    EntityArmorStand stand;

    private boolean ended = false;

    private static final Map<Material, Double> restitutionModifiers = new HashMap<Material, Double>(){{
        put(Material.GRASS, 0.2);
        put(Material.DIRT, 0.2);
        put(Material.SAND, 0.05);
        put(Material.LEAVES, 0.05);
        put(Material.WOOD, 0.4);
        put(Material.LOG, 0.4);
    }};

    public Projectile(CrisisGuns crisisGuns, Entity ent, Location loc, Grenade grenade, Material material) {
        this.crisisGuns = crisisGuns;
        this.material = material;
        this.ent = ent;
        this.loc = loc.clone();
        this.grenade = grenade;
    }

    public void start(Location start, double velocity) {
        WorldServer s = ((CraftWorld) ent.getLocation().getWorld()).getHandle();

        stand = new EntityArmorStand(s);
        stand.setLocation(ent.getLocation().getX(), ent.getLocation().getY(), ent.getLocation().getZ(), 0, 0);
        stand.setInvisible(true);
        stand.setRightArmPose(new Vector3f(0, -90F,  -18F));

        PacketPlayOutSpawnEntityLiving create = new PacketPlayOutSpawnEntityLiving(stand);
        PacketPlayOutEntityEquipment equipment = new PacketPlayOutEntityEquipment(stand.getId(), EnumItemSlot.MAINHAND, CraftItemStack.asNMSCopy(new ItemStack(material, 1)));
        for (Player p : ent.getWorld().getPlayers()) {
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(create);
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(equipment);
        }

        step(ent.getLocation().getDirection().multiply(velocity / 20.0), start, stand);
    }

    private void step(Vector path, Location start, EntityArmorStand stand) {
        grenade.onTick(start, tick);

        Vector pathfrac = new Vector();
        pathfrac.setX(1.0 / path.getX());
        pathfrac.setY(1.0 / path.getY());
        pathfrac.setZ(1.0 / path.getZ());

        BlockTarget block = GunUtil.getEndBlock(ent.getWorld(), start.toVector(), path.clone(), (int) Math.ceil(path.length()), pathfrac.clone(), null, false);

        double hitDistance = block != null
                ? block.isHit(start.toVector(), path.clone(), pathfrac.clone()).distance(start)
                : path.length();

        Location newStart = start.add(path.clone().normalize().multiply(hitDistance));

        stand.setLocation(newStart.getX(), newStart.getY() - 0.75, newStart.getZ(), 0.0F, 0.0F);

        PacketPlayOutEntityTeleport teleport = new PacketPlayOutEntityTeleport(stand);
        for (Player p : ent.getWorld().getPlayers()) {
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(teleport);
        }

        //newStart.getWorld().spawnParticle(Particle.FIREWORKS_SPARK, newStart.clone().add(0, 0.1, 0), 1, 0, 0,0, 0);

        //Hit a block, do impact calculation
        if (block != null) {
            //If grenade life should not be terminated
            if (!grenade.onImpact(path.clone(), newStart.clone())) {
                impactCalculations(path,  newStart.clone());
                if (path.length() > 0.1) {
                    block.getLocation().getWorld().playSound(block.getLocation(), Sound.BLOCK_ANVIL_PLACE, 1, 1);
                }
            } else { //Remove grenade
                for (Player p : ent.getWorld().getPlayers()) {
                    PacketPlayOutEntityDestroy kill = new PacketPlayOutEntityDestroy(stand.getId());
                    ((CraftPlayer) p).getHandle().playerConnection.sendPacket(kill);
                }
                return;
            }
        }

        //Apply water drag
        if (newStart.getBlock().isLiquid()) {
            if (path.length() > 0.1) {
                path.multiply(0.5);
            }
        }

        //Gravity
        if (!newStart.clone().add(0.0, -0.01, 0.0).getBlock().getType().isSolid()) {
            path.add(new Vector(0,  -0.045, 0));
        }

        if (!ended) {
            tick += block != null ? 3 : 1;
            Bukkit.getScheduler().scheduleSyncDelayedTask(Crisis.getInstance(), () -> step(path, newStart, stand), block != null ? 3 : 1);
        } else {
            for (Player p : ent.getWorld().getPlayers()) {
                PacketPlayOutEntityDestroy kill = new PacketPlayOutEntityDestroy(stand.getId());
                ((CraftPlayer) p).getHandle().playerConnection.sendPacket(kill);
            }
        }
    }

    /**
     * Does the necessary impact calculations on the path vector
     * @param path
     * @param impact
     */
    private void impactCalculations(Vector path, Location impact) {
        Block before = impact.clone().subtract(path.clone().normalize().multiply(0.001)).getBlock();
        Block hit = impact.clone().add(path.clone().normalize().multiply(0.001)).getBlock();

        BlockFace blockFace = hit.getFace(before);

        if (blockFace != null) {
            blockFace = blockFace == BlockFace.SELF ? BlockFace.UP : blockFace;
            if (hit.getType().isSolid() && before.getType().isSolid()) {
                blockFace = BlockFace.UP;
            }

            Vector hitPlain = new Vector(blockFace.getModX(), blockFace.getModY(), blockFace.getModZ());

            //Negate impact component
            path.setX(hitPlain.getX() != 0.0 ? -path.getX() : path.getX());
            path.setY(hitPlain.getY() != 0.0 ? -path.getY() : path.getY());
            path.setZ(hitPlain.getZ() != 0.0 ? -path.getZ() : path.getZ());

            if (restitutionModifiers.containsKey(hit.getType())) {
                path.multiply(restitutionModifiers.get(hit.getType()));
            } else {
                path.multiply(0.4);
            }
        }
    }

    public void end() {
        ended = true;
    }

}
