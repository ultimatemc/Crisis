package net.crisismc.crisis.guns.listeners;


import net.crisismc.crisis.Crisis;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockSpreadEvent;

public class GeneralListener implements Listener {


    public GeneralListener() {
        Crisis.getInstance().getServer().getPluginManager().registerEvents(this, Crisis.getInstance());
    }


    @EventHandler
    public void onBlockBurn(BlockBurnEvent event) {
        event.setCancelled(true);
        if (event.getBlock().getRelative(BlockFace.UP).getType() == Material.FIRE) {
            event.getBlock().getRelative(BlockFace.UP).setType(Material.AIR);
        }
        if (event.getBlock().getRelative(BlockFace.NORTH).getType() == Material.FIRE) {
            event.getBlock().getRelative(BlockFace.NORTH).setType(Material.AIR);
        }
        if (event.getBlock().getRelative(BlockFace.SOUTH).getType() == Material.FIRE) {
            event.getBlock().getRelative(BlockFace.SOUTH).setType(Material.AIR);
        }
        if (event.getBlock().getRelative(BlockFace.EAST).getType() == Material.FIRE) {
            event.getBlock().getRelative(BlockFace.EAST).setType(Material.AIR);
        }
        if (event.getBlock().getRelative(BlockFace.WEST).getType() == Material.FIRE) {
            event.getBlock().getRelative(BlockFace.WEST).setType(Material.AIR);
        }
    }

    @EventHandler
    public void onFireSpread(BlockSpreadEvent event) {
        if (event.getBlock().getType() == Material.AIR) event.setCancelled(true);

    }

    @EventHandler
    public void onBlockPhysics(BlockPhysicsEvent event) {
        if (event.getBlock().getType() == Material.SUGAR_CANE_BLOCK) event.setCancelled(true);
    }

}

