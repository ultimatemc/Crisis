package net.crisismc.crisis.guns.listeners;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.guns.CrisisGunPlayer;
import net.crisismc.crisis.guns.CrisisGuns;
import net.crisismc.crisis.guns.grenades.Frag;
import net.crisismc.crisis.guns.grenades.Gas;
import net.crisismc.crisis.guns.grenades.Molotov;
import net.crisismc.crisis.guns.weapons.Gun;
import net.crisismc.crisis.guns.weapons.GunKey;
import net.crisismc.crisis.guns.weapons.GunLoader;
import net.crisismc.crisis.guns.weapons.GunSpecifications;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PlayerListener implements Listener {

    CrisisGuns crisisGuns;

    public PlayerListener(CrisisGuns crisisGuns) {
        this.crisisGuns = crisisGuns;
        Crisis.getInstance().getServer().getPluginManager().registerEvents(this, Crisis.getInstance());
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player p = event.getPlayer();

        if (!crisisGuns.gunPlayers.containsKey(p.getUniqueId())) {
            crisisGuns.gunPlayers.put(p.getUniqueId(), new CrisisGunPlayer(p));
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player p = event.getPlayer();

        if (crisisGuns.gunPlayers.containsKey(p.getUniqueId())) {
            crisisGuns.gunPlayers.remove(p.getUniqueId());
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        //Throwing/Shooting
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            ItemStack item = event.getPlayer().getInventory().getItemInMainHand();
            Material inHand = item.getType();

            if (crisisGuns.gunSpecifications.containsKey(new GunKey(inHand, item.getDurability()))) {
                if (!item.hasItemMeta()) return;
                Gun gun = GunLoader.loadGun(item);
                if (gun == null) {
                    event.getPlayer().getInventory().setItemInMainHand(GunLoader.createGun(inHand, item.getDurability()));
                    gun = GunLoader.loadGun(item);
                }
                gun.handleClick(event);
            } else if (inHand == Molotov.MOLOTOV) {
                Molotov mov = new Molotov(event.getPlayer(), crisisGuns);
                mov.throwMolotov();
            } else if (inHand == Frag.FRAG) {
                Frag f = new Frag(event.getPlayer(), crisisGuns);
                f.throwFrag();
            } else if (inHand == Gas.GAS) {
                Gas g = new Gas(event.getPlayer(), crisisGuns);
                g.throwGas();
            }

        //Aiming
        } else if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
            ItemStack item = event.getPlayer().getInventory().getItemInMainHand();
            Material inHand = item.getType();
            if (crisisGuns.gunSpecifications.containsKey(new GunKey(inHand, item.getDurability()))) {
                CrisisGunPlayer gp = crisisGuns.gunPlayers.get(event.getPlayer().getUniqueId());
                gp.scoped = !gp.scoped;
                if (gp.scoped) {
                    gp.p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 5, true, false));
                } else {
                    if (gp.p.getPotionEffect(PotionEffectType.SLOW).getAmplifier() == 5) { //Ensures that the slowness effect came from this plugin
                        gp.p.removePotionEffect(PotionEffectType.SLOW);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onHotbarScroll(PlayerItemHeldEvent event) {
        CrisisGunPlayer gp = crisisGuns.gunPlayers.get(event.getPlayer().getUniqueId());
        gp.scoped = false;
        gp.p.removePotionEffect(PotionEffectType.SLOW);
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            Player damager = (Player) event.getDamager();
            ItemStack item = damager.getInventory().getItemInMainHand();
            GunSpecifications gunSpec = crisisGuns.gunSpecifications.get(new GunKey(item.getType(), item.getDurability()));
            if (gunSpec == null) return;

            double dmg = event.getOriginalDamage(EntityDamageEvent.DamageModifier.ARMOR);
            dmg = dmg - (dmg * gunSpec.caliber.armorPenetration);
            event.setDamage(EntityDamageEvent.DamageModifier.ARMOR, dmg);
        }
    }

    @EventHandler
    public void onReload(PlayerSwapHandItemsEvent event) {
        ItemStack item = event.getOffHandItem();
        if (crisisGuns.gunSpecifications.containsKey(new GunKey(item.getType(), item.getDurability()))) {
            Gun gun = GunLoader.loadGun(item);
            gun.reload(event.getPlayer());
        }
        event.setCancelled(true);
    }

    @EventHandler
    public void onItemCraft(CraftItemEvent event) {
        boolean isAmmo = false;
        //Money
        if (event.getRecipe().getResult().getType() == Material.DIAMOND) {
            return;
        }
        for (GunSpecifications gunSpec : crisisGuns.gunSpecifications.values()) {
            if (gunSpec.clipType == event.getRecipe().getResult().getType()) {
                isAmmo = true;
                break;
            }
        }
        if (!isAmmo) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onItemCraftPrep(PrepareItemCraftEvent event) {
        GunSpecifications gunSpec = null;
        for (GunSpecifications gs : crisisGuns.gunSpecifications.values()) {
            if (gs.clipType == event.getRecipe().getResult().getType()) {
                gunSpec = gs;
                break;
            }
        }

        if (gunSpec == null) return;

        CraftingInventory craftInv = event.getInventory();

        int ammo = 0;
        for (ItemStack item : craftInv.getMatrix()) {
            if (item == null || !item.hasItemMeta()) continue;
            ammo += GunLoader.getRemainingAmmo(item);
        }

        if (ammo > gunSpec.clipSize) ammo = gunSpec.clipSize;

        craftInv.setResult(GunLoader.getGunAmmo(craftInv.getResult().getType(), craftInv.getResult().getDurability(), ammo));
    }


    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        Player p = event.getPlayer();
        CrisisGunPlayer gp = crisisGuns.gunPlayers.get(p.getUniqueId());
        gp.speed = event.getFrom().distanceSquared(event.getTo());
    }
}
