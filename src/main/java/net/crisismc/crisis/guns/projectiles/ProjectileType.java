package net.crisismc.crisis.guns.projectiles;

import net.crisismc.crisis.guns.weapons.Gun;
import net.crisismc.crisis.guns.weapons.GunCaliber;
import org.bukkit.entity.Player;

public enum ProjectileType {
    BULLET() {
        @Override
        public WeaponProjectile getProjectile(Gun gun, GunCaliber caliber, Player player) {
            return new Bullet(gun, caliber, player);
        }
    },
    ROCKET() {
        @Override
        public WeaponProjectile getProjectile(Gun gun, GunCaliber caliber, Player player) {
            return new Rocket(gun, caliber, player);
        }
    };

    ProjectileType() {

    }
    public WeaponProjectile getProjectile(Gun gun, GunCaliber caliber, Player player) {
        return null;
    }
}


