package net.crisismc.crisis.guns.projectiles;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.guns.CrisisGunAPI;
import net.crisismc.crisis.guns.CrisisGuns;
import net.crisismc.crisis.guns.GunUtil;
import net.crisismc.crisis.guns.grenades.Frag;
import net.crisismc.crisis.guns.targets.LivingEntityTarget;
import net.crisismc.crisis.guns.targets.Target;
import net.crisismc.crisis.guns.targets.TargetRegistrar;
import net.crisismc.crisis.guns.weapons.Gun;
import net.crisismc.crisis.guns.weapons.GunCaliber;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.SplittableRandom;

public class Rocket implements WeaponProjectile {

    private Gun gun;
    private GunCaliber caliber;
    private LivingEntity entity;
    private SplittableRandom random;

    private Location startLoc;
    private List<Target> possibleTargets;

    public Rocket(Gun gun, GunCaliber caliber, LivingEntity entity) {
        this.gun = gun;
        this.caliber = caliber;
        this.entity = entity;
        random = new SplittableRandom();
    }

    @Override
    public List<Target> fire(List<Target> targets) {
        if (entity.isDead()) return null;
        possibleTargets = targets;
        startLoc = entity.getEyeLocation().add(0, -0.25, 0);

        Vector path = entity.getEyeLocation().getDirection().normalize();
        gun.calcAccuracy(path, entity);
        gun.calcRecoil(path);
        path.normalize();

        Vector pathfrac = new Vector();
        pathfrac.setX(1.0 / path.getX());
        pathfrac.setY(1.0 / path.getY());
        pathfrac.setZ(1.0 / path.getZ());

        // Narrow down the list of possible targets
        Vector min = Vector.getMinimum(entity.getEyeLocation().toVector(), entity.getLocation().toVector().add(path.clone().multiply(CrisisGuns.MAX_DISTANCE)));
        min.add(new Vector(-50, -25, -50));
        Vector max = Vector.getMaximum(entity.getEyeLocation().toVector(), entity.getLocation().toVector().add(path.clone().multiply(CrisisGuns.MAX_DISTANCE)));
        max.add(new Vector(100, 25, 100));

        if (possibleTargets == null) {
            possibleTargets = getPossibleTargets(entity.getWorld(), min, max);
        }

        playShotEffect(startLoc.toVector(), path.clone(), gun.getSpecifications().smokeSteps, gun.getSpecifications().smokeSpacing);

        step(startLoc.toVector(), path.multiply(caliber.muzzleVelocity / 20), caliber.penetration, null);
        return possibleTargets;
    }

    /**
     *
     * @param start
     * @param path Should have a length equal to the velocity (units m / (s * 20))
     * @param penLeft
     * @param ignoredTarget When the previous step ends at a target, passing it to the next target ensures it is ignored
     */
    private void step(Vector start, Vector path, int penLeft, Target ignoredTarget) {
        Vector pathfrac = new Vector();
        pathfrac.setX(1.0 / path.getX());
        pathfrac.setY(1.0 / path.getY());
        pathfrac.setZ(1.0 / path.getZ());

        //Bullet has hit an object and does not have the penetration to continue on
        //Max distance achieved
        if (penLeft <= 0 || start.distanceSquared(startLoc.toVector()) > Math.pow(CrisisGuns.MAX_DISTANCE, 2)) {
            return;
        }

        /*
         Go through the possible targets and figure out which one is closest
         */
        Target closestTarget = null;
        double closestDistSqrd = path.lengthSquared(); //Squared

        //Check block hits first
        Target block = GunUtil.getEndBlock(entity.getWorld(), start.clone(), path.clone(), (int) Math.ceil(path.length()), pathfrac.clone(), ignoredTarget);
        if (block != null) {
            closestTarget = block;
            closestDistSqrd = block.isHit(start, path.clone(), pathfrac.clone()).toVector().distanceSquared(start);
        }

        //Check Target hits
        for (Target target : possibleTargets) {
            if (target.equals(ignoredTarget)) continue;
            Location hitLoc = target.isHit(start.clone(), path.clone(), pathfrac.clone());
            if (hitLoc != null) {
                double dist = hitLoc.toVector().distanceSquared(start);
                if (dist < closestDistSqrd) {
                    closestTarget = target;
                    closestDistSqrd = dist;
                }
            }
        }

        double closestDistance = Math.sqrt(closestDistSqrd);

        //Handle Hit
        int lostPen = 0;
        if (closestTarget != null) {
            lostPen = handleHit(closestTarget, start.clone(), path.clone(), pathfrac.clone(), closestDistance, penLeft);
        }

        //Effects
        shotPathEffect(start.clone(), path.clone(), closestDistance);
        playShotSound(start.toLocation(entity.getWorld()), path.clone(), pathfrac.clone(), closestDistance);

        int nextPen =  penLeft - lostPen;
        int gunPen = gun.getSpecifications().caliber.penetration;
        if (nextPen > 0) path.multiply(((float) (gunPen - lostPen)) / ((float) gunPen));

        Vector nextStart = start.clone().add(path.clone().normalize().multiply(closestDistance));

        Target nextIgnoredTarget = closestTarget;

        //Apply penetration deflection
        Vector deflection = new Vector(Math.random() - 0.5, Math.random() - 0.5, Math.random() - 0.5);
        path.add(deflection.normalize().multiply(Math.sqrt(lostPen) / 2));

        Bukkit.getScheduler().scheduleSyncDelayedTask(Crisis.getInstance(), () -> step(nextStart, path, nextPen, nextIgnoredTarget), 1);
    }

    private int handleHit(Target closestTarget, Vector start, Vector path, Vector pathfrac, double distance, int penLeft) {
        int damage = gun.getSpecifications().caliber.calcDamage(startLoc.distance(closestTarget.getLocation()), path.length() * 20);

        //Penetration
        int pen = closestTarget.getPenetrationCost();
        if (pen < 0) pen = penLeft + 1;

        //Send hit to target
        boolean dead = closestTarget.onHit(gun, entity, damage);
        if (dead) {
            TargetRegistrar.removeCustomTarget(closestTarget);
        }

        //Explosion
        Frag f = new Frag((Player) entity, CrisisGunAPI.crisisGuns);
        Location loc = start.toLocation(entity.getWorld()).add(path.normalize().multiply(distance - 0.5));
        f.explode(loc);
        f.explodeEffect(loc);

        return pen;
    }

    private List<Target> getPossibleTargets(World world, Vector min, Vector max) {
        List<Target> targets = new ArrayList<>();

        for (LivingEntity entity : world.getLivingEntities()) {
            if (entity.equals(this.entity)) continue;
            if (entity.getLocation().toVector().isInAABB(min, max)) targets.add(new LivingEntityTarget(entity));
        }

        for (Target target : TargetRegistrar.getTargets()) {
            if(target.getLocation().toVector().isInAABB(min, max)) targets.add(target);
        }

        return targets;
    }

    private void shotPathEffect(Vector start, Vector path, double dist) {
        for (int i = 0; i < dist; i += 1) {
            entity.getWorld().spawnParticle(Particle.EXPLOSION_NORMAL, start.clone().add(path.clone().normalize().multiply(i)).toLocation(entity.getWorld()), 3, 0, 0, 0, 0.05);
        }
    }

    private void playShotEffect(Vector start, Vector path, int smokeSteps, double smokeSpacing) {
        path.normalize();
        path.multiply(smokeSpacing);
        for (int i = 0; i < smokeSteps; i++) {
            start.add(path);
            entity.getWorld().spawnParticle(Particle.SMOKE_NORMAL, start.toLocation(entity.getWorld()), 4, 0, 0, 0,0F);
        }
    }

    private void playShotSound(Location start, Vector path, Vector pathfrac, double distance) {

    }
}
