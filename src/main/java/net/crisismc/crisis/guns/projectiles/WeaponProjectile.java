package net.crisismc.crisis.guns.projectiles;

import net.crisismc.crisis.guns.targets.Target;

import java.util.List;

public interface WeaponProjectile {

    List<Target> fire(List<Target> possibleTargets);
}
