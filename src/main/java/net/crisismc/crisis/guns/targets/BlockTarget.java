package net.crisismc.crisis.guns.targets;

import net.crisismc.crisis.guns.GunUtil;
import net.crisismc.crisis.guns.weapons.Gun;
import net.minecraft.server.v1_12_R1.AxisAlignedBB;
import net.minecraft.server.v1_12_R1.BlockPosition;
import net.minecraft.server.v1_12_R1.IBlockData;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

public class BlockTarget implements Target {

    private Block block;

    public BlockTarget(Block block) {
        this.block = block;
    }

    @Override
    public boolean onHit(Gun gun, Entity ent, double damage) {
        return false;
    }

    @Override
    public Location isHit(Vector start, Vector path, Vector pathfrac) {

        net.minecraft.server.v1_12_R1.Block mb = net.minecraft.server.v1_12_R1.Block.getById(block.getTypeId());
        BlockPosition bp = new BlockPosition(block.getLocation().getBlockX(), block.getLocation().getBlockY(), block.getLocation().getBlockZ());
        IBlockData bd = ((CraftWorld) block.getWorld()).getHandle().getType(new BlockPosition(block.getX(), block.getY(), block.getZ()));
        AxisAlignedBB aabb = mb.b(bd, ((CraftWorld) block.getWorld()).getHandle(), bp);

        if (block.getType() == Material.PISTON_STICKY_BASE) {
            aabb = new AxisAlignedBB(0.125, 0, 0.125, 0.875, 1, 0.875);
        }
        if (aabb == null) return null;
        aabb = new AxisAlignedBB(block.getLocation().getBlockX() + aabb.a,
                block.getLocation().getBlockY() + aabb.b,
                block.getLocation().getBlockZ() + aabb.c,
                block.getLocation().getBlockX() + aabb.d,
                block.getLocation().getBlockY() + aabb.e,
                block.getLocation().getBlockZ() + aabb.f);

        double dist = GunUtil.getHitDistance(start, pathfrac, aabb);
        if (dist >= 0) {
            Vector loc = start.clone().add(path.clone().multiply(dist));
            return loc.toLocation(block.getWorld());
        }
        return null;

    }

    @Override
    public int getPenetrationCost() {
        switch (block.getType()) {
            case LOG:
                return 25;
            case WATER:
            case STATIONARY_WATER:
                return 10;
            case LEAVES:
                return 1;
            case GLASS:
            case STAINED_GLASS:
            case STAINED_GLASS_PANE:
            case THIN_GLASS:
                return 1;
            case WOODEN_DOOR:
                return 15;
            case WOOD:
                return 20;
            case FENCE:
            case JUNGLE_FENCE:
                return 10;
            case SANDSTONE:
            case SANDSTONE_STAIRS:
                return 30;
            case STONE:
                return 45;
            case COBBLESTONE:
            case COBBLESTONE_STAIRS:
            case MOSSY_COBBLESTONE:
            case SMOOTH_BRICK:
            case SMOOTH_STAIRS:
                return 45;
            case QUARTZ_STAIRS:
                return 5;
            case ACACIA_STAIRS:
                return 10;
            case NETHER_BRICK:
            case NETHER_BRICK_STAIRS:
                return 30;
            case QUARTZ_BLOCK:
                return 40;
            case STAINED_CLAY:
                return 30;
            case DIAMOND_ORE:
            case BRICK:
                return 75;
            case WOOL:
                return 20;
            default:
                return -1;
        }
    }

    @Override
    public Location getLocation() {
        return block.getLocation();
    }

    public Block getBlock() {
        return block;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BlockTarget) {
            return ((BlockTarget) obj).getBlock().equals(block);
        }
        return false;
    }


}
