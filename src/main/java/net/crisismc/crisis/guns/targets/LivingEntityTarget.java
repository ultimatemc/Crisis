package net.crisismc.crisis.guns.targets;

import net.crisismc.crisis.guns.GunUtil;
import net.crisismc.crisis.guns.weapons.Gun;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

public class LivingEntityTarget implements Target {

    private final LivingEntity entity;

    public LivingEntityTarget(LivingEntity entity) {
        this.entity = entity;
    }

    @Override
    public boolean onHit(Gun gun, Entity ent, double damage) {
        entity.damage(damage, ent);
        entity.setNoDamageTicks(0);

        return entity.isDead();
    }

    @Override
    public Location isHit(Vector start, Vector path, Vector pathfrac) {
        double dist = GunUtil.getHitDistance(start.clone(), pathfrac.clone(), ((CraftEntity) entity).getHandle().getBoundingBox());
        if (dist >= 0) {
            Vector loc = start.clone().add(path.clone().multiply(dist));
            return loc.toLocation(entity.getWorld());
        }
        return null;
    }

    @Override
    public int getPenetrationCost() {
        return 10;
    }

    @Override
    public Location getLocation() {
        return entity.getLocation();
    }

    public LivingEntity getEntity() {
        return entity;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof LivingEntityTarget) {
            return ((LivingEntityTarget) obj).getEntity().equals(entity);
        }
        return false;
    }
}
