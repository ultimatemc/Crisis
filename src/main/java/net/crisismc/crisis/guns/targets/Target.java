package net.crisismc.crisis.guns.targets;


import net.crisismc.crisis.guns.weapons.Gun;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

public interface Target {

    /**
     * @param gun The gun that inflicted the hit
     * @param ent The entity which fired the gun
     * @param damage The suggested damage to apply
     * @return Should return true if the CustomTarget has been destroyed
     */
    boolean onHit(Gun gun, Entity ent, double damage);

    /**
     * @param start
     * @param path
     * @param pathfrac
     * @return Returns where the given ray hits the target, null if it does not
     */
    Location isHit(Vector start, Vector path, Vector pathfrac);

    Location getLocation();

    /**
     *
     * @return
     */
    int getPenetrationCost();
}
