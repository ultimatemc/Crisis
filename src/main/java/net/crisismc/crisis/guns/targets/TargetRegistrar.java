package net.crisismc.crisis.guns.targets;

import java.util.ArrayList;
import java.util.List;

public class TargetRegistrar {

    private static List<Target> targets = new ArrayList<>();

    public static void registerCustomTarget(Target target) {
        targets.add(target);
    }

    public static List<Target> getTargets() {
        return targets;
    }

    public static void removeCustomTarget(Target target) {
        if (targets.contains(target)) {
            targets.remove(target);
        }
    }
}