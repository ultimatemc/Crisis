package net.crisismc.crisis.guns.weapons;

public interface DamageDropoff {

    int damageLost(double distance, double velocity);
}
