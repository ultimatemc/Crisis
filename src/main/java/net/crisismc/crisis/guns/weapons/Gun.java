package net.crisismc.crisis.guns.weapons;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.guns.CrisisGunPlayer;
import net.crisismc.crisis.guns.CrisisGuns;
import net.crisismc.crisis.guns.GunUtil;
import net.crisismc.crisis.guns.firemodes.BurstShot;
import net.crisismc.crisis.guns.firemodes.DelayShot;
import net.crisismc.crisis.guns.projectiles.Bullet;
import net.crisismc.crisis.guns.projectiles.WeaponProjectile;
import net.crisismc.crisis.guns.targets.Target;
import net.crisismc.crisis.lib.ChatUtil;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Gun {

    private String uuid;
    private ItemStack item;

    private final GunSpecifications gunSpecifications;
    private List<GunAttachment> attachments;

    //Transient
    private int lastFired = 0;
    private int lastClick = 0;
    private int reloadTime = 0;
    private int reloadAmount = 0;
    private boolean auto = false;

    //Transient recoil variables
    private double vertRecoil = 0;
    private double horiRecoil = 0;

    private final double VERTICAL_RECOIL = 64;
    private final double HORIZONTAL_RECOIL = 64;
    private final double RECOIL_RESET_TICKS = 4;

    private CrisisGuns crisisGuns;

    public Gun(CrisisGuns crisisGuns, GunSpecifications gunSpecifications, ItemStack item) {
        this.crisisGuns = crisisGuns;
        this.gunSpecifications = gunSpecifications;
        this.item = item;

        uuid = UUID.randomUUID().toString();
        attachments = new ArrayList<>();

    }

    public Gun(CrisisGuns crisisGuns, String uuid, ItemStack item, GunSpecifications gunSpecifications, List<GunAttachment> attachments) {
        this.crisisGuns = crisisGuns;
        this.uuid = uuid;
        this.item = item;
        this.gunSpecifications = gunSpecifications;
        this.attachments = attachments;
    }

    public void handleClick(PlayerInteractEvent event) {
        Player p = event.getPlayer();

        //Reset recoil
        double ticks = crisisGuns.tick - getLastFired();
        if (ticks >= RECOIL_RESET_TICKS) {
            vertRecoil = horiRecoil = 0;
        }

        if (gunSpecifications.auto) {
            if ((crisisGuns.tick - getLastClick()) >= 3 && (crisisGuns.tick - getLastClick()) <= 5) {
                setAuto(true);
            } else {
                setAuto(false);
            }
        }

        int remainingAmmo = getRemainingAmmo();
        if (remainingAmmo == 0) p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 1);
        if ((crisisGuns.tick - getLastFired()) >= gunSpecifications.bulletDelay && remainingAmmo > 0) {
            shoot(p);
            setLastFired(crisisGuns.tick);
            if (gunSpecifications.roundsPerBurst > 1) {
                new BurstShot(this, p, gunSpecifications.roundsPerBurst).runTaskTimer(Crisis.getInstance(), 0, 1);
            } else if (isAuto()) {
                int bulletDelay = gunSpecifications.bulletDelay;
                new DelayShot(crisisGuns, this, p, (4 / bulletDelay) - 1).runTaskTimer(Crisis.getInstance(), bulletDelay, bulletDelay);
            }
        }

        setLastClick(crisisGuns.tick);
    }

    public void shoot(Player p) {
        int ammo = getRemainingAmmo();
        if (ammo < 1) {
            p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 1);
            return;
        }
        List<Target> possibleTargets = null;
        for (int i = 0; i < gunSpecifications.bulletsPerRound; i++) {
            WeaponProjectile projectile = gunSpecifications.projectileType.getProjectile(this, gunSpecifications.caliber, p);
            setRemainingAmmo(ammo - 1, p);
            possibleTargets = projectile.fire(possibleTargets);
        }

        GunUtil.playRealisticSound(p.getLocation(), gunSpecifications.gunSound, gunSpecifications.gunSoundVolume, 1, 24, "_distant");

        if (getRemainingAmmo() == 0) reload(p);
    }

    public void reload(Player p) {
        if (getReloadTime() > 0) return; //Already reloading
        if (getRemainingAmmo() == gunSpecifications.clipSize) return; //No need to reload

        p.getWorld().playSound(p.getLocation(), gunSpecifications.gunSound + "_clipout", 1, 1);

        int clipAmmo = 0;
        if (p.getWorld().getName().equals(Crisis.WORLD_NAME)) { //Allow free reloads in ffa and spawn
            for (int index = 0; index < p.getInventory().getSize(); index++) {
                ItemStack item = p.getInventory().getItem(index);
                if (item == null) continue;
                if (item.getType() == gunSpecifications.clipType && item.getDurability() == gunSpecifications.clipTypeDamage && item.hasItemMeta()) {
                    clipAmmo = GunLoader.getRemainingAmmo(item);
                    if (item.getAmount() == 1)
                        p.getInventory().setItem(index, null);
                    else
                        item.setAmount(item.getAmount() - 1);
                    break;
                }
            }

            int ammo = getRemainingAmmo();
            if (ammo != 0) {
                ItemStack clip = GunLoader.getGunAmmo(gunSpecifications.clipType, gunSpecifications.clipTypeDamage, ammo);
                p.getInventory().addItem(clip);
            }

            if (clipAmmo == 0) {
                if (ammo == 0) {
                    p.sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "Out of ammo");
                }
                setRemainingAmmo(0, p);
                return;
            }
        } else {
            clipAmmo = gunSpecifications.clipSize;
        }

        setRemainingAmmo(0, p);
        setReloadTime(gunSpecifications.reloadTime);
        setReloadAmount(clipAmmo);
    }

    public void calcAccuracy(Vector path, LivingEntity shooter) {
        CrisisGunPlayer gp = crisisGuns.gunPlayers.get(shooter.getUniqueId());
        boolean moving = false;
        boolean jumping = !gp.isOnGround();

        if (!((Player) shooter).isSneaking()) {
            if (gp.speed > 0.04) moving = true;
        }

        if (jumping) {
            path.add(new Vector((Math.random() / gunSpecifications.jumpingAccuracy) - (1 / (gunSpecifications.jumpingAccuracy * 2)),
                    (Math.random() / gunSpecifications.jumpingAccuracy) - (1 / (gunSpecifications.jumpingAccuracy * 2)),
                    (Math.random() / gunSpecifications.jumpingAccuracy) - (1 / (gunSpecifications.jumpingAccuracy * 2))));
        } else if (moving) {
            path.add(new Vector((Math.random() / gunSpecifications.movingAccuracy) - (1 / (gunSpecifications.movingAccuracy * 2)),
                    (Math.random() / gunSpecifications.movingAccuracy) - (1 / (gunSpecifications.movingAccuracy * 2)),
                    (Math.random() / gunSpecifications.movingAccuracy) - (1 / (gunSpecifications.movingAccuracy * 2))));
        } else {
            path.add(new Vector((Math.random() / gunSpecifications.accuracy) - (1 / (gunSpecifications.accuracy * 2)),
                    (Math.random() / gunSpecifications.accuracy) - (1 / (gunSpecifications.accuracy * 2)),
                    (Math.random() / gunSpecifications.accuracy) - (1 / (gunSpecifications.accuracy * 2))));
        }
    }

    public void calcRecoil(Vector path) {
        double vertMod = (Math.random() / VERTICAL_RECOIL);
        double horiMod = ((Math.random() / HORIZONTAL_RECOIL) - (1 / (HORIZONTAL_RECOIL * 2)));

        vertRecoil += vertMod;
        horiRecoil += horiMod;

        Vector horiRecoilDir = new Vector(path.getZ(), 0, -path.getX());
        Vector vertRecoilDir = path.getCrossProduct(horiRecoilDir);

        horiRecoilDir.normalize();
        vertRecoilDir.normalize();

        path.add(vertRecoilDir.multiply(vertRecoil));
        path.add(horiRecoilDir.multiply(horiRecoil));
    }

    public int getRemainingAmmo() {
        return GunLoader.getRemainingAmmo(item);
    }

    public GunSpecifications getSpecifications() {
        return gunSpecifications;
    }

    public String getUuid() {
        return uuid;
    }

    public ItemStack getItem() {
        return item;
    }

    public void setItem(ItemStack item) {
        this.item = item;
    }

    public List<GunAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<GunAttachment> attachments) {
        this.attachments = attachments;
        GunLoader.saveGun(this);
    }

    public int getLastFired() {
        return lastFired;
    }

    public void setLastFired(int lastFired) {
        this.lastFired = lastFired;
    }

    public int getReloadTime() {
        return reloadTime;
    }

    public void setReloadTime(int reloadTime) {
        this.reloadTime = reloadTime;
    }

    public int getReloadAmount() {
        return reloadAmount;
    }

    public void setReloadAmount(int reloadAmount) {
        this.reloadAmount = reloadAmount;
    }

    public void setRemainingAmmo(int ammo, Player p) {
        GunLoader.setRemainingAmmo(item, ammo, p);
    }

    public int getLastClick() {
        return lastClick;
    }

    public void setLastClick(int lastClick) {
        this.lastClick = lastClick;
    }

    public boolean isAuto() {
        return auto;
    }

    public void setAuto(boolean auto) {
        this.auto = auto;
    }

    public void bulletTest(Player player) {
        new Bullet(this, gunSpecifications.caliber, player).fire(null);
    }
}
