package net.crisismc.crisis.guns.weapons;

public enum GunCaliber {

    M9("9mm", 4, 375, 0.02, 1, 10, 0, 2),
    M40A1("7.62×51mm", 18, 800, 0.01, 1, 40, 0, 2),
    WINCHESTER_MODEL_70(".243 Winchester", 12, 900, 0.01, 1, 35, 0, 2),
    AK47("7.62×39mm", 5, 715, 0.01, 1, 30, 0, 2),
    DESERT_EAGLE(".44 Magnum", 7, 470, 0.02, 1, 20, 0, 2),
    M16A4("5.56×45mm", 5, 960, 0.02, 1, 26, 0, 2),
    MP5("9mm", 4, 375, 0.02, 1, 10, 0, 2),
    SCAR("7.62x51mm", 8, 715, 0.01, 1, 35, 0, 2),
    WATERPIPE("Birdshot shell", 2, 300, 0.03, 1, 2, 0, 2),
    THOMPSON(".45 ACP", 4, 285, 0.02, 1, 8, 0, 2),
    LAW("66 mm rocket", 20, 145, 0, 1, 1, 0, 1);


    public String name;

    public final int damage;
    public final double muzzleVelocity;
    public final double distanceDropoff;
    public final double velocityDropoff;
    public final int penetration;
    public final int armorPenetration;
    public final double headshotMultiplier;

    /**
     *
     * @param name
     * @param damage The initial damage of the bullet (hit points)
     * @param muzzleVelocity The velocity the bullet should exit the gun at (m/s)
     * @param distanceDropoff A constant applied to the distance when calculating damage
     * @param velocityDropoff A constant applied to the ratio between current and muzzle velocity when calculating damage
     * @param penetration
     * @param armorPenetration
     * @param headshotMultiplier
     */
    GunCaliber(String name, int damage, double muzzleVelocity, double distanceDropoff, double velocityDropoff, int penetration, int armorPenetration, double headshotMultiplier) {
        this.name = name;
        this.damage = damage;
        this.distanceDropoff = distanceDropoff;
        this.muzzleVelocity = muzzleVelocity;
        this.velocityDropoff = velocityDropoff;
        this.penetration = penetration;
        this.armorPenetration = armorPenetration;
        this.headshotMultiplier = headshotMultiplier;
    }

    public int calcDamage(double distance, double velocity) {
        return (int) Math.round((damage - (distanceDropoff * distance)) / (velocityDropoff * (muzzleVelocity / velocity)));
    }
}
