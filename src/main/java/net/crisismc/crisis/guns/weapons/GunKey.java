package net.crisismc.crisis.guns.weapons;

import org.bukkit.Material;

/**
 * Created by uksspy on 11/4/2017.
 */
public class GunKey {

    public final Material mat;
    public final short damage;

    public GunKey(Material mat, short damage) {
        this.mat = mat;
        this.damage = damage;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof GunKey) {
            GunKey gk = ((GunKey) obj);
            return gk.mat == mat && gk.damage == damage;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return 31 * mat.hashCode() * damage;
    }
}
