package net.crisismc.crisis.guns.weapons;

import net.crisismc.crisis.guns.CrisisGuns;
import net.crisismc.crisis.lib.ItemBuilder;
import net.crisismc.crisis.lib.NbtTags;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GunLoader {

    public static CrisisGuns crisisGuns;

    private static Map<String, Gun> loadedGuns = new HashMap<>();

    public static ItemStack createGun(Material mat, short damage) {
        if (crisisGuns.gunSpecifications.containsKey(new GunKey(mat, damage))) {
            GunSpecifications gunSpecs = crisisGuns.gunSpecifications.get(new GunKey(mat, damage));

            ItemStack item = new ItemBuilder(mat, 1)
                    .setName(ChatColor.translateAlternateColorCodes('&',gunSpecs.gunName) + ChatColor.DARK_AQUA + " <0>")
                    .toItemStack();

            Gun gun = new Gun(crisisGuns, gunSpecs, item);
            item = saveGun(gun);
            loadedGuns.put(gun.getUuid(), gun);

            return item;
        }
        return null;
    }

    public static Gun loadGun(ItemStack item) {
        if (crisisGuns.gunSpecifications.containsKey(new GunKey(item.getType(), item.getDurability()))) {
            String uuid = NbtTags.getNBTData(item, "crisis_gun_uuid");
            if (uuid != null && !uuid.equals("")) {
                if (loadedGuns.containsKey(uuid)) {
                    return loadedGuns.get(uuid);
                } else {
                    /*List<GunAttachment> attachments = Arrays
                            .stream(NbtTags.getNBTData(item, "crisis_gun_attachments").split(","))
                            .map(attachmentStr -> GunAttachment.valueOf(attachmentStr)).collect(Collectors.toList());*/

                    Gun gun = new Gun(crisisGuns, uuid, item, crisisGuns.gunSpecifications.get(new GunKey(item.getType(), item.getDurability())), new ArrayList<>());
                    loadedGuns.put(gun.getUuid(), gun);
                    return gun;
                }
            }
        }
        return null;
    }

    public static ItemStack saveGun(Gun gun) {
        ItemStack item = gun.getItem();
        item = NbtTags.setNBTData(item, "crisis_gun_uuid", gun.getUuid());
        /*item = NbtTags.setNBTData(item, "crisis_gun_attachments", gun.getAttachments().stream().map(GunAttachment::toString)
                .collect(Collectors.joining(",")));*/

        gun.setItem(item);

        return item;
    }

    public static ItemStack getGunAmmo(Material material, short damage, int ammo) {
        GunSpecifications gunSpec = null;
        for (GunSpecifications gs : crisisGuns.gunSpecifications.values()) {
            if (gs.clipType == material && gs.clipTypeDamage == damage) {
                gunSpec = gs;
                break;
            }
        }

        if (gunSpec != null) {
            ItemStack clip = new ItemStack(gunSpec.clipType);
            ItemMeta im = clip.getItemMeta();
            clip.setDurability(damage);

            im.setDisplayName(ChatColor.translateAlternateColorCodes('&',gunSpec.gunName) + " " + gunSpec.clipName + ChatColor.DARK_AQUA + " <" + ammo + ">");
            clip.setItemMeta(im);

            return NbtTags.setUnbreakable(clip);
        } else {
            return null;
        }
    }

    public static ItemStack getGunAmmo(Material material, short damage) {
        GunSpecifications gunSpec = null;
        for (GunSpecifications gs : crisisGuns.gunSpecifications.values()) {
            if (gs.clipType == material && gs.clipTypeDamage == damage) {
                gunSpec = gs;
                break;
            }
        }

        if (gunSpec != null) {
            ItemStack clip = new ItemStack(gunSpec.clipType);
            int ammo = gunSpec.clipSize;
            ItemMeta im = clip.getItemMeta();
            clip.setDurability(damage);

            im.setDisplayName(ChatColor.translateAlternateColorCodes('&',gunSpec.gunName) + " " + gunSpec.clipName + ChatColor.DARK_AQUA + " <" + ammo + ">");
            clip.setItemMeta(im);

            return NbtTags.setUnbreakable(clip);
        } else {
            return null;
        }
    }

    public static void setRemainingAmmo(ItemStack item, int ammo, Player p) {
        ItemMeta im = item.getItemMeta();
        String name = im.getDisplayName();
        name = name.split("<")[0] + "<" + ammo + ">";
        im.setDisplayName(name);
        item.setItemMeta(im);
        p.getInventory().setItemInMainHand(item);
    }

    public static int getRemainingAmmo(ItemStack item) {
        return Integer.parseInt(item.getItemMeta().getDisplayName().split("<")[1].replaceAll(">", ""));
    }
}
