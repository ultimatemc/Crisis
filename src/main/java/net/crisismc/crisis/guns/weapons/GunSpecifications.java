package net.crisismc.crisis.guns.weapons;

import net.crisismc.crisis.guns.projectiles.ProjectileType;
import org.bukkit.Material;

public class GunSpecifications {

    public String gunName = "Gun";

    public Material gunType = Material.BONE;
    public short gunTypeDamage = 0;
    public ProjectileType projectileType = ProjectileType.BULLET;
    public Material clipType = Material.SLIME_BALL;
    public short clipTypeDamage = 0;

    public String clipName = "Mag";

    public int roundsPerBurst = 1;
    public int bulletsPerRound = 1;
    public int bulletDelay = 1;
    public boolean auto = true;

    public double accuracy = 0;
    public double movingAccuracy = 0;
    public double jumpingAccuracy = 0;


    public int clipSize = 30;
    public int reloadTime = 20;

    public double smokeSpacing = 1;
    public int smokeSteps = 5;

    public String gunSound = "m40a1";
    public float gunSoundVolume = 0;
    public String reloadSound = "m40a1";

    public GunCaliber caliber = GunCaliber.M40A1;

    public GunSpecifications() {
    }
}
