package net.crisismc.crisis.inventory;


import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.*;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.lib.Callback;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Uses an anvil inventory to prompt the user to enter a String
 * Passes that string to caller using the Callback::callback() method
 */
public class AnvilNameSession implements Listener {

    private Crisis plugin;
    private Callback callback;
    private String title;
    private Player player;
    private ProtocolManager pm;
    private PacketListener pl;

    private String input;

    private Inventory inv;

    public AnvilNameSession(Crisis plugin, Callback callback, String title, ItemStack item, Player player, ProtocolManager pm) {
        this.plugin = plugin;
        this.callback = callback;
        this.title = title;
        this.player = player;
        this.pm = pm;

        inv = Bukkit.createInventory(null, InventoryType.ANVIL);
        inv.setItem(0, item);

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        pl = setupPacketListener();
    }

    private PacketListener setupPacketListener() {
        PacketListener pl = new PacketAdapter(plugin, ListenerPriority.NORMAL, PacketType.Play.Client.WINDOW_CLICK) {
            @Override
            public void onPacketReceiving(PacketEvent event) {
                if (!event.getPlayer().equals(player)) return;
                event.setCancelled(true);
                Bukkit.getScheduler().runTask(plugin, () -> {
                    if (event.getPacketType() == PacketType.Play.Client.WINDOW_CLICK) {
                        PacketContainer packet = event.getPacket();

                        int slot = packet.getIntegers().readSafely(1);
                        if (slot == 2) {
                            ItemStack itemStack = packet.getItemModifier().readSafely(0);
                            if (itemStack != null && itemStack.hasItemMeta()) {
                                callback.callback(title + ":" + itemStack.getItemMeta().getDisplayName());
                                player.closeInventory();
                                end();
                            } else {
                                callback.callback(title + ":");
                                player.closeInventory();
                                end();
                            }
                        }
                    }
                });
            }
        };
        pm.addPacketListener(pl);
        return pl;
    }
    /*
    @Override
    public void callback(String result) {
        System.out.println(result);
        input = result;
    }
    */

    public void openInv() {
        player.openInventory(inv);
    }

    private void end() {
        HandlerList.unregisterAll(this);
        pm.removePacketListener(pl);
    }

    /*
        Listeners
     */

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        if (!event.getPlayer().equals(player)) return;
        end();

    }
}
