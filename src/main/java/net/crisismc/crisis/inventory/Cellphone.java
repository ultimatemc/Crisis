package net.crisismc.crisis.inventory;

import net.md_5.bungee.api.ChatColor;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.blocks.Camera;
import net.crisismc.crisis.blocks.CameraSession;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Cellphone implements Listener {

    public final static String INV_NAME = ChatColor.DARK_GRAY + "Cameras";

    public static void openInventory(Player p) {
        Inventory inv = Bukkit.createInventory(null, 54, INV_NAME);

        int slot = 0;
        for (Camera cam : Crisis.getInstance().getBlockHandler().cameras.values()) {
            if (slot > 53) break;
            if (cam.getLoc().distanceSquared(p.getLocation()) <= 2500) {

                ItemStack camera = CrisisItem.CAMERA.getItemStack(1);
                ItemMeta im = camera.getItemMeta();
                im.setDisplayName(cam.getName());
                camera.setItemMeta(im);

                inv.setItem(slot, camera);
                slot++;
            }
        }
        p.openInventory(inv);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getClickedInventory() != null && event.getClickedInventory().getName().equals(INV_NAME)) {
            event.setCancelled(true);
            if (event.getCurrentItem() != null && event.getCurrentItem().hasItemMeta()) {
                String dispName = event.getCurrentItem().getItemMeta().getDisplayName();
                Crisis.getInstance().getBlockHandler().cameras.values().stream().filter(cam -> cam.getName().equals(dispName)).forEach(cam -> {

                    event.getWhoClicked().closeInventory();
                    new CameraSession((Player) event.getWhoClicked(), cam).start();
                    return;

                });
            }
        }
    }
}
