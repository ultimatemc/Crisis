package net.crisismc.crisis.inventory;

import net.minecraft.server.v1_12_R1.BlockPosition;
import net.minecraft.server.v1_12_R1.ContainerAnvil;
import net.minecraft.server.v1_12_R1.EntityHuman;
import net.crisismc.crisis.lib.Callback;

public class CrisisAnvilContainer extends ContainerAnvil {

    private Callback callback;

    public CrisisAnvilContainer(EntityHuman entity, Callback callback) {
        super(entity.inventory, entity.world, new BlockPosition(0, 0, 0), entity);
        this.callback = callback;
    }

    @Override
    public boolean a(EntityHuman entityhuman) {
        return true;
    }

    @Override
    public void a(String s) {
        super.a(s);
        callback.callback(s);
    }
}
