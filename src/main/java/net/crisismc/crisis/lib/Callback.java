package net.crisismc.crisis.lib;

public interface Callback {

    void callback(String result);
}
