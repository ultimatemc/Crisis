package net.crisismc.crisis.lib;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import static org.bukkit.ChatColor.*;

public class ChatUtil {

    /**
     * To be used whenever a chat message requires a prefix.
     * By using this method, design changes can be easily rolled out by simply changing this method
     *
     * @return The Crisis Prefix
     */
    public static String getPrefix() {
        return GRAY + "" + BOLD + "[" + YELLOW + "" + BOLD + "Crisis" + GRAY + "" + BOLD + "] ";
    }

    public static String formatDouble(double num) {
        NumberFormat formatter = new DecimalFormat("#0");
        return formatter.format(num);
    }
}
