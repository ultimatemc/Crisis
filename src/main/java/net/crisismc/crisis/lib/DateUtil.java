package net.crisismc.crisis.lib;

public class DateUtil {

    public static String getTimeFromMillis(long millis) {
        long hours = millis / 3600000;
        millis %= 3600000;
        long minutes = millis / 60000;
        millis %= 60000;
        long seconds = millis / 1000;

        String time = "";
        if (hours > 0) time = time + hours + "h ";
        if (minutes > 0) time = time + minutes + "m ";
        time = time + seconds + "s";

        return time;
    }
}
