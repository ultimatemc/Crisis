package net.crisismc.crisis.lib;

import org.bukkit.block.BlockFace;

public class DirectionUtil {
    public static final BlockFace[] axis = {BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST};

    public static BlockFace yawToFace(float yaw) {
        return axis[(java.lang.Math.round(yaw / 90.0F) & 0x3)];
    }
}
