package net.crisismc.crisis.lib;

import org.bukkit.inventory.PlayerInventory;

public interface FunctionalEquipper {

    void equip(PlayerInventory inv);
}
