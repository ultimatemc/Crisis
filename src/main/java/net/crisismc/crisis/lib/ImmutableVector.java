package net.crisismc.crisis.lib;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

public class ImmutableVector {

    private final int x;
    private final int y;
    private final int z;

    public ImmutableVector(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public ImmutableVector(Block b) {
        this.x = b.getX();
        this.y = b.getY();
        this.z = b.getZ();
    }

    public Block toBlock(World world) {
        return new Location(world, x, y, z).getBlock();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ImmutableVector)) return false;
        ImmutableVector other = (ImmutableVector) obj;
        return other.getX() == x && other.getY() == y && other.getZ() == z;
    }

    @Override
    public int hashCode() {
        int result = 37 + x;
        result = 37 * result + y;
        result = 37 * result + z;
        return result;
    }

    @Override
    public String toString() {
        return x + "," + y + "," + z ;
    }

    public static ImmutableVector fromString(String str) {
        String[] xyz = str.split(",");
        return new ImmutableVector(Integer.parseInt(xyz[0]), Integer.parseInt(xyz[1]), Integer.parseInt(xyz[2]));
    }
}