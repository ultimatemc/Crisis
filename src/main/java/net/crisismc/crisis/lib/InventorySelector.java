package net.crisismc.crisis.lib;

import net.crisismc.crisis.Crisis;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class InventorySelector<T> {

    private Inventory inventory;
    private int size;
    private String name;
    private List<T> values = new ArrayList<>();

    public InventorySelector(String name, int size) {

        this.size = size;
        this.name = name;
        while (values.size() < size) values.add(null);

        inventory = Bukkit.createInventory(null, size, name);
    }

    public void set(int index, ItemStack item, T value) {
        System.out.println(item);
        inventory.setItem(index, item);
        values.set(index, value);
    }

    public ItemStack get(int index) {
        return inventory.getItem(index);
    }

    public T getValue(int index) {
        return values.get(index);
    }

    public CompletableFuture<Integer> getSelection(Player player) {
        String id = UUID.randomUUID().toString();
        Inventory uniqueInv = Bukkit.createInventory(new UniqueInventoryHolder(id), size, name);

        //Copy over items
        for (int i = 0; i < size; i++) {
            if (inventory.getItem(i) != null) {
                uniqueInv.setItem(i, inventory.getItem(i));
            }
        }

        player.closeInventory();
        player.openInventory(uniqueInv);

        CompletableFuture<Integer> promise = new CompletableFuture<>();

        Crisis.getInstance().getServer().getPluginManager().registerEvents(new Listener() {
            @EventHandler
            public void inventoryClickEvent(InventoryClickEvent event) {
                if (UniqueInventoryHolder.matchesKey(event.getClickedInventory().getHolder(), id)) {
                    ItemStack item = event.getCurrentItem();
                    if (item != null && item.getType() != Material.AIR) {
                        promise.complete(event.getSlot());
                        HandlerList.unregisterAll(this);
                    }
                }
            }
            @EventHandler
            public void inventoryCloseEvent(InventoryCloseEvent event) {
                if (UniqueInventoryHolder.matchesKey(event.getInventory().getHolder(), id)) {
                    promise.complete(null);
                    HandlerList.unregisterAll(this);
                }
            }
        }, Crisis.getInstance());

        return promise;
    }
}
