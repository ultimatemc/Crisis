package net.crisismc.crisis.lib;

import net.crisismc.crisis.CrisisItem;
import org.bukkit.inventory.Inventory;

public class InventoryUtil {

    /**
     * Fills an inventory with Crisis inventory items
     * @param inv The inventory to fill
     */
    public static void fill(Inventory inv) {
        for (int i = 0; i < inv.getSize(); i++) {
            inv.setItem(i, CrisisItem.INVENTORY_ITEM.getItemStack(1));
        }
    }
}
