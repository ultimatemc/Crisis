package net.crisismc.crisis.lib;

import net.minecraft.server.v1_12_R1.NBTTagCompound;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.util.UUID;

public class NbtTags {

    public static NBTTagCompound getTag(ItemStack item) {
        if (item instanceof CraftItemStack) {
            try {
                Field field = CraftItemStack.class.getDeclaredField("handle");
                field.setAccessible(true);
                return ((net.minecraft.server.v1_12_R1.ItemStack) field.get(item)).getTag();
            } catch (Exception e) {
            }
        }
        return null;
    }

    public static ItemStack setTag(ItemStack item, NBTTagCompound tag) {
        CraftItemStack craftItem = null;
        if (item instanceof CraftItemStack) {
            craftItem = (CraftItemStack) item;
        } else {
            craftItem = CraftItemStack.asCraftCopy(item);
        }

        net.minecraft.server.v1_12_R1.ItemStack nmsItem = null;
        try {
            Field field = CraftItemStack.class.getDeclaredField("handle");
            field.setAccessible(true);
            nmsItem = ((net.minecraft.server.v1_12_R1.ItemStack) field.get(item));
        } catch (Exception e) {
        }
        if (nmsItem == null) {
            nmsItem = CraftItemStack.asNMSCopy(craftItem);
        }

        nmsItem.setTag(tag);
        try {
            Field field = CraftItemStack.class.getDeclaredField("handle");
            field.setAccessible(true);
            field.set(craftItem, nmsItem);
        } catch (Exception e) {
        }

        return craftItem;
    }

    public static ItemStack setUnstackable(ItemStack item) {
        item = setNBTData(item, UUID.randomUUID().toString(), UUID.randomUUID().toString());
        return item;
    }

    public static ItemStack setUnbreakable(ItemStack item) {
        if (item == null) return item;
        if (!(item instanceof CraftItemStack)) {
            item = CraftItemStack.asCraftCopy(item);
        }
        NBTTagCompound tag = getTag(item);
        if (tag == null) {
            tag = new NBTTagCompound();
        }
        tag.setByte("Unbreakable", (byte) 1);
        return setTag(item, tag);
    }

    public static ItemStack setNBTData(ItemStack item, String key, String value) {
        if (!(item instanceof CraftItemStack)) {
            item = CraftItemStack.asCraftCopy(item);
        }
        NBTTagCompound tag = getTag(item);
        if (tag == null) {
            tag = new NBTTagCompound();
        }
        tag.setString(key, value);
        return setTag(item, tag);
    }

    public static String getNBTData(ItemStack item, String key) {
        if (!(item instanceof CraftItemStack)) {
            item = CraftItemStack.asCraftCopy(item);
        }
        NBTTagCompound tag = getTag(item);
        if (tag == null) {
            tag = new NBTTagCompound();
        }
        return tag.getString(key);
    }
}
