package net.crisismc.crisis.lib;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.*;
import net.crisismc.crisis.Crisis;
import net.minecraft.server.v1_12_R1.BlockPosition;
import net.minecraft.server.v1_12_R1.PacketPlayOutOpenSignEditor;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class PlayerInput {

    //TODO complete
    public static CompletableFuture<Boolean> getConfirmation(Player player, String prompt) {
        CompletableFuture<Boolean> promise = new CompletableFuture<>();

        InventoryHolder unqiueHolder = new UniqueInventoryHolder(UUID.randomUUID().toString());
        Inventory inv = Bukkit.createInventory(unqiueHolder, 27, prompt);

        Crisis.getInstance().getServer().getPluginManager().registerEvents(new Listener() {
            @EventHandler
            public void inventoryClickEvent(InventoryClickEvent event) {

            }
            @EventHandler
            public void inventoryCloseEvent(InventoryCloseEvent event) {

            }
        }, Crisis.getInstance());

        return promise;
    }

    public static CompletableFuture<String[]> getString(Player player) {
        CompletableFuture<String[]> promise = new CompletableFuture<>();
        new StringInput(player, promise);

        return promise;
    }
}

class StringInput {

    private Player player;
    private CompletableFuture<String[]> promise;

    private PacketListener pl = null;

    StringInput(Player player, CompletableFuture<String[]> promise) {
        this.player = player;
        this.promise = promise;
        sendSignEditingPacket();
        setupPacketListener();
    }

    private void sendSignEditingPacket() {
        PacketPlayOutOpenSignEditor signEditPacket = new PacketPlayOutOpenSignEditor(BlockPosition.ZERO);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(signEditPacket);
    }

    private void setupPacketListener() {
        pl = new PacketAdapter(Crisis.getInstance(), ListenerPriority.NORMAL, PacketType.Play.Client.UPDATE_SIGN) {
            @Override
            public void onPacketReceiving(PacketEvent event) {
                if (!event.getPlayer().equals(player)) return;
                PacketContainer packet = event.getPacket();
                String[] text = packet.getStringArrays().readSafely(0);
                promise.complete(text);
                removePacketListener(pl);
            }
        };
        Crisis.getInstance().getProtocolManager().addPacketListener(pl);
    }

    private void removePacketListener(PacketListener pl) {
        Crisis.getInstance().getProtocolManager().removePacketListener(pl);
    }
}
