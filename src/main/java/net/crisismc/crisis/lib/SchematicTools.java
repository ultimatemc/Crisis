package net.crisismc.crisis.lib;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.blocks.BaseBlock;
import net.crisismc.crisis.Crisis;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class SchematicTools {

    public static List<ArmorStand> schematicToArmorStands(Location loc, CuboidClipboard schematic) {
        loc.setYaw(0);
        List<ArmorStand> stands = new ArrayList<>();
        Vector offset = schematic.getOffset();
        loc.add(offset.getX() * 0.675, offset.getY() * 0.675, offset.getZ() * 0.675);
        Location center = null;
        for (int x = 0; x < schematic.getWidth(); x++) {
            for (int y = 0; y < schematic.getHeight(); y++) {
                for (int z = 0; z < schematic.getLength(); z++) {
                    BaseBlock bb = schematic.getBlock(new Vector(x, y, z));
                    if (bb.getType() == 0) continue;
                    ArmorStand armorStand = (ArmorStand) loc.getWorld().spawnEntity(loc.clone().add(x - (x * 0.375), y - (y * 0.375), z - (z * 0.375)), EntityType.ARMOR_STAND);
                    armorStand.setHelmet(new ItemStack(bb.getType(), 1, (short) bb.getData()));
                    armorStand.setGravity(false);
                    armorStand.setVisible(false);
                    stands.add(armorStand);
                    //Diamond block
                    if (bb.getType() == 57) {
                        center = armorStand.getLocation().add(0, 1.2, 0);
                    }
                }
            }
        }

        if (center != null) {
            final Location middle = center;
            Bukkit.getScheduler().runTaskTimer(Crisis.getInstance(), () -> {
                for (ArmorStand stand : stands) {
                    Location newLocation = rotate(middle, stand.getLocation().clone(), 1);
                    stand.teleport(newLocation);
                }
            }, 0L, 1L);
        }

        return stands;
    }


    public static Location rotate(Location rotationPoint, Location armorstandLocation, double degrees) {
        rotationPoint.setY(armorstandLocation.getY());
        float oldyaw = armorstandLocation.getYaw() % 360.0F;

        armorstandLocation.setYaw(rotationPoint.getYaw());

        if (rotationPoint.equals(armorstandLocation)) {
            armorstandLocation.setYaw((float) (oldyaw + degrees));
            return armorstandLocation;
        }

        double d = rotationPoint.distance(armorstandLocation);
        double angle = Math.acos((armorstandLocation.getX() - rotationPoint.getX()) / d);

        if (armorstandLocation.getZ() < rotationPoint.getZ()) {
            double newangle = 2 * Math.PI - angle + (degrees * Math.PI / 180);
            double newx = Math.cos(newangle) * d;
            double newz = Math.sin(newangle) * d;
            armorstandLocation = rotationPoint.clone().add(newx, 0, newz);
            armorstandLocation.setYaw((float) ((oldyaw + degrees) % 360.0F));
            return armorstandLocation;
        } else {
            double newangle = angle + (degrees * Math.PI / 180);

            double newx = Math.cos(newangle) * d;
            double newz = Math.sin(newangle) * d;
            armorstandLocation = rotationPoint.clone().add(newx, 0, newz);
            armorstandLocation.setYaw((float) ((oldyaw + degrees) % 360.0F));
            return armorstandLocation;
        }
    }
}
