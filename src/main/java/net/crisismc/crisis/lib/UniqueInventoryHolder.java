package net.crisismc.crisis.lib;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

public class UniqueInventoryHolder implements InventoryHolder {

    private final String key;

    /**
     * @param key The unique key that will be used to identify this market session
     */
    public UniqueInventoryHolder(String key) {
        this.key = key;
    }


    public String getKey() {
        return key;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj instanceof UniqueInventoryHolder) {
            return ((UniqueInventoryHolder) obj).getKey().equals(key);
        }
        return false;
    }

    @Override
    public Inventory getInventory() {
        return null;
    }

    public static boolean matchesKey(InventoryHolder holder, String key) {
        if (holder instanceof  UniqueInventoryHolder) {
            return ((UniqueInventoryHolder) holder).getKey().equals(key);
        }
        return false;
    }
}
