package net.crisismc.crisis.listeners;


import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.CrisisItem;
import net.md_5.bungee.api.ChatColor;
import net.crisismc.crisis.player.CrisisPlayer;
import net.crisismc.crisis.lib.ChatUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.Random;

public class CrisisEntityListener implements Listener {

    boolean handlingEvent = false;

    public CrisisEntityListener(Crisis plugin) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onEntityDamageEntity(EntityDamageByEntityEvent event) {
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Crisis.getInstance(), () -> event.getEntity().setVelocity(new Vector(0, 0, 0)), 1);

        if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            Player p = (Player) event.getEntity();
            Player dmger = (Player) event.getDamager();
            if (dmger.getInventory().getItemInMainHand() != null) {
                ItemStack inHand = dmger.getInventory().getItemInMainHand();
                if (CrisisItem.getCrisisItem(inHand.getType(), inHand.getData().getData()).inflictsBleeding()) {
                    int armor = 0;
                    for (ItemStack item : p.getInventory().getArmorContents())
                        if (item != null && item.getType() != Material.AIR) armor++;
                    if (new Random().nextInt(armor + 1) == 0) CrisisPlayer.getCrisisPlayer(p).setBleeding(true, 5);
                }
            }
        }

        if (event.getEntityType() == EntityType.PLAYER) {
            Player p = (Player) event.getEntity();
            if (event.getDamager().getType() == EntityType.WOLF || event.getDamager().getType() == EntityType.POLAR_BEAR) {
                int armor = 0;
                for (ItemStack item : p.getInventory().getArmorContents())
                    if (item != null && item.getType() != Material.AIR) armor++;
                if (new Random().nextInt(armor + 1) == 0) CrisisPlayer.getCrisisPlayer(p).setBleeding(true, 5);
            }
        }

        if (event.getDamager().getType() == EntityType.PLAYER) {
            Player p = (Player) event.getDamager();
            if (p.getItemInHand().getType() == Material.INK_SACK) {
                event.setDamage(4);
            }
            if (p.getItemInHand().getType() == Material.WOOD_SWORD) {
                event.setDamage(4);
            }
        }
    }

    @EventHandler
    public void onEntityDamageEvent(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            Player p = (Player) event.getEntity();
            CrisisPlayer cp = CrisisPlayer.getCrisisPlayer(p);

            if (p.getWorld().getName().equalsIgnoreCase("world")) {
                if (event.getCause() == DamageCause.FALL && event.getDamage() > 8) {
                    cp.setLegBroken(true);
                    p.sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "You broke your leg! Find a splint to heal yourself.");
                }
            }
        }
    }
}
