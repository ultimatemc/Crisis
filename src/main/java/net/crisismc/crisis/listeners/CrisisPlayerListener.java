package net.crisismc.crisis.listeners;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.bases.BaseManager;
import net.crisismc.crisis.bases.C4;
import net.crisismc.crisis.building.SleepingBag;
import net.crisismc.crisis.inventory.Cellphone;
import net.crisismc.crisis.lib.ChatUtil;
import net.crisismc.crisis.player.CrisisPlayer;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.bukkit.Material.*;

public class CrisisPlayerListener implements Listener {

    private Crisis plugin;
    private BaseManager bm;

    public CrisisPlayerListener(Crisis plugin, BaseManager bm) {
        this.plugin = plugin;
        this.bm = bm;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (event.getBlock().getType() == Material.POWERED_RAIL) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        event.setCancelled(true);

        if (event.getBlock().getType() == Material.CARPET) {
            SleepingBag.onCarpetBreak(event.getPlayer(), event.getBlock());
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        CrisisPlayer cp = CrisisPlayer.getCrisisPlayer(event.getPlayer());
        if (cp.isLegBroken()) cp.handleBrokenLegMove(event);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player p = event.getPlayer();

		/*
         * Right Click
		 */
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {

            ItemStack item = p.getInventory().getItemInMainHand();
            if (item.getType() == CrisisItem.SLEEPING_BAG.getMaterial() && item.getData().getData() == CrisisItem.SLEEPING_BAG.getItemStack(1).getData().getData()) { //SLEEPING BAG
                if (SleepingBag.canPlace(p)) {
                    SleepingBag.placeBag(p);
                } else {
                    p.sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "You cannot placeChest a sleeping bag here.");
                }
            } else if (item.getType() == CrisisItem.BANDAGE.getMaterial()) {
                CrisisPlayer cp = CrisisPlayer.getCrisisPlayer(p);
                cp.useBandage();
            } else if (item.getType() == Material.EMPTY_MAP) {
                CrisisPlayer cp = CrisisPlayer.getCrisisPlayer(p);
                cp.handleBlueprintClick(item);
                event.setCancelled(true);
            } else if (item.getType() == CrisisItem.SPLINT.getMaterial()) {
                p.getInventory().setItemInMainHand(null);
                CrisisPlayer.getCrisisPlayer(p).setLegBroken(false);
                p.sendMessage(ChatUtil.getPrefix() + ChatColor.GREEN + "Your leg has been healed.");
            } else if (item.getType() == Material.GHAST_TEAR) {
                Cellphone.openInventory(p);
            }
        }

        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (p.getInventory().getItemInMainHand().getType() == Material.WOOD_HOE ||
                    p.getInventory().getItemInMainHand().getType() == Material.STONE_HOE ||
                    p.getInventory().getItemInMainHand().getType() == Material.IRON_HOE ||
                    p.getInventory().getItemInMainHand().getType() == Material.GOLD_HOE ||
                    p.getInventory().getItemInMainHand().getType() == Material.DIAMOND_HOE) {
                event.setCancelled(true);
            }

            if (p.getInventory().getItemInMainHand().getType() == Material.WOOD_SPADE ||
                    p.getInventory().getItemInMainHand().getType() == Material.STONE_SPADE ||
                    p.getInventory().getItemInMainHand().getType() == Material.IRON_SPADE ||
                    p.getInventory().getItemInMainHand().getType() == Material.GOLD_SPADE ||
                    p.getInventory().getItemInMainHand().getType() == Material.DIAMOND_SPADE) {
                event.setCancelled(true);
            }

            if (event.getClickedBlock().getType() == Material.NETHERRACK) {
                Random r = new Random();
                if (p.getInventory().getItemInMainHand().getType() == CrisisItem.MEAT_RAW.getMaterial()) {
                    if (r.nextInt(20) == 0) {
                        p.getWorld().spigot().playEffect(event.getClickedBlock().getLocation().add(0.5, 1.5, 0.5), Effect.FLAME, 0, 0, 0, 0, 0, 0.1F, 50, 64);
                        p.playSound(p.getLocation(), Sound.BLOCK_FIRE_AMBIENT, 2, 2);
                        p.getInventory().setItem(p.getInventory().getHeldItemSlot(), CrisisItem.MEAT_COOKED.getItemStack(1));
                    } else {
                        p.getWorld().spigot().playEffect(event.getClickedBlock().getLocation().add(0.5, 1.5, 0.5), Effect.LARGE_SMOKE, 0, 0, 0, 0, 0, 0.05F, 20, 64);
                        p.playSound(p.getLocation(), Sound.BLOCK_FIRE_EXTINGUISH, 1, 1);
                    }
                } else if (p.getInventory().getItemInMainHand().getType() == CrisisItem.SQUIRREL_RAW.getMaterial()) {
                    if (r.nextInt(20) == 0) {
                        p.getWorld().spigot().playEffect(event.getClickedBlock().getLocation().add(0.5, 1.5, 0.5), Effect.FLAME, 0, 0, 0, 0, 0, 0.1F, 50, 64);
                        p.playSound(p.getLocation(), Sound.BLOCK_FIRE_AMBIENT, 2, 2);
                        p.getInventory().setItem(p.getInventory().getHeldItemSlot(), CrisisItem.SQUIRREL_COOKED.getItemStack(1));
                    } else {
                        p.getWorld().spigot().playEffect(event.getClickedBlock().getLocation().add(0.5, 1.5, 0.5), Effect.LARGE_SMOKE, 0, 0, 0, 0, 0, 0.05F, 20, 64);
                        p.playSound(p.getLocation(), Sound.BLOCK_FIRE_EXTINGUISH, 1, 1);
                    }
                }
            }

            if (p.getInventory().getItemInMainHand().getType() == CrisisItem.C4_EXPLOSIVE.getMaterial()) {
                new C4(p, event.getClickedBlock(), event.getBlockFace(), bm);
            }
        }

    }


    @EventHandler
    public void onInvClick(InventoryClickEvent event) {
        if (event.getWhoClicked().getGameMode() != GameMode.CREATIVE) {
            if (event.getCurrentItem() == null) return;
            Material t = event.getCurrentItem().getType();
            if (t == PAINTING || t == ITEM_FRAME || t == FLOWER_POT) event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        CrisisPlayer.getCrisisPlayer(event.getEntity()).onDeath();
        event.setDroppedExp(0);

        //Remove inventory blockers from drops
        List<ItemStack> removeDrops = new ArrayList<>();
        for (ItemStack dropItem : event.getDrops()) {
            if (dropItem.getType() == CrisisItem.INVENTORY_BLOCKER.getMaterial()) {
                removeDrops.add(dropItem);
            }
        }
        event.getDrops().removeAll(removeDrops);

        event.getEntity().getInventory().clear();

        //No message
        event.setDeathMessage("");
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        event.setRespawnLocation(new Location(Bukkit.getWorld("spawn"), 31.5, 11.1, 5.5, 90, 0));
    }


    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerLogin(PlayerLoginEvent event) {
        if (!CrisisPlayer.crisisPlayers.containsKey(event.getPlayer().getUniqueId())) {
            new CrisisPlayer(event.getPlayer());
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (CrisisPlayer.crisisPlayers.containsKey(event.getPlayer().getUniqueId())) {
            CrisisPlayer.crisisPlayers.get(event.getPlayer().getUniqueId()).saveToDB();
            CrisisPlayer.crisisPlayers.remove(event.getPlayer().getUniqueId());
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getCurrentItem() != null && event.getCurrentItem().getType() == CrisisItem.INVENTORY_BLOCKER.getMaterial()) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onItemDrop(PlayerDropItemEvent event) {
        if (event.getItemDrop().getItemStack().getType() == CrisisItem.INVENTORY_BLOCKER.getMaterial()) {
            event.setCancelled(true);
        }
    }
}
