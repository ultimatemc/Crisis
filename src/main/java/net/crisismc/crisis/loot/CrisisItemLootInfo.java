package net.crisismc.crisis.loot;

public class CrisisItemLootInfo {

    public LootCategory lootCategory;
    public float weight;
    public int amount;

    public CrisisItemLootInfo(LootCategory lootCategory, float weight, int amount) {
        this.lootCategory = lootCategory;
        this.weight = weight;
        this.amount = amount;
    }
}
