package net.crisismc.crisis.loot;

import com.google.gson.annotations.SerializedName;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.lib.ImmutableVector;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class Loot {

    @SerializedName("loot")
    private Map<String, LootLocation> fileLoot = new HashMap<>();

    private transient Map<ImmutableVector, LootLocation> loot = new HashMap<>();

    private transient Queue<LootLocation> lootRespawnQueue = new LinkedList<>();

    public void setup() {
        for(Map.Entry<String, LootLocation> entry : fileLoot.entrySet()) {
            loot.put(ImmutableVector.fromString(entry.getKey()), entry.getValue());
        }

        List<LootLocation> unspawnedLoot = new ArrayList<>();
        for (LootLocation lootLoc : loot.values()) {
            if (!lootLoc.isSpawned()) {
                unspawnedLoot.add(lootLoc);
            } else {
                //Make sure spawned loot is actually spawned (has carpet block)
                Block lootBlock = lootLoc.getLocation().toBlock(Bukkit.getWorld("world"));
                if (lootBlock.getType() != Material.CARPET ||lootBlock.getData() != lootLoc.getLootCategory().getWoolType()) {
                    lootBlock.setType(Material.CARPET);
                    lootBlock.setData(lootLoc.getLootCategory().getWoolType());
                }
            }
        }
        Collections.sort(unspawnedLoot, (o1, o2) -> (int) (o1.getRespawnTime() - o2.getRespawnTime()));
        for (LootLocation lootLoc : unspawnedLoot) {
            lootRespawnQueue.add(lootLoc);
        }
        lootTask();
    }

    public void addLootLocation(Location loc, LootLocation lootLoc) {
        ImmutableVector vec = new ImmutableVector(loc.getBlock());
        loot.put(vec, lootLoc);
        fileLoot.put(vec.toString(), lootLoc);
    }

    public void onLootPickup(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getClickedBlock() != null && event.getClickedBlock().getType() == Material.CARPET) {
            ImmutableVector vec = new ImmutableVector(event.getClickedBlock());
            if (loot.containsKey(vec)) {
                LootLocation lootLoc = loot.get(vec);
                ItemStack item = lootLoc.getItem();
                Location loc = lootLoc.getLocation().toBlock(event.getPlayer().getWorld()).getLocation().add(0.5, 0.5, 0.5);
                loc.getWorld().dropItemNaturally(loc, item);
                loc.getWorld().spigot().playEffect(loc, Effect.TILE_DUST, 35, loc.getBlock().getData(), 0.5F, 0.5F, 0.5F, 0.1F, 4, 16);
                loc.getWorld().playSound(loc, Sound.BLOCK_CLOTH_BREAK, 1, 1);

                event.getClickedBlock().setType(Material.AIR);
                lootLoc.setRespawnTime(System.currentTimeMillis() + Crisis.getInstance().getLootManager().respawnDelay);
                lootLoc.setSpawned(false);
                lootRespawnQueue.offer(lootLoc);
            }
        }
    }

    private void lootTask() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Crisis.getInstance(), () -> {
            if (lootRespawnQueue.size() > 0) {
                if (lootRespawnQueue.peek().getRespawnTime() <= System.currentTimeMillis()) {
                    LootLocation lootLoc = lootRespawnQueue.poll(); // Remove head element
                    lootLoc.setSpawned(true);
                    lootLoc.setRespawnTime(0);

                    Block b = lootLoc.getLocation().toBlock(Bukkit.getWorld("world"));
                    b.setType(Material.CARPET);
                    b.setData(lootLoc.getLootCategory().getWoolType());
                }
            }
        }, 20, 20);
    }
}
