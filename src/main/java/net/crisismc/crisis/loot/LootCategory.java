package net.crisismc.crisis.loot;

import net.crisismc.crisis.CrisisItem;
import org.bukkit.inventory.ItemStack;

import java.util.NavigableMap;
import java.util.Random;
import java.util.TreeMap;

public enum LootCategory {
    RESIDENTIAL("Residential", (byte) 7),
    FOOD("Food", (byte) 7),
    MEDICAL("Medical", (byte) 6),
    INDUSTRIAL("Industrial", (byte) 5),
    MILITARY("Military", (byte) 11),
    MILITARY_GUNS("Military Guns", (byte) 11),
    MILITARY_CLOTHES("Military Clothes", (byte) 11);

    private RandomCollection<ItemStack> randomItems = new RandomCollection<>();
    private String name;
    private byte woolType;
    private Random r;

    LootCategory(String name, byte woolType) {
        this.name = name;
        this.woolType = woolType;
        r = new Random();
    }

    public String getName() {
        return name;
    }

    public byte getWoolType() {
        return woolType;
    }

    public void addItem(float weight, ItemStack item) {
        randomItems.add(weight, item);
    }

    public ItemStack randomItem() {
        ItemStack item = randomItems.next();
        // Money special case
        if (item.getType() == CrisisItem.MONEY.getMaterial()) {
            return CrisisItem.MONEY.getItemStack(((double) r.nextInt(item.getAmount() + 1))/100D);
        }
        item.setAmount(r.nextInt(item.getAmount()) + 1);
        return item;
    }

    class RandomCollection<E> {
        private final NavigableMap<Double, E> map = new TreeMap<>();
        private final Random random;
        private double total = 0;

        public RandomCollection() {
            this(new Random());
        }

        public RandomCollection(Random random) {
            this.random = random;
        }

        public void add(double weight, E result) {
            if (weight <= 0) return;
            total += weight;
            map.put(total, result);
        }

        public E next() {
            double value = random.nextDouble() * total;
            return map.ceilingEntry(value).getValue();
        }
    }
}

