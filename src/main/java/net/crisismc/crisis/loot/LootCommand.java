package net.crisismc.crisis.loot;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;

public class LootCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(ChatColor.RED + "Command Usage:");
            sender.sendMessage(ChatColor.RED + "/loot inv");
        } else if (args.length >= 1) {
            if (args[0].equalsIgnoreCase("inv") || args[0].equalsIgnoreCase("inventory")) {
                if (sender instanceof Player) {
                    Player p = (Player) sender;
                    Inventory inv = Bukkit.createInventory(null, LootCategory.values().length + (9 - (LootCategory.values().length % 9)), ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Loot Blocks");
                    int i = 0;
                    for (LootCategory lootCategory : LootCategory.values()) { //Populate inv
                        ItemStack carpet = new ItemStack(Material.CARPET, 1, lootCategory.getWoolType());
                        ItemMeta im = carpet.getItemMeta();
                        im.setDisplayName(lootCategory.getName());
                        List<String> lore = Arrays.asList(ChatColor.GOLD + "Loot Category: " + lootCategory);
                        im.setLore(lore);
                        carpet.setItemMeta(im);
                        inv.setItem(i, carpet);
                        i++;
                    }

                    p.openInventory(inv);
                } else {
                    sender.sendMessage(ChatColor.RED + "You must be a player to run this command.");
                }
            }
        }


        return false;
    }
}
