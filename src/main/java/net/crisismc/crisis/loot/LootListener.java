package net.crisismc.crisis.loot;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.lib.ImmutableVector;
import net.crisismc.crisis.lib.ChatUtil;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class LootListener implements Listener {

    private LootManager lootManager;

    public LootListener(LootManager lootManager) {
        this.lootManager = lootManager;
        Crisis.getInstance().getServer().getPluginManager().registerEvents(this, Crisis.getInstance());
    }

    @EventHandler(ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent event) {
        if (event.getBlockPlaced().getType() == Material.CARPET) {
            byte data = event.getBlockPlaced().getData();
            //Get lore containing loot category
            LootCategory lootCategory;
            try {
                lootCategory = LootCategory.valueOf(event.getPlayer().getInventory().getItemInMainHand().getItemMeta().getLore().get(0).split(": ")[1]);
            } catch (NullPointerException ex) {
                return;
            }
            lootManager.getLoot().addLootLocation(event.getBlockPlaced().getLocation(), new LootLocation(new ImmutableVector(event.getBlockPlaced()), lootCategory));
            event.getPlayer().sendMessage(ChatUtil.getPrefix() + ChatColor.GRAY + "Placed \"" + lootCategory.getName() + "\" loot pile.");
        }
    }

    @EventHandler
    public void onCarpetClick(PlayerInteractEvent event) {
        lootManager.getLoot().onLootPickup(event);
    }
}
