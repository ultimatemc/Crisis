package net.crisismc.crisis.loot;

import net.crisismc.crisis.lib.ImmutableVector;
import org.bukkit.inventory.ItemStack;

public class LootLocation {

    private ImmutableVector location;
    private LootCategory lootCategory;
    private long respawnTime;
    private boolean spawned;

    public LootLocation(ImmutableVector location, LootCategory lootCategory) {
        this.location = location;
        this.lootCategory = lootCategory;
        spawned = true;
    }

    public LootCategory getLootCategory() {
        return lootCategory;
    }

    /**
     * @return A random item from the specific loot category
     */
    public ItemStack getItem() {
        return lootCategory.randomItem();
    }

    public long getRespawnTime() {
        return respawnTime;
    }

    public void setRespawnTime(long respawnTime) {
        this.respawnTime = respawnTime;
    }

    public boolean isSpawned() {
        return spawned;
    }

    public void setSpawned(boolean spawned) {
        this.spawned = spawned;
    }

    public ImmutableVector getLocation() {
        return location;
    }

    public void setLocation(ImmutableVector location) {
        this.location = location;
    }
}
