package net.crisismc.crisis.loot;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.lib.ConfigLoader;

import java.io.File;
import java.io.IOException;

public class LootManager {

    private Loot loot;
    private Crisis plugin;

    // In milliseconds
    public final long respawnDelay = 10000L;

    public LootManager() {
        plugin = Crisis.getInstance();
        setupLootLists();
        setupLoot();
        new LootListener(this);
    }

    public void onDisable() {
        File lootFile = new File(plugin.getDataFolder().getPath() + File.separator + "loot.json");
        try {
            ConfigLoader.saveConfig(loot, lootFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setupLootLists() {
        for (CrisisItem item : CrisisItem.values()) {
            if (item.getLootInfo() == null) continue;
            for (CrisisItemLootInfo lootInfo : item.getLootInfo()) {
                lootInfo.lootCategory.addItem(lootInfo.weight, item.getItemStack(lootInfo.amount));
            }
        }
    }

    private void setupLoot() {
        File lootFile = new File(plugin.getDataFolder().getPath() + File.separator + "loot.json");
        if (lootFile.exists()) {
            try {
                loot = ConfigLoader.loadConfig(Loot.class, lootFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            loot = new Loot();
            try {
                lootFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        loot.setup();
    }

    public Loot getLoot() {
        return loot;
    }

    /*
       DONE lootTypes.add(new LootList("Residential", (byte) 0,
                new LootItem(CrisisItem.RIFLE_BOLT.getItemStack(1), 0.05),
                new LootItem(CrisisItem.SHOTGUN_WATERPIPE.getItemStack(1), 0.25),

                new LootItem(CrisisItem.STICK.getItemStack(8), 2),
                new LootItem(CrisisItem.LOG.getItemStack(2), 1),
                new LootItem(CrisisItem.ANIMAL_FAT.getItemStack(3), 1),
                new LootItem(CrisisItem.CLOTH.getItemStack(3), 1),

                new LootItem(CrisisItem.MEAT_RAW.getItemStack(1), 2),
                new LootItem(CrisisItem.CANNED_BEANS.getItemStack(1), 1),
                new LootItem(CrisisItem.CANNED_PEACHES.getItemStack(1), 1),
                new LootItem(CrisisItem.CANNED_SARDINES.getItemStack(1), 1),
                new LootItem(CrisisItem.MEAT_COOKED.getItemStack(1), 1),
                new LootItem(CrisisItem.HATCHET_STONE.getItemStack(1), 1),
                new LootItem(CrisisItem.HATCHET.getItemStack(1), 0.5),

                new LootItem(CrisisItem.SHIRT_BURLAP.getItemStack(1), 1),
                new LootItem(CrisisItem.TROUSERS_BURLAP.getItemStack(1), 1),
                new LootItem(CrisisItem.SHOES_BURLAP.getItemStack(1), 1)));


       DONE lootTypes.add(new LootList("Food", (byte) 6,
                new LootItem(CrisisItem.CANNED_BEANS.getItemStack(1), 1),
                new LootItem(CrisisItem.CANNED_PEACHES.getItemStack(1), 1),
                new LootItem(CrisisItem.CANNED_SARDINES.getItemStack(1), 1),
                new LootItem(CrisisItem.MEAT_COOKED.getItemStack(1), 1)));

        lootTypes.add(new LootList("Medical", (byte) 14,
                new LootItem(CrisisItem.BANDAGE.getItemStack(1), 10),
                new LootItem(CrisisItem.SPLINT.getItemStack(1), 1),
                new LootItem(CrisisItem.BANDAGE.getItemStack(3), 1)));

        lootTypes.add(new LootList("Industrial", (byte) 5,
                new LootItem(CrisisItem.LOG.getItemStack(3), 1),
                new LootItem(CrisisItem.FUEL_LOW_GRADE.getItemStack(5), 1),
                new LootItem(CrisisItem.METAL_ORE.getItemStack(5), 2),
                new LootItem(CrisisItem.SULFUR_ORE.getItemStack(5), 2),
                new LootItem(CrisisItem.METAL_FRAG.getItemStack(5), 2),
                new LootItem(CrisisItem.GUNPOWDER.getItemStack(3), 1),
                new LootItem(CrisisItem.GLASS_ROUGH.getItemStack(5), 1),
                new LootItem(CrisisItem.LANTERN.getItemStack(1), 1),
                new LootItem(CrisisItem.HAMMER.getItemStack(1), 0.5),
                new LootItem(CrisisItem.PICKAXE.getItemStack(1), 0.3),
                new LootItem(CrisisItem.PICK_ICE.getItemStack(1), 0.1),
                new LootItem(CrisisItem.AXE_REINFORCED.getItemStack(1), 0.1)));

        lootTypes.add(new LootList("Military", (byte) 4,
                new LootItem(CrisisItem.GUNPOWDER.getItemStack(3), 10),
                new LootItem(CrisisItem.AMMO_SHOTGUN_WATERPIPE.getItemStack(1), 5),
                new LootItem(CrisisItem.AMMO_RIFLE_BOLT.getItemStack(1), 10),
                new LootItem(CrisisItem.AMMO_AKS74U.getItemStack(1), 3),
                new LootItem(CrisisItem.AMMO_THOMPSON.getItemStack(1), 3),
                new LootItem(CrisisItem.AMMO_M40A1.getItemStack(1), 0.2),
                new LootItem(CrisisItem.AMMO_MP5.getItemStack(1), 3),
                new LootItem(CrisisItem.AMMO_M4A4.getItemStack(1), 3),
                new LootItem(CrisisItem.AMMO_SCARH_DMR.getItemStack(1), 2),
                new LootItem(CrisisItem.AMMO_M9.getItemStack(1), 3),
                new LootItem(CrisisItem.AMMO_DESERT_EAGLE.getItemStack(1), 3),
                new LootItem(CrisisItem.GRENADE.getItemStack(1), 0.8),

                new LootItem(CrisisItem.HELMET_COMBAT.getItemStack(1), 1),
                new LootItem(CrisisItem.VEST_COMBAT.getItemStack(1), 1),
                new LootItem(CrisisItem.PANTS_COMBAT.getItemStack(1), 1),
                new LootItem(CrisisItem.BOOTS_COMBAT.getItemStack(1), 1),
                new LootItem(CrisisItem.CHESTPLATE_KEVLAR.getItemStack(1), 0.05),

                new LootItem(CrisisItem.CANTEEN.getItemStack(1), 3),
                new LootItem(CrisisItem.BARBED_WIRE.getItemStack(1), 3),

                new LootItem(CrisisItem.RIFLE_BOLT.getItemStack(1), 0.8),
                new LootItem(CrisisItem.M9.getItemStack(1), 0.8),
                new LootItem(CrisisItem.DESERT_EAGLE.getItemStack(1), 0.5),
                new LootItem(CrisisItem.AKS74U.getItemStack(1), 0.3),
                new LootItem(CrisisItem.M4A4.getItemStack(1), 0.3),
                new LootItem(CrisisItem.MP5.getItemStack(1), 0.4),
                new LootItem(CrisisItem.THOMPSON.getItemStack(1), 0.5),
                new LootItem(CrisisItem.M40A1.getItemStack(1), 0.05),
                new LootItem(CrisisItem.SHOTGUN_WATERPIPE.getItemStack(1), 1),
                new LootItem(CrisisItem.SCARH_DMR.getItemStack(1), 0.3)));

        lootTypes.add(new LootList("Military Guns", (byte) 2,
                new LootItem(CrisisItem.M9.getItemStack(1), 0.5),
                new LootItem(CrisisItem.DESERT_EAGLE.getItemStack(1), 0.5),
                new LootItem(CrisisItem.AKS74U.getItemStack(1), 0.3),
                new LootItem(CrisisItem.M4A4.getItemStack(1), 0.3),
                new LootItem(CrisisItem.MP5.getItemStack(1), 0.4),
                new LootItem(CrisisItem.THOMPSON.getItemStack(1), 0.5),
                new LootItem(CrisisItem.M40A1.getItemStack(1), 0.05),
                new LootItem(CrisisItem.SHOTGUN_WATERPIPE.getItemStack(1), 2),
                new LootItem(CrisisItem.RIFLE_BOLT.getItemStack(1), 2),
                new LootItem(CrisisItem.SCARH_DMR.getItemStack(1), 0.3)));


        lootTypes.add(new LootList("Military Clothes", (byte) 11,
                new LootItem(CrisisItem.HELMET_COMBAT.getItemStack(1), 1),
                new LootItem(CrisisItem.VEST_COMBAT.getItemStack(1), 1),
                new LootItem(CrisisItem.PANTS_COMBAT.getItemStack(1), 1),
                new LootItem(CrisisItem.BOOTS_COMBAT.getItemStack(1), 1),
                new LootItem(CrisisItem.CHESTPLATE_KEVLAR.getItemStack(1), 0.05)));
    */
}
