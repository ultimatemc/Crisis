package net.crisismc.crisis.market;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.market.inventory.*;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CrisisMarket implements Listener {

    /**
     * Public entry point for the Crisis Market
     * Does all the necessary setup behind the scenes
     * @param player
     */
    public static void openInventory(Player player) {
        new CrisisMarket(player);
    }

    public final static double TAX = 0.15;

    private final Player player;
    private final MarketInventoryHolder invHolder;
    private Map<String, MarketInventory> inventories;

    private CrisisMarket(Player player) {
        this.player = player;
        invHolder = new MarketInventoryHolder(UUID.randomUUID().toString());
        inventories = new HashMap<>();

        initializeInventories();

        Crisis.getInstance().getServer().getPluginManager().registerEvents(this, Crisis.getInstance());

        inventories.get("main_menu").open(null);
    }

    /**
     * Initializes all the market inventories and links them together
     * See: https://docs.google.com/drawings/d/19s-DXaVexIJ_sQM-YyDifwD5dzezW0i4_iPAt9p-hn8/edit
     */
    private void initializeInventories() {
        MarketInventory main_menu = new MainMenu(this, player);
        inventories.put(main_menu.getName(), main_menu);

        /*
            Browse Listings
         */
        MarketInventory browse_menu = new BrowseMenu(this, player, main_menu);
        inventories.put(browse_menu.getName(), browse_menu);

        MarketInventory listing_view = new ListingView(this, player, browse_menu);
        inventories.put(listing_view.getName(), listing_view);

        MarketInventory purchase_view = new PurchaseView(this, player, listing_view);
        inventories.put(purchase_view.getName(), purchase_view);

        /*
            Manage your own listings
         */
        MarketInventory listing_management = new ListingManagement(this, player, main_menu);
        inventories.put(listing_management.getName(), listing_management);

        MarketInventory edit_listing_view = new EditListingView(this, player, listing_management);
        inventories.put(edit_listing_view.getName(), edit_listing_view);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        System.out.println(event.getInventory().getHolder()); //TODO remove debug
        if (event.getClickedInventory() != null && isMarketInventory(event.getClickedInventory())) {
            if (event.getCurrentItem().getType() == CrisisItem.INVENTORY_ITEM.getMaterial()) {
                event.setCancelled(true);
            }
            MarketInventoryHolder holder = (MarketInventoryHolder) event.getClickedInventory().getHolder();

            System.out.println(event.getAction() + " | " + event.getSlot() + " | " + event.getCurrentItem().getType()); //TODO remove debug

            //Distribute click events to the specific inventory handles the click originated from
            MarketInventory marketInv = inventories.get(holder.getInventoryName());
            System.out.println(marketInv); //TODO remove debug
            if (marketInv != null) {
                boolean cancel = marketInv.handleInventoryClick(event.getClickedInventory(), event.getAction(), event.getSlot(), event.getCurrentItem());
                if (cancel) {
                    event.setCancelled(true);
                    player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                }
            }
        }
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        if (event.getPlayer().equals(player)) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(Crisis.getInstance(), () -> {
                if (player.getOpenInventory().getTitle().equals("container.crafting")) {
                    tearDown();
                }
            }, 1);
        }
    }

    public boolean isMarketInventory(Inventory inv) {
        if (inv.getHolder() instanceof MarketInventoryHolder) {
            return ((MarketInventoryHolder) inv.getHolder()).getKey().equalsIgnoreCase(invHolder.getKey());
        }
        return false;
    }

    public MarketInventoryHolder createInvHolder(String invName) {
        return new MarketInventoryHolder(invHolder.getKey(), invName);
    }

    public MarketInventory getInventory(String key) {
        return inventories.get(key);
    }

    private void tearDown() {
        HandlerList.unregisterAll(this);
        inventories = null;
    }

}
