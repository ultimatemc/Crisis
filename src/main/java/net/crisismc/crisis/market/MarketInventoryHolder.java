package net.crisismc.crisis.market;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

public class MarketInventoryHolder implements InventoryHolder {

    private final String key;
    private final String inventory;

    /**
     * @param key The unique key that will be used to identify this market session
     */
    public MarketInventoryHolder(String key) {
        this.key = key;
        inventory = null;
    }

    /**
     * @param key The unique key that will be used to identify this market session
     * @param inventory The unique name of an inventory within the market
     */
    public MarketInventoryHolder(String key, String inventory) {
        this.key = key;
        this.inventory = inventory;
    }

    public String getKey() {
        return key;
    }

    public String getInventoryName() {
        return inventory;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MarketInventoryHolder) {
            return ((MarketInventoryHolder) obj).getKey().equals(key);
        }
        return false;
    }

    @Override
    public Inventory getInventory() {
        return null;
    }
}
