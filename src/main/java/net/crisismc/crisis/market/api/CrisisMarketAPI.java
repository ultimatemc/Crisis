package net.crisismc.crisis.market.api;


import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.market.api.exceptions.MarketException;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

import java.sql.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class CrisisMarketAPI {

    private static CrisisMarketAPI instance = new CrisisMarketAPI();
    private CrisisMarketDatabase db;

    private CrisisMarketAPI() {
        db = new CrisisMarketDatabase(Crisis.getInstance().db.getDataSource());
    }

    protected static CrisisMarketDatabase getMarketDatabase() {
        return instance.db;
    }

    /**
     * Creates a market listing
     * @param player The player who creates the listing
     * @param items A list of identical items that are added to the transaction
     * @param price The price (in cents) the items are set at
     * @return
     */
    public static CompletableFuture<MarketListing> createMarketListing(Player player, List<ItemStack> items, int price) {
        CompletableFuture<MarketListing> promise = new CompletableFuture<>();

        //Ensure all items are identical
        Material matCompare = items.get(0).getType();
        MaterialData dataCompare = items.get(0).getData();
        String nameCompare = items.get(0).hasItemMeta() ? items.get(0).getItemMeta().getDisplayName() : "";

        for (ItemStack item : items) {
            Material mat = items.get(0).getType();
            MaterialData data = items.get(0).getData();
            String name = items.get(0).hasItemMeta() ? items.get(0).getItemMeta().getDisplayName() : "";

            if (mat != matCompare || !data.equals(dataCompare) || !name.equals(nameCompare)) {
                promise.completeExceptionally(new MarketException("All items must be identical"));
                return promise;
            }

            //Check that guns are unloaded
            if (name.matches("<[1-9]+>|<[0-9][0-9]+>")) {
                promise.completeExceptionally(new MarketException("Guns added to listings must be unloaded"));
                return promise;
            }
        }

        ItemStack listingItem = items.get(0);
        int total = 0;
        for (ItemStack item : items) total += item.getAmount();
        listingItem.setAmount(total);

        Date date = new Date(System.currentTimeMillis());
        final int amount = total;

        Bukkit.getScheduler().runTaskAsynchronously(Crisis.getInstance(), () ->
                promise.complete(instance.db.createListing(player, date, listingItem, amount, price, Crisis.getInstance().getEconomy().getEcon())));

        return promise;
    }

    /**
     * Used to a player's listings and all transactions associated with them
     * @param player
     * @return A promise that completes to a List of MarketListings with their transaction history cached
     */
    public static CompletableFuture<List<MarketListing>> getPlayerListingsAndTransactions(OfflinePlayer player) {
        CompletableFuture<List<MarketListing>> promise = new CompletableFuture<>();

        Bukkit.getScheduler().runTaskAsynchronously(Crisis.getInstance(), () ->
                promise.complete(instance.db.getPlayerMarketListings(player, true)));

        return promise;
    }

    /**
     * Used to a player's listings
     * @param player
     * @return A promise that completes to a List of the provided Player's MarketListings
     */
    public static CompletableFuture<List<MarketListing>> getPlayerListings(OfflinePlayer player) {
        CompletableFuture<List<MarketListing>> promise = new CompletableFuture<>();

        Bukkit.getScheduler().runTaskAsynchronously(Crisis.getInstance(), () ->
                promise.complete(instance.db.getPlayerMarketListings(player, false)));

        return promise;
    }

}
