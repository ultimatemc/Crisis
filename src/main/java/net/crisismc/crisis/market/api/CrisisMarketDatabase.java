package net.crisismc.crisis.market.api;

import com.zaxxer.hikari.HikariDataSource;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.database.StorageUtil;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;
import java.sql.*;
import java.sql.Date;
import java.util.*;

public class CrisisMarketDatabase {

    private HikariDataSource ds;

    private final String INSERT_TRANSACTION = "INSERT INTO Market_Transactions VALUES(?,?,?,?,?)";
    private final String INSERT_LISTING = "INSERT INTO Market_Listings VALUES(?,?,?,?,?)";

    private final String SELECT_LISTING_TRANSACTIONS = "SELECT * FROM Market_Transactions WHERE listing=?";
    private final String SELECT_PLAYER_LISTINGS = "SELECT * FROM Market_Listings WHERE player=?";

    private final String DELETE_LISTING = "DELETE FROM Market_Listings WHERE id=?";

    public CrisisMarketDatabase(HikariDataSource ds) {
        this.ds = ds;
    }

    /**
     * Must be called async
     * @param player
     * @param listing
     * @param quantity
     * @param price
     * @return
     */
    public MarketTransaction createTransaction(OfflinePlayer player, int listing, Date timestamp, int quantity, double price) {
        try (Connection connection = ds.getConnection();

            PreparedStatement insert = connection.prepareStatement(INSERT_TRANSACTION, Statement.RETURN_GENERATED_KEYS)) {
            insert.setString(1, player.getUniqueId().toString());
            insert.setInt(2, listing);
            insert.setDate(3, timestamp);
            insert.setInt(4, quantity);
            insert.setInt(5, (int) Math.round(price * 100));

            insert.execute();

            int id = insert.getGeneratedKeys().getInt(1);
            return new MarketTransaction(id, player, listing, timestamp, quantity, price);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public MarketListing createListing(OfflinePlayer player, Date creationTime, ItemStack item, int amount, double price, Economy econ) {
        try (Connection connection = ds.getConnection()) {

            PreparedStatement insert = connection.prepareStatement(INSERT_LISTING, Statement.RETURN_GENERATED_KEYS);
            insert.setString(1, player.getUniqueId().toString());
            insert.setDate(2, creationTime);
            insert.setString(3, StorageUtil.itemToBase64(item));
            insert.setInt(4, amount);
            insert.setInt(5, (int) Math.round(price * 100));

            insert.execute();

            int id = insert.getGeneratedKeys().getInt(1);
            return new MarketListing(id, player, creationTime, item, amount, price, Crisis.getInstance().getEconomy().getEcon());

        } catch (SQLException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<MarketTransaction> getTransactions(int listing) {
        List<MarketTransaction> transactions = new ArrayList<>();

        try (Connection connection = ds.getConnection()) {

            PreparedStatement select = connection.prepareStatement(SELECT_LISTING_TRANSACTIONS);
            select.setInt(1, listing);

            ResultSet result = select.executeQuery();
            while (result.next()) {
                int id = result.getInt("id");
                OfflinePlayer player = Bukkit.getOfflinePlayer(UUID.fromString(result.getString("player")));
                Date timestamp = result.getDate("timestamp");
                int quantity = result.getInt("quantity");
                int amount = result.getInt("amount");

                transactions.add(new MarketTransaction(id, player, listing, timestamp, quantity, amount));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

        return transactions;
    }

    public boolean deleteListing(int id) {
        try (Connection connection = ds.getConnection()) {

            PreparedStatement select = connection.prepareStatement(DELETE_LISTING);
            select.setInt(1, id);

            return select.execute();

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<MarketListing> getPlayerMarketListings(OfflinePlayer player, boolean includeTransactions) {
        List<MarketListing> results = new ArrayList<>();

        try (Connection connection = ds.getConnection()) {

            PreparedStatement select = connection.prepareStatement(SELECT_PLAYER_LISTINGS);
            select.setString(1, player.getUniqueId().toString());

            ResultSet result = select.executeQuery();
            while (result.next()) {
                int id = result.getInt("id");
                OfflinePlayer listingPlayer = Bukkit.getOfflinePlayer(UUID.fromString(result.getString("player")));
                Date creationTime = result.getDate("creation_time");
                ItemStack item = StorageUtil.itemFromBase64(result.getString("item"));
                int amount = result.getInt("amount");
                double price = (double) result.getInt("price") / 100.0D;

                MarketListing listing = new MarketListing(
                        id,
                        listingPlayer,
                        creationTime,
                        item,
                        amount,
                        price,
                        Crisis.getInstance().getEconomy().getEcon()
                );

                if (includeTransactions) {
                    listing.setTransactions(getTransactions(listing.getId()));
                }
                results.add(listing);
            }

        } catch (SQLException | IOException e) {
            e.printStackTrace();
            return null;
        }
        return results;
    }
}
