package net.crisismc.crisis.market.api;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.market.CrisisMarket;
import net.crisismc.crisis.market.api.exceptions.InsufficientFundsException;
import net.crisismc.crisis.market.api.exceptions.InsufficientItemsException;
import net.crisismc.crisis.market.api.exceptions.MarketException;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class MarketListing {

    private int id;
    private OfflinePlayer player;
    private Date creationTime;
    private ItemStack item;
    private int amount;
    private double price;

    private Economy econ;

    private List<MarketTransaction> transactions;

    /**
     * @param id The unique primary key id of the listing
     * @param player The player who created the listing
     * @param creationTime The time the listing was created
     * @param item The item being sold
     * @param amount The amount of items being sold in the listing
     * @param price The price per item
     * @param econ The Economy hook
     */
    public MarketListing(int id, OfflinePlayer player, Date creationTime, ItemStack item, int amount, double price, Economy econ) {
        this.id = id;
        this.player = player;
        this.creationTime = creationTime;
        this.item = item;
        this.amount = amount;
        this.price = price;
        this.econ = econ;
    }

    /**
     * Purchase a given quantity of items from the listing
     * @param purchaseAmount
     * @param buyer
     * @throws InsufficientFundsException If the player specified does not have the necessary funds to purchase the given quantity of items
     * @throws InsufficientItemsException If the listing contains less than the amount of items specified
     * @return The ItemStacks representing the items purchased from the listing
     */
    public CompletableFuture<List<ItemStack>> purchase(int purchaseAmount, Player buyer) throws InsufficientItemsException, InsufficientFundsException {
        if (amount < purchaseAmount) throw new InsufficientItemsException();
        if (econ.getBalance(buyer) < (purchaseAmount * price)) throw new InsufficientFundsException();

        CompletableFuture<List<ItemStack>> promise = new CompletableFuture<>();

        //Setup list of purchased items
        List<ItemStack> purchasedItems = new ArrayList<>();
        int stacks = purchaseAmount / item.getMaxStackSize();
        for (int i = 0; i < stacks; i++) {
            ItemStack item = this.item.clone();
            item.setAmount(item.getMaxStackSize());
            purchasedItems.add(item);
        }
        ItemStack item = this.item.clone();
        item.setAmount(purchaseAmount - (stacks * this.item.getMaxStackSize()));

        //Async
        Bukkit.getScheduler().runTaskAsynchronously(Crisis.getInstance(), () -> {
            MarketTransaction transaction = CrisisMarketAPI.getMarketDatabase().createTransaction(buyer, id, new Date(System.currentTimeMillis()), purchaseAmount, price);
            if (transaction != null) {
                promise.complete(purchasedItems);
            } else {
                promise.completeExceptionally(new MarketException("Failed to complete market transaction"));
            }
        });

        return promise;
    }

    public CompletableFuture<List<MarketTransaction>> getTransactions() {
        CompletableFuture<List<MarketTransaction>> promise = new CompletableFuture<>();

        //Async
        Bukkit.getScheduler().runTaskAsynchronously(Crisis.getInstance(), () -> {
                List<MarketTransaction> transactions = CrisisMarketAPI.getMarketDatabase().getTransactions(id);
                if (transactions != null) {
                    promise.complete(transactions);
                } else {
                    promise.completeExceptionally(new MarketException("An unknown error occurs while retrieving transactions"));
                }
        });

        return promise;
    }

    public List<MarketTransaction> getCachedTranscations() {
        return transactions;
    }

    public ItemStack getDisplayItem() {
        ItemStack listingItem = getItem();
        ItemMeta im = listingItem.getItemMeta();
        List<String> lore = im.getLore();

        lore.add(ChatColor.BLUE + "=== Listing Info ===");
        lore.add(ChatColor.BLUE + "Amount: " + getAmount());
        lore.add(ChatColor.BLUE + "Price: $" + getPrice());
        lore.add(ChatColor.BLUE + "Created: " + getCreationTime().toString());
        lore.add(ChatColor.BLUE + "ID: " + getId());

        im.setLore(lore);
        listingItem.setItemMeta(im);

        return listingItem;
    }

    public ItemStack getDisplayItem(List<MarketTransaction> transactions) {
        ItemStack listingItem = getDisplayItem();
        ItemMeta im = listingItem.getItemMeta();
        List<String> lore = im.getLore();

        int sold = 0;
        double profit = 0;
        for (MarketTransaction transaction : transactions) {
            sold += transaction.getQuantity();
            profit += transaction.getPrice() * transaction.getQuantity();
        }

        lore.add(ChatColor.BLUE + "--- Activity ---");
        lore.add(ChatColor.BLUE + "Amount Sold: " + sold);
        lore.add(ChatColor.BLUE + "Profit: " + (profit - (profit * CrisisMarket.TAX)));

        im.setLore(lore);
        listingItem.setItemMeta(im);

        return listingItem;
    }

    public CompletableFuture<Boolean> delete() {
        CompletableFuture<Boolean> promise = new CompletableFuture<>();

        Bukkit.getScheduler().runTaskAsynchronously(Crisis.getInstance(), () -> promise.complete(CrisisMarketAPI.getMarketDatabase().deleteListing(id)));

        return promise;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public OfflinePlayer getPlayer() {
        return player;
    }

    public void setPlayer(OfflinePlayer player) {
        this.player = player;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public ItemStack getItem() {
        return item;
    }

    public void setItem(ItemStack item) {
        this.item = item;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Economy getEcon() {
        return econ;
    }

    public void setEcon(Economy econ) {
        this.econ = econ;
    }

    public void setTransactions(List<MarketTransaction> transactions) {
        this.transactions = transactions;
    }
}
