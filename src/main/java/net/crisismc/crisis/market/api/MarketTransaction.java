package net.crisismc.crisis.market.api;

import org.bukkit.OfflinePlayer;

import java.util.Date;

public class MarketTransaction {

    private final int id;
    private final OfflinePlayer player;
    private final int listing;
    private final Date timestamp;
    private final int quantity;
    private final double price;

    /**
     * An immutable object representing a market transaction
     * @param id The unique id of this transaction
     * @param player The player who initiated the transaction
     * @param listing The id of the listing this transaction is associated to
     * @param timestamp The time at which this transaction occurred
     * @param quantity The amount of items that were transferred
     * @param price The price at which the items were purchased
     */
    public MarketTransaction(int id, OfflinePlayer player, int listing, Date timestamp, int quantity, double price) {
        this.id = id;
        this.player = player;
        this.listing = listing;
        this.timestamp = timestamp;
        this.quantity = quantity;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public OfflinePlayer getPlayer() {
        return player;
    }

    public int getListing() {
        return listing;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }
}
