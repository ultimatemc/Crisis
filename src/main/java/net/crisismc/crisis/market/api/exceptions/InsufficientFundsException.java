package net.crisismc.crisis.market.api.exceptions;

public class InsufficientFundsException extends Exception {

    public InsufficientFundsException() {

    }

    public InsufficientFundsException(String message) {
        super(message);
    }
}
