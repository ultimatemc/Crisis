package net.crisismc.crisis.market.api.exceptions;

public class InsufficientItemsException extends Exception {

    public InsufficientItemsException() {

    }

    public InsufficientItemsException(String message) {
        super(message);
    }
}
