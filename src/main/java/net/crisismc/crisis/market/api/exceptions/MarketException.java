package net.crisismc.crisis.market.api.exceptions;

public class MarketException extends Exception {

    public MarketException() {}

    public MarketException(String message) {
        super(message);
    }
}
