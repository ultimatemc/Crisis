package net.crisismc.crisis.market.inventory;

import net.crisismc.crisis.lib.InventoryUtil;
import net.crisismc.crisis.lib.ItemBuilder;
import net.crisismc.crisis.lib.PlayerInput;
import net.crisismc.crisis.market.CrisisMarket;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class BrowseMenu extends MarketInventory {

    private Inventory inv;

    public BrowseMenu(CrisisMarket market, Player p, MarketInventory previous) {
        super(p, market, "browse_menu", previous);
        inv = createInventory();
    }

    private Inventory createInventory() {
        Inventory inv = Bukkit.createInventory(getMarket().createInvHolder(getName()), 27, "Browse");
        InventoryUtil.fill(inv);

        inv.setItem(11, new ItemBuilder(Material.COMPASS, 1).setName(ChatColor.GOLD + "Search").toItemStack());
        inv.setItem(13, new ItemBuilder(Material.LAVA_BUCKET, 1).setName(ChatColor.GOLD + "Popular").toItemStack());
        inv.setItem(15, new ItemBuilder(Material.WATCH, 1).setName(ChatColor.GOLD + "Recent").toItemStack());

        inv.setItem(26, new ItemBuilder(Material.REDSTONE, 1).setName(ChatColor.GRAY + "Back").toItemStack());

        return inv;
    }

    @Override
    public void open(Object args) {
        getPlayer().openInventory(inv);
    }

    @Override
    public boolean handleInventoryClick(Inventory inv, InventoryAction action, int slot, ItemStack item) {
        switch (slot) {
            case 11:
                PlayerInput.getString(getPlayer()).thenAccept((strs) -> {
                    String input = strs[0] + " " + strs[1] + " " + strs[2] + " " + strs[3];
                    getPlayer().sendMessage(input);
                });
                return true;
            case 13:
                return true;
            case 15:
                return true;
            case 26:
                goBack();
                return true;
            default:
                return false;
        }
    }
}
