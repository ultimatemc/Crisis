package net.crisismc.crisis.market.inventory;

import net.crisismc.crisis.market.CrisisMarket;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class EditListingView extends MarketInventory {

    private Inventory inv;

    public EditListingView(CrisisMarket market, Player p, MarketInventory previous) {
        super(p, market, "edit_listing_view", previous);
    }

    private Inventory createInventory() {
        Inventory inv = Bukkit.createInventory(getMarket().createInvHolder(getName()), 54, "");

        return inv;
    }

    @Override
    public void open(Object args) {

    }

    @Override
    public boolean handleInventoryClick(Inventory inv, InventoryAction action, int slot, ItemStack item) {
        return true;
    }
}
