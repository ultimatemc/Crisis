package net.crisismc.crisis.market.inventory;

import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.lib.ChatUtil;
import net.crisismc.crisis.lib.ItemBuilder;
import net.crisismc.crisis.market.CrisisMarket;
import net.crisismc.crisis.market.api.CrisisMarketAPI;
import net.crisismc.crisis.market.api.MarketListing;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class ListingManagement extends MarketInventory {

    List<Inventory> invs = new ArrayList<>();
    int currentInv = 0;

    public ListingManagement(CrisisMarket market, Player p, MarketInventory previous) {
        super(p, market, "listing_management", previous);
    }

    private void setupInventories() {
        CrisisMarketAPI.getPlayerListingsAndTransactions(getPlayer()).thenAccept(listings -> {
            System.out.println(listings.size() + " Player listing(s) retrieved");

            int amount = listings.size();
            for (int i = 0; i < (amount / 45) + 1; i++) {
                Inventory inv = Bukkit.createInventory(getMarket().createInvHolder(getName()), 54, "Listing Management - Page " + (i + 1) + "/" + ((amount / 54) + 1));
                setupNavBar(inv);
                invs.add(inv);
            }

            int i = 0;
            for(MarketListing listing : listings) {
                Inventory inv = invs.get(i / 45);
                inv.setItem(i % 45, listing.getDisplayItem(listing.getCachedTranscations()));
                i++;
            }

            //TODO check if Still in inventory
            getPlayer().closeInventory();
            getPlayer().openInventory(invs.get(0));

        });
    }

    private Inventory getLoadingInv() {
        Inventory inv = Bukkit.createInventory(getMarket().createInvHolder(getName()), 54, "Listing Management - Loading...");
        setupNavBar(inv);
        return inv;
    }

    private void setupNavBar(Inventory inv) {
        inv.setItem(45, new ItemBuilder(Material.CARROT_ITEM, 1).setName("Previous Page").toItemStack());
        inv.setItem(46, new ItemBuilder(Material.FLOWER_POT_ITEM, 1).setName("Next Page").toItemStack());
        for (int i = 47; i <= 52; i++) {
            if (i == 49) {
                inv.setItem(i, new ItemBuilder(Material.GREEN_RECORD, 1).setName(ChatColor.GREEN + "" + ChatColor.BOLD + "Add Listing").toItemStack());
            } else {
                inv.setItem(i, CrisisItem.INVENTORY_ITEM.getItemStack(1));
            }
        }
        inv.setItem(53, new ItemBuilder(Material.REDSTONE, 1).setName(ChatColor.GRAY + "Back").toItemStack());
    }

    @Override
    public void open(Object args) {
        if (invs.size() == 0) {
            getPlayer().openInventory(getLoadingInv());
            setupInventories();
        } else {
            getPlayer().openInventory(invs.get(0));
        }
    }

    @Override
    public boolean handleInventoryClick(Inventory inv, InventoryAction action, int slot, ItemStack item) {
        if (inv.getName().contains("Loading")) {
            getPlayer().sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "Listings loading...");
            return true;
        }
        switch (slot) {
            case 45:
                if (currentInv > 0) {
                    currentInv--;
                    getPlayer().openInventory(invs.get(currentInv));
                }
                return true;
            case 46:
                if (currentInv < invs.size() - 1) {
                    currentInv++;
                    getPlayer().openInventory(invs.get(currentInv));
                }
                return true;
            case 49:

                return true;
            case 53:
                goBack();
                currentInv = 0;
                return true;
            default:
                return false;
        }
    }
}
