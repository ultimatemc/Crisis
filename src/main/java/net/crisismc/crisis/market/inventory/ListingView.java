package net.crisismc.crisis.market.inventory;

import net.crisismc.crisis.market.CrisisMarket;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ListingView extends MarketInventory {

    public ListingView(CrisisMarket market, Player p, MarketInventory previous) {
        super(p, market, "listing_view", previous);
    }

    @Override
    public void open(Object args) {

    }

    @Override
    public boolean handleInventoryClick(Inventory inv, InventoryAction action, int slot, ItemStack item) {
        return true;
    }
}
