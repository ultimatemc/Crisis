package net.crisismc.crisis.market.inventory;

import net.crisismc.crisis.lib.InventoryUtil;
import net.crisismc.crisis.lib.ItemBuilder;
import net.crisismc.crisis.market.CrisisMarket;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class MainMenu extends MarketInventory {

    Inventory inv;

    public MainMenu(CrisisMarket market, Player p) {
        super(p, market, "main_menu");
        inv = createInventory();
    }

    private Inventory createInventory() {
        Inventory inv = Bukkit.createInventory(getMarket().createInvHolder("main_menu"), 9, "Market");
        InventoryUtil.fill(inv);

        inv.setItem(3, new ItemBuilder(Material.COMPASS, 1).setName(ChatColor.GOLD + "Browse Listings").toItemStack());
        inv.setItem(5, new ItemBuilder(Material.ARROW, 1).setName(ChatColor.GOLD + "Manage Your Listings").toItemStack());

        return inv;
    }

    @Override
    public void open(Object args) {
        getPlayer().openInventory(inv);
    }

    @Override
    public boolean handleInventoryClick(Inventory inv, InventoryAction action, int slot, ItemStack item) {
        switch (slot) {
            case 3:
                getMarket().getInventory("browse_menu").open(null);
                return true;
            case 5:
                getMarket().getInventory("listing_management").open(null);
                return true;
            default:
                return true;
        }
    }
}
