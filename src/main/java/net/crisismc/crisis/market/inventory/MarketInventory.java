package net.crisismc.crisis.market.inventory;

import net.crisismc.crisis.market.CrisisMarket;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public abstract class MarketInventory {

    private Player player;
    private final CrisisMarket market;
    private final String name;
    private final MarketInventory previous;

    public MarketInventory(Player player, CrisisMarket market, String name) {
        this.player = player;
        this.market = market;
        this.name = name;
        this.previous = null;
    }

    public MarketInventory(Player player, CrisisMarket market, String name, MarketInventory previous) {
        this.player = player;
        this.market = market;
        this.name = name;
        this.previous = previous;
    }

    protected void goBack() {
        previous.open(null);
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public CrisisMarket getMarket() {
        return market;
    }

    public String getName() {
        return name;
    }

   public abstract void open(Object args);

    /**
     * @param action
     * @param slot
     * @param item
     * @return Should the click be cancelled?
     */
    public abstract boolean handleInventoryClick(Inventory inv, InventoryAction action, int slot, ItemStack item);
}
