package net.crisismc.crisis.market.inventory;

import net.crisismc.crisis.market.CrisisMarket;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class PurchaseView extends MarketInventory {

    public PurchaseView(CrisisMarket market, Player p, MarketInventory previous) {
        super(p, market, "purchase_view", previous);
    }

    @Override
    public void open(Object args) {

    }

    @Override
    public boolean handleInventoryClick(Inventory inv, InventoryAction action, int slot, ItemStack item) {
        return true;
    }
}
