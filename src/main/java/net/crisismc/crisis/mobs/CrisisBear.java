package net.crisismc.crisis.mobs;

import net.minecraft.server.v1_12_R1.*;
import net.crisismc.crisis.lib.MobUtil;

import java.util.LinkedHashSet;

public class CrisisBear extends EntityPolarBear {


    public CrisisBear(World world) {
        super(world);

        LinkedHashSet goalB = (LinkedHashSet) MobUtil.getPrivateField("b", PathfinderGoalSelector.class, goalSelector);
        goalB.clear();
        LinkedHashSet goalC = (LinkedHashSet) MobUtil.getPrivateField("c", PathfinderGoalSelector.class, goalSelector);
        goalC.clear();
        LinkedHashSet targetB = (LinkedHashSet) MobUtil.getPrivateField("b", PathfinderGoalSelector.class, targetSelector);
        targetB.clear();
        LinkedHashSet targetC = (LinkedHashSet) MobUtil.getPrivateField("c", PathfinderGoalSelector.class, targetSelector);
        targetC.clear();


        this.goalSelector.a(0, new PathfinderGoalFloat(this));
        this.goalSelector.a(4, new PathfinderGoalMeleeAttack(this, 1.0D, false));
        this.goalSelector.a(5, new PathfinderGoalRandomStroll(this, 0.8D));
        this.goalSelector.a(6, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 8.0F));
        this.goalSelector.a(6, new PathfinderGoalRandomLookaround(this));
        this.targetSelector.a(2, new PathfinderGoalNearestAttackableTarget(this, EntityHuman.class, true));

	        /*
            this.goalSelector.a(0, new PathfinderGoalFloat(this));
	        this.goalSelector.a(7, new PathfinderGoalRandomStroll(this, 1.0D));
	        this.goalSelector.a(8, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 8.0F));
	        this.goalSelector.a(8, new PathfinderGoalRandomLookaround(this));
	        this.targetSelector.a(1, new PathfinderGoalHurtByTarget(this, true));
	        this.targetSelector.a(2, new PathfinderGoalAvoidTarget(this, EntityHuman.class, 6.0F, 1.0F, 1.2D));
	        */

    }

    @Override
    protected void initAttributes() {
        super.initAttributes();

        //getAttributeInstance(GenericAttributes.maxHealth).setValue(30.0D);
        getAttributeInstance(GenericAttributes.FOLLOW_RANGE).setValue(32.0D);
        getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).setValue(0.45D); //0.25

        //getAttributeMap().b(GenericAttributes.ATTACK_DAMAGE);
        //getAttributeInstance(GenericAttributes.ATTACK_DAMAGE).setValue(6.0D);
    }
}
