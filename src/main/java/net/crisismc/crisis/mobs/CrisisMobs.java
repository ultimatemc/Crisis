package net.crisismc.crisis.mobs;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;

public enum CrisisMobs {

    PIG {
        @Override
        public void spawn(Location loc) {
            CrisisEntityTypes.spawnEntity(new CrisisPig(((CraftWorld) loc.getWorld()).getHandle()), loc);
        }
    },
    WOLF {
        @Override
        public void spawn(Location loc) {
            CrisisEntityTypes.spawnEntity(new CrisisWolf(((CraftWorld) loc.getWorld()).getHandle()), loc);
        }
    },
    BEAR {
        @Override
        public boolean canSpawn(Location loc) {
            loc = loc.add(-1, 0, -1);
            for (int x = 0; x < 3; x++) {
                for (int y = 0; y < 2; y++) {
                    for (int z = 0; z < 3; z++) {
                        if (loc.add(x, y, z).getBlock().getType().isSolid()) return false;
                    }
                }
            }

            return true;
        }

        @Override
        public void spawn(Location loc) {
            CrisisEntityTypes.spawnEntity(new CrisisBear(((CraftWorld) loc.getWorld()).getHandle()), loc);
        }
    };

    public boolean canSpawn(Location loc) {
        return !loc.getBlock().getType().isSolid();
    }

    public void spawn(Location loc) {
        return;
    }
}
