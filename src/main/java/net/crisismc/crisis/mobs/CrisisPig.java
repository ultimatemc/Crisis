package net.crisismc.crisis.mobs;

import net.crisismc.crisis.lib.MobUtil;
import net.minecraft.server.v1_12_R1.*;

import java.util.LinkedHashSet;

public class CrisisPig extends EntityPig {

    public CrisisPig(World world) {
        super(world);

        LinkedHashSet goalB = (LinkedHashSet) MobUtil.getPrivateField("b", PathfinderGoalSelector.class, goalSelector);
        goalB.clear();
        LinkedHashSet goalC = (LinkedHashSet) MobUtil.getPrivateField("c", PathfinderGoalSelector.class, goalSelector);
        goalC.clear();
        LinkedHashSet targetB = (LinkedHashSet) MobUtil.getPrivateField("b", PathfinderGoalSelector.class, targetSelector);
        targetB.clear();
        LinkedHashSet targetC = (LinkedHashSet) MobUtil.getPrivateField("c", PathfinderGoalSelector.class, targetSelector);
        targetC.clear();


        this.goalSelector.a(0, new PathfinderGoalFloat(this));
        this.goalSelector.a(1, new PathfinderGoalPanic(this, 1.75D)); //1.25D
        this.goalSelector.a(3, new PathfinderGoalAvoidTarget(this, EntityHuman.class, 12.0F, 2.0D, 2.0D)); //6,1,1.2
        this.goalSelector.a(4, new PathfinderGoalMeleeAttack(this, 1.0D, false));
        this.goalSelector.a(5, new PathfinderGoalRandomStroll(this, 0.8D));
        this.goalSelector.a(6, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 8.0F));
        this.goalSelector.a(6, new PathfinderGoalRandomLookaround(this));

	        /*
            this.goalSelector.a(0, new PathfinderGoalFloat(this));
	        this.goalSelector.a(7, new PathfinderGoalRandomStroll(this, 1.0D));
	        this.goalSelector.a(8, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 8.0F));
	        this.goalSelector.a(8, new PathfinderGoalRandomLookaround(this));
	        this.targetSelector.a(1, new PathfinderGoalHurtByTarget(this, true));
	        this.targetSelector.a(2, new PathfinderGoalAvoidTarget(this, EntityHuman.class, 6.0F, 1.0F, 1.2D));
	        */

    }
}
