package net.crisismc.crisis.mobs;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.CrisisItem;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class MobSpawning {

    public List<Material> spawnBlocks = new ArrayList<>();
    private RandomCollection<ItemStack> randomMobs = new RandomCollection<>();

    public MobSpawning(Crisis plugin) {
        spawnBlocks.add(Material.GRASS);

        randomMobs.add(6, CrisisMobs.PIG);
        randomMobs.add(3, CrisisMobs.WOLF);
        randomMobs.add(1, CrisisMobs.BEAR);

        new MobSpawningListener(plugin, this);
    }

    public boolean spawnMob(Location loc) {
        Random rand = new Random();
        int range = 50;
        //Find random nearby spawn location;
        loc.setX((loc.getX() + rand.nextInt(range * 2)) - 50);
        loc.setZ((loc.getZ() + rand.nextInt(range * 2)) - 50);

        loc = getSpawnLoc(loc);
        if (loc == null) return false;


        for (Player p : loc.getWorld().getPlayers()) {
            if (p.getLocation().distanceSquared(loc) < 2500) return false;
        }

        CrisisMobs mob = randomMobs.next();
        if (mob.canSpawn(loc)) {
            System.out.println(mob.getClass().getName());
            mob.spawn(loc.add(0.5, 0.5, 0.5));
            return true;
        }


        return false;
    }

    private Location getSpawnLoc(Location loc) {
        loc.setY(200.0);
        Block block = null, lastBlock = null;
        while (loc.getY() >= 0) {
            block = loc.getBlock();
            if (lastBlock != null && spawnBlocks.contains(block.getType()) && !lastBlock.getType().isSolid())
                return lastBlock.getLocation();

            lastBlock = block;
            loc.setY(loc.getY() - 1.0D);

        }

        return null;

    }

}

class MobSpawningListener implements Listener {

    private MobSpawning mobs;

    public MobSpawningListener(Crisis plugin, MobSpawning mobs) {
        this.mobs = mobs;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onEntitySpawn(CreatureSpawnEvent event) {
        if (event.getSpawnReason() == SpawnReason.NATURAL) {
            event.setCancelled(true);
            System.out.println(mobs.spawnMob(event.getLocation()));
        }
    }

    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        if (event.getEntityType() == EntityType.PIG) {
            Random r = new Random();
            event.getDrops().clear();
            event.getDrops().add(CrisisItem.MEAT_RAW.getItemStack(1));
            event.getDrops().add(CrisisItem.MEAT_RAW.getItemStack(1));
            event.getDrops().add(CrisisItem.CLOTH.getItemStack(r.nextInt(3) + 1));
            event.getDrops().add(CrisisItem.ANIMAL_FAT.getItemStack(r.nextInt(3) + 2));
            event.getDrops().add(CrisisItem.BONE_FRAGMENTS.getItemStack(r.nextInt(3) + 2));
            event.setDroppedExp(0);
        } else if (event.getEntityType() == EntityType.WOLF) {
            Random r = new Random();
            event.getDrops().clear();
            event.getDrops().add(CrisisItem.MEAT_RAW.getItemStack(1));
            event.getDrops().add(CrisisItem.CLOTH.getItemStack(r.nextInt(3) + 2));
            event.getDrops().add(CrisisItem.ANIMAL_FAT.getItemStack(r.nextInt(3) + 1));
            event.getDrops().add(CrisisItem.BONE_FRAGMENTS.getItemStack(r.nextInt(3) + 2));
            event.setDroppedExp(0);
        } else if (event.getEntityType() == EntityType.POLAR_BEAR) {
            Random r = new Random();
            event.getDrops().clear();
            event.getDrops().add(CrisisItem.MEAT_RAW.getItemStack(1));
            event.getDrops().add(CrisisItem.MEAT_RAW.getItemStack(1));
            event.getDrops().add(CrisisItem.MEAT_RAW.getItemStack(1));
            event.getDrops().add(CrisisItem.MEAT_RAW.getItemStack(1));
            event.getDrops().add(CrisisItem.CLOTH.getItemStack(r.nextInt(5) + 10));
            event.getDrops().add(CrisisItem.ANIMAL_FAT.getItemStack(r.nextInt(5) + 10));
            event.getDrops().add(CrisisItem.BONE_FRAGMENTS.getItemStack(r.nextInt(5) + 3));
            event.setDroppedExp(0);
        }

    }
}

class RandomCollection<E> {
    private final NavigableMap<Double, CrisisMobs> map = new TreeMap<>();
    private final Random random;
    private double total = 0;

    public RandomCollection() {
        this(new Random());
    }

    public RandomCollection(Random random) {
        this.random = random;
    }

    public void add(double weight, CrisisMobs pig) {
        if (weight <= 0) return;
        total += weight;
        map.put(total, pig);
    }

    public CrisisMobs next() {
        double value = random.nextDouble() * total;
        return map.ceilingEntry(value).getValue();
    }
}
