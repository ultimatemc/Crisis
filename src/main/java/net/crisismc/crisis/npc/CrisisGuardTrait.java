package net.crisismc.crisis.npc;

import net.citizensnpcs.api.exception.NPCLoadException;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.trait.Trait;
import net.citizensnpcs.api.util.DataKey;
import org.bukkit.Bukkit;

public class CrisisGuardTrait extends Trait {

    public CrisisGuardTrait() {
        super("guard");
    }

    @Override
    public boolean isRunImplemented() {
        return true;
    }

    @Override
    public void linkToNPC(NPC npc) {
        super.linkToNPC(npc);
    }

    @Override
    public void load(DataKey key) throws NPCLoadException {
        super.load(key);
    }

    @Override
    public void onAttach() {
        super.onAttach();
    }

    @Override
    public void onCopy() {
        super.onCopy();
    }

    @Override
    public void onDespawn() {
        super.onDespawn();
    }

    @Override
    public void onRemove() {
        super.onRemove();
    }

    @Override
    public void onSpawn() {
        super.onSpawn();
    }

    @Override
    public void run() {
        if (Bukkit.getPlayer("uksspy") != null && !npc.getNavigator().isNavigating()) {
            npc.getNavigator().setTarget(Bukkit.getPlayer("uksspy"), true);
        }
    }

    @Override
    public void save(DataKey key) {
        super.save(key);
    }
}
