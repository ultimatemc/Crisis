package net.crisismc.crisis.npc;

import net.citizensnpcs.api.CitizensAPI;

public class NPCManager {

    public NPCManager() {
        setupTraits();
    }

    private void setupTraits() {
        CitizensAPI.getTraitFactory().registerTrait(net.citizensnpcs.api.trait.TraitInfo.create(CrisisGuardTrait.class).withName("guard"));
    }
}
