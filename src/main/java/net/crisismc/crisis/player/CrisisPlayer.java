package net.crisismc.crisis.player;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.building.SleepingBag;
import net.crisismc.crisis.crafting.CrisisRecipe;
import net.crisismc.crisis.database.CrisisPlayerLoader;
import net.crisismc.crisis.lib.ChatUtil;
import net.crisismc.crisis.lib.PacketUtil;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class CrisisPlayer {

    public static CrisisPlayerLoader cpLoader;
    public static Map<UUID, CrisisPlayer> crisisPlayers = new ConcurrentHashMap<>();

    public Player p;
    public UUID uuid;


    //Stuff from db
    private double food;
    private double water;
    private int xp;
    private int lifeXp;
    private boolean legBroken;
    private boolean hasBed;
    private SleepingBag bed;
    private List<CrisisRecipe> knownRecipes = new ArrayList<>();
    private CrisisPlayerUpgrades playerUpgrades;

    //Resources
    private long resourceTime = 0;
    private String lastResource = "";
    private int resourceAmount = 0;

    //Status
    private boolean bleeding = false;
    private int bleedAmount = 0;
    private byte legMsg = 0;

    //Transient
    private int level = -1;
    private int calculatedXp = 0;

    private int upgradeLevel = -1;
    private int upgradeCalculatedXp = 0;

    //Spawning
    private int spawnDelay = 0;


    public CrisisPlayer(Player p) {
        this.p = p;
        this.uuid = p.getUniqueId();
        cpLoader.loadCrisisPlayer(p, this);
    }

    public static CrisisPlayer getCrisisPlayer(Player p) {
        return crisisPlayers.get(p.getUniqueId());
    }

    public static void saveCrisisPlayers() {
        for (CrisisPlayer cp : crisisPlayers.values()) {
            cpLoader.saveCrisisPlayerSync(cp, cp.p.getUniqueId().toString());
        }
    }

    public static void task(Crisis plugin) {
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
            int tick = 1;

            public void run() {

                for (CrisisPlayer cp : crisisPlayers.values()) {
                    if (cp.bleeding) {
                        cp.p.getWorld().spawnParticle(Particle.REDSTONE, cp.p.getLocation().add(0, 1.0, 0), 1, 0.1, 0.1, 0.1, 0);
                        if (tick % 60 == 0) {
                            double health = cp.p.getHealth();
                            health = health - 1;
                            if (health < 1) {
                                health = 0;
                            }
                            cp.p.setHealth(health);

                            cp.bleedAmount -= 1;
                            if (cp.bleedAmount <= 0) cp.setBleeding(false, 0);
                        }
                    }

                    if (tick % 6000 == 0) {
                        if (cp.p.getHealth() != 0) {
                            double health = cp.p.getHealth() + 1;
                            if (health > cp.p.getMaxHealth()) health = cp.p.getMaxHealth();
                            cp.p.setHealth(health);
                        }
                    }
                    if (tick % 20 == 0) {
                        cp.tickSpawnDelay();
                    }
                }
                tick++;
            }
        }, 1, 1);
    }

    public void handleBrokenLegMove(PlayerMoveEvent event) {
        Location f = event.getFrom();
        Location t = event.getTo();

        //If horizontal is different
        if ((f.getX() != t.getX() || f.getZ() != t.getZ()) && isOnGround()) {
            if (!p.isSneaking()) {
                f.setPitch(t.getPitch());
                f.setYaw(t.getYaw());
                p.teleport(f);
                legMsg++;
                if (legMsg == 20) {
                    p.sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "Your leg is broken, you must shift to walk.");
                    legMsg = 0;
                }
            }
        }
    }

    public void handleBlueprintClick(ItemStack item) {
        if (!item.hasItemMeta()) return; //Not a blueprint

        //Isolate the blueprint recipe name
        String recipeStr = item.getItemMeta().getDisplayName().split("Blueprint")[0];
        CrisisRecipe cr = CrisisRecipe.getRecipe(recipeStr);
        if (!knownRecipes.contains(cr)) {
            knownRecipes.add(cr);
            p.sendMessage(ChatUtil.getPrefix() + ChatColor.GRAY + "Learned how to craft " + ChatColor.GOLD + ChatColor.stripColor(cr.getResult().getItemMeta().getDisplayName().split("<")[0]));
            p.getInventory().setItemInMainHand(null);
        } else {
            p.sendMessage(ChatUtil.getPrefix() + ChatColor.GRAY + "You already know how to craft that item");
            p.getInventory().setItemInMainHand(item);
            p.updateInventory();
        }
    }

    public List<String> getScoreboardContent() {
        List<String> lines = new ArrayList<>();
        lines.add(p.getName());
        return lines;
    }


    public void updateDbValues(double food, double water, int xp, int lifeXp, boolean legBroken, boolean hasBed, SleepingBag bed, List<CrisisRecipe> knownRecipes) {
        this.food = food;
        this.water = water;
        this.xp = xp;
        this.lifeXp = lifeXp;
        this.legBroken = legBroken;
        this.hasBed = hasBed;
        this.bed = bed;
        this.setKnownRecipes(knownRecipes);
    }


    public void tickSpawnDelay() {
        if (spawnDelay > 0) spawnDelay--;
    }

    public void onDeath() {
        water = 3001;
        food = 3001;
        lifeXp = 0;
        setBleeding(false, 0);
        spawnDelay = 60;
        legBroken = false;
        update();
    }

    public void setBleeding(boolean bleeding, int bleedAmount) {
        this.bleeding = bleeding;
        this.bleedAmount = bleedAmount;

        if (bleeding) {
            PacketUtil.sendWorldBorderPacket(p, Integer.MAX_VALUE);
        } else {
            PacketUtil.sendWorldBorderPacket(p, 0);
        }
    }

    public void useBandage() {
        p.playEffect(p.getLocation(), Effect.STEP_SOUND, Material.WOOL);
        p.getInventory().removeItem(CrisisItem.BANDAGE.getItemStack(1));
        p.sendMessage(ChatUtil.getPrefix() + ChatColor.GREEN + "You have bandaged your wound.");
        setBleeding(false, 0);
    }

    public void sendResourceMessage(String resource, int amount) {
        if (resource.equals(lastResource) && System.currentTimeMillis() - resourceTime < 4000) {
            resourceAmount += amount;
        } else {
            resourceAmount = amount;
        }
        PacketUtil.sendActionBarPacket(p, ChatColor.GREEN + "+" + resourceAmount + " " + resource);
        lastResource = resource;
        resourceTime = System.currentTimeMillis();
    }

    public void saveToDB() {
        cpLoader.saveCrisisPlayer(this);
    }

    private void update() {
        int f = (int) food;
        if (f > 3100) f = 3000;
        if (f < 1) f = 0;
        p.setFoodLevel((f / 150));

        int w = (int) water;
        if (w > 3100) w = 3100;
        if (w < 1) w = 0;
        p.setLevel((w / 60));
        if (p.getLevel() > 50) p.setLevel(50);
    }

    public void remove() {
        crisisPlayers.remove(uuid);
    }


    public double getFood() {
        return food;
    }


    public void setFood(double food) {
        this.food = food;
        update();
    }


    public double getWater() {
        return water;
    }


    public void setWater(double water) {
        this.water = water;
        update();
    }


    public boolean isLegBroken() {
        return legBroken;
    }


    public void setLegBroken(boolean legBroken) {
        this.legBroken = legBroken;
    }


    public boolean hasBed() {
        return hasBed;
    }


    public void setHasBed(boolean hasbed) {
        this.hasBed = hasbed;
    }


    public SleepingBag getBed() {
        return bed;
    }


    public void setBed(SleepingBag b) {
        bed = b;
    }

    public int getSpawnDelay() {
        return spawnDelay;
    }

    public void setSpawnDelay(int spawnDelay) {
        this.spawnDelay = spawnDelay;
    }

    public List<CrisisRecipe> getKnownRecipes() {
        return knownRecipes;
    }

    public void setKnownRecipes(List<CrisisRecipe> knownRecipes) {
        this.knownRecipes = knownRecipes;
    }

    private boolean isOnGround() {
        return (p.getLocation().add(0, -0.1, 0).getBlock().getType().isSolid()
                || p.getLocation().add(-0.3, -0.1, -0.3).getBlock().getType().isSolid()
                || p.getLocation().add(-0.3, -0.1, 0.3).getBlock().getType().isSolid()
                || p.getLocation().add(0.3, -0.1, 0.3).getBlock().getType().isSolid()
                || p.getLocation().add(-0.3, -0.1, 0.3).getBlock().getType().isSolid());
    }

    public int getXp() {
        return xp;
    }

    public int getLevel() {
        if (level == -1) {
            level = calculateLevel();
            calculatedXp = xp;
        } else if (xp != calculatedXp) {
            level = calculateLevel();
        }
        return level;
    }

    public int getUpgradeLevel() {
        if (upgradeLevel == -1) {
            upgradeLevel = calculateUpgradeLevel();
            upgradeCalculatedXp = lifeXp;
        } else if (lifeXp != upgradeCalculatedXp) {
            upgradeLevel = calculateUpgradeLevel();
        }
        return upgradeLevel;
    }

    public int getXpToNextLevel() {
        return levelXp(getLevel() + 1) - xp;
    }

    public String getLevelProgress() {
        double levelXp = levelXp(getLevel() + 1) - levelXp(getLevel());
        double levelLeft = getXpToNextLevel();
        return ChatUtil.formatDouble((levelXp - levelLeft) / levelXp * 100);
    }

    public int getXpToNextUpgradeLevel() {
        return levelXp(getUpgradeLevel() + 1) - lifeXp;
    }

    private int calculateLevel() {
        // 10^(1/3) = 2.15443469003D
        return (int) Math.floor(Math.cbrt(Math.sqrt(Math.pow(xp, 2) + 160*xp) + xp + 80) / 2.15443469003D + 4 * 2.15443469003D / Math.cbrt(Math.sqrt(Math.pow(xp, 2) + 160*xp) + xp + 80) - 3);
    }

    private int calculateUpgradeLevel() {
        // 10^(1/3) = 2.15443469003D
        return (int) Math.floor(Math.cbrt(Math.sqrt(Math.pow(lifeXp, 2) + 160*lifeXp) + lifeXp + 80) / 2.15443469003D + 4 * 2.15443469003D / Math.cbrt(Math.sqrt(Math.pow(lifeXp, 2) + 160*lifeXp) + lifeXp + 80) - 3);
    }

    private int levelXp(int x) {
        return 5 * (x - 1) * (x + 5) * (x + 5);
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public void awardXp(int xp) {
        this.xp += xp;
        this.lifeXp += xp;
    }

    public int getLifeXp() {
        return lifeXp;
    }

    public void setLifeXp(int lifeXp) {
        this.lifeXp = lifeXp;
    }

    public CrisisPlayerUpgrades getPlayerUpgrades() {
        return playerUpgrades;
    }

    public void setPlayerUpgrades(CrisisPlayerUpgrades playerUpgrades) {
        this.playerUpgrades = playerUpgrades;
    }
}
