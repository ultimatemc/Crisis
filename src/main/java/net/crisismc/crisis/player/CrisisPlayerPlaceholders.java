package net.crisismc.crisis.player;

import be.maximvdw.placeholderapi.PlaceholderAPI;
import net.crisismc.crisis.Crisis;
import org.bukkit.Bukkit;

public class CrisisPlayerPlaceholders {

    public CrisisPlayerPlaceholders() {
        if(Bukkit.getPluginManager().isPluginEnabled("MVdWPlaceholderAPI")) {
            registerPlaceholders();
        }
    }

    private void registerPlaceholders() {
        PlaceholderAPI.registerPlaceholder(Crisis.getInstance(), "crisis_xp", event -> {
            if (event.isOnline()) {
                CrisisPlayer cp = CrisisPlayer.getCrisisPlayer(event.getPlayer());
                if (cp == null) return "Loading...";
                return "" + cp.getXp();
            }
            return "";
        });

        PlaceholderAPI.registerPlaceholder(Crisis.getInstance(), "crisis_lifexp", event -> {
            if (event.isOnline()) {
                CrisisPlayer cp = CrisisPlayer.getCrisisPlayer(event.getPlayer());
                if (cp == null) return "Loading...";
                return "" + cp.getLifeXp();
            }
            return "";
        });

        PlaceholderAPI.registerPlaceholder(Crisis.getInstance(), "crisis_level", event -> {
            if (event.isOnline()) {
                CrisisPlayer cp = CrisisPlayer.getCrisisPlayer(event.getPlayer());
                if (cp == null) return "Loading...";
                return "" + cp.getLevel();
            }
            return "";
        });

        PlaceholderAPI.registerPlaceholder(Crisis.getInstance(), "crisis_upgradelevel", event -> {
            if (event.isOnline()) {
                CrisisPlayer cp = CrisisPlayer.getCrisisPlayer(event.getPlayer());
                if (cp == null) return "Loading...";
                return "" + cp.getUpgradeLevel();
            }
            return "";
        });

        PlaceholderAPI.registerPlaceholder(Crisis.getInstance(), "crisis_xptolevel", event -> {
            if (event.isOnline()) {
                CrisisPlayer cp = CrisisPlayer.getCrisisPlayer(event.getPlayer());
                if (cp == null) return "Loading...";
                return "" + cp.getXpToNextLevel();
            }
            return "";
        });

        PlaceholderAPI.registerPlaceholder(Crisis.getInstance(), "crisis_xptoupgrade", event -> {
            if (event.isOnline()) {
                CrisisPlayer cp = CrisisPlayer.getCrisisPlayer(event.getPlayer());
                if (cp == null) return "Loading...";
                return "" + cp.getXpToNextUpgradeLevel();
            }
            return "";
        });

        PlaceholderAPI.registerPlaceholder(Crisis.getInstance(), "crisis_levelprogress", event -> {
            if (event.isOnline()) {
                CrisisPlayer cp = CrisisPlayer.getCrisisPlayer(event.getPlayer());
                if (cp == null) return "Loading...";
                return cp.getLevelProgress();
            }
            return "";
        });
    }

}
