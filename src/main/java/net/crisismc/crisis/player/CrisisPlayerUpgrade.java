package net.crisismc.crisis.player;

public enum CrisisPlayerUpgrade {
    VITALITY, SUSTENANCE
}
