package net.crisismc.crisis.resources;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.player.CrisisPlayer;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import java.util.ArrayList;
import java.util.List;

public class ResourceManager {


    private Crisis plugin;

    List<Stone> harvestedStone = new ArrayList<>();
    List<Wood> harvestedWood = new ArrayList<>();

    public ResourceManager(Crisis plugin) {
        this.plugin = plugin;
        resourceTask();
        new ResourceListener(plugin, this);
    }

    public void onDisable() {
        for (Stone stone : harvestedStone) {
            stone.getLoc().getBlock().setType(Material.STONE);
        }

        for (Wood wood : harvestedWood) {
            wood.getLoc().getBlock().setType(Material.LOG);
        }
    }

    private void resourceTask() {
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            List<Stone> stoneRemove = new ArrayList<>();
            for (Stone stone : harvestedStone) {
                stone.setMinutesLeft(stone.getMinutesLeft() - 1);
                if (stone.getMinutesLeft() <= 0) {
                    stoneRemove.add(stone);
                    stone.getLoc().getBlock().setType(Material.STONE);
                }
            }
            harvestedStone.removeAll(stoneRemove);

            List<Wood> woodRemove = new ArrayList<>();
            for (Wood wood : harvestedWood) {
                wood.setMinutesLeft(wood.getMinutesLeft() - 1);
                if (wood.getMinutesLeft() <= 0) {
                    woodRemove.add(wood);
                    wood.getLoc().getBlock().setData((byte) 0);
                }
            }
            harvestedWood.removeAll(woodRemove);
        }, 1200, 1200);
    }
}

class ResourceListener implements Listener {

    private ResourceManager rm;

    public ResourceListener(Crisis plugin, ResourceManager rm) {
        this.rm = rm;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.getPlayer().getGameMode() == GameMode.CREATIVE || !event.getPlayer().getWorld().getName().equalsIgnoreCase("world")) return;

        if (event.getBlock().getType() == Material.STONE || event.getBlock().getType() == Material.COBBLESTONE) {
            if (Stone.onStoneBreak(event.getBlock(), event.getPlayer(), event.getPlayer().getInventory().getItemInMainHand()))
                rm.harvestedStone.add(new Stone(event.getBlock().getLocation(), 30));
            event.setCancelled(true);
        }
        if (event.getBlock().getType() == Material.LOG) {
            if (Wood.onWoodBreak(event.getBlock(), event.getPlayer(), event.getPlayer().getInventory().getItemInMainHand()))
                rm.harvestedWood.add(new Wood(event.getBlock().getLocation(), 15));
            event.setCancelled(true);
        }

		/*
         * Infinite Resources
		 */
        if (event.getBlock().getType() == Material.SAND && event.getBlock().getData() == (byte) 0) {
            event.getPlayer().getInventory().addItem(CrisisItem.SAND.getItemStack(1));
            CrisisPlayer.crisisPlayers.get(event.getPlayer().getUniqueId()).sendResourceMessage("Sand", 1);
            event.setCancelled(true);
        }
    }
}
