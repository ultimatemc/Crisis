package net.crisismc.crisis.resources;

import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.building.Town;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.player.CrisisPlayer;
import net.crisismc.crisis.lib.ChatUtil;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class Stone {

    private Location loc;
    private int minutesLeft;

    public Stone(Location loc, int minutesLeft) {
        this.setLoc(loc);
        this.setMinutesLeft(minutesLeft);
    }

    public Location getLoc() {
        return loc;
    }

    public void setLoc(Location loc) {
        this.loc = loc;
    }

    public int getMinutesLeft() {
        return minutesLeft;
    }

    public void setMinutesLeft(int minutesLeft) {
        this.minutesLeft = minutesLeft;
    }

    public static boolean onStoneBreak(Block b, Player p, ItemStack item) {
        CrisisPlayer cp = CrisisPlayer.crisisPlayers.get(p.getUniqueId());

        Location loc = b.getLocation();
        for (Town town : Crisis.TOWNS) {
            if (town.isInside(b.getLocation())) return false;
        }

        if (b.getType() == Material.STONE) {
            Random r = new Random();
            p.getInventory().addItem(CrisisItem.STONE.getItemStack(1));
            cp.awardXp(1);
            String resourceStr = "Stone";

            if (r.nextInt(5) == 0) {
                if (r.nextBoolean()) {
                    p.getInventory().addItem(CrisisItem.SULFUR_ORE.getItemStack(1));
                    resourceStr = resourceStr + " & 1 Sulfur Ore";
                } else {
                    p.getInventory().addItem(CrisisItem.METAL_ORE.getItemStack(1));
                    resourceStr = resourceStr + " & 1 Metal Ore";
                }
            }
            cp.sendResourceMessage(resourceStr, 1);

            p.getWorld().playSound(p.getLocation(), Sound.BLOCK_STONE_BREAK, 4, 0.7F);
            b.setType(Material.COBBLESTONE);
            return true;

        } else if (b.getType() == Material.COBBLESTONE) {
            p.sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "There is no more stone here.");
            p.playSound(p.getLocation(), Sound.BLOCK_STONE_PLACE, 1, 1);
            return false;
        }
        return false;

    }
}
