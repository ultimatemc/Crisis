package net.crisismc.crisis.resources;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.CrisisItem;
import net.crisismc.crisis.player.CrisisPlayer;
import net.crisismc.crisis.building.Town;
import net.crisismc.crisis.lib.ChatUtil;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class Wood {

    private Location loc;
    private int minutesLeft;

    public Wood(Location loc, int minutesLeft) {
        this.setLoc(loc);
        this.setMinutesLeft(minutesLeft);
    }

    public Location getLoc() {
        return loc;
    }

    public void setLoc(Location loc) {
        this.loc = loc;
    }

    public int getMinutesLeft() {
        return minutesLeft;
    }

    public void setMinutesLeft(int minutesLeft) {
        this.minutesLeft = minutesLeft;
    }

    public static boolean onWoodBreak(Block b, Player p, ItemStack item) {
        CrisisPlayer cp = CrisisPlayer.crisisPlayers.get(p.getUniqueId());

        Location loc = b.getLocation();
        for (Town town : Crisis.TOWNS) {
            if (town.isInside(b.getLocation())) return false;
        }


        if (b.getType() == Material.LOG) {
            if (b.getData() == 0) {
                Random r = new Random();
                p.getInventory().addItem(CrisisItem.STICK.getItemStack(1));
                cp.sendResourceMessage("Wood", 1);
                cp.awardXp(1);

                p.getWorld().playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_BREAK_DOOR_WOOD, 4, 1);
                b.setData((byte) 2);
                return true;

            } else if (b.getData() == 2) {
                p.sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "There is no more wood here.");
                p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_DOOR_WOOD, 1, 1);
                return false;
            } else {
                p.sendMessage(ChatUtil.getPrefix() + ChatColor.RED + "This log is too rotten to be harvested.");
                return false;
            }

        }
        return false;

    }
}
