package net.crisismc.crisis.statistics;

import java.util.UUID;

public class PlayerStatistics {

    final UUID uuid;

    private long playTime;
    private int ffaKills;
    private int kills;
    private int ffaDeaths;
    private int deaths;

    public PlayerStatistics(UUID uuid) {
        this.uuid = uuid;
        playTime = ffaKills = kills = ffaDeaths = deaths = 0;
    }

    public PlayerStatistics(UUID uuid, long play_time, int kills_ffa, int kills, int deaths_ffa, int deaths) {
        this.uuid = uuid;
        this.playTime = play_time;
        this.ffaKills = kills_ffa;
        this.kills = kills;
        this.ffaDeaths = deaths_ffa;
        this.deaths = deaths;
    }

    public UUID getUuid() {
        return uuid;
    }

    public long getPlayTime() {
        return playTime;
    }

    public void setPlayTime(long playTime) {
        this.playTime = playTime;
    }

    public int getFfaKills() {
        return ffaKills;
    }

    public void setFfaKills(int ffaKills) {
        this.ffaKills = ffaKills;
    }

    public void addFFaKill() {
        ffaKills ++;
    }

    public int getKills() {
        return kills;
    }

    public void addKill() {
        kills ++;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public int getFfaDeaths() {
        return ffaDeaths;
    }

    public void setFfaDeaths(int ffaDeaths) {
        this.ffaDeaths = ffaDeaths;
    }

    public void addFfaDeath() {
        ffaDeaths ++;
    }

    public int getDeaths() {
        return deaths;
    }

    public void addDeath() {
        deaths ++;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }
}
