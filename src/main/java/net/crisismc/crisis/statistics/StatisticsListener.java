package net.crisismc.crisis.statistics;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.ffa.CrisisFFA;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;

import static org.bukkit.ChatColor.*;


public class StatisticsListener implements Listener {

    private StatisticsManager sm;

    public StatisticsListener(StatisticsManager sm) {
        this.sm = sm;
        Crisis.getInstance().getServer().getPluginManager().registerEvents(this, Crisis.getInstance());
    }

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent event) {
        sm.getStatsLoader().loadPlayerStatistics(event.getPlayer()).thenAccept(ps -> sm.addPlayerStatistics(ps));
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player p = event.getPlayer();
        if (sm.getPlayerStatistics(p) != null) {
            sm.getStatsLoader().savePlayerStatistics(sm.getPlayerStatistics(p), false);
            sm.removePlayerStatistics(sm.getPlayerStatistics(p));
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player p = event.getEntity();
        if (p == p.getKiller()) return;
        if (p.getWorld().getName().equalsIgnoreCase("ffa")) {
            sm.getPlayerStatistics(p).addFfaDeath();

            if (p.getKiller() != null) {

                // Messages
                p.sendMessage(CrisisFFA.getDeathMessage(p.getKiller(), p.getKiller().getLocation().distance(p.getLocation())));
                p.getKiller().sendMessage(CrisisFFA.getKillMessage(p.getKiller(), p, p.getKiller().getLocation().distance(p.getLocation())));

                sm.getPlayerStatistics(p.getKiller()).addFFaKill();
                for (PotionEffect potionEffect : p.getKiller().getActivePotionEffects()) {
                    if (potionEffect.getType().getName().equals("FAST_DIGGING")) {
                        sm.getPlayerStatistics(p.getKiller()).addFFaKill();
                        p.getKiller().sendMessage(GREEN + "" + BOLD + "[+1 Kill] " + GRAY + "Bonus kill awarded for being in control point");
                        break;
                    }
                }
            }
        } else if (p.getWorld().getName().equalsIgnoreCase("world")) {
            sm.getPlayerStatistics(p).addDeath();

            if (p.getKiller() != null) {
                sm.getPlayerStatistics(p.getKiller()).addKill();
            }
        }
    }
}
