package net.crisismc.crisis.statistics;

import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.database.StatisticsLoader;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class StatisticsManager {

    private StatisticsLoader statsLoader;

    private Map<UUID, PlayerStatistics> players = new ConcurrentHashMap<>();
    private StatisticsListener listener;

    private List<PlayerStatistics> topFfaKills;

    public StatisticsManager(StatisticsLoader statsLoader) {
        this.statsLoader = statsLoader;

        listener = new StatisticsListener(this);
        onLoad();
        task();

        new StatisticsPlaceholders(this);
    }

    private void task() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Crisis.getInstance(), new Runnable() {
            int seconds = 0;

            @Override
            public void run() {
                //Save statistics once a minute
                if (seconds % 60 == 0) {
                    for (Map.Entry<UUID, PlayerStatistics> entry : players.entrySet()) {
                        statsLoader.savePlayerStatistics(entry.getValue(), false);
                    }

                    //Update top kills
                    statsLoader.topFfaKills().thenAccept(result -> topFfaKills = result);
                }

                for (Map.Entry<UUID, PlayerStatistics> entry : players.entrySet()) {
                    entry.getValue().setPlayTime(entry.getValue().getPlayTime() + 1000);
                }

                seconds++;
            }
        }, 20, 20);
    }

    private void onLoad() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            statsLoader.loadPlayerStatistics(p).thenAccept(ps -> players.put(p.getUniqueId(), ps));
        }
    }

    public void onShutDown() {
        for (Map.Entry<UUID, PlayerStatistics> entry : players.entrySet()) {
            statsLoader.savePlayerStatistics(entry.getValue(), true);
        }
    }

    public List<PlayerStatistics> getTopFfaKills() {
        return topFfaKills;
    }

    public void addPlayerStatistics(PlayerStatistics ps) {
        players.put(ps.getUuid(), ps);
    }

    public void removePlayerStatistics(PlayerStatistics ps) {
        players.remove(ps);
    }

    public PlayerStatistics getPlayerStatistics(Player p) {
        return players.get(p.getUniqueId());
    }

    public StatisticsLoader getStatsLoader() {
        return statsLoader;
    }
}
