package net.crisismc.crisis.statistics;

import be.maximvdw.placeholderapi.PlaceholderAPI;
import net.crisismc.crisis.Crisis;
import org.bukkit.Bukkit;

public class StatisticsPlaceholders {

    private StatisticsManager sm;
    public StatisticsPlaceholders(StatisticsManager sm) {
        this.sm = sm;

        if(Bukkit.getPluginManager().isPluginEnabled("MVdWPlaceholderAPI")) {
            registerPlaceholders();
        }
    }

    private void registerPlaceholders() {
        PlaceholderAPI.registerPlaceholder(Crisis.getInstance(), "crisis_ffakills", event -> {
            if (event.isOnline()) {
                PlayerStatistics ps = sm.getPlayerStatistics(event.getPlayer());
                if (ps == null) return "Loading...";
                return "" + ps.getFfaKills();
            }
            return "";
        });

        PlaceholderAPI.registerPlaceholder(Crisis.getInstance(), "crisis_ffadeaths", event -> {
            if (event.isOnline()) {
                PlayerStatistics ps = sm.getPlayerStatistics(event.getPlayer());
                if (ps == null) return "Loading...";
                return "" + ps.getFfaDeaths();
            }
            return "";
        });

        for (int i = 1; i <= 5; i++) {
            int place = i;
            PlaceholderAPI.registerPlaceholder(Crisis.getInstance(), "crisis_topffaplayers_" + i, event -> {
                if (sm.getTopFfaKills() != null && sm.getTopFfaKills().size() >= place) {
                    PlayerStatistics stats = sm.getTopFfaKills().get(place - 1);
                    if (stats != null) {
                        return Bukkit.getOfflinePlayer(stats.getUuid()).getName();
                    }  else {
                        return "";
                    }
                } else {
                    return "Loading...";
                }
            });
        }

        for (int i = 1; i <= 5; i++) {
            int place = i;
            PlaceholderAPI.registerPlaceholder(Crisis.getInstance(), "crisis_topffakills_" + i, event -> {
                if (sm.getTopFfaKills() != null && sm.getTopFfaKills().size() >= place) {
                    PlayerStatistics stats = sm.getTopFfaKills().get(place - 1);
                    if (stats != null) {
                        return "" + stats.getFfaKills();
                    }  else {
                        return "";
                    }
                } else {
                    return "Loading...";
                }
            });
        }
    }

}
