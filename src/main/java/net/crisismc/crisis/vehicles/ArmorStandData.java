package net.crisismc.crisis.vehicles;

import net.minecraft.server.v1_12_R1.EntityArmorStand;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

public class ArmorStandData {

    public EntityArmorStand stand;
    public ItemStack equipment;
    public Location loc;

    public ArmorStandData(EntityArmorStand stand, ItemStack equipment, Location loc) {
        this.stand = stand;
        this.equipment = equipment;
        this.loc = loc;
    }
}
