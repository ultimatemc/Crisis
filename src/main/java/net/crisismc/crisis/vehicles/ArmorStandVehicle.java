package net.crisismc.crisis.vehicles;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.blocks.BaseBlock;
import net.crisismc.crisis.Crisis;
import net.crisismc.crisis.lib.SchematicTools;
import net.minecraft.server.v1_12_R1.EntityArmorStand;
import net.minecraft.server.v1_12_R1.EnumItemSlot;
import net.minecraft.server.v1_12_R1.WorldServer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class ArmorStandVehicle {

    private Map<String, ArmorStandVehicleSection> sections = new HashMap<>();
    private ArmorStandVehicleSection main;
    final private Location loc;
    private int taskId;
    private long tick = 0;
    private boolean valid = true;

    /**
     * @param loc      The center location of the vehicle (relative to center diamond block and schematic offsets)
     * @param sections The schematics that make up the vehicle
     */
    public ArmorStandVehicle(Location loc, Map<String, CuboidClipboard> sections) {
        this.loc = loc;
        for (Map.Entry<String, CuboidClipboard> entry : sections.entrySet()) {
            this.sections.put(entry.getKey(), schematicToSection(entry.getKey(), loc.clone(), entry.getValue()));
            calibrateLocation(this.sections.get(entry.getKey()));
        }
    }

    public ArmorStandVehicle(Location loc) {
        this.loc = loc;
    }

    private void calibrateLocation(ArmorStandVehicleSection section) {
        org.bukkit.util.Vector offset = loc.toVector().subtract(section.getCenter().loc.toVector()).add(section.getOffset().multiply(0.675));
        for (ArmorStandData standData : section.getStands()) {
            standData.loc = standData.loc.add(offset);
        }
    }

    protected void registerSection(String name, CuboidClipboard section) {
        this.sections.put(name, schematicToSection(name, loc.clone(), section));
        calibrateLocation(this.sections.get(name));
        if (sections.size() == 1) {
            main = this.sections.get(name);
        }
    }

    protected void registerSection(String name, ArmorStandVehicleSection section) {
        this.sections.put(name, section);
        if (sections.size() == 1) {
            main = this.sections.get(name);
        }
    }

    protected void start() {
        taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(Crisis.getInstance(), () -> {
            tick++;
            tick(tick);
            draw(tick);
        }, 1, 1);
    }

    private ArmorStandVehicleSection schematicToSection(String name, Location loc, CuboidClipboard schematic) {
        WorldServer s = ((CraftWorld) loc.getWorld()).getHandle();
        loc.setYaw(0);
        List<ArmorStandData> stands = new ArrayList<>();
        Vector offset = schematic.getOffset();
        loc.add(offset.getX() * 0.675, offset.getY() * 0.675, offset.getZ() * 0.675);
        ArmorStandData center = null;

        for (int x = 0; x < schematic.getWidth(); x++) {
            for (int y = 0; y < schematic.getHeight(); y++) {
                for (int z = 0; z < schematic.getLength(); z++) {
                    BaseBlock bb = schematic.getBlock(new Vector(x, y, z));
                    if (bb.getType() == 0) continue;
                    EntityArmorStand stand = new EntityArmorStand(s);
                    ArmorStandData standData;
                    Location location = loc.clone().add(x - (x * 0.375), y - (y * 0.375), z - (z * 0.375));
                    ItemStack equipment = new ItemStack(bb.getType(), 1, (short) bb.getData());
                    stand.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
                    stand.setSlot(EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(equipment));
                    stand.setInvisible(true);
                    standData = new ArmorStandData(stand, equipment, location);
                    //Diamond block
                    if (bb.getType() == 57) {
                        standData.equipment = new ItemStack(Material.AIR, 1);
                        center = standData;
                    }
                    stands.add(standData);
                }
            }
        }

        if (center == null) {
            throw new Error("Missing a center diamond block");
        }

        return new ArmorStandVehicleSection(name, stands, center, schematic.getOffset());
    }

    public void destroy() {
        Bukkit.getScheduler().cancelTask(taskId);
        sections.values().forEach(ArmorStandVehicleSection::destroy);
        valid = false;
    }

    public void move(org.bukkit.util.Vector movement) {
        for (String section : sections.keySet()) {
            moveSection(section, movement);
        }
        rotateSection(main.getName(), movement);
    }

    protected void moveSection(String sectionName, org.bukkit.util.Vector movement) {
        ArmorStandVehicleSection section = sections.get(sectionName);

        for (ArmorStandData standData : section.getStands()) {
            standData.loc = standData.loc.add(movement);
        }
    }

    public void rotate(org.bukkit.util.Vector bearing) {
        for (String section : sections.keySet()) {
            rotateSection(section, bearing);
        }
    }


    protected void rotateSection(String sectionName, org.bukkit.util.Vector bearing) {
        ArmorStandVehicleSection section = sections.get(sectionName);
        double bearingYaw = new Location(section.getCenter().loc.getWorld(), 0, 0, 0).setDirection(bearing).getYaw();
        double currentYaw = section.getCenter().loc.getYaw();
        rotateSection(sectionName, (float) (bearingYaw - currentYaw));
    }

    protected void rotateSection(String sectionName, float degrees) {
        if (degrees < 0.0001) return;
        ArmorStandVehicleSection section = sections.get(sectionName);

        for (ArmorStandData standData : section.getStands()) {
            standData.loc = SchematicTools.rotate(section.getCenter().loc.clone(), standData.loc.clone(), degrees);
        }
        //section.setCenter(rotate(section.getCenter(), section.getCenter(), degrees));
    }



    public boolean isValid() {
        return valid;
    }

    public Location getLocation() {
        return main.getCenter().loc;
    }

    private void draw(long tick) {
        for (ArmorStandVehicleSection section : sections.values()) {
            section.draw(tick);
        }
    }

    protected ArmorStandVehicleSection getSection(String sectionName) {
        return sections.get(sectionName);
    }

    protected abstract void tick(long tick);

    public abstract void changeYaw(double degrees);

    public abstract void changePitch(double degrees);

}
