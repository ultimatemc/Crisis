package net.crisismc.crisis.vehicles;

import net.minecraft.server.v1_12_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.craftbukkit.v1_12_R1.util.WeakCollection;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.Collection;
import java.util.List;

public class ArmorStandVehicleSection {

    private String name;
    private List<ArmorStandData> stands;
    private ArmorStandData center;
    private Vector offset;
    private Collection<Player> players = new WeakCollection<>();

    public ArmorStandVehicleSection(String name, List<ArmorStandData> stands, ArmorStandData center, com.sk89q.worldedit.Vector offset) {
        this.name = name;
        this.stands = stands;
        this.center = center;
        this.offset = new Vector(offset.getX(), offset.getY(), offset.getZ());
    }

    public ArmorStandVehicleSection(String name, List<ArmorStandData> stands, ArmorStandData center, Vector offset) {
        this.name = name;
        this.stands = stands;
        this.center = center;
        this.offset = offset;
    }

    public void draw(long tick) {
        for (ArmorStandData standData : stands) {
            standData.stand.setLocation(standData.loc.getX(), standData.loc.getY(), standData.loc.getZ(), standData.loc.getYaw(), standData.loc.getPitch());
        }

        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!player.getWorld().equals(center.loc.getWorld())) continue;
            if (players.contains(player)) {
                if (player.getLocation().clone().add(0, center.loc.getY() - player.getLocation().getY(), 0).distanceSquared(center.loc) > 40000) {
                    players.remove(player);
                    destroyStands(player);
                } else if (player.getLocation().clone().add(0, center.loc.getY() - player.getLocation().getY(), 0).distanceSquared(center.loc) > 25600) {
                    drawStands(player);
                } else {
                    //drawStands(player);
                    updateStands(player);
                }
            } else {
                if (player.getLocation().distanceSquared(center.loc) <= 65536) {
                    drawStands(player);
                    players.add(player);
                }
            }
        }
    }

    public void destroy() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            for (ArmorStandData stand : stands) {
                PacketPlayOutEntityDestroy kill = new PacketPlayOutEntityDestroy(stand.stand.getId());
                ((CraftPlayer) player).getHandle().playerConnection.sendPacket(kill);
            }
        }
    }

    private void updateStands(Player p) {
        for (ArmorStandData standData : stands) {
            PacketPlayOutEntityTeleport teleport = new PacketPlayOutEntityTeleport(standData.stand);
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(teleport);

            PacketPlayOutEntity.PacketPlayOutEntityLook look = new PacketPlayOutEntity.PacketPlayOutEntityLook(standData.stand.getId(),
                    (byte) standData.loc.getYaw(),
                    (byte) standData.loc.getPitch(),
                    false);
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(look);
        }
    }

    private void destroyStands(Player p) {
        for (ArmorStandData standData : stands) {
            PacketPlayOutEntityDestroy kill = new PacketPlayOutEntityDestroy(standData.stand.getId());
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(kill);
        }
    }

    public void drawStands(Player p) {
        for (ArmorStandData standData : stands) {
            PacketPlayOutSpawnEntityLiving create = new PacketPlayOutSpawnEntityLiving(standData.stand);
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(create);

            PacketPlayOutEntityEquipment equipment = new PacketPlayOutEntityEquipment(standData.stand.getId(), EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(standData.equipment));
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(equipment);

            PacketPlayOutEntity.PacketPlayOutEntityLook look = new PacketPlayOutEntity.PacketPlayOutEntityLook(standData.stand.getId(),
                    (byte) standData.loc.getYaw(),
                    (byte) standData.loc.getPitch(),
                    false);
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(look);
        }
    }

    public List<ArmorStandData> getStands() {
        return stands;
    }

    public ArmorStandData getCenter() {
        return center;
    }

    public void setCenter(ArmorStandData center) {
        this.center = center;
    }

    public Vector getOffset() {
        return offset;
    }

    public void setOffset(Vector offset) {
        this.offset = offset;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
